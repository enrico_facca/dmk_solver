program simple
use KindDeclaration
use Globals
use AbstractGeometry
use TimeInputs  
use DmkControls
use TdensPotentialSystem
use DmkP1P0Discretization
use DmkInputsData
use TimeFunctionals
implicit none

type(abs_simplex_mesh),target :: grid,grid0,subgrid
type(abs_simplex_mesh), pointer :: grid_pot
type(file) :: fgrid,fforcing
type(TimeData) :: forcing
type(DmkCtrl) :: ctrl
integer :: lun_err=6,i,info,id_subgrid
integer, allocatable :: topol(:,:)
type(evolfun) :: timefun
integer :: debug


real(kind=double), allocatable :: forcing_cell(:),tdens(:),pot(:)
logical :: endfile
real(kind=double) :: plapl

type(file) :: fsubgrid
type(tdpotsys), target :: tdpot
!type(p1p0_space_discretization), target :: p1p0
type(DmkInputs), target :: inputs_data
character (len=512) :: pathgrid,pathforcing,input,fname,&
     pathtdens,pathpot
real(kind=double), allocatable :: forcing_subgrid(:)
real(kind=double), allocatable :: dirac(:)
logical :: rc
integer :: res,res_read
type(file) :: fmat
integer :: ntdens,npot
integer :: stderr,nargs,narg

stderr=0

nargs = command_argument_count() 
narg = 0
if (nargs .ne. 6) call err_handle(stderr,narg)

!
! get grid path
!
narg=1
call  get_command_argument(narg, input,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(input,*,iostat=res_read) pathgrid
   write(*,*) 'Grid from', etb(pathgrid)
end if

!
! get grid path
!
narg=narg+1
call  get_command_argument(narg, input,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(input,*,iostat=res_read)   id_subgrid
   write(*,*) 'id_subgrid', id_subgrid
end if


! 
! get forcing path
!
narg=narg+1
call  get_command_argument(narg, input,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(input,*,iostat=res_read) pathforcing
   write(*,*) 'Forcing from', etb(pathforcing)
end if


!
! get pflux
!
narg=narg+1
call  get_command_argument(narg, input,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(input,*,iostat=res_read) plapl
   write(*,*) 'Plapl =', plapl
end if

!
! get pflux
!
narg=narg+1
call  get_command_argument(narg, input,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(input,*,iostat=res_read) pathtdens
end if

narg=narg+1
call  get_command_argument(narg, input,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(input,*,iostat=res_read) pathpot
end if

!
! read grid
! 
call fgrid%init(lun_err,pathgrid,10,'in')
call grid%read_mesh(lun_err,fgrid)
call fgrid%kill(lun_err)

!
! read forcing
!
call fforcing%init(lun_err,pathforcing,11,'in')
call forcing%init(lun_err, fforcing, 1,grid%ncell)
call forcing%set(lun_err, fforcing, 0.0d0,endfile)
allocate(forcing_cell(grid%ncell),tdens(grid%ncell),pot(grid%nnode),topol(3,grid%ncell))
topol=grid%topol(1:3,1:grid%ncell)
do i=1,grid%ncell
   forcing_cell(i) =forcing%TDactual(1,i)
end do


!
! using optdmk
!
!tdens=one
!pot=zero
!call otpdmk(grid%nnodeincell,grid%nnode,grid%ncell,&
!     topol,grid%coord,&
!     pflux,forcing_cell,&
!     tdens,pot,&
!     ctrl,info)

!
! using steay
!
if (id_subgrid==1) then
   call data2grids(&
        lun_err,&
        grid%nnodeincell,grid%nnode,grid%ncell, &
        grid%coord,topol,&
        grid0,subgrid)

   call grid%build_size_cell(lun_err)
   call grid%build_normal_cell(lun_err)
   call grid%build_bar_cell(lun_err) 
   
   ntdens=grid%ncell
   grid_pot=>subgrid
   npot=grid_pot%nnode

   call fmat%init(0,'subgrid.dat',10000,'out')
   call grid_pot%write_mesh(lun_err,fmat)
   call fmat%kill(0)

   call fmat%init(0,'parent.dat',10000,'out')
   call grid_pot%write_parent(lun_err,fmat)
   call fmat%kill(0)
   
else
   !
   ! init grid
   !
   call grid%build_size_cell(lun_err)
   call grid%build_normal_cell(lun_err)
   call grid%build_bar_cell(lun_err)  
   ntdens=grid%ncell
   grid_pot=> grid
   npot=grid_pot%nnode
end if
   
!
! inputs data
!
call inputs_data%init(lun_err, ntdens,npot,set2default=.True.)
inputs_data%pflux=(plapl-4)/(plapl-2)
write(*,*)'inputs created'


!
! init set controls
!
! globals controls
ctrl%selection=0
ctrl%threshold_tdens=1.d-10
ctrl%debug=1
ctrl%min_tdens = 1.0d-15


! linear solver ctrl
!ctrl%outer_solver_approach='ITERATIVE'
ctrl%outer_solver_approach='AGMG'
ctrl%outer_prec_type='IC'
ctrl%outer_krylov_scheme='PCG'
ctrl%outer_prec_n_fillin=30
ctrl%outer_prec_tol_fillin=1d-3
ctrl%relax4prec=0d-09
ctrl%relax_direct=1d-09

ctrl%inner_solver_approach='ITERATIVE'

ctrl%outer_imax=500
ctrl%outer_iprt=0
ctrl%outer_tolerance=1d-12
ctrl%tolerance_linear_newton=1d-05
ctrl%inner_imax=5

! time  ctrl
! evolution controls
ctrl%max_time_iterations=1000
ctrl%user_system_variation=1
ctrl%tolerance_system_variation=1d-12
ctrl%norm_tdens=0.0d0
! time stepping scheme
ctrl%time_discretization_scheme=4
! time stepping scheme controls
ctrl%max_nonlinear_iterations=20
ctrl%max_restart_update=1
ctrl%epsilon_W22=1d-8

! time step size controls
ctrl%deltat = 0.1
ctrl%deltat_control=2
ctrl%deltat_expansion_rate=2.0d0
ctrl%deltat_lower_bound=0.01
ctrl%deltat_upper_bound=0.6


! info , saving ctrl
ctrl%info_update=3
ctrl%info_state=2
ctrl%id_save_dat=3
ctrl%lun_statistics=10
ctrl%lun_out=6
ctrl%lun_tdens = 1234
ctrl%fn_tdens=etb(pathtdens)
ctrl%lun_pot=1235
ctrl%fn_pot=etb(pathpot)


ctrl%max_nonlinear_iterations=20
ctrl%tolerance_nonlinear=1d-12
ctrl%relax4prec=0.0d-10


allocate(&
     dirac(grid%nnode),&
     stat=res)
if (res.ne.0) rc = IOerr(lun_err, err_alloc, 'otpdmk', &
     ' work arrays forcing_subgrid dirac_subgrid ', res)

if (id_subgrid==1) then
   dirac=zero
   call build_subgrid_rhs(grid_pot,inputs_data%rhs,forcing_cell, dirac)
else
   call forcing2rhs(grid_pot,forcing_cell,inputs_data%rhs)
end if
if (debug>0)  write(*,*)'rhs created',ntdens,npot

   
deallocate(&
     dirac,&
     stat=res)
if (res.ne.0) rc = IOerr(lun_err, err_dealloc, 'otpdmk', &
     ' work arrays forcing_subgrid dirac_subgrid ', res)
fname='rhs.dat'
call fmat%init(0,fname,10000,'out')
call write_steady(0, 10000, npot,inputs_data%rhs )
call fmat%kill(0)


!
! init tdpot
!
call tdpot%init( lun_err, ntdens, npot,1)
tdpot%tdens=one
tdpot%pot=zero
if (debug>0) write(*,*)'tdpot created'

call timefun%init(lun_err,ctrl%max_time_iterations,&
     ctrl%max_nonlinear_iterations)

call dmkp1p0_steady_data(&
     grid,grid_pot,id_subgrid,&
     tdpot,inputs_data,&
     ctrl,info,timefun)
write(*,*)'FINISHED'





end program simple

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./timedata2vtk_grid.out <grid> <subgrid> <parent> <rhs> <tdens> <pot> <lambda> <transf> <stat> '
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid       (in ) : triangulation in ascii for tdens '
  write(lun_err,*) ' id_subgrid (in ) : 0/1 flag for subgrid ' 
  write(lun_err,*) ' forcing    (in ) : forcing term defined of cells'
  write(lun_err,*) ' plapl      (in ) : p-Laplacian power'
  write(lun_err,*) ' tdens      (out ) : tdens'
  write(lun_err,*) ' pot        (out) : pot'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
