#!pyhton
#
# Compute x,y,z coordinate of test in section 5 of
# 
# @article{Itoh-Sinclair:2004,
#   author =	 "Itoh, Jin-ichi and Sinclair, Robert",
#   fjournal =	 "Experimental Mathematics",
#   journal =	 "Experiment. Math.",
#   number =	 3,
#   pages =	 "309--325",
#   publisher =	 "A K Peters, Ltd.",
#   title =	 "Thaw: A Tool for Approximating Cut Loci on a
#                   Triangulation of a Surface",
#   url =		 "https://projecteuclid.org:443/euclid.em/1103749839",
#   volume =	 13,
#   year =	 2004
# }

# formule taken from

# @book{book:{291291},
#    title =     {Riemannian geometry},
#    author =    {Wilhelm P. A. Klingenberg},
#    publisher = {Walter de Gruyter},
#    isbn =      {9783110145939,3110145936},
#    year =      {1995},
#    series =    {De Gruyter Studies in Mathematics},
#    edition =   {2 Rev Sub},
#    volume =    {},
#   }


# usage pyhton "gridin" "gridout" x y z

import sys
sys.path.append('../')
import meshtools as mt
import numpy as np

a=0.2
b=0.6
c=1.0

a0=a**2
a1=b**2
a2=c**2

u1=float(sys.argv[1])
u2=float(sys.argv[2])

x=np.sqrt(a0*(u1-a0)*(u2-a0)/((a1-a0)*(a2-a0)))
y=np.sqrt(a1*(u1-a1)*(u2-a1)/((a0-a1)*(a2-a1)))
z=np.sqrt(a2*(u1-a2)*(u2-a2)/((a0-a2)*(a1-a2)))

print (x,y,z) 
