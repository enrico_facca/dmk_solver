#!/bin/bash

base=runs/ellipsoid_second
input_base=inputs_ellipsoid.ctrl


#grid0=sphere_prj.dat 
#projector=project_sphere.py

grid0=ellipsoid_grid.dat
projector=project_ellipsoid.py


folder_grid0=/home/fh/src/muffe_p1p0/preprocess/2d_assembly/cut_locus
#folder_grid0=/home/enrico/bbmuffe/repositories/muffe_p1p0/preprocess/surface_assembly/sphere_otp



#sed "s|folder_grid|$folder_grid0|" inputs_sphere.ctrl > inputs.ctrl 
sed "s|folder_grid|$folder_grid0|" $input_base > inputs.ctrl 
sed -i "s|grid2use|$grid0|" inputs.ctrl


./dmk_folder.py assembly ${base}_nref0 inputs.ctrl 


folder_before=${base}_nref0

for i in 1 2 3 4
do
    #
    # project previous subgrid
    # 
    echo 'before' $folder_before
    cd ${folder_grid0}
    echo 'cur dir'
    pwd
    python $projector /home/fh/projects/dmk_surface/$folder_before/input/subgrid.dat /home/fh/projects/dmk_surface/$folder_before/input/subgrid_prj.dat
    cd -
      
    #
    # set grid 
    #
    folder_grid=/home/fh/projects/dmk_surface/${folder_before}/input
    grid=subgrid_prj.dat
  
    sed "s|folder_grid|$folder_grid|" $input_base > inputs.ctrl 
    sed -i "s|grid2use|$grid|" inputs.ctrl
    
    #
    # set folder name 
    #
    folder=${base}_nref${i}
    echo $folder

    #
    # create inputs
    #
    ./dmk_folder.py assembly $folder inputs.ctrl 


    
    #
    # set name for next start
    #
    folder_before=$folder
done


