#!/usr/bin/env python
# coding: utf-8


# 
# Loading stardard and dmk pyhton modules
#

# Standard tools
import sys
import numpy as np
import os


# set mesh
nref=int(sys.argv[1])


# set lift
power=int(sys.argv[2])
h0=1/8*np.sqrt(2)
h=h0/(2**nref)
lift=(h)**power


# set variable
variable='gfvar'

# set tow level grid
use_subgrid=np.zeros(1,dtype=np.int8)
sub=int(sys.argv[3])
use_subgrid=sub
#ctrl=int(sys.argv[4])
potof=int(sys.argv[4])

folder="runs/reg_nref"+str(nref)+"/"

grid=folder+"grid.dat"
subgrid=folder+"subgrid.dat"
parent=folder+"parent.dat"
rhs=folder+"rhssub"+str(sub)+".dat"

str_out="nref"+str(nref)+"_hp"+str(power)+"_pot"+str(potof)
opttdens=folder+"out/"+str_out+"_opttdens.dat"
optpot=folder+"out/"+str_out+"_optpot.dat"
trans="square"
outfile=folder+"out/"+str_out+"_statistics.dat"
command=" ".join(["~/srcs/dmk_solver/build/tools/hessian_eigenvalues.out",
 grid,subgrid,parent,rhs,opttdens,optpot,str(lift),trans,outfile])

os.system(command)

