#!/usr/bin/env python
# coding: utf-8


# 
# Loading stardard and dmk pyhton modules
#

# Standard tools
import sys
import numpy as np
import os
import pandas as pd
from astropy.io import ascii
from astropy.table import Table


current_source_dir=os.getcwd()

# Import I/O for timedata
try:
    sys.path.append('../../../../../globals/python/timedata/')
    import timedata as td
except:
    print("Global repo non found")

# Import geometry tools
sys.path.append('../../../../../geometry/python/')
import meshtools as mt
sys.path.append('../../../preprocess/assembly/')
import example_grid

# Import dmk tools
sys.path.append('../../')
import dmk_p1p0 


relative_libpath='../../../../build/python/fortran_python_interface/'
dmk_lib_path=os.path.abspath(os.path.normpath(current_source_dir+'/'+relative_libpath))
sys.path.append(dmk_lib_path)
from dmk import (Dmkcontrols,  # controls for dmk simulations)
                 Timefunctionals, # information of time/algorithm evolution
                 Dmkp1P0Discretization,
                 dmkp1p0_steady_data,
                 Tdenspotentialsystem,
                 Dmkinputsdata,
                 Abstractgeometry,
                 write_parent,
                )

relative_libpath='../../../../otp_solver/python/'
dmk_lib_path=os.path.abspath(os.path.normpath(current_source_dir+'/'+relative_libpath))
sys.path.append(dmk_lib_path)
from dmk_p1p0 import (init_dmk,
                     set_forcing_term,
                      check_inputs,
                     getgeometry)


# Import plot tools
import matplotlib.pyplot as plt
import sys

# set mesh
nref=int(sys.argv[1])


# set lift
power=int(sys.argv[2])
lift=((1/8)/(2**nref))**power

print(lift)

def opt_tdens(x,y):
    return 
    


#lift=10**(-power)

# set variable
variable='gfvar'

# set tow level grid
use_subgrid=np.zeros(1,dtype=np.int8)
sub=int(sys.argv[3])
use_subgrid=sub
time_control=int(sys.argv[4])



# label for file
str_power="test_hp"+f"{power:01d}"
str_subgrid=f"sub{use_subgrid:01d}"
str_time_ctrl=f"tc{time_control:1d}"

print(str_power,str_subgrid)


#
# read mesh
#
dir_path='runs/reg_nref'+str(nref)+'/'
file_path=dir_path+'grid.dat'
[coord,topol,flags]=mt.read_grid(file_path)

# try read initial data
try:
    tdens0path=sys.argv[5]
    tdens0=td.read_steady_timedata(tdens0path).flatten()
    print('Tdens read from' +str(tdens0path))
except:
    tdens0=np.ones(len(topol))

#
# We create piecewise constant rapresentations of $f,f^+,f-$ and $\mu^*$,
# evaluating the function on cell centroids.
#
value=1.0

# define source and sink functions, value of grid centroids
def source(coord):
    x=coord[0]; y=coord[1]
    if (x<1.0/2.0):
        fvalue=0.0
        if ((x >= 1.0/8.0) and (x<=3.0/8.0) and
            (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
            fvalue=value
    else:
        fvalue=0.0
        
    return fvalue;

def sink(coord):
    x=coord[0]; y=coord[1] 
    if (x>=1.0/2.0):
        fvalue=0.0
        if ((x >= 5.0/8.0) and (x<=7.0/8.0) and
            (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
            fvalue=value
    else:
        fvalue=0.0       
    return fvalue;
# For this case there exists an explicit solution of the Monge-Kantorivich equations:
# The optimal transport density:
def optimal_transport_density(coord):
    x=coord[0]; y=coord[1];
    fvalue=0.1
    if ((x >= 1.0/8.0) and (x<=3.0/8.0) and
        (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
        fvalue=(x-1.0/8.0)*value
    elif ((x >= 3.0/8.0) and (x<=5.0/8.0) and
          (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
        fvalue=(3.0/8.0-1.0/8.0)*value 
    elif ((x >= 5.0/8.0) and (x<=7.0/8.0) and
          (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
        fvalue=(7.0/8.0-x)*value
    else:
        fvalue=0.0
    return fvalue;
# exact wasserstein distance
exact_wass1=1.0/8.0


def optimal_transport_density_delta(coord, delta):
    return max(optimal_transport_density(coord)-delta,0.0);

# and the Kantorovich potential
def kantorovich_potenial(coord):
    x=coord[0];        
    return -x;

# compute functions on cell centroids
ncell=len(topol)
bar_cell=mt.make_bar(coord,topol).transpose()
size_cell=mt.make_size(coord,topol);
source_cell=np.zeros([ncell]); sink_cell=np.zeros([ncell]); opttdens_cell=np.zeros([ncell]); opttdens_cell_delta=np.zeros([ncell])
for i in range(ncell):
    source_cell[i] = source(bar_cell[:,i])
    sink_cell[i]   = sink(bar_cell[:,i])
    opttdens_cell[i] = optimal_transport_density(bar_cell[:,i])
    opttdens_cell_delta[i] = optimal_transport_density_delta(bar_cell[:,i],lift)
    forcing_cell=source_cell-sink_cell#*(np.dot(size_cell,source_cell)/np.dot(size_cell,sink_cell))

td.write_steady_timedata(dir_path+'forcing.dat',forcing_cell)
td.write_steady_timedata(dir_path+'opttdens.dat',opttdens_cell)


"""
import matplotlib.tri as mtri
#
# plot forcing term
#
triang = mtri.Triangulation(coord.transpose()[0,:], coord.transpose()[1,:], topol)
fig1, ax1 = plt.subplots(figsize=(8, 8)); ax1.set_aspect('equal')
tpc = ax1.tripcolor(triang, forcing_cell , cmap='RdBu_r')
fig1.colorbar(tpc)
ax1.set_title('Forcing term $f=f^+-f^-$')
plt.show()
"""

# We load the controls from file. You can either change it in the file or change some controls in the next cell.

# In[6]:


# init controls
ctrl = dmk_p1p0.init_dmkctrl(tdens_gfvar=variable,
                             explicit_implicit='implicit')

# set flag grids
ctrl.id_subgrid=use_subgrid
print(ctrl.time_discretization_scheme)

# The user can change any controls inside the ctrl type.
# Here we set the saving frequency (save all) and the convergence tolerance
ctrl.id_save_dat=0
ctrl.fn_tdens=dir_path+'tdens'+str_power+str_subgrid+'.dat'
ctrl.fn_pot=dir_path+'pot'+str_power+str_subgrid+'.dat'
ctrl.fn_statistics=dir_path+'dmk'+str_power+str_subgrid+'.log'


tolerance_kkt=1e-09

#ctrl.max_time_iterations=3
ctrl.tolerance_system_variation=1e-08
ctrl.tolerance_nonlinear=1e-02

# approach for newton
ctrl.solve_jacobian_approach= 'reduced'
ctrl.relax_limit=1e0


# uncomment and try also this setting
ctrl.outer_solver_approach='ITERATIVE' 
ctrl.outer_solver_approach='AGMG' # also AGMG if available
ctrl.outer_krylov_scheme='PCG'
ctrl.outer_tolerance = 1e-7
ctrl.outer_iprt=0
ctrl.outer_imax=800

# prec for iterative solver
ctrl.outer_prec_type='ILU'
ctrl.outer_prec_n_fillin=0
ctrl.outer_prec_tol_fillin=1e-3

# prec for iterative solver
ctrl.inner_solver_approach='AGMG'
ctrl.inner_krylov_scheme='PCG'
ctrl.inner_tolerance=5e-3
ctrl.inner_iprt=0
ctrl.inner_imax=20

# prec for iterative solver
ctrl.inner_prec_type='ILU'
ctrl.inner_prec_n_fillin=10
ctrl.inner_prec_tol_fillin=1e-2


ctrl.outer_prec_n_fillin=0
ctrl.outer_prec_tol_fillin=1e-1



# relaxations
ctrl.relax_direct=1e-10
ctrl.relax4prec=1e-11
ctrl.min_tdens=0

if (time_control==0):
    ctrl.time_discretization_scheme = 1
    ctrl.max_time_iterations = 40000
    ctrl.deltat = 0.25
    ctrl.deltat_control=2
    ctrl.deltat_expansion_rate=1.05
    ctrl.deltat_lower_bound=1e-2
    ctrl.deltat_upper_bound=1e0
    ctrl.max_restart_update=1

else:
    ctrl.time_discretization_scheme = 4
    ctrl.max_time_iterations = 40
    ctrl.deltat = 2
    ctrl.deltat_control=2
    ctrl.deltat_expansion_rate=1.1
    contraction_rate=1.5
    ctrl.deltat_lower_bound=1e-2
    ctrl.deltat_upper_bound=1e6
    ctrl.max_restart_update=4

ctrl.max_time_iterations = 100
ctrl.tolerance_nonlinear=1e-03
    
# print and info
ctrl.info_update=4
ctrl.info_state=1

# 
ctrl.user_system_variation=1
ctrl.user_control_set=1
ctrl.user_control_reset=1



work_ctrl = Dmkcontrols.DmkCtrl()
Dmkcontrols.dmkctrl_copy(ctrl,work_ctrl)


#
# init type for storing evolution/algorithm info
#
timefun=Timefunctionals.evolfun()
Timefunctionals.evolfun_constructor(timefun, 0,
                                    ctrl.max_time_iterations,
                                    ctrl.max_nonlinear_iterations)


# Init. Fortran derivied types: 
# meshes tdens/pot containers, inputs container ans discretization 
[ grid,subgrid,tdpot,dmkin] = init_dmk(topol, coord, ctrl.id_subgrid)



if (use_subgrid==1):
    [topol_subgrid,coord_subgrid]=getgeometry(subgrid)
    mt.write_grid(coord_subgrid.transpose(),topol_subgrid,dir_path+'subgrid.dat')
    write_parent(subgrid,dir_path+'parent.dat')



# Init spatial discretization
p1p0=Dmkp1P0Discretization.dmkp1p0()
print(use_subgrid)
Dmkp1P0Discretization.dmkp1p0_constructor(p1p0,ctrl,use_subgrid,grid,subgrid)




# shorthand
ntdens=grid.ncell
npot=subgrid.nnode

# set tdens-pot variable
tdpot.tdens[:]= tdens0[:]
#tdpot.tdens[:]=opttdens_cell_delta[:]
tdpot.pot[:]=0.0
#
# Set inputs
#

# integrate forcing term w.r.t. p1 base function
dmkin =  set_forcing_term(dmkin,subgrid,forcing_cell,np.zeros(npot))
td.write_steady_timedata(dir_path+'rhs'+str_subgrid+'.dat',dmkin.rhs)

save4matlab=True
dir_matlab=dir_path+'/matlab/'
if (True):
    if not os.path.exists(dir_matlab):
        os.makedirs(dir_matlab)
    Dmkp1P0Discretization.write4matlab(p1p0,dir_matlab)
    td.write_steady_timedata(dir_matlab+'rhs_integrated.dat',dmkin.rhs)
    td.write_steady_timedata(dir_matlab+'forcing.dat',forcing_cell)
    td.write_steady_timedata(dir_matlab+'opttdens.dat',opttdens_cell)
    exit()

# set pflux exponent 
dmkin.pflux = 1
dmkin.lambda_lift=lift
if ( check_inputs(dmkin) != 0):
    info=-1
# set reference solutions if exist
if 'optimal_tdens' in locals():
    Dmkinputsdata.set_optimal_tdens(dmkin,0,optimal_tdens)
if 'optimal_pot' in locals() :
    Dmkinputsdata.set_optimal_pot(dmkin,0,optimal_pot)


# In[10]:


# solve with dmk
info=np.zeros(1,dtype=np.int8)
i=np.zeros(1,dtype=np.int8)

# solve with dmk
info=np.zeros(1,dtype=np.int8);flag=np.ones(1,dtype=np.int8);
flag_task=np.ones(1,dtype=np.int8);current_time=np.zeros(1)
dmkin.steady=True

#
fileID_csv=open(dir_path+'dmk'+str_power+str_subgrid+str_time_ctrl+'.csv', 'w')
column_names=[
    'kkt',
    'max_grad_squared_minus_one',
    'min_tdens',
    'norm_kkt_before',
    'var_tdens',
]
csv_line=','.join(map(str,column_names
    ))+'\n'
fileID_csv.write(csv_line)

norm_grad_on_cells = np.zeros(grid.ncell)
tdens_old = np.zeros(grid.ncell)
kkt_estimate = np.zeros(grid.ncell)

rows=[]
err_pot=[]
total_restart=0

save_data=False
if ( save_data):
    fkkt=open(dir_path+'kkt'+str_power+str_subgrid+str_time_ctrl+'.dat','w')
    td.write2file(fkkt,0.0,True,kkt_estimate,steady=False)
    ftdens=open(dir_path+'tdens'+str_power+str_subgrid+str_time_ctrl+'.dat','w')
    td.write2file(ftdens, 0.0,True,tdpot.tdens,steady=False)
    fnorm=open(dir_path+'norm_grad_squared'+str_power+str_subgrid+str_time_ctrl+'.dat','w')
    td.write2file(fnorm, 0.0,True,norm_grad_on_cells,steady=False)

while ( flag > 0 ):
    Dmkp1P0Discretization.dmkp1p0_cycle_reverse_communication(p1p0,
                                                              flag,flag_task,info,
                                                              current_time,
                                                              dmkin,tdpot,ctrl,work_ctrl)
    if ( flag[0] == 2 ):          
        # Fill dmk inputs with data 
        dmkin.time=current_time                     
    elif( flag[0] == 3 ):           
        # Right before new update when tdpot and inputs_data are syncronized.          

        # compute the norm of the gradient
        pot=np.array(tdpot.pot).copy()
        Dmkp1P0Discretization.dmk_p1p0_build_norm_grad_dyn(p1p0,pot,2.0,norm_grad_on_cells)
        
        # error w.r.t. tdens exact solution 
        error_tdens=np.sum(grid.size_cell*abs(tdpot.tdens-opttdens_cell))
        print('error tdens=',error_tdens)
        
        # save tdens before update
        tdens_old[:]=tdpot.tdens
        
    elif( flag[0] == 4 ):           
        # 
        # Optional: user-defined evaluation of steady state
        # User must set flag "user_system_variation=1"
        # in controls
        # compute the norm of the gradient
        print(str_power,str_subgrid,str_time_ctrl)

        
        pot=np.array(tdpot.pot).copy()
        Dmkp1P0Discretization.dmk_p1p0_build_norm_grad_dyn(p1p0,tdpot.pot,2.0,norm_grad_on_cells)
        max_grad_squared_minus_one=max(norm_grad_on_cells)-1.0
        print('max |\grad pot|-1=',max_grad_squared_minus_one)
        print(str(f'{np.amin(tdpot.tdens):.1e}'),
              '<= TDENS <=',
              str(f'{np.amax(tdpot.tdens):.1e}'))

        
        
        kkt_estimate[:] = - norm_grad_on_cells[:] + 1
        norm_kkt_before = np.linalg.norm(kkt_estimate, np.inf);
        for i in range(grid.ncell):
            if (tdpot.tdens[i] > tolerance_kkt):
                kkt_estimate[i] = abs(kkt_estimate[i])
            else:
                kkt_estimate[i] = max(0.0, -kkt_estimate[i] )

        print(str(f'{np.amin(kkt_estimate):.3e}'),
              '<= kkt <=',
              str(f'{np.amax(kkt_estimate):.3e}'))


        
      
        max_kkt=np.linalg.norm(kkt_estimate, np.inf);
        

        #ctrl.tolerance_nonlinear=min(1e-2,var_tdens*10**6)
        


        L1_error_tdens=(np.sum(grid.size_cell*abs(tdpot.tdens-opttdens_cell))/
                        np.sum(grid.size_cell*abs(opttdens_cell)))

        gfvar_new=np.sqrt(tdpot.tdens)
        gfvar_old=np.sqrt(tdens_old)
        gfvar_increment=np.sqrt(np.sum(grid.size_cell*abs(gfvar_new-gfvar_old)**2))
        norm_grad_lyap = np.linalg.norm(gfvar_new * (norm_grad_on_cells - 1.0),np.inf)

        var_tdens=norm_grad_lyap

        print('var tdens=',var_tdens)
        
        total_restart+=tdpot.nrestart_update
        print('nrestart=',tdpot.nrestart_update,'total',total_restart)
        row=[
            tdpot.time,
            max_kkt,
            gfvar_increment,
            max_grad_squared_minus_one,
            np.amin(tdpot.tdens),
            norm_kkt_before,
            np.linalg.norm(tdpot.tdens*(norm_grad_on_cells-1.0),np.inf),
            L1_error_tdens,
            ctrl.deltat,
            norm_grad_lyap,
            ]
        rows.append(row)
        csv_line=','.join(map("{:.3e}".format,row))+'\n'
       

        break_cycle=var_tdens < ctrl.tolerance_system_variation
        
        fileID_csv.write(csv_line)

        if (save_data):
            td.write2file(fkkt,tdpot.time,False,kkt_estimate,steady=break_cycle)
            td.write2file(ftdens,tdpot.time,False,tdpot.tdens,steady=break_cycle)
            td.write2file(fnorm,tdpot.time,False,norm_grad_on_cells,steady=break_cycle)
        
        if ( break_cycle ):
            break
            
    elif( flag[0] == 5 ):           
        # 
        # Optional: user-defined set the controls for the next update
        # User must set flag "user_control_set=1"
        # ctrl.deltat=1.05*ctrl.deltat
        #
        if (time_control == 0):
            ctrl.deltat = ctrl.deltat
        elif (time_control == 1):
            ctrl.deltat = ctrl.deltat_expansion_rate*ctrl.deltat
        print('new deltat', ctrl.deltat)
        
        
    elif( flag[0] == 6 ):           
        # 
        # Optional: reset controls after update failure 
        # User must set flag "user_control_reset=1"
        ctrl.deltat = ctrl.deltat/contraction_rate
        print('reset deltat', ctrl.deltat)

fileID_csv.close()
if (save_data):
    fkkt.close()
    ftdens.close()
    fnorm.close()


column_names=[
    'time',
    'kkt',
    'gfvar_increment',
    'max_grad_squared_minus_one',
    'min_tdens',
    'norm_kkt_before',
    'var_tdens',
    'L1_error_tdens',
    'deltat',
    'Linfty_grad_lyap'
]
df = pd.DataFrame(columns=column_names, data=rows)
table = Table.from_pandas(df)
for i in column_names:
    table[i].info.format = '7.5e'
ascii.write(table,
            dir_path+'dmk'+str_power+str_subgrid+str_time_ctrl+'.csv',
            #format='fixed_width',
            #bookend=False,
            delimiter=',',
            overwrite=True)
#,
#formats={'kkt': '0.2e', 'max_grad_squared_minus_one': '0.4f'})

print('total nrestart=',total_restart)

info=info[0]

import pyvtk
vtk = pyvtk.VtkData(pyvtk.UnstructuredGrid(points=coord,triangle=topol),
                    pyvtk.CellData(
                        pyvtk.Scalars(opttdens_cell,name='OptTdens'),
                        pyvtk.Scalars(tdpot.tdens,name='ApproxTdens'),
                        pyvtk.Scalars(norm_grad_on_cells,name='ApproxGrad2'),
                    ),
                    'Data')
vtk.tofile(dir_path+'data'+str_power+str_subgrid+str_time_ctrl+'.vtk')


# Copy before freeing memory.
# Use np.array() function because you will get errors otherwise
opttdens=np.array(tdpot.tdens).copy()
optpot=np.array(tdpot.pot).copy()

td.write_steady_timedata(dir_path+'opttdens'+str_power+str_subgrid+str_time_ctrl+'.dat',opttdens)
td.write_steady_timedata(dir_path+'optpot'+str_power+str_subgrid+str_time_ctrl+'.dat',optpot)
td.write_steady_timedata(dir_path+'optgrad'+str_power+str_subgrid+str_time_ctrl+'.dat',norm_grad_on_cells)





# free memory
Tdenspotentialsystem.tdpotsys_destructor(tdpot, 0)
Dmkinputsdata.dmkinputs_destructor(dmkin,0)
Abstractgeometry.mesh_destructor(grid, 0)
Abstractgeometry.mesh_destructor(subgrid, 0)

"""
# plot forcing term

triang = mtri.Triangulation(coord.transpose()[0,:], coord.transpose()[1,:], topol)
fig1, ax1 = plt.subplots(figsize=(8, 8)); ax1.set_aspect('equal')
tpc = ax1.tripcolor(triang, opttdens , cmap='RdBu_r')
fig1.colorbar(tpc)
ax1.set_title('Optimal Transport Density $\mu^*$')
plt.show()
mt.write_grid(coord_subgrid.transpose(),topol_subgrid,dir_path+'subgrid.dat')
"""

# In[12]:

"""
# plot convergence toward steady state
print(timefun.last_time_iteration)
time      = np.array(timefun.time[0:timefun.last_time_iteration]);
cpu_time  = np.array(timefun.cpu_time[0:timefun.last_time_iteration])
var_tdens = np.array(timefun.var_tdens[1:timefun.last_time_iteration])
wass1     = np.array(timefun.lyapunov[1:timefun.last_time_iteration])
errwass1=(wass1[:]-exact_wass1)/(exact_wass1)

plt.figure(1)
plt.subplot(211)
plt.yscale('log')
plt.ylabel('var($\mu$)')
plt.xlabel('CPU time (s)')
plt.grid(True)
plt.plot(cpu_time[1:], var_tdens, 'bo--')

plt.subplot(212)
plt.yscale('log')
plt.ylabel('Relative Err. Wasserstein-1 distance')
plt.xlabel('CPU time (s)')
plt.grid(True)
plt.plot(cpu_time[1:], errwass1, 'ro--')
plt.show()

"""




