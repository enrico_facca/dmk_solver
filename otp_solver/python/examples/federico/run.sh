nref=$1;
power=$2;
sub=$3
ctrl=$4
folder=$(echo runs/rect_cnst_nref${nref}/);
echo $folder;

python run.py ${nref} ${power} ${sub} ${ctrl}
~/srcs/dmk_solver/build/tools/hessian_eigenvalues.out ${folder}grid.dat ${folder}subgrid.dat ${folder}parent.dat ${folder}rhssub${sub}.dat ${folder}opttdens${power}sub${sub}.dat ${folder}optpot${power}sub${sub}.dat 1e-${power} square ${folder}stat${power}sub${sub}.dat
