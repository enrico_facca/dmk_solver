nref=$1;
power=$2;
sub=$3
ctrl=$4
folder=$(echo runs/rect_cnst_nref${nref}/);
echo $folder;

~/srcs/dmk_solver/build/tools/hessian_eigenvalues.out ${folder}grid.dat ${folder}subgrid.dat ${folder}parent.dat ${folder}rhssub${sub}.dat ${folder}opttdenshp${power}sub${sub}tc${ctrl}.dat ${folder}optpothp${power}sub${sub}tc${ctrl}.dat 1e-${power} square ${folder}stat${power}sub${sub}tc${ctrl}.dat
