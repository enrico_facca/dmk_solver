# 
# Loading stardard and dmk pyhton modules
#

# Standard tools
import sys
import numpy as np


# Import I/O for timedata
sys.path.append('../../../../../globals/python/timedata/')
import timedata as td
#print("Global repo non found")

# Import geometry tools
sys.path.append('../../../../../geometry/python/')
import meshtools as mt
import os
sys.path.append('../../../preprocess/assembly/')
print(os.listdir('../../../preprocess/assembly/'))
import example_grid

# Import dmk tools
sys.path.append('../../')
import dmk_p1p0 
sys.path.append('../../../../build/python/fortran_python_interface/')
from dmk import (Dmkcontrols,    # controls for dmk simulations)
                 Timefunctionals, # information of time/algorithm evolution
                Dmkinputsdata, # structure variable containg inputs data
                 build_subgrid_rhs, #procedure to preprocess forcing term f
                 Tdenspotentialsystem, # input/output result tdens, pot
                dmkp1p0_steady_data   # main interface subroutine
                )
# Import plot tools
import matplotlib.pyplot as plt

# set mesh size 
ndiv=20
length=1.0/float(ndiv)
nref=0

# set grid example
flag_grid='unitsquare'

# build grid using prebuild examples 
points, vertices, coord,topol,element_attributes = example_grid.example_grid(flag_grid,length)
print('topol computed!')
# initialized fortran variable for the spatial discretization
[grid,subgrid]=dmk_p1p0.init_geometry(topol, coord, 1)
ncell=grid.ncell
ntdens=grid.ncell
npot=subgrid.nnode

print('grid defined!')

#
# set number, value and location of source and sink points
# 
Nplus=3
Nminus=2

fplus=[1,2,3]
fminus=[4,2]

xplus=[[0.1,0.2],[0.3,0.4],[0.1,0.7]]
xminus=[[0.6,0.2],[0.8,0.4]]

# set array forcing_dirac "evoluation" f=f^{+}-f^{-} on grid nodes
forcing_dirac=np.zeros(grid.nnode)
for i in range(Nplus):
    inode=mt.Inode(coord,xplus[i])
    forcing_dirac[inode]=fplus[i]
for i in range(Nminus):
    inode=mt.Inode(coord,xminus[i])
    forcing_dirac[inode]=-fminus[i]

ctrl = Dmkcontrols.DmkCtrl()
ctrl.tolerance_system_variation = .000001
print(ctrl.tolerance_system_variation)

dmkin=Dmkinputsdata.DmkInputs()
Dmkinputsdata.dmkinputs_constructor(dmkin,0,ntdens,npot,True)

dmkin.pflux = 1.5
print(dmkin.pflux)

tdpot=Tdenspotentialsystem.tdpotsys()
Tdenspotentialsystem.tdpotsys_constructor(tdpot,0,ntdens, npot,1)
tdpot.tdens[:]=1.0

#
# init type for storing evolution/algorithm info
#
timefun=Timefunctionals.evolfun()
Timefunctionals.evolfun_constructor(timefun, 0,
                                        ctrl.max_time_iterations,
                                        ctrl.max_nonlinear_iterations)

# solve with dmk
info=0
dmkp1p0_steady_data(grid, subgrid, tdpot, dmkin, ctrl, info,timefun=timefun)

print(tdpot.tdens)