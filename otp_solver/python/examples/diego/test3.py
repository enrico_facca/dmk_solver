# 
# Loading stardard and dmk pyhton modules
#

# Standard tools
import sys
import numpy as np
#!pip install pymesh
#!pip install numpy
root = '/home/dtheuerkauf/Nextrout/dmk_utilities/'

# Import I/O for timedata
try:
    sys.path.append(root+'globals/python/timedata/')
    import timedata as td
except:
    print("Global repo non found")

# Import geometry tools
sys.path.append(root+'geometry/python/')
import meshtools as mt
sys.path.append(root+'dmk_solver/otp_solver/preprocess/assembly/')
import example_grid

# Import dmk tools
sys.path.append(root+'dmk_solver/otp_solver/python')
import dmk_p1p0 
sys.path.append(root+'dmk_solver/build/python/fortran_python_interface/')
from dmk import (Dmkcontrols,    # controls for dmk simulations)
                 Timefunctionals, # information of time/algorithm evolution
                Dmkinputsdata, # structure variable containg inputs data
                 build_subgrid_rhs, #procedure to preprocess forcing term f
                 Tdenspotentialsystem, # input/output result tdens, pot
                dmkp1p0_steady_data   # main interface subroutine
                )
# Import plot tools
import matplotlib.pyplot as plt


# set mesh size 
ndiv=25
length=1.0/float(ndiv)
nref=1

# set grid example
flag_grid='unitsquare'

# build grid using prebuild examples 
points, vertices, coord,topol,element_attributes = example_grid.example_grid(flag_grid,length)

# initialized fortran variable for the spatial discretization
[grid,subgrid]=dmk_p1p0.init_geometry(topol, coord, 1)
ncell=grid.ncell
ntdens=grid.ncell
npot=subgrid.nnode