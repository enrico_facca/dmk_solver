#setup 
import numpy as np
import sys
import os

# Import I/O for timedata
try:
    sys.path.append('../../../../globals/python/timedata/')
    import timedata as td
except:
    print("Global repo non found")


#
# import fortran_pyhton_interface library 
#
current_source_dir=os.path.dirname(os.path.realpath(__file__))
relative_libpath='../../build/python/fortran_python_interface/'
dmk_lib_path=os.path.abspath(os.path.normpath(current_source_dir+'/'+relative_libpath))
sys.path.append(dmk_lib_path)
print(dmk_lib_path)
try:
    from dmk import (
    Dmkcontrols,          # controls for dmk simulations
    Abstractgeometry,     # grids and geometry
    data2grids,           # two grids initializer
    data2grid,            # one grid initializer
    Dmkinputsdata,        # inputs data ( rhs, kappas,beta, etc)
    build_subgrid_rhs,    # integrate forcing on subgrid
    forcing2rhs,          # integrate forcing on grid 
    Tdenspotentialsystem, # input/output result tdens, pot
    dmkp1p0_steady_data,   # main interface subroutine
        Timefunctionals, # information of time/algorithm evolution)
    Dmkp1P0Discretization
    )
except:
    print("Compile with:")
    print("mkdir build; cd build; cmake f2py_interface_dmk../; make ")



#
# Initilized controls. Pass file path
# with controls varaibles or used default
# varaibles. Pass
# 'tdens'/'gfvar'      to select tdens or gfvar dynamics 
# explicit'/'implicit' to select explicit or implicit time-stepping
def init_dmkctrl(filename=None,
                 tdens_gfvar='tdens',
                 explicit_implicit='explicit'):
    
    
    # init set controls
    ctrl = Dmkcontrols.DmkCtrl()
    if ( not (filename is None) ):
        Dmkcontrols.get_from_file(ctrl,filename)
    else:
        #
        # Set Default controls
        #
    
        # time evolution controls
        if (explicit_implicit == 'implicit'):
            if (tdens_gfvar == 'tdens'):
                ctrl.time_discretization_scheme=2
            elif (tdens_gfvar == 'gfvar'):
                ctrl.time_discretization_scheme=4
            else:
                print( 'Not supported',tdens_gfvar,' <tdens> or <gfvar>')
            ctrl.max_time_iterations=100
            ctrl.max_nonlinear_iterations=20
            ctrl.tolerance_nonlinear=1e-10
            ctrl.deltat = 1
            ctrl.deltat_control=2
        elif (explicit_implicit == 'explicit'):
            if (tdens_gfvar == 'tdens'):
                ctrl.time_discretization_scheme=1
            elif (tdens_gfvar == 'gfvar'):
                ctrl.time_discretization_scheme=2
            else:
                print( 'Not supported',tdens_gfvar,' <tdens> or <gfvar>')
            ctrl.max_time_iterations=1000
            ctrl.max_nonlinear_iterations=20
            ctrl.tolerance_nonlinear=1e-10
            ctrl.deltat = 0.1
            ctrl.deltat_control=3
            
        ctrl.max_restart_update=5

        # time step size controls
        ctrl.deltat_expansion_rate=2
        ctrl.deltat_lower_bound=0.01
        ctrl.deltat_upper_bound=1000
        
        # time stepping scheme controls
        ctrl.epsilon_w22=1e-8        

        # globals controls
        ctrl.min_tdens = 1e-13
        ctrl.selection=0
        ctrl.threshold_tdens=1e-10
        
        # Info 
        ctrl.debug=0
        ctrl.info_state=1
        ctrl.info_update=2
        
        # Data saving
        ctrl.id_save_dat=3
        ctrl.fn_tdens='tdens.dat'
        ctrl.fn_pot='pot.dat'
        ctrl.fn_statistics='dmk.log'
        
        
        # Linear solver ctrl
        ctrl.outer_solver_approach='ITERATIVE'
        ctrl.outer_krylov_scheme='PCG'
        if ( ctrl.time_discretization_scheme == 2):
            ctrl.outer_krylov_scheme='BICGSTAB'
        ctrl.outer_imax=400
        ctrl.outer_tolerance=1e-5
        ctrl.outer_isol=1
        ctrl.outer_iort=1
        
        ctrl.outer_prec_type='IC'
        ctrl.outer_prec_n_fillin=30
        ctrl.outer_prec_tol_fillin=1e-5
        ctrl.relax4prec=1e-09
        ctrl.relax_direct=1e-12
        

        ctrl.inner_solver_approach='ITERATIVE'
        ctrl.inner_krylov_scheme='PCG'
        ctrl.inner_imax=100
        ctrl.inner_tolerance=1e-12
        
        ctrl.inner_prec_type='IC'
        ctrl.inner_prec_n_fillin=30
        ctrl.inner_prec_tol_fillin=1e-5
        
    #
    # we need to call the constructor here
    # to complete the initialization
    #
    Dmkcontrols.dmkctrl_constructor(ctrl)
        
    return ctrl;

#
# Function initializing grids from raw data topol and coord
#
def init_geometry(topol, coord, id_subgrid):
    # Handle different shape and pyhton ordering
    if ( topol.shape[0] > topol.shape[1]  ):
        topolfortran=np.array(topol).transpose()
    else: 
        topolfortran=np.array(topol)
    ncell=topolfortran.shape[1]
    
    coord=np.array(coord)
    order=np.amin(topolfortran)
    nnode=np.amax(topolfortran)
    if ( order == 0 ):
        topolfortran=topolfortran+1
        nnode=nnode+1
    

    #
    nnode=np.amax(topolfortran)
    coordfortran=np.zeros([3,nnode])
    if ( coord.shape[1] <= 3 ):
        coord=coord.transpose()
    print(ncell, nnode,coord.shape[0])
    coordfortran[0:coord.shape[0],:]=coord[0:coord.shape[0],:]
        
        
    #
    # init grids
    #
    if (id_subgrid == 1):
        grid=Abstractgeometry.abs_simplex_mesh()
        subgrid=Abstractgeometry.abs_simplex_mesh()
        data2grids(6, topolfortran.shape[0], nnode, ncell,
                   coordfortran, topolfortran,
                   grid,subgrid)
    else:
        grid=Abstractgeometry.abs_simplex_mesh()
        data2grid(0, # logical error unit 
                  3,nnode,ncell, # nnodeincell, nnode, ncell
                  coordfortran, topolfortran, # coordinate, topology
                  grid)        # fortran-python derived type
        subgrid=Abstractgeometry.abs_simplex_mesh()
        data2grid(0, # logical error unit 
                  3,nnode,ncell, # nnodeincell, nnode, ncell
                  coordfortran, topolfortran, # coordinate, topology
                  subgrid) 
        #subgrid=grid # this should create a pointer to grid
        #print('Use 2 level grids')

    return grid,subgrid;

def getgeometry(grid):
    topol = (grid.topol[0:3,:].transpose()-1).astype(int)
    coord = grid.coord.copy()
    return topol, coord;
    
#
# Init tdpot
#
def init_tdpot(ntdens,npot,tdens0=None,pot0=None):
 # Init tdens-pot variable
    tdpot=Tdenspotentialsystem.tdpotsys()
    Tdenspotentialsystem.tdpotsys_constructor(tdpot,0,ntdens, npot,1)
    if (tdens0 is None):
        tdpot.tdens[:]= 1.0
    else:
        tdpot.tdens=tdens0

    if (pot0 is None):
        tdpot.pot[:]=0.0
    else:
        tdpot.pot=pot0

    return tdpot;


#
# Initilized dmk varible give tdens grid
#
def init_dmk(topol,coord,id_subgrid):
    # Init. meshes
    grid, subgrid = init_geometry(topol, coord, id_subgrid)
    ntdens=grid.ncell
    npot=subgrid.nnode

    # Init tdens-pot variable
    tdpot=Tdenspotentialsystem.tdpotsys()
    Tdenspotentialsystem.tdpotsys_constructor(tdpot,0,ntdens, npot,1)
    tdpot.tdens[:]= 1.0
    tdpot.pot[:]=0.0

    # Set inputs
    dmkin=Dmkinputsdata.DmkInputs()
    Dmkinputsdata.dmkinputs_constructor(dmkin,0,ntdens,npot,True)

   
    
    return grid,subgrid,tdpot,dmkin;






#
# Compute Optimal Transport Problem for cost equal to Eucledain distance
# INPUTS:
#  topol, coord : mesh topology  and coordinates
#  forcing      : function f^+,f^- valued on mesh cells
#  tolerance    : tolerances to achieved (measured as tdens variation)
#  [ctrl]       : dmk controls
#  [tdens0]     : initial guess for optimal transport density

def solve_MongeKantorovichEquations(topol,coord,forcing,tolerance=1e-5,ctrl=None,tdens0=None):
    print(forcing.shape)
    return solve_MinFluxProblem(topol,coord,forcing,pflux=1.0,tolerance=1e-5,ctrl=ctrl,tdens0=tdens0);

#
# Run DMK dynamics for different exponent pflux
#
def solve_MinFluxProblem(topol,coord,
                         forcing_cells=None,
                         forcing_nodes=None,
                         pflux=1.0,tolerance=1e-5,
                         ctrl=None,
                         tdens0=None,
                         pot0=None,
                         optimal_tdens=None,
                         optimal_pot=None):
    #
    # set controls
    #    
    if ( ctrl is None):
        ctrl = init_dmkctrl()
        ctrl.tolerance_system_variation=tolerance

    print('ctrl.id_subgrid',ctrl.id_subgrid)
        
    # Init. meshes tdens/pot containers, inputs container ans discretization 
    [ grid,subgrid,tdpot,dmkin] = init_dmk(topol, coord, ctrl.id_subgrid)

    # Init spatial discrteization
    p1p0=Dmkp1P0Discretization.dmkp1p0()
    Dmkp1P0Discretization.dmkp1p0_constructor(p1p0,ctrl,ctrl.id_subgrid,grid,subgrid)

    # shorthand
    ntdens=grid.ncell
    npot=subgrid.nnode

    # set tdens-pot variable
    if ( tdens0 is None ):
        tdpot.tdens[:]= 1.0
    else:
        tdpot.tdens[:]=tdens0[:]
    if ( pot0 is None ):
        tdpot.pot[:]=0.0
    else:
        tdpot.pot[:]=pot0[:]

    #
    # Set inputs
    #
    
    # integrate forcing term w.r.t. p1 base function
    dmkin =  set_forcing_term(dmkin,subgrid,forcing_cells,forcing_nodes)
    # set pflux exponent 
    dmkin.pflux = pflux
    if ( check_inputs(dmkin) != 0):
        info=-1
        return info
    # set reference solutions if exist
    if ( not (optimal_tdens is None) ) :
        Dmkinputsdata.set_optimal_tdens(dmkin,0,optimal_tdens)
    if ( not (optimal_pot is None) ) :
        Dmkinputsdata.set_optimal_pot(dmkin,0,optimal_pot)

    #
    # init type for storing evolution/algorithm info
    #
    timefun=Timefunctionals.evolfun()
    Timefunctionals.evolfun_constructor(timefun, 0,
                                        ctrl.max_time_iterations,
                                        ctrl.max_nonlinear_iterations)
    
    # solve with dmk
    # solve with dmk
    info=np.zeros(1,dtype=np.int8)
    dmkp1p0_steady_data(grid, subgrid, 1,tdpot, dmkin, ctrl, info,timefun=timefun)
    info=info[0]

    # Copy before freeing memory.
    # Use np.array() function because you will get errors otherwise
    tdens = np.array(tdpot.tdens).copy()
    pot   = np.array(tdpot.pot).copy()
    [topol_subgrid,coord_subgrid]=getgeometry(subgrid)
    

    # free memory
    Tdenspotentialsystem.tdpotsys_destructor(tdpot, 0)
    Dmkinputsdata.dmkinputs_destructor(dmkin,0)
    Abstractgeometry.mesh_destructor(grid, 0)
    Abstractgeometry.mesh_destructor(subgrid, 0)
    
    return info, tdens,pot, topol_subgrid, coord_subgrid,timefun;

#
# Function to assign inputs associate to a
# forcing term in the form
# forcing = f + h with
# f: density of measure continous w.r.t. to Lebesge
# h: sum of Dirac funciton
# Here we pass the arrays:
#
# forcing_cells[\icell] = \int_{icell} f(x) dx / \int_{\icell}
# forcing_node[\inode]  = h(x_\inode) 
#

def set_forcing_term(dmkin,subgrid,forcing_cells=None,forcing_nodes=None):

    if (forcing_cells is None):
        forcing_cells=np.zeros(dmkin.ntdens)

    if (forcing_nodes is None):
        forcing_nodes=np.zeros(dmkin.npot)

    if (subgrid.grid_level==0):
        forcing2rhs(subgrid,forcing_cells,dmkin.rhs)
    else:
        build_subgrid_rhs(subgrid, dmkin.rhs, forcing_cells, forcing_nodes)
    
    return dmkin;

def check_inputs(dmkin):
    # if no dirichelet nodes is given
    # the rhs must sum to zero.
    #
    # rhs[i]=\int_{\Omega} f(x)\Psi_{i} + h([x,y,z]_i)
    #
    info=0
    if (dmkin.ndir == 0):
        imbalance=np.sum(dmkin.rhs)
        if (  imbalance > 1e-12) :
            print("Forcing term is not balance:",imbalance)

    return info;

    

#
# Run DMK dynamics for different exponent pflux
#
def solve_dmk(grid, subgrid, tdpot, dmkin, ctrl, info,timefun):
    # solve with dmk
    info=0
    dmkp1p0_steady_data(grid, subgrid, 1,tdpot, dmkin, ctrl, info,timefun=timefun)

    return grid, subgrid, tdpot, dmkin, ctrl, info,timefun;

#
#
# get non-Fortran data from Fortran type
# 
def get_dmk_solution( tdpot, grid, subgrid ):           
    # Copy before freeing memory.
    # Use np.array() function because you will get errors otherwise
    tdens = np.array(tdpot.tdens)
    pot   = np.array(tdpot.pot)
    topol_subgrid=np.array(subgrid.topol)
    coord_subgrid=np.array(subgrid.coord)

    return tdens,pot, topol_subgrid, coord_subgrid;
#
# interface for dmk solver with time varying forcing term
#
# INPUTS:
# coord, topol   : triangualtion
# forcing        :  FUNCTION with
#                  input  = (time, coord=[x,y,z] ) and
#                  output = (function value, steady=logical)
# dirac_forcing  : FUNCTION with
#                  input  = (time, node index ) and
#                  output = (function value, steady=logical)
# pflux(optional): pflux exponent (default=1.0)
# ctrl (optional): controls
#
# OUPUTS:


# def dmk_time_varying_inputs(coord,topol,
#                             forcing=None,
#                             dirac_forcing=None,
#                             pflux=1.0,ctrl=None):

#     # either one must be passed
#     if ( (forcing is None) and (dirac_forcing is None) ):
#         return;
        
#     # use default controls
#     if ( ctrl is None):
#         ctrl=init_dmkctrl()
#         ctrl_copy=ctrl
        
#     #
#     # init. discretization varaible
#     #
#     [grid,subgrid,tdpot,dmkin,p1p0]=new_init_dmk(topol,coord,ctrl)

#     # set inputs
#     dmkin.pflux=1.0
#     forcing_cell=np.zeros([grid.ncell])
#     forcing_node=np.zeros([grid.nnode])
    
#     info=0
#     flag=1
#     current_time=dmkin.time
#     while ( flag != 0 ):
#         Dmkp1P0Discretization.dmkp1p0_cycle_reverse_communication(
#             flag,info,current_time,
#             inputs_data,tdpot,ctrl,simple_ctrl)
        
#         if ( flag == 2 ):
#             #            
#             # User must fill ode_inputs with data at current time
#             #
#             inputs_data.time=current_time
            
#             steady = True
#             # evalute continous forcing term at barycenters
#             if ( forcing is not None ) :
#                 steady=True
#                 for i in range(grid.ncell):
#                     [forcing_cell[i],steady_forcing] = forcing_(current_time, grid%bar_cell[:,i])
#                     steady = steady and steady_forcing 
                    
#             # evalute continous forcing term at barycenters
#             if ( dirac_forcing is not None ) :
#                 for i in range(grid.nnode):
#                     [forcing_node[i],steady_dirac] = dirac_forcing(current_time, inode)
#                     steady = steady and steady_dirac
                                
#             # integrate forcing term w.r.t. p1 base function
#             build_subgrid_rhs(subgrid, dmkin.rhs, forcing, np.zeros(grid.nnode))

#         elif( flag == 3 ):           
#             # 
#             # Right before new update when tdpot and inputs_data are syncronized.
#             # The user can compute print any information regarding
#             # the state of the system
#             #
#             print(flag)
            
#         elif( flag == 4 ):           
#             # 
#             # Optional: user-defined evaluation of steady state
#             # User must set flag "user_system_variation=1"
#             # in controls
#             print(flag)

#         elif( flag == 5 ):           
#             # 
#             # Optional: user-defined set the controls for the next update
#             # User must set flag "user_control_set=1"

#         #elif( flag == 6 ):           
#             # 
#             # Optional: reset controls after update failure 
#             # User must set flag "user_control_reset=1"
#          #   print(flag)
#     #
#     # In case of negative info, break cycle, free memory
#     #
#     if ( info == 0 ):
#         flag=0
    
#     return;

#
# set the dirichlet nodes and values
#
def set_dirichlet(dmkin,dirichlet_nodes,dirichlet_values):
    # set dirichlet boundary conditions
    ndir=len(dirichlet_nodes)
    dmkin.ndir=ndir
    dmkin.dirichlet_nodes[0:ndir]=dirichlet_nodes[0:ndir]+1
    dmkin.dirichlet_values[0:ndir]=dirichlet_values[0:ndir]
    return dmkin;


def dmk_time_varying_forcing(dmkp1p0,
                             dmkin,
                             ctrl=None,
                             tdens0=None,
                             pot0=None,
                             time_zero=0.0,
                             otptdens=None,
                             optpot=None):    

    ##################################################################
    # INITIALIZATION STARTS
    ##################################################################

    # set main dimension
    ntdens=len(topol)
    if (np.amin(topol) == 0 ):
        # 0-based numbering
        npot=np.amax(topol)+1
    elif ( np.amin(topol) == 1) :
        # 1-based numbering
        npot=np.amin(topol)
    else:
        print('Something is wrong woth topol')
        return;
        
    #
    # Init and set controls
    #
    if ( ctrl is None):
        ctrl=init_dmkctrl()
    work_ctrl = Dmkcontrols.DmkCtrl()
    Dmkcontrols.dmkctrl_copy(ctrl,work_ctrl)
        
    #
    # Init. discretization varaible
    #
    topolfortran=init_topol_fortran(topol)
    admk=Admkwrap4Python.admkwrap()
    Admkwrap4Python.graph_admk_constructor(admk, ctrl, ntdens, topolfortran, weight=weight)

    #
    # Init and set tdens/pot varaibles
    #
    # init tdens pot varaible
    tdpot=init_tdpot(ntdens,npot,npotentials=1,
                     t0=time_zero,tdens0=tdens0,
                     pot0=pot0,opttdens=opttdens,optpot=optpot)
        
    #
    # Init and set inputs
    # 
    dmkin=Dmkinputsdata.DmkInputs()
    Dmkinputsdata.dmkinputs_constructor(dmkin,0,ntdens,npot,True)
    if (time_zero is None):
        time_zero = 0.0
    dmkin.pflux=pflux


    #
    # init type for storing evolution/algorithm info
    #
    timefun=Timefunctionals.evolfun()
    Timefunctionals.evolfun_constructor(timefun, 0,
                                        ctrl.max_time_iterations,
                                        ctrl.max_nonlinear_iterations)

    ##################################################################
    # INITIALIZATION DONE
    ##################################################################

    
    ###################################################################
    # run main cycle
    ##################################################################

    # init flags for reverse communication
    flags=Admkwrap4Python.flags()
    flags.info=0
    flags.flag=1
    flags.flag_task=0

    # set initial time
    flags.current_time=time_zero

    # cycle
    while ( not flags.flag == 0 ):
        Admkwrap4Python.admk_cycle_reverse_communication(admk,
                                                         flags,
                                                         dmkin,
                                                         tdpot,
                                                         work_ctrl,
                                                         ctrl)
        flag=flags.flag
        current_time=flags.current_time
        
        if ( flag == 2 ):
            #            
            # Fill dmk inputs with data (rhs, kappa, beta, etc)
            # at current time
            #
            #print('Asked time', current_time,'ctrl.deltat',ctrl.deltat)
            dmkin.time=current_time
            if ( not dmkin.steady):
                steady = True
                # evalute funcition at time and node
                [dmkin.rhs, dmkin.steady] = forcing_function(current_time, npot)
                if (np.sum(dmkin.rhs) > 1.0e-12):
                     info=flags.info
                     tdens = np.array(tdpot.tdens)
                     pot   = np.array(tdpot.pot)
                     return info, tdens,pot;
                    
                
                
        elif( flag == 3 ):           
            # 
            # Right before new update when tdpot and inputs_data are syncronized.
            # The user can compute print any information regarding
            # the state of the system
            #
            #print('Wass=',np.dot(tdpot.pot,dmkin.rhs))
            if ( not( optpot is None  ) ) :
                root = np.unravel_index(np.argmin(optpot, axis=None), optpot.shape)
                temp=tdpot.pot+optpot
                temp=temp-tdpot.pot[root]
                print('err_pot',np.linalg.norm(temp)/np.linalg.norm(optpot))

            # store evolution system in timefun 
            Admkwrap4Python.store_evolution_info(admk, tdpot,dmkin,ctrl,timefun)
            

            
        elif( flag == 4 ):           
            # 
            # Optional: user-defined evaluation of steady state
            # User must set flag "user_system_variation=1"
            # in controls
            tdpot.system_variation=lin.linalg.norm(tdpot.pot)+lin.linalg.norm(tdpot.tdens)

        elif( flag == 5 ):           
            # 
            # Optional: user-defined set the controls for the next update
            # User must set flag "user_control_set=1"
            ctrl.deltat=1.05*ctrl.deltat
            print('new deltat', ctrl.deltat)
        elif( flag == 6 ):           
            # 
            # Optional: reset controls after update failure 
            # User must set flag "user_control_reset=1"
            ctrl.deltat=0.5*ctrl.deltat

    # get info
    info=flags.info
    tdens = np.array(tdpot.tdens)
    pot   = np.array(tdpot.pot)
    
    
    # free memory
    Tdenspotentialsystem.tdpotsys_destructor(tdpot, 0)
    Dmkinputsdata.dmkinputs_destructor(dmkin,0)
    Admkwrap4Python.admk_destructor(admk, 0)
    
    return info,tdens, pot, timefun;


def remove_region(tdpot,
                  cells_grid_to_remove=None,
                  nodes_subgrid_to_remove=None):
    """
    Pass the list of cells (grid_tdens) and nodes (subgrid) to be removed 
    
    argument:
    tdpot: Tdenspotentialsystem 
    cells_grid_to_remove : list with indeces of cells to be remove
    cells_grid_to_remove : list with indeces of nodes to be remove
    
    returns:
    tdpot: Tdenspotentialsystem that will be updated only on
    the active cells and nodes.
    """
    tdpot.ntdens_off=len(cells_grid_to_remove)
    tdpot.ntdens_on=tdpot.ntdens-tdpot.ntdens_off
    len(tdpot.inactive_tdens)
    tdpot.inactive_tdens[0:tdpot.ntdens_off]=np.array(cells_grid_to_remove)+1
    tdpot.active_tdens[0:tdpot.ntdens_on]=np.array(list(set(range(tdpot.ntdens))-set(cells_grid_to_remove)))+1
    
    tdpot.npot_off = len(nodes_subgrid_to_remove)
    tdpot.npot_on = tdpot.npot-tdpot.npot_off
    tdpot.inactive_pot[0:tdpot.npot_off] = np.array(nodes_subgrid_to_remove)+1
    tdpot.active_pot[0:tdpot.npot_on] = np.array(list(set(range(tdpot.npot))-set(nodes_subgrid_to_remove)))+1

    return tdpot
