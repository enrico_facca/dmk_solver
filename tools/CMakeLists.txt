add_executable ( hessian_eigenvalues${EXECUTABLE_EXTENSION} hessian_eigenvalues/src/main.f90)
target_link_libraries( hessian_eigenvalues${EXECUTABLE_EXTENSION} dmk linalg agmg arpack lapack blas hsl)
