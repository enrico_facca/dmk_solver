!>---------------------------------------------------------------------
!> 
!> \author{Enrico Facca}
!>
!> DESCRIPTION: 
!>
!> Program to compute minimal eigavalue of Hessain of Lyapunov
!> Functional. It take a timedata sequane of tdens.dat and pot.dat
!>
!> 
!> REVISION HISTORY:
!> 20201109 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program HessianEigenvalue
  use Globals
  use Scratch
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use Eigenv
  use BackSlashSolver
  use DenseMatrix
  use DmkP1P0Discretization
  use DmkControls
  
  use TimeInputs
  use TdensPotentialSystem
  
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  !use, intrinsic :: iso_fortran_env, only : &
  !     stdin=>input_unit, &
  !     stdout=>output_unit, &
  !     stderr=>error_unit  
  implicit none
  
  
  
  ! strings for paths
  character(len=256) :: input,fn_grid,fn_subgrid,fn_parent, fn_rhs, fn_tdens, fn_pot,fn_stat
  character(len=256) :: msg,outformat,transformation
  character(len=512) :: msg2
  
  ! files
  type(file) :: fgrid,fsubgrid, fparent, frhs, ftdens, fpot,factive
  type(file) :: fstatistics

  
  ! timedata varibles
  type(TimeData) :: tdrhs,tdtdens,tdpot

  ! grids
  type(abs_simplex_mesh):: grid,subgrid

  ! spatial discretization
  type(dmkp1p0),target :: p1p0
  type(DmkCtrl) :: ctrl ! useless but required for intialization

  ! matrices
  type(array_linop) :: list(5)
  real(kind=double) :: alphas(5),right_scale

  type(spmat),target :: stiff
  type(spmat),target :: stiff_relax
  type(scalmat) :: identity_npot
  type(spmat),target :: Bmatrix
  type(TransposeMatrix), target :: BTmatrix
  type(spmat),target :: sparse_kernel_T
  
  type(densemat), target :: ones
  type(TransposeMatrix), target :: onesT,sparse_kernel
  type(new_linop) , target :: WWT,KKT
  type(pair_linop), target :: orthogonal_projector
  type(scalmat),target :: identity
  

  type(inv), target :: iterative_solver, multigrid_solver
  type(pair_linop) , target :: pseudo_inverse
  type(pair_linop) , target :: Hessian

  type(diagmat) :: D_eigenvalues
  type(densemat) :: Q_eigenvectors

  type(diagmat) :: lapack_D_eigenvalues
  type(densemat) :: lapack_Q_eigenvectors
  type(densemat) :: explicit_hessian,explicit_stiff
  
  type(scrt) :: aux

  type(diagmat) :: D_eigenvalues_stiff
  type(densemat) :: Q_eigenvectors_stiff
  
  ! linear solver controls
  type(input_solver) :: ctrl_solver
  type(input_prec) :: ctrl_prec

  type(tdpotsys) :: tdens_potential
      
  
  
  ! euclidian norm
  real(kind=double) :: dnrm2,ddot

  integer :: stdout=6
  integer :: stderr=6
  integer :: info
  integer :: icell
  
  ! work 
  real(kind=double), allocatable  :: tdens(:)
  real(kind=double), allocatable  :: pot(:)
  real(kind=double), allocatable  :: rhs(:)
  real(kind=double), allocatable  :: scr_ntdens(:)
  real(kind=double), allocatable  :: scr_npot(:)
  real(kind=double), allocatable  :: scr_npot2(:)
  real(kind=double), allocatable  :: eigenvalues(:)
  real(kind=double), allocatable  :: eigenvectors(:,:)
  real(kind=double), allocatable  :: temp(:)

  logical :: endreading,endfile1,endfile2,endfile
  logical :: compute_eigen_stiff,compute_eigen,compute_vectors
  logical :: use_arpack, use_lapack
  real(kind=double) :: time, tzero,lift,maxres,maxresnorm,relax_direct
  integer :: res_read
  logical :: rc
  integer :: res,i,j,itime,n1,n2,n3,nb,ilaenv,inode
  integer :: nnode, npot,pair,nev,nev_stiff,ntdens
  integer :: nargs,narg
  character(len=256) :: arg

  real(kind=double) :: abs_res
  character(len=1) creturn

  type(inverse) :: inverse_S


  creturn = achar(13)
  

  nargs = command_argument_count() 
  narg = 0
  if (nargs .ne. 9) call err_handle(stderr,narg)
  
  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) fn_grid
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if


  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) fn_subgrid
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if


  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) fn_parent
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) fn_rhs
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if


  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) fn_tdens
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) fn_pot
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,*,iostat=res_read) lift
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) transformation
     if (res_read .ne. 0) call err_handle(stderr,narg)
     if ( .not. &
          ( ( etb(transformation) =='identity') .or.&
          ( etb(transformation) =='square'  )  ) ) then
        write(*,*) etb(transformation)
        call err_handle(stderr,narg)
     end if
  end if

  
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a)',iostat=res_read) fn_stat
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if
  
 
  ! grid
  call fgrid%init(stderr,etb(fn_grid),20,'in')
  call grid%read_mesh(stderr,fgrid)
  call fgrid%kill(stderr)
  call grid%build_size_cell(stderr)
  call grid%build_normal_cell(stderr)

  ! rhs
  call frhs%init(stderr, etb(fn_rhs), 11,'in')
  call tdrhs%init(stderr,frhs)

  ! tdens 
  call ftdens%init(stderr, etb(fn_tdens), 12,'in')
  call tdtdens%init(stderr,ftdens)

  ! pot
  call fpot%init(stderr, etb(fn_pot), 13,'in')
  call tdpot%init(stderr,fpot)

  if ( ( (tdpot%ndata .ne. tdrhs%ndata) ))  then
     write(*,*) 'dimension of pot rhs must be the same '
     return
  end if
  if ( tdpot%ndata == grid%nnode) then
     ctrl%id_subgrid=0
     subgrid=grid
  else
     ctrl%id_subgrid=1
     !  subgrid
     call fsubgrid%init(stderr,etb(fn_subgrid),20,'in')
     call subgrid%read_mesh(stderr,fsubgrid)
     call fsubgrid%kill(stderr)
     call fparent%init(stderr,etb(fn_parent),20,'in')  
     call subgrid%read_parent(stderr,fparent)
     call fparent%kill(stderr)
     call subgrid%build_size_cell(stderr)
     call subgrid%build_normal_cell(stderr)
  end if
  
  ! set dimension
  npot=tdrhs%ndata
  ntdens=grid%ncell
  
  
  ! creat p1p0 type
  if ( ctrl%id_subgrid == 1 ) then       
     call p1p0%init(&         
          ctrl,&
          ctrl%id_subgrid,grid, subgrid)
  else
     call p1p0%init(&
          ctrl,&
          ctrl%id_subgrid,grid, grid)
  end if

  !
  ! set controls
  !
  pair=4
  nev=2*pair
  nev_stiff=subgrid%nnode

  compute_eigen=.True.
  compute_vectors=.True.
  use_lapack=.True.
  !use_lapack=.False.
  
  use_arpack = .True.
  use_arpack = .False.

  compute_eigen_stiff=.False.


  call fstatistics%init(stderr,etb(fn_stat),30,'out')
  write(fstatistics%lun,*) 'lift=', lift, 'transform=', etb(transformation) ,' meshpar= ', grid%meshpar(0)
  write(fstatistics%lun,*) 'EigMin, EigMax,Cond,MaxResInversion'
    
  !
  ! allocate space
  !
  allocate(&
       tdens(ntdens),&
       scr_ntdens(ntdens),&
       scr_npot(npot),&
       scr_npot2(npot),&
       pot(npot),&
       rhs(npot),&
       eigenvalues(nev),&
       eigenvectors(ntdens,nev),&
       temp(npot),&
       )
  call tdens_potential%init(6,ntdens, npot)

  
  call D_eigenvalues%init(stderr,nev)
  call Q_eigenvectors%init(stderr,ntdens,nev)

  call D_eigenvalues_stiff%init(stderr,npot)
  call Q_eigenvectors_stiff%init(stderr,npot,npot)
  

 

  
  call explicit_hessian%init(stderr,ntdens,ntdens,is_symmetric=.True.)
  explicit_hessian%name='Hbuilt'
           
  call lapack_D_eigenvalues%init(stderr,ntdens)
  call lapack_Q_eigenvectors%init(stderr,ntdens,ntdens)
  i=-999
  call explicit_hessian%spectral_decomposition(stderr,&
       i,&
       lapack_D_eigenvalues%diagonal,aux=aux)
  !       ,lapack_Q_eigenvectors%coeff)
  call aux%init(stderr, 0 , i )

  

  
  
  
  
  ! read rhs
  tzero=max(max(tdtdens%TDtime(1),tdpot%TDtime(1)),tdrhs%TDtime(1))
  call tdrhs%set(stderr, frhs, tzero, endfile)
  do i=1,npot
     rhs(i)=tdrhs%TdActual(1,i)
  end do

  ! init orthogonalization matrix
  call ones%init(6,&
       npot, 1,&
       ! optional arguments
       is_symmetric=.False.)
  ones%name='W'
  ones%coeff=one
  ones%coeff=ones%coeff/&
       dnrm2(npot,&
       ones%coeff(1:npot,1),1)


  call onesT%init(ones)
  onesT%name='WT'
  
  call identity%eye(npot)

  
  WWT=ones*onesT
  write(*,*) 'WWT'
  !!  $  list(1)%linop => ones 
!!$  list(2)%linop => onesT
!!$  call WWT%init(6, &
!!$       2, list,&
!!$       is_symmetric=.True., triangular='N')

  WWT%name='WWT'


  
  alphas(1)=one
  list(1)%linop => identity 

  alphas(2)=-one/&
       ddot(npot,&
       ones%coeff(1:npot,1),1,&
       ones%coeff(1:npot,1),1)
  list(2)%linop => WWT
  !write(*,*) alphas(2),npot/2
  call orthogonal_projector%init(info,6,2,list,'LC',alphas)
  orthogonal_projector%is_symmetric=.True.
  orthogonal_projector%name='P'
  write(*,*) 'Proj'
  
  !
  ! read data and write z_out
  !
  endreading=.false.
  time  = tzero
  itime = 0
  do while ( .not. endreading )
     !
     ! get tdens and pot from file
     !
     call tdtdens%set(stderr, ftdens, time, endfile1)
     call tdpot%set(stderr, fpot, time, endfile2)
     if (endfile1 .or. endfile1) exit
     do i=1,ntdens
        tdens_potential%tdens(i)=tdtdens%TdActual(1,i)
     end do
     do i=1,npot
        tdens_potential%pot(i)=tdpot%TdActual(1,i)
     end do

     !
     ! selection of active region based on tdens
     !
     ctrl%selection = 0
     ctrl%threshold_tdens=1e-16
     if ( ctrl%selection .ne. 0)  then
        call p1p0%set_active_tdpot(tdens_potential,ctrl)
        write(*,*) 'Number approximate zero', tdens_potential%npot_off, 'threshold=', ctrl%threshold_tdens

        call sparse_kernel_T%init(stderr,&
             tdens_potential%npot_off,npot, tdens_potential%npot_off,&
             'csr',&
                                ! optional arguments
             is_symmetric=.False.)
        sparse_kernel_T%ia(1)=1
        do i=1,tdens_potential%npot_off
           sparse_kernel_T%ja(i)=tdens_potential%inactive_pot(i)
           sparse_kernel_T%coeff(i)=one
           sparse_kernel_T%ia(i+1)= sparse_kernel_T%ia(i)+1
        end do
        sparse_kernel_T%name='KT'

        call sparse_kernel%init(sparse_kernel_T)
        sparse_kernel%name='K'

        KKT=sparse_kernel*sparse_kernel_T
!!$     list(1)%linop => sparse_kernel
!!$     list(2)%linop => sparse_kernel_T
!!$     call KKT%init(6, &
!!$          2, list,&
!!$          is_symmetric=.True., triangular='N')
!!$     KKT%name='KKT'
     end if
        

     !
     ! if both file are in steady state, exit 
     !
     if ( tdtdens%steadyTD .and. tdpot%steadyTD) then
        endreading=.True.
     end if

     !
     ! assembly stiff of tdens = A(mu+lift)
     !
     scr_ntdens= tdens_potential%tdens + lift 
     call p1p0%assembly_stiffness_matrix(stderr, scr_ntdens,stiff)
     stiff%name='S'

     
     ! compute residue
     call stiff%mxv(tdens_potential%pot,scr_npot)
     scr_npot=scr_npot-rhs


     ! print state
     outformat=ctrl%formatting('ar')
     write(*,outformat) ' TIME ',time
     
     outformat=ctrl%formatting('rar')
     write(*,ctrl%formatting('rar')) &
          minval(tdens_potential%tdens),'<= TDENS <=',maxval(tdens_potential%tdens)

     abs_res=dnrm2(npot,scr_npot,1)/dnrm2(npot,rhs,1)
     
     outformat=ctrl%formatting('ar')
     write(*,outformat) ' res |A u - b|/|b| read ',abs_res 
     call p1p0%build_norm_grad_dyn(tdens_potential%pot,2.0d0,scr_ntdens)
     write(*,ctrl%formatting('rar')) minval(scr_ntdens),'<= |\Grad POT|^2x <=',maxval(scr_ntdens)

     !
     ! init inverse S
     !
     ctrl_solver%approach='ITERATIVE'
     ctrl_solver%scheme='PCG'
     ctrl_solver%tol_sol=1e-13
     ctrl_solver%print_info_solver = .False.
     ctrl_solver%isol=0
     ctrl_solver%print_info_solver=.False.
     ctrl_solver%iort=1

     !
     ctrl_prec%prec_type='IC'
     ctrl_prec%n_fillin=40
     ctrl_prec%tol_fillin=1.0d-5

     !
     call iterative_solver%init(stiff,ctrl_solver,&
     ctrl_prec,ortogonalization_matrix=WWT)
     iterative_solver%is_symmetric=.True.
     iterative_solver%name='invS'


     
     !
     ! init inverse S + relax I via multigrid
     !
     ctrl_solver%approach='AGMG'
     ctrl_solver%scheme='PCG'
     ctrl_solver%tol_sol=1e-11
     ctrl_solver%print_info_solver = .False.
     ctrl_solver%isol=0
     ctrl_solver%print_info_solver=.False.
     ctrl_solver%iort=1

     ! add relaxation
     stiff_relax=stiff
     relax_direct=1e-12
     call identity_npot%eye(stiff%nrow)  
     call stiff_relax%aMpN(relax_direct,identity_npot)

     call multigrid_solver%init( stiff_relax,ctrl_solver ) 
     multigrid_solver%is_symmetric=.True.
     multigrid_solver%name='invSr'


     !
     !
     ! init inverse S
     !
     ctrl_solver%approach='ITERATIVE'
     ctrl_solver%scheme='FGMRES'
     ctrl_solver%tol_sol=1e-13
     ctrl_solver%print_info_solver = .False.
     ctrl_solver%isol=0
     ctrl_solver%print_info_solver=.False.
     ctrl_solver%iort=1
     call inverse_S%init( 6,stiff%nrow)
     call stiff%info(6)
     call inverse_S%set(stiff,ctrl_solver,prec_left=multigrid_solver,&
          ortogonalization_matrix=WWT)
     
     !
     ! solve S pot = rhs more accurately
     !
     if ( abs_res >= 1e-11 ) then
        call iterative_solver%Mxv(rhs,tdens_potential%pot,info)
        call iterative_solver%info_solver%info(6)
        call stiff%info(6)
        call stiff%mxv(tdens_potential%pot,scr_npot)
        scr_npot=scr_npot-rhs
        
        write(*,outformat) '  res  |A u - b|/|b| new  ', dnrm2(npot,scr_npot,1)/dnrm2(npot,rhs,1)
        call p1p0%build_norm_grad_dyn(tdens_potential%pot,2.0d0,scr_ntdens)
        write(*,ctrl%formatting('rar')) minval(scr_ntdens),'<= |\Grad POT|^2x <=',maxval(scr_ntdens)
     end if
        
     !
     ! assembly B matrix
     !
     call p1p0%assembly_BC_matrix(tdens_potential%pot, 2.0d0, p1p0%B_matrix)
     Bmatrix%name='B'
     if (transformation == 'square') then
        scr_ntdens=two*sqrt(tdens_potential%tdens) ! mu=sigma^2
        call p1p0%B_matrix%DxM( stderr,scr_ntdens )
     end if
     
     !
     ! implcit definition of BT
     !
     call BTmatrix%init(p1p0%B_matrix)
     BTmatrix%name='BT'

     if ( compute_eigen_stiff ) then
        write(*,*) 'form matrix'
        call explicit_stiff%form(stderr,stiff)
        write(*,*) 'formed matrix'
        call explicit_stiff%spectral_decomposition(stderr,&
                info,&        
                D_eigenvalues_stiff%diagonal,&
                Q_eigenvectors_stiff%coeff)
        write(*,*) 'eigs info',info

        !
        ! print residual
        !
        write(*,*) 'Smallest Eigenvalues Stiff - via Lapack'
        outformat=ctrl%formatting('aiararar')
        i=0
        do i=1,nev_stiff
           scr_npot=zero
           call stiff%Mxv(Q_eigenvectors_stiff%coeff(1:npot,i),scr_npot)
           scr_npot=scr_npot-D_eigenvalues_stiff%diagonal(i)*Q_eigenvectors_stiff%coeff(1:npot,i)
           write(*,outformat) 'Eig ', i, ' = ', D_eigenvalues_stiff%diagonal(i), &
                ' res= ', dnrm2(npot,scr_npot,1),&
                'f^t eig',ddot(npot,&
                Q_eigenvectors_stiff%coeff(1:npot,i),1,&
                rhs,1)
           
        end do

        

        
        
!!$        scr_npot=zero
!!$        scr_npot(tdens_potential%active_pot(1:tdens_potential%npot_on))=one
!!$        call factive%init(stderr, etb('active.dat'), 122,'out')
!!$        call write_steady(stderr, factive%lun, npot, scr_npot,factive%fn)
!!$        call factive%kill(stderr)

       
        
        
        
!!$        call arpack_wrap( &
!!$             stiff ,6,&
!!$             nev_stiff,nev_stiff*40,'SM', 1000,1d-8,&
!!$             D_eigenvalues_stiff%diagonal, Q_eigenvectors_stiff%coeff,&
!!$             i,print_info=.True.)
!!$        write(*,*) 'Info ARAPCK=', i

        
     end if

     if ( compute_eigen ) then
        !
        ! init pseudo inverse S
        !
        list(1)%linop => orthogonal_projector 
        list(2)%linop => iterative_solver
        list(3)%linop => orthogonal_projector
        !write(*,*) 'init pseudo inverse'
        call pseudo_inverse%init(info,6, &
             3, list,'LP')
        pseudo_inverse%is_symmetric=.True.
        pseudo_inverse%name='pinvS'
        !write(*,*) 'pseudo inverse'

        
        !
        ! Hessian = B S^{-1} B^{T}
        !
        list(1)%linop => p1p0%B_matrix 
        list(2)%linop => pseudo_inverse
        list(3)%linop => BTmatrix

        !write(*,*) 'hessian'

        call hessian%init(info,6, &
             3, list,'LP')
        hessian%is_symmetric=.True.
        hessian%name='H'
        !write(*,*) 'hessian'


        if ( use_lapack ) then
           maxres=0.0
           maxresnorm=0.0
           write(*,*) 'Build Hessian, B A^{-1} BT (B=A^{l}(mu) or B=A^{l}(mu)*diag(sigma)'
           do i=1,ntdens
              ! mg
              ! compute S^{-1} BT(:,i)
              !

              ! copy in scr_npot = BT(:,i)
              scr_npot=zero
              do j=p1p0%B_matrix%ia(i),p1p0%B_matrix%ia(i+1)-1
                 scr_npot(p1p0%B_matrix%ja(j)) = p1p0%B_matrix%coeff(j)
              end do
              ! apply inverse
              temp(1:npot)=zero
              call iterative_solver%Mxv(scr_npot,temp(1:npot))
              !call inverse_S%Mxv(scr_npot,temp(1:npot))
              !call multigrid_solver%Mxv(scr_npot,temp(1:npot))

              !call stiff%Mxv(temp(1:npot),scr_npot2)
              !scr_npot2=scr_npot-scr_npot2
              !write(*,*)'

              call stiff%Mxv(temp(1:npot),scr_npot2)
              scr_npot2=scr_npot2-scr_npot

              abs_res=dnrm2(npot, scr_npot2,1)
              
              ! in case of failure 
              if (abs_res> 1e-12) then
                 !write(*,*) ' '
                 write(*,ctrl%formatting('aiar')) 'Tdens(',i,')',tdens_potential%tdens(i)
                 call stiff%Mxv(temp(1:npot),scr_npot2)
                 scr_npot2=scr_npot2-scr_npot
                 write(*,*) 'res', dnrm2(npot,scr_npot2,1),'rhs',dnrm2(npot,scr_npot,1)
                 
                 !call multigrid_solver%Mxv(scr_npot,temp(1:npot))
                 !call multigrid_solver%info_solver%info(6)
                 !call stiff%Mxv(temp(1:npot),scr_npot2)
                 !scr_npot2=scr_npot2-scr_npot
                 !write(*,*) 'real mg res', dnrm2(npot,scr_npot2,1)/dnrm2(npot,scr_npot,1)
                 
                 
                 !iterative_solver%ctrl_solver%scheme='MINRES'
                 !call iterative_solver%Mxv(scr_npot,temp(1:npot))
                 !call iterative_solver%info_solver%info(6)
                 !iterative_solver%ctrl_solver%scheme='PCG'
              end if

              ! compute BS^{-1} BT(:,i)
              call p1p0%B_matrix%Mxv(temp(1:npot),explicit_hessian%coeff(1:ntdens,i))
              
              maxres=max(maxres,abs_res)
              maxresnorm=max(maxres,iterative_solver%info_solver%resnorm)
              call iterative_solver%info_solver%info2str(msg)
              if (mod(i,10).eq.0) then
                 write(6,'(a,f3.0,a,1pe9.2,1pe9.2,1pe9.2,a,a)',ADVANCE='NO') &
                      creturn,i*1.0d2/ntdens,'% hessian built : Max err , resnorm, |rhs| = ', maxres,&
                      maxresnorm, dnrm2(npot,scr_npot,1),' |  ', etb(msg)
                 !write(*,*) etb(msg2)
              end if
              ! if ( iterative_solver%info_solver%resreal > ctrl_solver%tol_sol ) write(*,'(a)') etb(msg)
           end do

           ! add extra term for square trasformation
           if (transformation == 'square') then
              call p1p0%build_norm_grad_dyn(tdens_potential%pot,two, p1p0%norm_grad_dyn)
              do icell=1,ntdens                 
                 explicit_hessian%coeff(icell,icell)=explicit_hessian%coeff(icell,icell)+&
                      two*onehalf*p1p0%grid_tdens%size_cell(icell)*(one-p1p0%norm_grad_dyn(icell))
              end do
           end if

           
           
           write(*,'(f4.0,a)') 1.0d2,'% hessian built'
           
                      
           call Q_eigenvectors%info(6)
           write(*,*) 'Spectral decomposition starts',explicit_hessian%ncol
           info=0
           call explicit_hessian%spectral_decomposition(stderr,&
                info,&
                lapack_D_eigenvalues%diagonal,&
                !lapack_Q_eigenvectors%coeff,&
                aux=aux)
           write(*,*) 'Spectral decomposition completed. info= ',info

           !
           ! print residual
           !
           write(*,*) 'Lapack Eigenvalues'
           outformat=ctrl%formatting('aiarar')
           do i=1,nev/2
              scr_npot=zero
              call explicit_hessian%Mxv(lapack_Q_eigenvectors%coeff(1:ntdens,i),scr_ntdens)
              scr_ntdens=scr_ntdens-lapack_D_eigenvalues%diagonal(i)*lapack_Q_eigenvectors%coeff(1:ntdens,i)
              write(*,outformat) 'Eig ', i, ' = ', lapack_D_eigenvalues%diagonal(i), &
                   ' res= ', dnrm2(ntdens,scr_ntdens,1)/lapack_D_eigenvalues%diagonal(i)
           end do
           do i=ntdens-nev/2+1,ntdens
              scr_npot=zero
              call explicit_hessian%Mxv(lapack_Q_eigenvectors%coeff(1:ntdens,i),scr_ntdens)
              scr_ntdens=scr_ntdens-lapack_D_eigenvalues%diagonal(i)*lapack_Q_eigenvectors%coeff(1:ntdens,i)
              write(*,outformat) 'Eig ', i, ' = ', lapack_D_eigenvalues%diagonal(i), &
                   ' res= ', dnrm2(ntdens,scr_ntdens,1)/lapack_D_eigenvalues%diagonal(i)
           end do
           outformat=ctrl%formatting('ar')
           write(*,*) 'Conditioning number', &
                lapack_D_eigenvalues%diagonal(ntdens)/lapack_D_eigenvalues%diagonal(1)

           !
           ! generalized eigenval
           ! Az=lambda B z
           ! We scale by sqrt of B=mass_grid
           !
           if (.False.) then
           scr_ntdens=1.0d0/sqrt(grid%size_cell)
           do j=1,ntdens
              right_scale=scr_ntdens(j)
              do i=1,ntdens 
                 explicit_hessian%coeff(i,j)=&
                      scr_ntdens(i)*&
                      explicit_hessian%coeff(i,j)*right_scale
              end do
           end do
           write(*,*) 'Spectral decomposition starts',explicit_hessian%ncol
           info=0
           call explicit_hessian%spectral_decomposition(stderr,&
                info,&
                lapack_D_eigenvalues%diagonal,&
                !lapack_Q_eigenvectors%coeff,&
                aux=aux)
           write(*,*) 'Spectral decomposition completed. info= ',info

           !
           ! print residual
           !
           write(*,*) 'Lapack Eigenvalues'
           outformat=ctrl%formatting('aiarar')
           do i=1,nev/2
              scr_npot=zero
              call explicit_hessian%Mxv(lapack_Q_eigenvectors%coeff(1:ntdens,i),scr_ntdens)
              scr_ntdens=scr_ntdens-lapack_D_eigenvalues%diagonal(i)*lapack_Q_eigenvectors%coeff(1:ntdens,i)
              write(*,outformat) 'Eig ', i, ' = ', lapack_D_eigenvalues%diagonal(i), &
                   ' res= ', dnrm2(ntdens,scr_ntdens,1)/lapack_D_eigenvalues%diagonal(i)
           end do
           do i=ntdens-nev/2+1,ntdens
              scr_npot=zero
              call explicit_hessian%Mxv(lapack_Q_eigenvectors%coeff(1:ntdens,i),scr_ntdens)
              scr_ntdens=scr_ntdens-lapack_D_eigenvalues%diagonal(i)*lapack_Q_eigenvectors%coeff(1:ntdens,i)
              write(*,outformat) 'Eig ', i, ' = ', lapack_D_eigenvalues%diagonal(i), &
                   ' res= ', dnrm2(ntdens,scr_ntdens,1)/lapack_D_eigenvalues%diagonal(i)
           end do
           outformat=ctrl%formatting('ar')
           write(*,*) 'Conditioning number', &
                lapack_D_eigenvalues%diagonal(ntdens)/lapack_D_eigenvalues%diagonal(1)

        end if
      
           write(fstatistics%lun,'(1pe16.8,a,1pe16.8,a,1pe16.8,a,1pe16.8)') &
                lapack_D_eigenvalues%diagonal(1),',',&
                lapack_D_eigenvalues%diagonal(ntdens),',',&
                lapack_D_eigenvalues%diagonal(ntdens)/lapack_D_eigenvalues%diagonal(1),',',&
                maxres
           

        end if

        if ( use_arpack ) then
           !
           ! compute minimal eigenvalue
           !
           call arpack_wrap( &
                hessian ,6,&
                nev,nev*4,'BE', 1000,1d-10,&
                D_eigenvalues%diagonal, Q_eigenvectors%coeff,&
                i)
           write(*,*) 'Info ARAPCK=', i

           !
           ! print residual
           !
           write(*,*) 'ARpack Eigenvalues'
           outformat=ctrl%formatting('aiarar')
           do i=1,nev/2
              scr_npot=zero
              call hessian%Mxv(Q_eigenvectors%coeff(1:ntdens,i),scr_ntdens)
              scr_ntdens=scr_ntdens-D_eigenvalues%diagonal(i)*Q_eigenvectors%coeff(1:ntdens,i)
              write(*,outformat) 'Eig ', i, ' = ', D_eigenvalues%diagonal(i), &
                   ' res= ', dnrm2(ntdens,scr_ntdens,1)/D_eigenvalues%diagonal(i)
           end do
           do i=1,nev/2
              j=ntdens+i-nev/2
              scr_npot=zero
              call hessian%Mxv(Q_eigenvectors%coeff(1:ntdens,i),scr_ntdens)
              scr_ntdens=scr_ntdens-D_eigenvalues%diagonal(i)*Q_eigenvectors%coeff(1:ntdens,i)
              write(*,outformat) 'Eig ', j, ' = ', D_eigenvalues%diagonal(i), &
                   ' res= ', dnrm2(ntdens,scr_ntdens,1)/D_eigenvalues%diagonal(i)
           end do
           outformat=ctrl%formatting('ar')
           write(*,*) 'Conditioning number', &
                lapack_D_eigenvalues%diagonal(1)/lapack_D_eigenvalues%diagonal(nev)

        end if
           
     end if
     call iterative_solver%kill(stderr)
     
     !
     ! exit if one file reached the there is no time exit
     !
     if ( endfile1 .or. endfile2) then
        exit
     end if


     time = min(tdtdens%TDtime(2),tdpot%TDtime(2))

     call pseudo_inverse%kill(stderr)
     call hessian%kill(stderr)
     call KKT%kill(stderr)
  end do

  if( use_lapack) call explicit_hessian%kill(stderr)

  call fstatistics%kill(stderr)


  
end PROGRAM HessianEigenvalue






subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./timedata2vtk_grid.out <grid> <subgrid> <parent> <rhs> <tdens> <pot> <lift> <transf> <stat> '
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid     (in ) : triangulation in ascii for tdens '
  write(lun_err,*) ' subgrid  (in ) : triangulation in ascii for pot ' 
  write(lun_err,*) ' parent   (in ) : relation grids tdens-pot'
  write(lun_err,*) ' rhs      (in ) : integrated rhs'
  write(lun_err,*) ' tdens    (in ) : tdens'
  write(lun_err,*) ' pot      (in ) : pot'
  write(lun_err,*) ' lift     (in ) : lift'
  write(lun_err,*) ' transf   (in ) : identity/square'
  write(lun_err,*) ' statistic(in ) : path for statistics'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
