#!/usr/bin/env python

import click
import os
import python_script.dmk_folder_structure as dmk 
import python_script.utilities as util
from shutil import copyfile

#
# include path to muffe_p1p0
#
file = open('location_repository.txt', 'r')
repo = file.read().strip()+'/'
file.close()
import sys
sys.path.append(str(repo))
sys.path.append(str(repo)+'/preprocess')
sys.path.append(str(repo)+'/preprocess/2d_assembly')
import timedata as timedata 
import meshtools as mt



def find_path_file(abspath,flag_input):
    fnames=abspath+'/muffa.fnames'
    path=util.search_read(fnames,flag_input,0)
    if ( path[0]=='/') :
        return path
    else :
        return os.path.normpath(abspath+'/'+path)


@click.group()
def cli():
    pass

@cli.command()
@click.argument('fname')
def init(fname):
    """Create empty folder"""
    folder=dmk.dmk_folder_structure(fname)
    folder.mkdirs()
    click.echo('Initialized dmk folder %s' % fname)


@cli.command()
@click.argument('path_original')
@click.argument('path_copy')
def copy(path_original, path_copy):
    """ Create a copy with muffa.fnames pointing 
    \b
    to the inputs of the orignal
    """
    #
    # init. original and copy structure (creates copy folders if required )
    #
    original=dmk.dmk_folder_structure(path_original)
    copy=dmk.dmk_folder_structure(path_copy)
    if ( not os.path.exists(copy.abspath) ):
        print 'making folders'
        copy.mkdirs()
    
    #
    # get relative path
    #
    rel_path=(os.path.relpath(original.abspath, copy.abspath))
    
    #
    # copy fnames files
    #
    src=original.abspath+'/muffa.fnames'
    dst=copy.abspath+'/muffa.fnames'
    print src
    print dst
    copyfile(src,dst)
    
    #
    # reassign all input file via muffa.fnames
    #
    input_flags=['flag_grid',
                 'flag_subgrid',
                 'flag_parent',
                 'flag_kappa',
                 'flag_pflux',
                 'flag_pmass',
                 'flag_decay',
                 'flag_tdens0',
                 'flag_rhs_integrated']
    
    #
    # read directory
    #
    directory=util.search_read(str(dst), 'flag_directory', 0)

    #
    # read file path
    #
    for flag in input_flags:
        original_path=util.search_read(str(dst), flag, 0)
        print (flag, original_path)
        if (original_path[0] == '/'):
            #
            # absolute path case
            #
            redirect=original_path
        else:
            #
            # relarive path
            #
            redirect=rel_path+'/'+original_path
        util.replace(str(dst),flag,0,redirect)

    click.echo('Folder %s copy into %s' % (original.path,copy.path) )


@cli.command()
@click.argument('folder_name')
def optvtk(folder_name):
    """Make vtk files of optimal data"""
    #
    # check existence
    #
    if ( not os.path.exists(folder_name) ) :
        print 'folder < ' + str(folder_name) + ' > not found'
        return
    #
    # program used
    #
    file = open('location_repository.txt', 'r')
    repo = file.read().strip()+'/'
    file.close()
    program=repo+'../geometry/timedata2vtk/timedata2vtk.out '
    program=os.path.normpath(program)
    #
    # get input files
    #
    folder=dmk.dmk_folder_structure(folder_name)
    grid_path=find_path_file(folder.abspath,'flag_grid')
    subgrid_path=find_path_file(folder.abspath,'flag_subgrid')
            
    print 'optimal_tdens'
    command=(str(program)      + ' ' +
             str(grid_path)    + ' ' +
             str(folder.result)+'/opt_tdens.dat' + ' ' +
             str(folder.output_vtk)+'/')
    os.system(command)

    print 'optimal_potential'
    command=(str(program)      + ' ' +
             str(subgrid_path)    + ' ' +
             str(folder.result)+'/opt_pot.dat' + ' ' +
             str(folder.output_vtk)+'/')
    os.system(command)

@cli.command()
@click.argument('folder_name')
def timevtk(folder_name):
    """Make vtk files of time evolution"""
    #
    # check existence
    #
    if ( not os.path.exists(folder_name) ) :
        print 'folder < ' + str(folder_name) + ' > not found'
        return
    #
    # program used
    #
    file = open('location_repository.txt', 'r')
    repo = file.read().strip()+'/'
    file.close()
    program=repo+'../geometry/timedata2vtk/timedata2vtk.out '

    #
    # get input files
    #
    folder=dmk.dmk_folder_structure(folder_name)
    grid_path=find_path_file(folder.abspath,'flag_grid')
    subgrid_path=find_path_file(folder.abspath,'flag_subgrid')
            
    print 'tdens'
    command=(str(program)      + ' ' +
             str(grid_path)    + ' ' +
             str(folder.result)+'/tdens.dat' + ' ' +
             str(folder.output_vtk+'/'))
    os.system(command)

    print 'pot'
    command=(str(program)      + ' ' +
             str(subgrid_path)    + ' ' +
             str(folder.result)+'/pot.dat' + ' ' +
             str(folder.output_vtk+'/'))
    os.system(command)

@cli.command()
@click.argument('folder_name')
@click.argument('flag_input')
@click.argument('input_file_path')
def assign(folder_name, flag_input, input_file_path):
    """Assign an input file"""
    #
    # get relative path of file with respect to folder
    #
    folder=dmk.dmk_folder_structure(folder_name)
    rel_path=os.path.relpath(str(input_file_path), str(folder.abspath))
    util.replace(folder.abspath+'/muffa.fnames',flag_input,0,rel_path)

@cli.command()
@click.argument('folder_name')
def optgrad(folder_name):
    """Compute gradient of optimal potential """
    #
    # path to program used
    #
    file = open('location_repository.txt', 'r')
    repo = file.read().strip()+'/'
    file.close()
    program=repo+'../p1galerkin/pot2grad/pot2grad.out '
    
    #
    # set inputs and outputs
    #
    folder=dmk.dmk_folder_structure(folder_name)
    subgrid_path=+'/'+util.search_read(folder.abspath+'/muffa.fnames','flag_subgrid',0)
    command=(str(program)        + ' ' +
             str(subgrid_path)   + ' ' +
             str(folder.result) +'opt_pot.dat' + ' ' +
             str(folder.result) +'opt_grad.dat')
    os.system(command)

@cli.command()
@click.argument('folder_name')
def optfield(folder_name):
    """Compute optimal field (Evans-Gangbo-1999)"""
    #
    # path to program used
    #
    program=os.path.normpath(repo+
                             '../build_optimal_vector_field/code/optimal_vector_field.out')
    
    #
    # get input files
    #
    folder=dmk.dmk_folder_structure(folder_name)
    grid_path=find_file_path(folder.abspath,'flag_grid')
    subgrid_path=find_file_path(folder.abspath,'flag_subgrid')
    parent_path=find_file_path(folder.abspath,'flag_parent')
    command=('../build_optimal_vector_field/code/optimal_vector_field.out '+
             grid_path   + ' ' + 
             subgrid_path+ ' ' +
             parent_path+ ' ' +
             str(paths.input) +'forcing.dat'+ ' ' +
             str(paths.result)+'optdens.dat'+ ' ' +
             str(paths.result)+'opt_pot.dat' + ' ' +
             str(paths.result)+'optfield.dat')
    os.system(command)
    
@cli.command()
@click.argument('folder_name')
@click.argument('file_ctrl')
def assembly(folder_name,file_ctrl):
    """Use muffe_p1p0/preprocess examples"""
    folder=dmk.dmk_folder_structure(folder_name)
    folder.mkdirs()
    # repository location
    file = open('location_repository.txt', 'r')
    repo = os.path.abspath(file.read().strip())
    file.close()

    #
    # copy folder_names file 
    #
    src=repo+'/code/muffa.fnames'
    dst=folder.abspath+'/muffa.fnames'
    copyfile(src, dst)
    
    #
    # get assembler location
    # 
    absctrl=os.path.abspath(file_ctrl)
    domain=util.search_read(absctrl,'flag_domain',0)
    assembly_folder = repo+'/preprocess/'+domain+'_assembly/'
    
    #
    # mov into assembly folder and create inputs
    #
    cwd=os.getcwd()
    os.chdir(assembly_folder)
    command=('python main.py'  +' '+
             str(absctrl) +' '+
             str(folder.input)  +' '+
             str(folder.input_vtk) )
    print command
    os.system(command)
    os.chdir(cwd)

@cli.command()
@click.argument('folder_name')
@click.argument('ctrlfile')
def run(folder_name,ctrlfile):
    """Run simulation given folder and control file"""
    folder=dmk.dmk_folder_structure(folder_name)
    
    # repository location
    file = open('location_repository.txt', 'r')
    repo = os.path.abspath(file.read().strip())
    file.close()
    program=repo+'/code/muffa.out'

    #
    # copy control file
    #
    dst=folder.input+'/muffa.ctrl'
    copyfile(os.path.abspath(ctrlfile),dst )



    #
    # mov into assembly folder and create inputs
    #
    cwd=os.getcwd()
    os.chdir(folder.abspath)
    os.system(program)
    os.chdir(cwd)

@cli.command()
@click.argument('folder_name')
def err_distance(folder_name):
    """Compute err=pot-exact_pot (after centering)"""
    #
    # get relative path of file with respect to folder
    #
    folder=dmk.dmk_folder_structure(folder_name)
    
    # read optimal solution and get point dist(p)=0
    opt_pot=timedata.read_steady_timedata(str(folder.input+'/opt_pot.dat'))
    print opt_pot.shape
    for i in range(len(opt_pot)):
        if (opt_pot[i] == 0.0 ):
            inode_sub = i
            break

    print ('inode', inode_sub)

    # find the corresponding value
    opt_pot=timedata.read_steady_timedata(str(folder.result+'/opt_pot.dat'))
    shift=opt_pot[inode_sub]
    
    print (shift)

    #
    # shift optimal potential (otp_pot_shifted.dat) 
    # and optimal potential (pot_shifted.dat)
    # and compute errors
    
    #
    # program used
    #
    program=os.path.normpath(repo+
                             '../globals/axpy_timedata/axpy.out')
    #
    # shift potentials
    #
    command=(program + ' ' +
             '-1.0 '+str(shift)+ ' ' +
             str(folder.result)+'/opt_pot.dat' + ' ' +
             str(folder.result)+'/opt_pot_shifted.dat')
    print command
    os.system(command)

    # command=(program + ' ' +
    #          '-1.0 '+str(shift)+ ' ' +
    #          str(folder.result)+'/pot.dat' + ' ' +
    #          str(folder.result)+'/pot_shifted.dat')
    # os.system(command)
      
    command=(program + ' ' +
             '-1.0 ' +
             str(folder.input+'/opt_pot.dat') + ' ' +
             str(folder.result+'/opt_pot_shifted.dat')+ ' ' +
             str(folder.result+'/err_opt_pot_shifted.dat'))
    print command
    os.system(command)

    # command=(program + ' ' +
    #          '-1.0 '+
    #          str(folder.input+'/opt_pot.dat') + ' ' +
    #          str(folder.result+'/pot_shifted.dat')+ ' ' +
    #          str(folder.result)+'/err_pot_shifted.dat')
    # os.system(command)

    #
    # program used
    #
    program=os.path.normpath(repo+
                             '../geometry/lp_norm_timedata/lp_norm.out')
    
    command=(program + ' ' +
             str(folder.input)+'/subgrid.dat' + ' ' +
             str(folder.result)+'/err_opt_pot_shifted.dat' + ' ' +
             ' 2.0')
    os.system(command)
'''
@cli.command()
@click.argument('folder_name')
@click.argument('min')
@click.argument('max')
def get_graph(folder_name,min,max):
    """Build graph thresholding opt_tdens"""
    folder=dmk.dmk_folder_structure(folder_name)

    #
    # build graph of cell connection
    #
    full_graph=str(folder.input+'/graph_cell.dat')
    if ( not os.path.exists(full_graph) ):
        program=os.path.normpath(repo+
                                 '../geometry/grid2graph/grid2graph.out')
        command=(program + ' ' +
                 str(folder.input+'/grid.dat')+ ' ' +
                 full_graph + ' cell')
        os.system(command)

    #
    # build graph thresholding opt_tdens
    #
    program=os.path.normpath(repo+
                             '../geometry/selection_grid/selection_grid.out')
    command=(program + ' ' +
             full_graph + ' ' +
             str(folder.result+'/opt_tdens.dat') + ' ' +
             str(min) + ' ' + str(max)+ ' ' +
             str(folder.result+'/opt_graph.dat') + ' ' +
             str(folder.result+'/parent_graph.dat'))    
    os.system(command)

    #
    # print vtk
    #
    print str(folder.output_vtk)+'/opt_graph.vtk'
    program=os.path.normpath(repo+'../geometry/grid2vtk/grid2vtk.out ')
    command=(str(program)      + ' ' +
             str(folder.result+'/opt_graph.dat') + ' ' +
             str(folder.output_vtk)+'/opt_graph.vtk')
    os.system(command)
'''
@cli.command()
@click.option('-n', default='o', show_default=True,type=str)
@click.argument('folder_name')
@click.argument('min')
@click.argument('max')
def get_graph(n,folder_name,min,max):
    """Build graph thresholding opt_tdens or thresholding all the obtained tdens"""
    folder=dmk.dmk_folder_structure(folder_name)
    # This divides the tdens file in separated files, one for each iter number
    ### https://stackoverflow.com/questions/546508/how-can-i-split-a-file-in-python
    input = open(folder.result+'/opt_tdens.dat', 'r').read().split('\n')
    firstLine=input[0]
    dim_=int(input[0].split()[1])
    outputBase = folder.result+'/tdens'

    # This is shorthand and not friendly with memory
    # on very large files (Sean Cavanagh), but it works.
    input = open(folder.result+'/tdens.dat', 'r').read().split('\n')

    splitLen = dim_+2
    at = 0
    for lines in range(0, len(input), splitLen):
        # First, get the list slice
        outputData = input[lines+1:lines+splitLen+1]
        # Now open the output file, join the new slice with newlines
        # and write it out. Then close the file.
        output = open(outputBase + str(at) + '.dat', 'w')
        output.write(firstLine+'\n')
        output.write('\n'.join(outputData))
        output.write('\n time     1e+30     ')
        output.close()
        # Increment the counter
        at += 1
    
    #
    # build graph of cell connection
    #
    full_graph=str(folder.input+'/graph_cell.dat')

    if ( not os.path.exists(full_graph) ):
        program=os.path.normpath(repo+
                                 '../geometry/grid2graph/grid2graph.out')
        command=(program + ' ' +
                 str(folder.input+'/grid.dat')+ ' ' +
                 full_graph + ' cell')
        os.system(command)

    if n == 'a':
        # -a stands for 'all'
        print '-a. Building all the graphs'
        program1=os.path.normpath(repo+
                                 '../geometry/selection_grid/selection_grid.out')
        program2=os.path.normpath(repo+'../geometry/grid2vtk/grid2vtk.out ')
        for i in range(at-1):
            
            command=(program1 + ' ' +
                 full_graph + ' ' +
                 str(folder.result+'/tdens'+str(i)+'.dat') + ' ' +
                 str(min) + ' ' + str(max)+ ' ' +
                 str(folder.result+'/graph'+str(i)+'.dat') + ' ' +
                 str(folder.result+'/parent_graph.dat'))
    
            os.system(command)
            #
            # print vtk
            #
            
            command=(str(program2)      + ' ' +
                     str(folder.result+'/graph'+str(i)+'.dat') + ' ' +
                     str(folder.output_vtk)+'/graph'+str(i)+'.vtk')
            os.system(command)

    elif n=='o':
        # -o stand for 'optimal'
        print '-o. Building optimal graphs'
    
        #
    # build graph thresholding opt_tdens
    #
        program=os.path.normpath(repo+
                                 '../geometry/selection_grid/selection_grid.out')
        command=(program + ' ' +
                 full_graph + ' ' +
                 str(folder.result+'/opt_tdens.dat') + ' ' +
                 str(min) + ' ' + str(max)+ ' ' +
                 str(folder.result+'/opt_graph.dat') + ' ' +
                 str(folder.result+'/parent_graph.dat'))
    
        os.system(command)

        #
        # print vtk
        #
        program=os.path.normpath(repo+'../geometry/grid2vtk/grid2vtk.out ')
        command=(str(program)      + ' ' +
                 str(folder.result+'/opt_graph.dat') + ' ' +
                 str(folder.output_vtk)+'/opt_graph.vtk')
        os.system(command)
    else:
        print '-'+str(n)+'. Not a valid input.'

if __name__ == '__main__':
    cli()

