program graph_dmk
use KindDeclaration
use Globals
use AbstractGeometry
use TimeInputs  
use DmkControls
use TdensPotentialSystem
use DmkInputsData
implicit none
type(abs_simplex_mesh) :: grid
type(file) :: fgrid,frhs,fweigth,fopttdens,foptpot,fctrl
character(len=256) :: path_grid,path_rhs,path_weigth,path_tdens,path_pot,arg,path_opttdens,path_optpot,path_ctrl
type(TimeData) :: tdrhs,tdweigth,tdopttdens,tdoptpot
type(DmkCtrl) :: ctrl
type(tdpotsys) :: tdpot
type(DmkInputs) :: dmkin

integer :: info,narg,res,res_read
integer, allocatable :: topol(:,:)
integer :: i,stderr,nargs,npot,ntdens

real(kind=double), allocatable :: rhs(:),tdens(:),pot(:)
real(kind=double), allocatable ::  weigth(:),opttdens(:),optpot(:)
logical :: endfile
real(kind=double) :: pflux

stderr=0

!
! read path from user
!
nargs = command_argument_count() 
narg = 0

narg=1
call  get_command_argument(narg, arg,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(arg,'(a256)',iostat=res_read) path_grid
   if (res_read .ne. 0) call err_handle(stderr,narg)
   ! read grid
   call fgrid%init(stderr,path_grid,2000,'in')
   call grid%read_mesh(stderr,fgrid)
   allocate(topol(2,grid%ncell))
   topol=grid%topol(1:2,1:grid%ncell)
   ntdens=grid%ncell
   npot=grid%nnode
   write(*,*) etb(path_grid),ntdens,npot
   call grid%kill(0)
end if


narg=narg+1
call  get_command_argument(narg, arg,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(arg,'(a256)',iostat=res_read) path_ctrl
   if (res_read .ne. 0) call err_handle(stderr,narg)
   ! read rhs
   call fctrl%init(stderr,path_ctrl,101,'in')
   call ctrl%readfromfile(101)
   call fctrl%kill(stderr)
end if

narg=narg+1
call  get_command_argument(narg, arg,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(arg,'(a256)',iostat=res_read) path_rhs
   if (res_read .ne. 0) call err_handle(stderr,narg)
   ! read rhs
   call frhs%init(stderr,path_rhs,101,'in')
   call tdrhs%init(stderr, frhs, 1,npot)
   call tdrhs%set(stderr, frhs, 0.0d0,endfile)
   write(*,*) etb(path_rhs)
end if


narg=narg+1
call  get_command_argument(narg, arg,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(arg,'(a256)',iostat=res_read) path_weigth
   if (res_read .ne. 0) call err_handle(stderr,narg)
   ! read weigth
   call fweigth%init(stderr,path_weigth,111,'in')
   call tdweigth%init(stderr, fweigth, 1,ntdens)
   call tdweigth%set(stderr, fweigth, 0.0d0,endfile)
   write(*,*) etb(path_weigth)
end if


narg=narg+1
call  get_command_argument(narg, arg,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(arg,'(a256)',iostat=res_read) path_tdens
   if (res_read .ne. 0) call err_handle(stderr,narg)
end if

narg=narg+1
call  get_command_argument(narg, arg,status=res)
if (res .ne. 0) then
   call err_handle(stderr,narg)
else
   read(arg,'(a256)',iostat=res_read) path_pot
   if (res_read .ne. 0) call err_handle(stderr,narg)
end if

!
! set arrays
!
call tdpot%init(0,ntdens, npot,1)
tdpot%tdens=one
tdpot%pot=zero

!
! set inputs
!
allocate(weigth(ntdens),opttdens(ntdens),optpot(npot))
call dmkin%init(6,ntdens, npot,.True.)
do i=1,npot
   dmkin%rhs(i) = tdrhs%TDactual(1,i)
end do

do i=1,ntdens
   weigth(i) = tdweigth%TDactual(1,i)
end do


! solutions (if passed)
if (nargs .ge. 7) then
   if (nargs .eq. 7) then
      narg=7
      call  get_command_argument(narg, arg,status=res)
      if (res .ne. 0) then
         call err_handle(stderr,narg)
      else
         read(arg,'(a256)',iostat=res_read) path_opttdens
         if (res_read .ne. 0) call err_handle(stderr,narg)
      end if
      ! read optimal solutions if available
      call fopttdens%init(stderr,path_opttdens,121,'in')
      call tdopttdens%init(stderr, fopttdens)
      call tdopttdens%set(stderr, fopttdens, 0.0d0,endfile)
      if (tdopttdens%ndata .eq. ntdens) then
         do i=1,ntdens
            opttdens(i)=tdopttdens%TDactual(1,i)
         end do
         call set_optimal_tdens(dmkin,stderr,opttdens)
      else
         do i=1,npot
            optpot(i)=tdopttdens%TDactual(1,i)
         end do
         call set_optimal_pot(dmkin,stderr,optpot)
         write(*,*) 'Optimal Potential read from', etb(path_opttdens)
      end if
      call tdopttdens%kill(stderr) 
   else
      narg=7
      call  get_command_argument(narg, arg,status=res)
      if (res .ne. 0) then
         call err_handle(stderr,narg)
      else
         read(arg,'(a256)',iostat=res_read) path_opttdens
         if (res_read .ne. 0) call err_handle(stderr,narg)
      end if
      call fopttdens%init(stderr,path_opttdens,121,'in')
      write(*,*) 'Optimal Tdens read from', etb(path_opttdens)
      call tdopttdens%init(stderr, fopttdens)
      call tdopttdens%set(stderr, fopttdens, 0.0d0,endfile)
      do i=1,ntdens
         opttdens(i)=tdopttdens%TDactual(1,i)
      end do
      call set_optimal_tdens(dmkin,stderr,opttdens)
      call tdopttdens%kill(stderr) 


      ! read optpot
      narg=8
      call  get_command_argument(narg, arg,status=res)
      if (res .ne. 0) then
         call err_handle(stderr,narg)
      else
         read(arg,'(a256)',iostat=res_read) path_optpot
         if (res_read .ne. 0) call err_handle(stderr,narg)
      end if
      ! read optimal solutions if available
      call foptpot%init(stderr,path_optpot,121,'in')
      write(*,*) 'Optimal Potential read from', etb(path_optpot)
      call tdoptpot%init(stderr, foptpot)
      call tdoptpot%set(stderr, foptpot, 0.0d0,endfile)
      do i=1,npot
         optpot(i)=tdoptpot%TDactual(1,i)
      end do
      call set_optimal_pot(dmkin,stderr,optpot)
      call tdoptpot%kill(stderr)
      
   end if
else
   write(*,*) 'Optimal solution not passed'
end if
      
! info , saving ctrl
ctrl%id_save_dat=3
ctrl%lun_statistics=10
ctrl%lun_out=6


ctrl%lun_tdens=2041
ctrl%fn_tdens=etb(path_tdens)

ctrl%lun_pot=2144
ctrl%fn_pot=etb(path_pot)



info=0
call DMKGraph_steady_data(&
     ntdens,topol,weigth,tdpot,dmkin,&
     ctrl,info,opttdens,optpot)

end program graph_dmk

subroutine err_handle(stderr, narg)
  use Globals
  implicit none

  integer, intent(in) :: stderr
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(stderr,*) ' Error passing arguments, correct usage:'
  write(stderr,*) ' ./graph_dmk.out <graph> <rhs> <weigth> <tdens> <pot>]'
  write(stderr,*) ' where '
  write(stderr,*) ' graph    (in )      : graph in ascii'
  write(stderr,*) ' ctrl     (in )      : control file'
  write(stderr,*) ' rhs      (in )      : timedata with rhs'
  write(stderr,*) ' weigth   (in )      : timedata with weigth'
  write(stderr,*) ' tdens    (out)      : timedata with tdens'
  write(stderr,*) ' pot      (out)      : timedata with pot'
  rc = IOerr(stderr, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
