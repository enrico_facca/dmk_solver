#setup 
import numpy as np
import sys
import os
from scipy import sparse as sp
from scipy import linalg
#
# import fortran_pyhton_interface library
# (assuming default compilation in build/)
#
current_source_dir=os.path.dirname(os.path.realpath(__file__))
relative_libpath='../../build/python/fortran_python_interface/'
dmk_lib_path=os.path.abspath(os.path.normpath(current_source_dir+'/'+relative_libpath))
try:
    sys.path.append(dmk_lib_path)
    from dmk import (
        Dmkcontrols,          # controls for dmk simulations
        Dmkinputsdata,        # inputs data ( rhs, kappas,beta, etc) 
        Tdenspotentialsystem, # output result tdens, pot
        dmkgraph_steady_data,  # main interface subroutine
        Timefunctionals, # information of time/algorithm evolution)
        Admkwrap4Python, # alebraig dmk model
    )
except:
    print("Compile with:")
    print("mkdir build; cd build; cmake f2py_interface_dmk../; make ")

# import timedata
relative_libpath='../../../globals/python/timedata/'
globals_lib_path=os.path.abspath(os.path.normpath(current_source_dir+'/'+relative_libpath))
try:
    sys.path.append(globals_lib_path)
    import timedata as td
except:
    print("NOT FOUND",globals_lib_path)



#
# Solve min |v|^{\gamma} A v = b
# with \gamma=(2-pflux)
# A = signed incidence matrix of graph \in R^{Nedge,Nnode}
# b = rhs \in R^{Nnode}
#
# USAGE:
# [info, tdens, pot,flux, timefun]=dmk_graph(topol,rhs,pflux=1.0,
#              [tolerance=1e-5,weight,ctrl,tdens0,pot0,opttdens,optpot])
#
# INPUTS:
# topol              integer matrix: Matrix (dimension=[2,Nedge])
#                                      describing the node-node connection.
#                                      0-based index.
# rhs                  real array  : Forcing term in R^Nnode. Must sum to zero.
# pflux                real scalar : Exponent for optimization.
#                                      gamma=2-pflux
# tolerance (optional) real scalar : Tolerance to achieve
#                                      If both tolerance and ctrl are
#                                      passed, this is the tolerance used
# weight (optional)    real array  : Edge weight (default=1.0)
# ctrl (optional)      DmkCtrl type: Controls (default see init_dmkctrl())
# tdens0 (optional)    real array  : Initial tdens (Dimension=Nedge)
# pot0 (optional)      real array  : Initial pot (Dimension=Nnode)
# opttdens (optional)  real array  : Optimal tdens (Dimension=Nedge) to check error
# optpot (optional)    real array  : Optimal pot (Dimension=Nnode) to check error
# 
# RETURN:
# info                      integer: Flag for algorithm execution
#                                    0=Ok not 0=Some errors occured
# tdens               real array  : Final tdens solution (Dimension=Nedge)
# pot                 real array  : Final pot solution (Dimension=Nnode)
# flux                real array  : Final flux solution=Diag(Tdens) Weight^{-1} A^T Pot (Dimension=Nedge)
# timefun             evolfun     : Collection of data from time evolution.
def dmk_graph(topol,rhs,
              pflux=1.0,
              tolerance=None,
              weight=None,
              ctrl=None,
              tdens0=None,
              pot0=None,
              opttdens=None,
              optpot=None):

    # Dimensions
    ntdens=len(topol)
    npot=len(rhs)

    # init tdens pot varaible
    tdpot=init_tdpot(ntdens,npot,npotentials=1,
                     t0=0.0,tdens0=tdens0,pot0=pot0)
    

    # set inputs
    dmkin=Dmkinputsdata.DmkInputs()
    Dmkinputsdata.dmkinputs_constructor(dmkin,0,ntdens,npot,True)
    dmkin.rhs   = rhs
    dmkin.pflux = pflux
    td.write_steady_timedata('rhs_steady.dat',dmkin.rhs)
    if ( not ( optpot is None) ) :
        Dmkinputsdata.set_optimal_pot(dmkin,0,optpot)
    if ( not ( opttdens is None) ) :
        Dmkinputsdata.set_optimal_tdens(dmkin,0,opttdens)
    
    if (weight is None):
        weight = np.ones(ntdens)
    
    #
    # set cotronls
    # if tolerance is passed it will replace the
    # tolerance in ctrl.
    #
    if ( ctrl is None):
        ctrl=init_dmkctrl()
        if ( tolerance is None ):
            # set to default
            ctrl.tolerance_system_variation=1e-5
    else:
        if ( not (tolerance is None) ):
            # overwrite the tolerance
            ctrl.tolerance_system_variation=tolerance
    
    

    # Handle different shape and pyhton edges ordering 
    if ( topol.shape[1] == 2 ):
        topolfortran=topol.transpose()
    else: 
        topolfortran=topol
    if ( np.amin(topolfortran) == 0 ):
        topolfortran=topolfortran+1
    topolfortran=np.sort(topolfortran,axis=0)


    #
    # init type for storing evolution/algorithm info
    #
    timefun=Timefunctionals.evolfun()
    Timefunctionals.evolfun_constructor(timefun, 0,
                                        ctrl.max_time_iterations,
                                        ctrl.max_nonlinear_iterations)

    flux=np.zeros(ntdens)
    #
    # solve optimal transport problem on graphs
    #
    info=np.zeros(1,dtype=np.int8)
    dmkgraph_steady_data(ntdens,
                         topolfortran,
                         weight,
                         tdpot,
                         dmkin,
                         ctrl,
                         info,
                         timefun,
                         flux)
    info = int( info[0] )
    tdens= tdpot.tdens.copy()
    pot  = tdpot.pot.copy()

    # free memory
    Tdenspotentialsystem.tdpotsys_destructor(tdpot, 0)
    Dmkinputsdata.dmkinputs_destructor(dmkin,0)

    
    
    return info, tdens, pot,flux, timefun;


def forcing_test(time,npot):
    values=np.zeros(npot)
    values[0]=np.cos(10*time)
    values[100]=np.cos(10*time)
    values[200]=-2*np.cos(10*time)
    steady=False
    return values, steady;

def steady_forcing_test(time,npot):
    values=np.zeros([npot])
    values[0]=1
    values[100]=1
    values[200]=-4
    values[300]=1
    values[400]=1
    steady=True
    return values, steady;

#
# Init tdens pot container
#
def init_tdpot(ntdens,npot,npotentials=1, # dimension
               t0=0.0,tdens0=None,pot0=None):  # itatial guess
    tdpot=Tdenspotentialsystem.tdpotsys()
    Tdenspotentialsystem.tdpotsys_constructor(tdpot,0,ntdens, npot,npotentials)
    if ( tdens0 is None):
        tdpot.tdens[:]=1.0
    else:
        print(tdens0[0])
        if (min(tdens0)<=0.0):
            print('Initial tdens must have positive entries')
            sys.exit()
        tdpot.tdens=tdens0
        if (min(tdpot.tdens)<=0.0):
            print('Initial tdpo.tdens must have positive entries')
            sys.exit()
            
    if ( pot0 is None):
        tdpot.pot[:]=0.0
    else:
        tdpot.pot=pot0
    tdpot.time=t0

    return tdpot;

#
# Initialize controls from file or with default settings
#.
# USAGE:
# ctrl=init_dmkctrl([filename,tdens_gfvar,explicit_implicit])
#
# INPUTS:
# filename (in,optional)   string:
#                          Path with file listing all controls.
# tdens_gfvar(in,optional) string:
#                          Dynamic used. Must be equal to
#                          'tdens'(default) or 'gfvar'
# explicit_implicit(in,optional) string:
#                          Time stepping used. Must be equal to
#                          'explicit'(default) or 'implicit'
# 
# RETURN:
# ctrl                     DmkCtrl: Derived type containg all
#                          controls for DMK evolution
def init_dmkctrl(filename=None,
                 tdens_gfvar='tdens',
                 explicit_implicit='explicit'):
    
    #
    # Set Default controls
    #

    # init set controls
    ctrl = Dmkcontrols.DmkCtrl()

    
    if ( not (filename is None) ):
        Dmkcontrols.get_from_file(ctrl,filename)
    else:
        #
        # Set Default controls
        #
    
        # time evolution controls
        if (explicit_implicit == 'implicit'):
            if (tdens_gfvar == 'tdens'):
                ctrl.time_discretization_scheme=2
            elif(tdens_gfvar == 'gfvar'):
                ctrl.time_discretization_scheme=4
            else:
                raise Exception(('tdens_gfvat must be equal' +
                             'to tdens or gfvar. Passed:'+tdens_gfvar) )
            # convergence controls
            ctrl.max_time_iterations=100
            ctrl.max_nonlinear_iterations=20
            ctrl.tolerance_nonlinear=1e-10

            # time step size controls
            ctrl.deltat = 0.1
            ctrl.deltat_control=2
            ctrl.deltat_expansion_rate=2
            ctrl.deltat_lower_bound=0.01
            ctrl.deltat_upper_bound=1000
            
        elif (explicit_implicit == 'explicit'):
            if (tdens_gfvar == 'tdens'):
                ctrl.time_discretization_scheme=1
            elif(tdens_gfvar == 'gfvar'):
                ctrl.time_discretization_scheme=3
            else:
                raise Exception(('tdens_gfvat must be equal' +
                                 'to tdens or gfvar. Passed:'+tdens_gfvar) )
            # convergence controls
            ctrl.max_time_iterations=1000
            ctrl.max_nonlinear_iterations=20
            ctrl.tolerance_nonlinear=1e-10

            # time step size controls
            ctrl.deltat = 0.1
            ctrl.deltat_control=2
            ctrl.deltat_expansion_rate=1.05
            ctrl.deltat_lower_bound=0.01
            ctrl.deltat_upper_bound=0.8
        else:
            raise Exception(('explicit_implicit must be equal' +
                             'to explicit or implicit. Passed:'+explicit_implicit) )
        
    # restarst setting     
    ctrl.max_restart_update=5

    
   
    # time stepping scheme controls
    ctrl.epsilon_w22=1e-8


    # globals controls
    ctrl.min_tdens = 1e-13
    ctrl.selection=0
    ctrl.threshold_tdens=1e-10

    # Info 
    ctrl.debug=0
    ctrl.info_state=0
    ctrl.info_update=0

    # Data saving
    ctrl.id_save_dat=0
  

    # Linear solver ctrl
    ctrl.outer_solver_approach='ITERATIVE'
    ctrl.outer_krylov_scheme='BICGSTAB'
    ctrl.outer_imax=100
    ctrl.outer_tolerance=1e-5
    ctrl.outer_isol=1
    ctrl.outer_iort=1
    
    ctrl.outer_prec_type='IC'
    ctrl.outer_prec_n_fillin=20
    ctrl.outer_prec_tol_fillin=1e-3
    ctrl.relax4prec=1e-09
    ctrl.relax_direct=1e-12
   

    ctrl.inner_solver_approach='ITERATIVE'
    ctrl.inner_krylov_scheme='PCG'
    ctrl.inner_imax=100
    ctrl.inner_tolerance=1e-12
    
    ctrl.inner_prec_type='IC'
    ctrl.inner_prec_n_fillin=30
    ctrl.inner_prec_tol_fillin=1e-5

    #
    # we need to call the constructor here
    # to complete the initialization
    #
    Dmkcontrols.dmkctrl_constructor(ctrl)

    return ctrl;

#
# preprocess topol to be compatible with
# fortran 
#
def init_fortran_topol(topol):
    if ( topol.shape[1] == 2 ):
        topolfortran=topol.transpose()
    else: 
        topolfortran=topol
    if ( np.amin(topolfortran) == 0 ):
        topolfortran=topolfortran+1
    topolfortran=np.sort(topolfortran,axis=0)

    return topolfortran;
        

def dmk_time_varying_forcing(topol,
                             forcing_function,
                             pflux=1.0,
                             weight=None,
                             ctrl=None,
                             tdens0=None,
                             pot0=None,
                             time_zero=0.0,
                             otptdens=None,
                             optpot=None):    

    ##################################################################
    # INITIALIZATION STARTS
    ##################################################################

    # set main dimension
    ntdens=len(topol)
    if (np.amin(topol) == 0 ):
        # 0-based numbering
        npot=np.amax(topol)+1
    elif ( np.amin(topol) == 1) :
        # 1-based numbering
        npot=np.amin(topol)
    else:
        print('Something is wrong woth topol')
        return;
        
    #
    # Init and set controls
    #
    if ( ctrl is None):
        ctrl=init_dmkctrl()
    work_ctrl = Dmkcontrols.DmkCtrl()
    Dmkcontrols.dmkctrl_copy(ctrl,work_ctrl)
        
    #
    # Init. discretization varaible
    #
    topolfortran=init_topol_fortran(topol)
    admk=Admkwrap4Python.admkwrap()
    Admkwrap4Python.graph_admk_constructor(admk, ctrl, ntdens, topolfortran, weight=weight)

    #
    # Init and set tdens/pot varaibles
    #
    # init tdens pot varaible
    tdpot=init_tdpot(ntdens,npot,npotentials=1,
                     t0=time_zero,tdens0=tdens0,
                     pot0=pot0,opttdens=opttdens,optpot=optpot)
        
    #
    # Init and set inputs
    # 
    dmkin=Dmkinputsdata.DmkInputs()
    Dmkinputsdata.dmkinputs_constructor(dmkin,0,ntdens,npot,True)
    if (time_zero is None):
        time_zero = 0.0
    dmkin.pflux=pflux


    #
    # init type for storing evolution/algorithm info
    #
    timefun=Timefunctionals.evolfun()
    Timefunctionals.evolfun_constructor(timefun, 0,
                                        ctrl.max_time_iterations,
                                        ctrl.max_nonlinear_iterations)

    ##################################################################
    # INITIALIZATION DONE
    ##################################################################

    
    ###################################################################
    # run main cycle
    ##################################################################

    # init flags for reverse communication
    flags=Admkwrap4Python.flags()
    flags.info=0
    flags.flag=1
    flags.flag_task=0

    # set initial time
    flags.current_time=time_zero

    # cycle
    while ( not flags.flag == 0 ):
        Admkwrap4Python.admk_cycle_reverse_communication(admk,
                                                         flags,
                                                         dmkin,
                                                         tdpot,
                                                         work_ctrl,
                                                         ctrl)
        flag=flags.flag
        current_time=flags.current_time
        
        if ( flag == 2 ):
            #            
            # Fill dmk inputs with data (rhs, kappa, beta, etc)
            # at current time
            #
            #print('Asked time', current_time,'ctrl.deltat',ctrl.deltat)
            dmkin.time=current_time
            if ( not dmkin.steady):
                steady = True
                # evalute funcition at time and node
                [dmkin.rhs, dmkin.steady] = forcing_function(current_time, npot)
                if (np.sum(dmkin.rhs) > 1.0e-12):
                     info=flags.info
                     tdens = np.array(tdpot.tdens)
                     pot   = np.array(tdpot.pot)
                     return info, tdens,pot;
                    
                
                
        elif( flag == 3 ):           
            # 
            # Right before new update when tdpot and inputs_data are syncronized.
            # The user can compute print any information regarding
            # the state of the system
            #
            #print('Wass=',np.dot(tdpot.pot,dmkin.rhs))
            if ( not( optpot is None  ) ) :
                root = np.unravel_index(np.argmin(optpot, axis=None), optpot.shape)
                temp=tdpot.pot+optpot
                temp=temp-tdpot.pot[root]
                print('err_pot',np.linalg.norm(temp)/np.linalg.norm(optpot))

            # store evolution system in timefun 
            Admkwrap4Python.store_evolution_info(admk, tdpot,dmkin,ctrl,timefun)
            

            
        elif( flag == 4 ):           
            # 
            # Optional: user-defined evaluation of steady state
            # User must set flag "user_system_variation=1"
            # in controls
            tdpot.system_variation=lin.linalg.norm(tdpot.pot)+lin.linalg.norm(tdpot.tdens)

        elif( flag == 5 ):           
            # 
            # Optional: user-defined set the controls for the next update
            # User must set flag "user_control_set=1"
            ctrl.deltat=1.05*ctrl.deltat
            print('new deltat', ctrl.deltat)
        elif( flag == 6 ):           
            # 
            # Optional: reset controls after update failure 
            # User must set flag "user_control_reset=1"
            ctrl.deltat=0.5*ctrl.deltat

    # get info
    info=flags.info
    tdens = np.array(tdpot.tdens)
    pot   = np.array(tdpot.pot)
    
    
    # free memory
    Tdenspotentialsystem.tdpotsys_destructor(tdpot, 0)
    Dmkinputsdata.dmkinputs_destructor(dmkin,0)
    Admkwrap4Python.admk_destructor(admk, 0)
    
    return info,tdens, pot, timefun;


def topol2incidence(topol):
    if (topol.shape[1]==2):
        topol=topol.transpose()
    ntdens=topol.shape[1]

    if (np.amin(topol) == 0 ):
        # 0-based numbering
        npot=np.amax(topol)+1
    elif ( np.amin(topol) == 1) :
        # 1-based numbering
        npot=np.amin(topol)
        npot      = npot
    print(ntdens,npot)
        
    indptr  = np.zeros([2*ntdens]).astype(int) # rows
    indices = np.zeros([2*ntdens]).astype(int) # columns
    data    = np.zeros([2*ntdens])                # nonzeros
    for i in range(ntdens):
        indptr[2*i:2*i+2]  = int(i)
        indices[2*i] = int(topol[0,i])
        indices[2*i+1] = int(topol[1,i])
        data[2*i:2*i+2]    = [1.0,-1.0]
        #print(topol[i,:],indptr[2*i:2*i+2],indices[2*i:2*i+2],data[2*i:2*i+2])
        signed_incidence_matrix = sp.csr_matrix((data, (indptr,indices)),shape=(ntdens, npot))

        
    return signed_incidence_matrix;
        
