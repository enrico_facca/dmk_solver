# Python program showing
# abstract base class work
 
from abc import ABC, abstractmethod
import time as cputiming
import copy

class Problem(ABC): 
    @abstractmethod
    def Syncronize(self,Unknows,Inputs, ctrls):
        return Unknows,info;

    @abstractmethod
    def Update(self,Unknows,Inputs, ctrls):
        return Unknows,info;

class CycleControls():
    def __init__(self,max_iterations=100,max_restart_update=10):
        # Order comunication flag 
        self.flag=0
        # State comunication flag
        self.info=0
        # Update counter
        self.iterations=0
        # maximum number of updates
        self.max_iterations=max_iterations
        # Cpu conter
        self.cpu_time=0
        # Restart counter
        self.nrestart_update=0
        # maximum number of restart
        self.max_restart_update=max_restart_update

        
        

def reverse_communication(flags,# cycle controls (inout)
                          admk, # problem        (inout)
                          tdpot,# unkwon         (inout)
                          dmkin, # inputs        (in) 
                          ctrl): # controls      (in)

    print('flags.flag=',flags.flag)
    if (flags.flag == 0):
        # syncronize
        start_time = cputiming.time()            
        [tdpot,info]=admk.Syncronize(tdpot,dmkin,ctrl)
        syncr_time=cputiming.time()-start_time
        flags.cpu_time+=syncr_time
        print('SYNCRONIZE AT TIME: CPU', '{:.2f}'.format(syncr_time))
        if ( info == 0):
            # study system
            flags.flag=3
            flags.info=0
        else:
            flags.flag=-2
            flags.info=0
            
        return flags, admk, tdpot;
    if (flags.flag == 2 ):
        # user has settled inputs and controls
        # now we  update
        flags.info=0
            
        # Update cycle. 
        # If it succees goes to the evaluation of
        # system varaition (flag ==4 ).
        # In case of failure, reset controls
        # and ask new inputs or ask the user to
        # reset controls (flag == 6 ).
        
        if (  flags.nrestart_update == 0):
            print(' ')
            print('UPDATE '+
                  str(flags.iterations))
            cpu_update=0.0
        else:
            print('UPDATE '+
                  str(flags.time_iterations+1)+
                  ' | TIME STEP = '+
                  str(flags.nrestart_update))
        #
        # update unknows
        #
        start_time = cputiming.time()
        [tdpot,info]=admk.Update(tdpot,dmkin,ctrl)    
        cpu_update += cputiming.time()-start_time
        
        #
        # check for successfulls update
        #
        if ( info == 0):
            # info for succesfull update
            flags.iterations+=1
            flags.cpu_time+=cputiming.time()-start_time
            if ( flags.nrestart_update == 0):
                print ('UPDATE SUCCEED CPU =','{:.2f}'.format(cpu_update))
            else:
                print('UPDATE SUCCEED '+
                      str(flags.nrestart_update) +
                      ' RESTARTS CPU =','{:.2f}'.format(cpu_update))
            print(' ')
            #
            # Ask to the user to evalute if stop cycling 
            # 
            # 
            flags.flag       = 4 
            flags.info       = 0 
        else:
            #
            # update failed
            #
            print('UPDATE FAILURE')
            
            # try one restart more
            flags.nrestart_update += 1
            
            # stop if number max restart update is passed 
            if ( flags.nrestart_update >= flags.max_restart_update):
                flags.flag       = -1  # breaking cycle
                flags.info       = -1 
            else:
                # ask the user to reset controls and inputs
                flags.flag =  2
                flags.info = -1
        return flags, admk, tdpot;
       
    if (flags.flag ==3):
        print('ITER: ', flags.iterations,
              ' CPU: ', '{:.2f}'.format(flags.cpu_time),
        )
        # check if maximum number iterations was achieded 
        if ( flags.iterations >= flags.max_iterations):
            flags.flag = -1
            flags.info = -1
            print(' Update Number exceed limits'+
                  str(flags.max_iterations))
            # break cycle
            return flags,admk,tdpot;

        # New update is required thus
        # ask for new controls
        tdpot.nrestart_update = 0 # we reset the count of restart
        flags.flag = 2
        flags.info = 0
        return flags,admk, tdpot;

    if (flags.flag == 4):
        # ask user new inputs in order to update 
        flags.flag = 3
        flags.info = 0
        return flags, admk, tdpot;
    
class Unknows:
    def __init__(self,x0):
        self.x=x0

class Inputs:
    def __init__(self,a):
        self.a=a

class ctrls:
    def __init__(seUnknows.xlf,step0,max_iterations=100,max_restart_update=10):
        self.step=step0
        self.max_restart_update=max_restart_update
        self.max_iterations=max_iterations
        
class ParabolaMinimum(Problem): 
    def Syncronize(self,Unknows,Inputs, ctrls):
        info=0
        return Unknows,info;

    def Update(self,Unknows,Inputs, ctrls):
        Unknows.x=Unknows.x-ctrls.step*Inputs.a*Unknows.x
        info=0
        return Unknows,info;
        
# init solution container and copy it
sol=Unknows(1)
sol_old=copy.deepcopy(sol)

print(sol_old.x)

# init inputs data
data=Inputs(0.5)

# init problem controls
ctrl=ctrls(0.1)

# init problems
simple_parabola=ParabolaMinimum()

# init update cycle controls
flags=CycleControls()

while(flags.flag >=0):
    # call reverse communication
    flags, simple_parabola, sol=reverse_communication(flags, simple_parabola, sol, data, ctrl)

    ###################################################
    # select action according to flag.flag and flag.info
    ######################################################

    # set inputs and controls before update 
    if (flags.flag == 2) and (flags.info==0) :
        print('time to fix inputs and controls for next update')

        # copy data before update
        sol_old.x=copy.deepcopy(sol.x)

    ###################################################
    # set inputs and reset controls due to failure
    ###################################################
    if (flags.flag == 2) and not (flags.info==0) :
        print('time to reset controls')

    #########################################################
    # Do what ever you want of with the data but not touch it
    ########################################################
    if ( flags.flag == 3):
        print('time study the system')
        print('sol=', sol.x,'energy=',data.a*sol.x**2/2.0)
        print('iter=',flags.iterations)

    #########################################################
    # Define you stop criteria and set flags.flag=-1 to exit from the
    # cycle. Set flags.info=0 if you reached your scope. 
    ########################################################
    if ( flags.flag == 4):
        # evalute system variation
        var=abs(sol.x-sol_old.x)/ctrl.step
        print( 'var=',var,'old=',sol_old.x,'new',sol.x)
        # break cycle if convergence is achieved 
        if ( var < 1e-3):
            flags.flag=-1
            flags.info=0
        
print('final x=',sol.x)
        
        
        
        
        



