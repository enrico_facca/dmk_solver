#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#setup 
import sys
import numpy as np
import scipy
import matplotlib.pyplot as plt
import networkx as nx
sys.path.append('../../')
import admk_graph 
# Import I/O for timedata
try:
    sys.path.append('../../../../../globals/python/timedata/')
    import timedata as td
except:
    print("Global repo non found")

# Import geometry tools
sys.path.append('../../../../../geometry/python/')
import meshtools as mt

# Import class (type) for setting controls
sys.path.append('../../../../build/python/fortran_python_interface/')
from dmk import (Dmkcontrols,  # controls for dmk simulations
                 Timefunctionals # information of time/algorithm evolution)
                )


# Load data (graph $\mathcal{G}$, forcing $b=b^{+}-b^{-}$ , optimal potential)

# In[ ]:


# read graph and coordinate graphs
inputs_folder='../inputs/'
test=sys.argv[1]
coord,topol,flags = mt.read_grid(inputs_folder+'eikonal_'+test+'/grid_'+test+'_graph.dat')

# read rhs
rhs=td.read_steady_timedata(inputs_folder+'eikonal_'+test+'/eik_'+test+'_rhs.dat').flatten()
isource=np.argmax(rhs)
print('Source node',isource, 'located at:', coord[isource,:])

# read weight
weigth=td.read_steady_timedata(inputs_folder+'eikonal_'+test+'/weight_'+test+'.dat').flatten()

# read optimal solution 
optpot=td.read_steady_timedata(inputs_folder+'eikonal_'+test+'/eik_'+test+'_optpot.dat').flatten()


# In[ ]:


# init and set controls
ctrl = Dmkcontrols.DmkCtrl()
Dmkcontrols.get_from_file(ctrl,'dmk.ctrl')
# if and where save data
ctrl.id_save_dat=1
ctrl.fn_tdens='tdens.dat'
ctrl.fn_pot='pot.dat'
ctrl.fn_statistics='dmk.log'

[info,tdens,pot,timefun] = admk_graph.pyadmk_graph(topol,rhs,1.0,1e-10,weigth,ctrl,optpot=optpot)
if (info==0):
    print('Convergence achieved')

exit()

# In[ ]:


# plot convergence toward steady state
time      = np.array(timefun.time[0:timefun.last_time_iteration]);
cpu_time  = np.array(timefun.cpu_time[0:timefun.last_time_iteration])
var_tdens = np.array(timefun.var_tdens[1:timefun.last_time_iteration])
err_pot   = np.array(timefun.err_pot[0:timefun.last_time_iteration])
plt.figure(1)
plt.subplot(211)
plt.yscale('log')
plt.ylabel('var($\mu$)')
plt.xlabel('time (t)')
plt.grid(True)
plt.plot(time[1:], var_tdens, 'bo--')

plt.subplot(212)
plt.yscale('log')
plt.ylabel('err($u$)')
plt.xlabel('time (t)')
plt.grid(True)
plt.plot(time, err_pot, 'ro--')
plt.show()

plt.figure(2)
plt.subplot(211)
plt.yscale('log')
plt.ylabel('var($\mu$)')
plt.xlabel('CPU time (s)')
plt.grid(True)
plt.plot(cpu_time[1:], var_tdens, 'bo--')

plt.subplot(212)
plt.yscale('log')
plt.ylabel('err($pot$)')
plt.xlabel('CPU time (s)')
plt.grid(True)
plt.plot(cpu_time, err_pot, 'ro--')
plt.show()


# In[ ]:




