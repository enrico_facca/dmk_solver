#!/usr/bin/env python
# coding: utf-8

# In[1]:


#setup 
import sys
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
sys.path.append('../../')
import dmk_graph 
# Import I/O for timedata
try:
    sys.path.append('../../../../../globals/python/timedata/')
    import timedata as td
except:
    print("Global repo non found")

# Import geometry tools
sys.path.append('../../../../../geometry/python/')
import meshtools as mt

# Import class (type) for setting controls
sys.path.append('../../../../build/python/fortran_python_interface/')
from dmk import (Dmkcontrols,  # controls for dmk simulations
                 Timefunctionals, # information of time/algorithm evolution)
                Admkwrap4Python)


# Load data (graph $\mathcal{G}$, forcing $b=b^{+}-b^{-}$ , optimal potential)

# read graph and coordinate graphs
coord,topol,flags = mt.read_grid('grid_0032_graph.dat')
ntdens=len(topol)
npot=np.amax(topol)+1

print('|Nodes|=',npot,' |Edges|=',ntdens)

weight=np.zeros(ntdens)
for i in range(ntdens):
    weight[i]=np.linalg.norm(coord[topol[i,1]]-coord[topol[i,0]])



# init and set controls
ctrl = Dmkcontrols.DmkCtrl()
Dmkcontrols.get_from_file(ctrl,'dmk.ctrl')
# if and where save data
ctrl.id_save_dat=1
ctrl.fn_tdens='tdens.dat'
ctrl.fn_pot='pot.dat'
# if and where save log
ctrl.id_save_statistics=1
ctrl.fn_statistics='dmk.log'
ctrl.tolerance_system_variation=1e-5
# if print info 
# 


#
# set forcing term
#

#forcing_function=dmk_graph.forcing_test;
forcing_function=dmk_graph.steady_forcing_test;


print(min(weight),max(weight))
[info,tdens,pot,_] = dmk_graph.dmk_time_varying_forcing(topol,
                                                      forcing_function,
                                                      pflux=1.5,
                                                      weight=weight, ctrl=ctrl)

#if (info==0):
#    print('Convergence achievied')
[rhs,steady]=dmk_graph.steady_forcing_test(0.0,npot)
print(npot)


ctrl.fn_tdens='tdens_bis.dat'
ctrl.fn_pot='pot_bis.dat'
ctrl.fn_statistics='dmk_bis.log'
[info,tdens,pot,_,_] =  dmk_graph.dmk_graph(topol,rhs,pflux=1.5,
              tolerance=1e-5,
              weight=weight,
              ctrl=ctrl) 




