#setup 
import numpy as np
from scipy import sparse as sp
from scipy import linalg
import time as cputiming
import sys
import os
#
# import fortran_pyhton_interface library
# (assuming default compilation in build/)
#
current_source_dir=os.path.dirname(os.path.realpath(__file__))
relative_libpath='../../build/python/fortran_python_interface/'
dmk_lib_path=os.path.abspath(os.path.normpath(current_source_dir+'/'+relative_libpath))
try:
    sys.path.append(dmk_lib_path)
    from dmk import (
        Dmkcontrols,          # controls for dmk simulations
        Dmkinputsdata,        # inputs data ( rhs, kappas,beta, etc) 
        Tdenspotentialsystem, # output result tdens, pot
        dmkgraph_steady_data,  # main interface subroutine
        Timefunctionals, # information of time/algorithm evolution)
        Admkwrap4Python, # alebraig dmk model
    )
except:
    print("Compile with:")
    print("mkdir build; cd build; cmake f2py_interface_dmk../; make ")

# import timedata
relative_libpath='../../../globals/python/timedata/'
globals_lib_path=os.path.abspath(os.path.normpath(current_source_dir+'/'+relative_libpath))
try:
    sys.path.append(globals_lib_path)
    import timedata as td
except:
    print("NOT FOUND",globals_lib_path)

#from PyTrilinos import Epetra

# init
#Epetra.Init()
#Comm = Epetra.PyComm()
#Map = Epetra.Map(4, 0, Comm)
"""
class admk_trilinos:   
    def __init__(self, topol, weight=np.ones(len(topol)),Map):
        self.topol   = topol
        self.weight  = weight
        self.invweight  = 1.0/weight
        self.matrixAT = Epetra.CrsMatrix(Epetra.Copy,Map,3)
        NumLocalRows = Map.NumMyElements()
        for ii in range(0, NumLocalRows):
            i = Map.GID(ii)
                Indices = [topol[0,i], topol[1,i]]
                Values = [1.0, -1.0]
                self.matrixAT.InsertGlobalValues(i, Values, Indices)
        ierr = self.matrixAT.FillComplete()
        #ierr = Epetra.CrsMatrix.SetUseTranspose(True,self.matrixA)

    def build_stiff(self,tdens):
        tdens=tdens*self.invweight;
        for ii in range(0, NumLocalRows):
            i = Map.GID(ii)
                Indices = [topol[0,i], topol[1,i]]
                Values = [1.0, -1.0]
                self.matrixAT.InsertGlobalValues(i, Values, Indices)
        
        

def syncronize(self,tdpot, dmkin,ctrl,info):

def update(self,tdpot, dmkin,ctrl,info):
"""

class info_linalg:
    def __init__(self):
        self.info = 0
        self.iter = 0
        self.resini =0.0
        self.realres=0.0
    def __str__(self):
        strout=(str(self.info)+' '+
                str(self.iter)+' '+
                str('{:.2E}'.format(self.resini))+' '
                +str('{:.2E}'.format(self.realres)))
        return strout;
    def addone(self, xk):
        self.iter += 1

class pyadmk:   
    def __init__(self, topol, weight=None):
        self.ntdens    = len(topol)
        if (np.amin(topol) == 0 ):
            # 0-based numbering
            npot=np.amax(topol)+1
        elif ( np.amin(topol) == 1) :
            # 1-based numbering
            npot=np.amin(topol)
        self.npot      = npot
        
        self.topol     = topol
        if (weight is None):
            weight=np.ones(len(topol))
        self.weight    = weight
        self.invweight = 1.0/weight
        indptr  = np.zeros([2*self.ntdens]).astype(int) # rows
        indices = np.zeros([2*self.ntdens]).astype(int) # columns
        data    = np.zeros([2*self.ntdens])                # nonzeros
        for i in range(self.ntdens):
            indptr[2*i:2*i+2]  = int(i)
            indices[2*i] = int(topol[i,0])
            indices[2*i+1] = int(topol[i,1])
            data[2*i:2*i+2]    = [1.0,-1.0]
            #print(topol[i,:],indptr[2*i:2*i+2],indices[2*i:2*i+2],data[2*i:2*i+2])
        self.matrixAT = sp.csr_matrix((data, (indptr,indices)),shape=(self.ntdens, self.npot))
        self.matrixA  = sp.csr_matrix.transpose(self.matrixAT)

    def build_stiff(self,tdens):
        tdens_w=tdens*self.invweight
        diagt=sp.diags(tdens_w)
        temp=np.dot(diagt,self.matrixAT)
        stiff=np.dot(self.matrixA,temp)#*diagt*self.matrixAT
        return stiff;
        

    def syncronize(self,tdpot, dmkin,ctrl,info):
        #assembly stiff
        print('{:.2E}'.format(min(tdpot.tdens))+'<=TDENS<='+'{:.2E}'.format(max(tdpot.tdens)))
        start_time = cputiming.time() 
        stiff=self.build_stiff(tdpot.tdens)
        print('ASSEMBLY'+'{:.2f}'.format(-(start_time - cputiming.time()))) 
        rhs=dmkin.rhs.copy()
        
        
        #
        # solve linear system
        #

        # init counter
        info_solver = info_linalg()
        info_solver.resini=linalg.norm(stiff*tdpot.pot-dmkin.rhs)/linalg.norm(dmkin.rhs)
        
        # ground solution
        inode=np.argmax(abs(dmkin.rhs))
        grounding=True
        #grounding=False
        if (grounding):
            stiff[inode,inode]=1.0e20
            rhs[inode]=0.0
        else:
            stiff=stiff+1e-12*sp.identity(tdpot.npot)
        

        scaling=True
        #scaling =False
        if ( scaling ):
            d=stiff.diagonal()
            diag_sqrt=sp.diags(1.0/np.sqrt(d))

            matrix2solve  = diag_sqrt*(stiff*diag_sqrt)
            rhs2solve     = diag_sqrt*rhs
            x0            = diag_sqrt*tdpot.pot
        else:
            matrix2solve = stiff
            rhs2solve    = rhs
            x0           = tdpot.pot

        start_time = cputiming.time()
        ilu = sp.linalg.spilu(matrix2solve,
                       drop_tol=ctrl.outer_prec_tol_fillin,
                       fill_factor=ctrl.outer_prec_n_fillin)
        
        print('ILU'+'{:.2f}'.format(-(start_time - cputiming.time()))) 
        #print(ilu)
        prec = lambda x: ilu.solve(x)
        M = sp.linalg.LinearOperator((tdpot.npot,tdpot.npot), prec)

        
        
        # solve linear system
        #print(info_solver)
        start_time = cputiming.time()
        [pot,info_solver.info]=sp.linalg.bicgstab(matrix2solve, rhs2solve, x0=x0,
                                                  tol=ctrl.tolerance_nonlinear, #restart=20,         
                                                  maxiter=ctrl.outer_imax,atol=1e-16,
                                                  M=M,
                                                  callback=info_solver.addone)


        
        if (scaling) :
            pot=diag_sqrt*pot
            rhs2solve=np.sqrt(d)*rhs2solve
        print('LINSOL'+'{:.2f}'.format(-(start_time - cputiming.time())))

        tdpot.pot=pot.copy()
        # compute res residuum
        #stiff=self.build_stiff(tdpot.tdens)
        info_solver.realres=linalg.norm(matrix2solve*tdpot.pot-rhs2solve)/linalg.norm(rhs2solve)
        print(info_solver)
        
        if (info_solver.info !=0 ) :
            info=1
        
        return tdpot, info;
    
    
    def update(self,tdpot, dmkin,ctrl,info):
        # compute update
        grad=self.invweight * (self.matrixAT*tdpot.pot)
        flux=tdpot.tdens*grad
        print('{:.2E}'.format(min(tdpot.tdens))+'<=TDENS<='+'{:.2E}'.format(max(tdpot.tdens)))
        print('{:.2E}'.format(min(tdpot.pot))+'<=POT  <='+'{:.2E}'.format(max(tdpot.pot)))
        print('{:.2E}'.format(min(grad))+'<=GRAD <='+'{:.2E}'.format(max(grad)))
        
        res=self.matrixA*flux-dmkin.rhs
        print(linalg.norm(res))
        #print('{:.2E}'.format(min(normgrad))+'<=GRAD<='+'{:.2E}'.format(max(normgrad)))
        pflux=dmkin.pflux
        pode =dmkin.pode
        pmass=dmkin.pmass
        update=-(tdpot.tdens**pflux) * (grad ** pode) + tdpot.tdens**pmass

        # update tdens
        tdpot.tdens = tdpot.tdens - ctrl.deltat * update

        [tdpot,info] = self.syncronize(tdpot, dmkin,ctrl,info)

        if ( info == 0) :
            tdpot.deltat=ctrl.deltat
            tdpot.time_old = tdpot.time
            tdpot.time=tdpot.time+ctrl.deltat
            tdpot.iter_nonlinear = 1
            tdpot.time_iteration = tdpot.time_iteration + 1
            tdpot.time_update    = tdpot.time_update + 1
    
        return tdpot, info;
    

        
def pyadmk_reverse_communication(admk,
                                 flags,
                                 dmkin,
                                 tdpot,
                                 work_ctrl,
                                 ctrl):

    #print(flags.flag,flags.flag_task,flags.info,tdpot.time_update)
    info=flags.info
    
    if (flags.flag ==1):
        # return and read inputs at time zero
        flags.flag=2
        flags.flag_task=1
        flags.input_time = tdpot.time
        tdpot.first = True
        return  admk, flags, dmkin, tdpot, work_ctrl, ctrl;
    if (flags.flag ==2):
        if (flags.flag_task==1):
            # syncronize
            start_time = cputiming.time()            
            [tdpot,info]=admk.syncronize(tdpot,dmkin,ctrl,info)
            syncr_time=cputiming.time()-start_time
            tdpot.cpu_time+=syncr_time
            print('SYNCRONIZE AT TIME: ','{:.2E}'.format(tdpot.time), 'CPU', '{:.2f}'.format(syncr_time))
            if ( info == 0):
                if ( tdpot.time_iteration > 0 ):
                    # ask to measure variations
                    flags.flag=4
                    flags.info=0
                else:
                    # study system
                    flags.flag=3
                    flags.info=0
            else:
                flags.flag=-2
                flags.info=0

            return admk, flags, dmkin, tdpot, work_ctrl, ctrl;

        if (flags.flag_task==2):
            # update
            flags.info=0
            
            # Update cycle. 
            # If it succees goes to the evaluation of
            # system varaition (flag ==4 ).
            # In case of failure, reset controls
            # and ask new inputs or ask the user to
            # reset controls (flag == 6 ).
            
            
            #
            #  update member pot, tdens, gfvar of type tdpotsys
            #
            tdpot.tdens_old = tdpot.tdens
            tdpot.gfvar_old = tdpot.gfvar
            tdpot.pot_old   = tdpot.pot
            tdpot.time_old  = tdpot.time

            if (  tdpot.nrestart_update == 0):
                print(' ')
                print('UPDATE '+
                      str(tdpot.time_iteration+1)+
                      ' | TIME STEP = '+
                      str(ctrl.deltat))
                cpu_update=0.0
            else:
                print('UPDATE '+
                      str(tdpot.time_iteration+1)+
                      ' | TIME STEP = '+
                      str( ctrl.deltat)+
                      ' RESTART '+
                      str(tdpot.nrestart_update))
            #
            # update tdpot
            #
            start_time = cputiming.time()
            [tdpot,info]=admk.update(tdpot,dmkin,ctrl,info)    
            cpu_update += cputiming.time()-start_time
            
            #
            #
            #
            if ( info == 0):
                # info for succesfull update
                tdpot.cpu_time+=cputiming.time()-start_time
                if ( tdpot.nrestart_update == 0):
                   print ('UPDATE SUCCEED CPU =','{:.2f}'.format(cpu_update))
                else:
                    print('UPDATE SUCCEED '+
                          str(tdpot%nrestart_update) +
                          ' RESTARTS CPU =','{:.2f}'.format(cpu_update))
                print(' ')

                #
                # syncronized tdpot and dmkin
                # For example explicit Euler use the Inputs
                # at t^{k} while at this point tdens-pot are 
                # at t^{k+1}
                #
                if ( (not dmkin.steady) and
                     (abs(tdpot.time-dmkin.time) > 1e-13) ):
                    flags.flag       = 2          # asking for inputs
                    flags.input_time = tdpot.time # syncronized at tdpot%time
                    flags.flag_task  = 1          # syncronize
                    flags.info       = 0 
                   
                    #out_format=ctrl%formatting('arra')
                    print('NOT STEADY INPUTS and (TDPOT TIME),(INPUT TIME) - SYNCRONIZATE')
                   
                    # return to user 
                    return admk, flags, dmkin, tdpot, work_ctrl, ctrl;
                else:
                    #
                    # Evalutate variation of tdens-potential system
                    # with type procedure compute_tdpot_variation
                    # or ask the user to compute it sending flag = 4.
                    #
                    flags.flag       = 4          # asking for inputs
                    flags.input_time = tdpot.time # syncronized at tdpot%time
                    flags.flag_task  = 1          # syncronize
                    flags.info       = 0 
                    return admk, flags, dmkin, tdpot, work_ctrl, ctrl;
            else:
                #
                # update failed
                #
                print('UPDATE FAILURE')

                # try one restart more
                tdpot.nrestart_update = tdpot.nrestart_update + 1

                # stop if number max restart update is passed 
                if (tdpot.nrestart_update >= ctrl.max_restart_update):
                   flags.flag       = -2  # breaking cycle
                   flags.info       = -1 
                else:
                    # ask the user to reset controls
                    flags.flag =  6
                    flags.info = -1

                # restores previous data
                tdpot.tdens = tdpot.tdens_old 
                tdpot.gfvar = tdpot.gfvar_old 
                tdpot.pot   = tdpot.pot_old
                tdpot.time  = tdpot.time_old

                # break cycle
                return admk, flags, dmkin, tdpot, work_ctrl, ctrl;
           
    if (flags.flag ==3):
        if (tdpot.time_update == 0 ):
            print('ITER: ', tdpot.time_iteration,
                  ' TIME: ', '{:.2E}'.format(tdpot.time),
                  ' CPU: ', '{:.2f}'.format(tdpot.cpu_time),
            )
        else:
            print('ITER: ' , tdpot.time_iteration,
                  ' TIME: ', '{:.2E}'.format(tdpot.time),
                  ' CPU: ' , '{:.2f}'.format(tdpot.cpu_time),
                  ' VAR: ' , '{:.2E}'.format(tdpot.system_variation))
        
        
        # check if maximum number iterations was achieded 
        if ( tdpot.time_update >= ctrl.max_time_iterations):
            flags.flag = 0
            flags.info = -1
            print(' Update Number exceed limits'+
                  str(ctrl.max_time_iterations))
            # break cycle
            return admk, flags, dmkin, tdpot, work_ctrl, ctrl;

        # check convergence
        if (tdpot.time_update > 0 ):
            # check if convergence was achieved
            if ( tdpot.system_variation <= ctrl.tolerance_system_variation):
                # convergence achieved. Set flag for exit
                flags.flag = 0
                flags.info = 0
                return admk, flags, dmkin, tdpot, work_ctrl, ctrl;
            
        # convergence not achieved or no udpdate was performed
        # New update is required thus
        # ask for new controls
        tdpot.nrestart_update = 0
        flags.flag = 5
        flags.info = 0
        return admk, flags, dmkin, tdpot, work_ctrl, ctrl;
             
             
    if (flags.flag ==4):
        # var system
        flags.flag=3
        return admk, flags, dmkin, tdpot, work_ctrl, ctrl;

    if (flags.flag ==5):
        # new contry ols are settled
        # ask user new inputs
        # order to update
        flags.flag=2
        flags.flag_task=2
        return admk, flags, dmkin, tdpot, work_ctrl, ctrl;

    if (flags.flag ==6):
        # reset controls after failures
        # ask for inputs
        flags.flag=2
        return admk, flags, dmkin, tdpot, work_ctrl, ctrl;

    
def pyadmk_graph(topol,rhs,
                 pflux=1.0,
                 tolerance=1e-5,
                 weight=None,
                 ctrl=None,
                 time_zero=0.0,
                 tdens0=None,
                 pot0=None,
                 opttdens=None,
                 optpot=None):

    # Dimensions
    ntdens=len(topol)
    npot=len(rhs)

    # init tdens pot varaible
    tdpot=init_tdpot(ntdens,npot,npotentials=1,
                     t0=0.0,tdens0=tdens0,pot0=pot0,
                     opttdens=opttdens,optpot=optpot)
    

    # set inputs
    dmkin=Dmkinputsdata.DmkInputs()
    Dmkinputsdata.dmkinputs_constructor(dmkin,0,ntdens,npot,True)
    dmkin.steady= True
    dmkin.rhs   = rhs
    dmkin.pflux = pflux
    if ( not ( optpot is None) ) :
        Dmkinputsdata.set_optimal_pot(dmkin,0,optpot)
    if ( not ( opttdens is None) ) :
        Dmkinputsdata.set_optimal_tdens(dmkin,0,opttdens)
    
    if (weight is None):
        weight = np.ones(ntdens)
    
    #
    # set controls
    #
    if ( ctrl is None):
        ctrl=init_dmkctrl()
        ctrl.tolerance_system_variation=tolerance
    work_ctrl = Dmkcontrols.DmkCtrl()
    Dmkcontrols.dmkctrl_copy(ctrl,work_ctrl)
    
    


    #
    # init type for storing evolution/algorithm info
    #
    timefun=Timefunctionals.evolfun()
    Timefunctionals.evolfun_constructor(timefun, 0,
                                        ctrl.max_time_iterations,
                                        ctrl.max_nonlinear_iterations)

    #
    # init admk container
    #
    admk=pyadmk(topol, weight=weight)
    
    #
    # solve optimal transport problem on graphs
    #
    info=np.zeros(1,dtype=np.int8)


    # init flags for reverse communication
    flags=Admkwrap4Python.flags()
    flags.info=0
    flags.flag=1
    flags.flag_task=0

    # set initial time
    flags.current_time=time_zero

    # cycle
    while ( not flags.flag == 0 ):
        pyadmk_reverse_communication(admk,
                                     flags,
                                     dmkin,
                                     tdpot,
                                     work_ctrl,
                                     ctrl)
        flag=flags.flag
        if (flag <0):
            break
        current_time=flags.current_time
        
        if ( flag == 2 ):
            #            
            # Fill dmk inputs with data (rhs, kappa, beta, etc)
            # at current time
            #
            #print('Asked time', current_time,'ctrl.deltat',ctrl.deltat)
            dmkin.time=current_time     
                
        elif( flag == 3 ):           
            # 
            # Right before new update when tdpot and inputs_data are syncronized.
            # The user can compute print any information regarding
            # the state of the system
            #
            filename='pot'+str(tdpot.time_iteration)+'.dat'
            td.write_steady_timedata(filename,tdpot.pot)
            filename='tdens'+str(tdpot.time_iteration)+'.dat'
            td.write_steady_timedata(filename,tdpot.tdens)
            if ( not( optpot is None  ) ) :
                root = np.unravel_index(np.argmin(optpot, axis=None), optpot.shape)
                temp=tdpot.pot+optpot
                #temp=temp-tdpot.pot[root]
                print('err_pot',np.linalg.norm(temp)/np.linalg.norm(optpot))
            
        elif( flag == 4 ):           
            # 
            # Optional: user-defined evaluation of steady state
            # User must set flag "user_system_variation=1"
            # in controls
            tdpot.system_variation=linalg.norm(tdpot.tdens-tdpot.tdens_old)/(
                ctrl.deltat*linalg.norm(tdpot.tdens_old))

        elif( flag == 5 ):           
            # 
            # Optional: user-defined set the controls for the next update
            # User must set flag "user_control_set=1"
            if ( ctrl.deltat_control == 1 ) :
                ctrl.deltat=ctrl.deltat
            elif ( ctrl.deltat_control == 2 ) :
                ctrl.deltat=min(max(ctrl.deltat_lower_bound,ctrl.deltat_expansion_rate*ctrl.deltat),ctrl.deltat_upper_bound)
            else:
                print('Not supported')
        elif( flag == 6 ):           
            # 
            # Optional: reset controls after update failure 
            # User must set flag "user_control_reset=1"
            ctrl.deltat=0.5*ctrl.deltat

    # get info
    info=flags.info
    tdens= tdpot.tdens
    pot  = tdpot.pot

    # free memory
    Tdenspotentialsystem.tdpotsys_destructor(tdpot, 0)
    Dmkinputsdata.dmkinputs_destructor(dmkin,0)

    
    
    return info, tdens, pot, timefun;


def forcing_test(time,npot):
    values=np.zeros(npot)
    values[0]=np.cos(10*time)
    values[100]=np.cos(10*time)
    values[200]=-2*np.cos(10*time)
    steady=False
    return values, steady;

def steady_forcing_test(time,npot):
    values=np.zeros([npot])
    values[0]=1
    values[100]=1
    values[200]=-4
    values[300]=1
    values[400]=1
    steady=True
    return values, steady;

#
# Init tdens pot container
#
def init_tdpot(ntdens,npot,npotentials=1,t0=0,tdens0=None,pot0=None):
    tdpot=Tdenspotentialsystem.tdpotsys()
    Tdenspotentialsystem.tdpotsys_constructor(tdpot,0,ntdens, npot,npotentials)
    if ( tdens0 is None):
        tdpot.tdens[:]=1.0
    else:
        print(tdens0[0])
        if (min(tdens0)<=0.0):
            print('Initial tdens must have positive entries')
            exit()
        tdpot.tdens=tdens0
        if (min(tdpot.tdens)<=0.0):
            print('Initial tdpo.tdens must have positive entries')
            exit()
            
    if ( pot0 is None):
        tdpot.pot[:]=0.0
    else:
        tdpot.pot=pot0

    tdpot.time=t0
    
    
    return tdpot;

#
# Initilized controls. Pass file path
# with controls varaibles or used default
# varaibles
#
def init_dmkctrl(filename=None):
    
    #
    # Set Default controls
    #

    # init set controls
    ctrl = Dmkcontrols.DmkCtrl()

    
    # time evolution controls
    ctrl.max_time_iterations=100
    ctrl.max_nonlinear_iterations=20
    ctrl.tolerance_system_variation=1e-04
    ctrl.tol_nonlinear=1e-08
    ctrl.max_restart_update=4

    # time step size controls
    ctrl.deltat = 1
    ctrl.deltat_control=3
    ctrl.deltat_expansion_rate=2
    ctrl.deltat_lower_bound=0.01
    ctrl.deltat_upper_bound=1000

    # time stepping scheme controls
    ctrl.time_discretization_scheme=1
    ctrl.epsilon_w22=1e-8


    # globals controls
    ctrl.min_tdens = 1e-13
    ctrl.selection=0
    ctrl.threshold_tdens=1e-10

    # Info 
    ctrl.debug=2
    ctrl.info_state=2
    ctrl.info_update=3

    # Data saving
    ctrl.id_save_dat=0
  

    # Linear solver ctrl
    ctrl.outer_solver_approach='ITERATIVE'
    ctrl.outer_krylov_scheme='BICGSTAB'
    ctrl.outer_imax=100
    ctrl.outer_tolerance=1e-12
    ctrl.outer_isol=1
    ctrl.outer_iort=1
    
    ctrl.outer_prec_type='IC'
    ctrl.outer_prec_n_fillin=20
    ctrl.outer_prec_tol_fillin=1e-3
    ctrl.relax4prec=1e-09
    ctrl.relax_direct=1e-12
   

    ctrl.inner_solver_approach='ITERATIVE'
    ctrl.inner_krylov_scheme='PCG'
    ctrl.inner_imax=100
    ctrl.inner_tolerance=1e-12
    
    ctrl.inner_prec_type='IC'
    ctrl.inner_prec_n_fillin=30
    ctrl.inner_prec_tol_fillin=1e-5

    #
    # we need to call the constructor here
    # to complete the initialization
    #
    Dmkcontrols.dmkctrl_constructor(ctrl)

    return ctrl;

#
# preprocess topol to be compatible with
# fortran 
#
def init_fortran_topol(topol):
    if ( topol.shape[1] == 2 ):
        topolfortran=topol.transpose()
    else: 
        topolfortran=topol
    if ( np.amin(topolfortran) == 0 ):
        topolfortran=topolfortran+1
    topolfortran=np.sort(topolfortran,axis=0)

    return topolfortran;
        

