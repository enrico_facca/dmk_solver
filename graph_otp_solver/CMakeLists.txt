add_executable (dmk_graph_from_file${EXECUTABLE_EXTENSION} srcs/main.f90)
target_link_libraries( dmk_graph_from_file${EXECUTABLE_EXTENSION} dmk linalg agmg arpack hsl lapack blas)
