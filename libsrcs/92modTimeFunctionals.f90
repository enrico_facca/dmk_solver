module TimeFunctionals
  use Globals
  implicit none
  private
  public :: evolfun_constructor, evolfun_destructor
  !>-------------------------------------------------------------
  !> Structure collecting the "time" evolution of
  !> integer and real quantities of dmk dynamics.
  !> It can be used to store evolution of problem
  !> depending varaibles (e.g. the Wasserstein distance
  !> for the Optimal Transport Solver) of
  !> algorithm information (CPU timings, iterations number,etc.)
  !>-------------------------------------------------------------
  type, public :: evolfun
     !> Number of time iterations
     integer :: max_time_iterations=0
     !> Max Number of non linaera iterations 
     integer :: max_iter_nonlinear=0
     !> Last iterations stored
     integer :: last_time_iteration=0
     !> Time sequence
     real(kind=double), allocatable :: time(:)
     !> Variation system
     real(kind=double), allocatable :: system_variation(:)
     !>---------------------------------------------------------
     !> Integer functionals
     !>---------------------------------------------------------
     !> Number of iterations for iterative linear solver
     !> Dimension(max_iter_nonlinear,0:max_time_iterations)
     integer, allocatable :: iter_solver(:,:)
     !> Total number of iterations for iterative solvers
     !> Dimension(0:max_time_iterations)
     integer, allocatable :: iter_total(:)
     !> Average number of iterations for iterativelinear solver
     !> for each time step
     !> Dimension(0:max_time_iterations)
     integer, allocatable :: iter_media(:)
     !>---------------------------------------------------------
     !> Number of fix point iterations
     !> if a non linear equation need to be solved
     !> Dimension(0:max_time_iterations)
     integer, allocatable :: iter_nonlinear(:)
     !>---------------------------------------------------------
     !> Number of fill-in used
     !> Dimension(0:max_time_iterations)
     integer, allocatable :: nfillin(:)
     !>---------------------------------------------------------
     !> Flag 0-1 if Preconditioner is calculated
     !> Dimension(0:max_time_iterations)
     integer, allocatable :: prec_calc(:,:)
     !> Flag 0-1-2 if prec. tuning via spectral info is calculated
     !> 0=not computed
     !> 1=Computed with DACG
     !> 2=Computed with LANCZOS
     !> Dimension(0:max_time_iterations)
     integer, allocatable :: tuning_calc(:,:)
     !> Number of restart of newton iteration
     !> Dimension(0:max_time_iterations)
     integer, allocatable :: nrestart_newton(:)
     !>---------------------------------------------------------
     !> Real valued functionals
     !>---------------------------------------------------------
     !> Timestep collection
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: deltat(:)
     !> Variation of the tdens-pot system
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable ::  variation_system(:)
     !> Variation of the tdens
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable ::  var_tdens(:)   
     !> Variation of the tdens
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable ::  var_tdens_linfty(:)   
     !> Integral of tdens
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable ::  mass_tdens(:)
     !>---------------------------------------------------------
     !> Integral of weighted_mass of tdens
     !> $ 1/2 int_{\Domain} \frac{\Tdens^{\Pwmass}}{\Pwmass}$
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: weighted_mass_tdens(:)
     !>------------------------------------------------------------
     !> Dissipated Energy
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: energy(:)
     !>------------------------------------------------------------
     !> Lyapunov Functional=Energy + Wmass
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: lyapunov(:)
     !>------------------------------------------------------------
     !> Min values of tdens
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: min_tdens(:)
     !>------------------------------------------------------------
     !> Max Values of tdens
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: max_tdens(:)
     !>------------------------------------------------------------
     !> Entropy = $ int_{\Domain}\Tdens*ln(Tdens)$ 
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: entropy_tdens(:)
     !>------------------------------------------------------------
     !> Duality gap
     !> $\int_{\Omega}|q|^{pvel} - \int{\Omega} u f
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: duality_gap(:)
     !>------------------------------------------------------------
     !> Maximum value of norm of the gradient
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: max_nrm_grad(:)
     !>------------------------------------------------------------
     !> Maximum value of norm of the averaged gradient
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: max_nrm_grad_avg(:)
     !> Maximum value of D3
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: gradient_constrain(:)
     !> Maximum value of D3
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: gradient_constrain_filtered(:)
     !>------------------------------------------------------------
     !> Nterm of precodintior / nterm matrix
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: ntermp_nterm(:)
     !>------------------------------------------------------------
     !> Constraction constant of fix point iteration 
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: cnst_nonlinear(:)
     !>------------------------------------------------------------
     !> Residual od solverolv procedure
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: res_solver(:,:)
     !> Residual of system A(tdens) \pot = rhs
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: res_elliptic(:)
     !> Relative L1-error for tdens with respect to reference solution
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: err_tdens(:)
     !> Total Algorithm Time. Only update time is included
     !> It includes the time wasted in wrong caluclation
     real(kind=double), allocatable :: CPU_time(:)
     !> Eacj iterations Algorithm Time
     !> Only update time is included
     !> It includes the time wasted in wrong caluclation
     real(kind=double), allocatable :: CPU_iterations(:)
     !> CPU wassted in update f
     real(kind=double), allocatable :: CPU_iterations_wasted(:)
     !> Relative L2-error for pot with respect to reference solution
     !> Kantorovich potential
     !> Dimension(0:max_time_iterations)
     real(kind=double), allocatable :: err_pot(:)
   contains 
     !> Static constructor
     !> (procedure public for type evolfun)
     procedure, public, pass :: init => init_evolfun
     !> Static destructor
     !> (procedure public for type evolfun)
     procedure, public, pass :: kill => kill_evolfun
     !> Info procedure at give time iteration
     !> (procedure public for type evolfun)
     procedure, public, pass :: write2dat => write2dat_evolfun
  end type evolfun
contains
  !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type evolfun)
  !> Instantiate of variable of type evolfun
  !>
  !> usage:
  !>     call 'var'%init(lun_err, max_time_iterations,max_iter_nonlinear)
  !>
  !> where:
  !> \param[in] lun_er              -> integer .I/O unit for error msg
  !> \param[in] max_time_iterations -> integer. Max number of 
  !>                                   time iterations
  !> \param[in] max_iter_nonlinear  -> integer. Max number of non linear
  !>                                   solver iterations 
  !<-------------------------------------------------------------  
  subroutine init_evolfun(this,lun_err,max_time_iterations,max_iter_nonlinear)
    implicit none
    class(evolfun), intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    integer,        intent(in   ) :: max_time_iterations
    integer,        intent(in   ) :: max_iter_nonlinear
    ! local var
    logical :: rc
    integer :: res

    this%max_time_iterations         = max_time_iterations
    this% max_iter_nonlinear = max_iter_nonlinear

    allocate(&
         this%time(0:max_time_iterations),&
         this%system_variation(0:max_time_iterations),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'main', &
         ' error alloc. time system_variation',res)
    
    this%time      = zero
    this%system_variation=zero
    this%system_variation(0) = huge


    ! Integer arrays
    allocate(&
         this%iter_solver(max_iter_nonlinear,0:max_time_iterations),&
         this%iter_nonlinear(0:max_time_iterations),&
         this%iter_total(0:max_time_iterations),&
         this%iter_media(0:max_time_iterations),&
         this%nfillin(0:max_time_iterations),&
         this%prec_calc(max_iter_nonlinear,0:max_time_iterations),&
         this%tuning_calc(max_iter_nonlinear,0:max_time_iterations),&
         this%nrestart_newton(0:max_time_iterations),&         
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'init_evolfun', &
         ' type evolfun integer fucntionals',res)
    this%iter_solver    = 0 
    this%iter_total     = 0 
    this%iter_media     = 0 
    this%iter_nonlinear = 0
    this%nfillin        = 0
    this%prec_calc      = 0
    this%tuning_calc    = 0
    this%nrestart_newton= 0
    !
    ! Real valued functional 
    !
    allocate(&
         this%CPU_time(0:max_time_iterations),&
         this%CPU_iterations(0:max_time_iterations),&
         this%CPU_iterations_wasted(0:max_time_iterations),&
         this%mass_tdens(0:max_time_iterations),&
         this%weighted_mass_tdens(0:max_time_iterations),&
         this%energy(0:max_time_iterations),&
         this%lyapunov(0:max_time_iterations),&
         this%min_tdens(0:max_time_iterations),&
         this%max_tdens(0:max_time_iterations),&
         this%entropy_tdens(0:max_time_iterations),&
         this%duality_gap(0:max_time_iterations),&
         this%max_nrm_grad(0:max_time_iterations),&
         this%max_nrm_grad_avg(0:max_time_iterations),&
         this%ntermp_nterm(0:max_time_iterations),&
         this%cnst_nonlinear(0:max_time_iterations),&
         this%res_solver(max_iter_nonlinear,0:max_time_iterations),&
         this%res_elliptic(0:max_time_iterations),&
         this%err_tdens(0:max_time_iterations),&
         this%err_pot(0:max_time_iterations),&
         this%deltat(0:max_time_iterations),&
         this%gradient_constrain(0:max_time_iterations),&
         this%gradient_constrain_filtered(0:max_time_iterations),&
         this%var_tdens(0:max_time_iterations),&
         this%var_tdens_linfty(0:max_time_iterations),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'init_evolfun', &
         ' type evolfun real functionals',res)

    this%mass_tdens=huge
    this%weighted_mass_tdens=huge
    this%energy=huge
    this%lyapunov=huge
    this%min_tdens=huge
    this%max_tdens=huge
    this%entropy_tdens=huge
    this%duality_gap=huge
    this%max_nrm_grad=huge
    this%max_nrm_grad_avg=huge
    this%ntermp_nterm=huge
    this%cnst_nonlinear=huge
    this%res_solver=huge
    this%res_elliptic=huge
    this%err_tdens=huge
    this%CPU_time = huge
    this%err_pot=huge
    this%deltat=huge
    this%gradient_constrain=huge
    this%gradient_constrain_filtered=huge

  end subroutine init_evolfun

  !>-------------------------------------------------------------
  !> Constructor readable bt f90wrap
  !<-------------------------------------------------------------  
  subroutine evolfun_constructor(this,lun_err,max_time_iterations,&
       max_iter_nonlinear)
    implicit none
    type(evolfun),  intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    integer,        intent(in   ) :: max_time_iterations
    integer,        intent(in   ) :: max_iter_nonlinear

    call this%init(lun_err,max_time_iterations,max_iter_nonlinear)
    
  end subroutine evolfun_constructor

  
  !>-------------------------------------------------------------
  !> Static destructor of variable of type evolfun
  !> (procedure public for type evolfun)
  !>
  !> usage:
  !>     call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err              -> integer .I/O unit for error msg
  !<-------------------------------------------------------------  
  subroutine kill_evolfun(this,lun_err)
    implicit none
    class(evolfun), intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    !local
    logical :: rc
    integer :: res

    ! Integer arrays
    deallocate(&
         this%iter_solver,&
         this%iter_total,&
         this%iter_media,&
         this%iter_nonlinear,&
         this%nfillin,&
         this%prec_calc,&
         this%tuning_calc,&
         this%nrestart_newton,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc , 'init_evolfun', &
            ' type evolfun integer fucntionals',res)
   
    ! Real valued functional 
    deallocate(&
         this%CPU_time,&
         this%CPU_iterations,&
         this%CPU_iterations_wasted,&
         this%mass_tdens,&
         this%weighted_mass_tdens,&
         this%energy,&
         this%lyapunov,&
         this%min_tdens,&
         this%max_tdens,&
         this%entropy_tdens,&
         this%duality_gap,&
         this%max_nrm_grad,&
         this%max_nrm_grad_avg,&
         this%ntermp_nterm,&
         this%cnst_nonlinear,&
         this%res_solver,&
         this%res_elliptic,&
         this%err_tdens,&
         this%deltat,&
         this%err_pot,&
         this%gradient_constrain,&
         this%gradient_constrain_filtered,&
         this%var_tdens,&
         this%var_tdens_linfty,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc , 'init_evolfun', &
         ' type evolfun real functionals',res)

  
  end subroutine kill_evolfun

  

  !>-------------------------------------------------------------
  !> Destructor readable by f90wrap
  !<-------------------------------------------------------------  
  subroutine evolfun_destructor(this,lun_err,max_time_iterations,&
       max_iter_nonlinear)
    implicit none
    type(evolfun),  intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    integer,        intent(in   ) :: max_time_iterations
    integer,        intent(in   ) :: max_iter_nonlinear

    call this%kill(lun_err)

  end subroutine evolfun_destructor
  

  subroutine info_evolfun(this,lun,&
       option,&
       ctrl,&
       itemp)
    use DmkControls
    implicit none
    class(evolfun),     intent(in) :: this
    integer,            intent(in) :: lun
    character(len=256), intent(in) :: option
    type(DmkCtrl),      intent(in) :: ctrl
    integer,            intent(in) :: itemp
    !local
    character(len=256) :: out_format
    character(len=3)   :: sep
    

    select case (option)
    case ('tdens') 
       sep=' | '
       out_format=ctrl%formatting('aaaraaaraaar')

       write(lun,out_format) &
            'tdens : ',&
            'min  ',' = ', this%min_tdens(itemp), sep,&
            'max  ',' = ', this%max_tdens(itemp),sep,&
            'wmass',' = ', this%weighted_mass_tdens(itemp)

    case ('energy') 
       out_format=ctrl%formatting('aaaraaaraaar')

       write(lun,out_format) &
            'funct : ',&
            'ene  ',' = ', this%energy(itemp), sep,&
            'lyap ',' = ', this%lyapunov(itemp), sep, &
            'dgap ',' = ', this%duality_gap(itemp)

    case ('error_tdens')
       out_format=ctrl%formatting('aaar')
       write(lun,out_format) &
            'err   : ',&
            'td   ',' = ', this%err_tdens(itemp)


    case ('mkeqs')
       out_format=ctrl%formatting('aaaraaaraaar')
       write(lun,out_format) &
            'grad  : ',&
            'abs  ',' = ', this%max_nrm_grad(itemp), sep,&
            'avg  ',' = ', this%max_nrm_grad_avg(itemp), sep,&
            'D3   ',' = ', this%gradient_constrain(itemp)

    end select

  end subroutine info_evolfun



  subroutine write2dat_evolfun(this,&
       lun_err,lun,folder,&
       it_stop, &
       iformat,rformat)
    use Globals
    implicit none
    class(evolfun),     intent(in) :: this
    integer,            intent(in) :: lun_err
    integer,            intent(in) :: lun
    character (len=*) , intent(in) :: folder
    integer,            intent(in) :: it_stop
    character (len=*) , intent(in) :: rformat
    character (len=*),  intent(in) :: iformat
    !local
    logical :: rc
    integer :: res
    character (len=256) :: fname
    integer :: max_time_iterations,max_nonlin
    integer, allocatable :: ones(:),max_nnlin(:)

    allocate(ones(0:this%max_time_iterations),&
         max_nnlin(0:this%max_time_iterations),stat=res)
    if(res .ne. 0)  rc = IOerr(lun_err, err_alloc , 'write2dat_evolfun', &
         ' wotk array ones',res)

    ones=1
    max_nnlin=this%max_iter_nonlinear


    max_time_iterations = this%max_time_iterations
    max_nonlin  = this%max_iter_nonlinear

    ! integer arrays
    fname=etb(etb(folder)//'iter_solver.dat')
    call write_integer(lun_err,lun,fname,&
         max_nonlin,max_time_iterations,&
         max_nnlin,0,it_stop,&
         this%time, this%iter_solver,&
         iformat,rformat)
    

    fname=etb(etb(folder)//'iter_total.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%iter_total,&
         iformat,rformat)

    fname=etb(etb(folder)//'iter_media.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%iter_media,&
         iformat,rformat)
    
    fname=etb(etb(folder)//'iter_nonlinear.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%iter_nonlinear,&
         iformat,rformat)

    fname=etb(etb(folder)//'fillin.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%nfillin,&
         iformat,rformat)

    fname=etb(etb(folder)//'prec_calc.dat')
    call write_integer(lun_err,lun,fname,&
         max_nonlin,max_time_iterations,&
         max_nnlin,0,it_stop,&
         this%time, this%prec_calc,&
         iformat,rformat)

    fname=etb(etb(folder)//'tuning_calc.dat')
    call write_integer(lun_err,lun,fname,&
         max_nonlin,max_time_iterations,&
         this%iter_nonlinear,0,it_stop,&
         this%time, this%tuning_calc,&
         iformat,rformat)

    fname=etb(etb(folder)//'nrestart_newton.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%nrestart_newton,&
         iformat,rformat)


    fname=etb(etb(folder)//'mass_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%mass_tdens,&
         rformat)

    fname=etb(etb(folder)//'weighted_mass_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1, max_time_iterations,&
         ones, 0,it_stop,&
         this%time, this%weighted_mass_tdens,&
         rformat)

    fname=etb(etb(folder)//'energy.dat')
    call write_real(lun_err,lun,fname,&
         1, max_time_iterations,&
         ones, 0,it_stop,&
         this%time, this%energy,&
         rformat)
    fname=etb(etb(folder)//'lyapunov.dat')
    call write_real(lun_err,lun,fname,&
         1, max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%lyapunov,&
         rformat)


    fname=etb(etb(folder)//'min_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%min_tdens,&
         rformat)

    fname=etb(etb(folder)//'var_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,1,it_stop,&
         this%time, this%var_tdens,&
         rformat)

    fname=etb(etb(folder)//'max_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%max_tdens,&
         rformat)

    fname=etb(etb(folder)//'entropy.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%entropy_tdens,&
         rformat)
    


    fname=etb(etb(folder)//'duality_gap.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%duality_gap,&
         rformat)

    fname=etb(etb(folder)//'max_nrm_grad.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%max_nrm_grad,&
         rformat)

    fname=etb(etb(folder)//'max_nrm_grad_avg.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%max_nrm_grad_avg,&
         rformat)


    fname=etb(etb(folder)//'ntermp_nterm.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%ntermp_nterm,&
         rformat)

    fname=etb(etb(folder)//'cnst_nonlinear.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%cnst_nonlinear,&
         rformat)

    fname=etb(etb(folder)//'res_solver.dat')
    call write_real(lun_err,lun,fname,&
         max_nonlin,max_time_iterations,&
         max_nnlin,0,it_stop,&
         this%time, this%res_solver,&
         rformat)

    fname=etb(etb(folder)//'res_elliptic.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%res_elliptic,&
         rformat)

    fname=etb(etb(folder)//'err_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%err_tdens,&
         rformat)

    fname=etb(etb(folder)//'CPU_time.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%CPU_time,&
         rformat)

    fname=etb(etb(folder)//'time.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%time,&
         rformat)

    fname=etb(etb(folder)//'err_pot.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%err_pot,&
         rformat)

    fname=etb(etb(folder)//'deltat.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%deltat,&
         rformat)

    fname=etb(etb(folder)//'gradient_constrain.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%gradient_constrain,&
         rformat)

    fname=etb(etb(folder)//'gradient_constrain_filtered.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_iterations,&
         ones,0,it_stop,&
         this%time, this%gradient_constrain_filtered,&
         rformat)



    deallocate(ones,max_nnlin,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc , 'write2dat_evolfun', &
         ' wotk array ones',res)


  contains
    subroutine write_integer(&
         lun_err,&
         lun_file,&
         fname,&
         ndata,&
         max_time_iterations,&
         nonzeros,&
         it_start,it_stop,&
         time, iarray,&
         iformat,&
         rformat)
      implicit none
      integer,            intent(in   ) :: lun_err
      integer,            intent(in   ) :: lun_file
      character(len=*),   intent(inout) :: fname
      integer,            intent(in   ) :: ndata
      integer,            intent(in   ) :: max_time_iterations
      integer,            intent(in   ) :: nonzeros(0:max_time_iterations)
      integer,            intent(in   ) :: it_start,it_stop
      real(kind=double),  intent(in   ) :: time(0:max_time_iterations)
      integer,            intent(in   ) :: iarray(ndata,0:max_time_iterations)
      character(len=*),   intent(in   ) :: iformat
      character(len=*),   intent(in   ) :: rformat
      !local 
      integer :: res,i,j 
      logical :: rc
      character(len=256)  :: out_format,dataformat
      
      open(lun_file,file=fname, iostat = res)
      if(res .ne. 0) rc = IOerr(lun_err, err_IO,&
           'write2dat_evol_evolfunc', &
           'err open file '//etb(fname), res)
      do i = it_start, it_stop
         out_format =etb('(')!//etb(rformat)//',1x,')
         write(dataformat,'(I2,a,a,a)') &
              max(1,nonzeros(i)),'(1x,',etb(iformat),')'      
         dataformat=etb(dataformat)
         out_format=etb(etb(out_format)//etb(dataformat)//')')         
         write(lun,out_format) &!time(i), &
              (iarray(j,i),j=1,max(nonzeros(i),1))
      end  do
      close(lun_file)


    end subroutine write_integer

    subroutine write_real(&
         lun_err,&
         lun_file,&
         fname,&
         ndata,&
         max_time_iterations,&
         nonzeros,&
         it_start,it_stop,&
         time, rarray,&
         rformat)
      implicit none
      integer,            intent(in   ) :: lun_err
      integer,            intent(in   ) :: lun_file
      character(len=*),   intent(inout) :: fname
      integer,            intent(in   ) :: ndata
      integer,            intent(in   ) :: max_time_iterations
      integer,            intent(in   ) :: nonzeros(0:max_time_iterations)
      integer,            intent(in   ) :: it_start,it_stop
      real(kind=double),  intent(in   ) :: time(0:max_time_iterations)
      real(kind=double),  intent(in   ) :: rarray(ndata,0:max_time_iterations)
      character(len=*),   intent(in   ) :: rformat
      !local 
      integer :: res,i,j
      logical :: rc
      character(len=256)  :: out_format,dataformat

      open(lun_file,file=fname, iostat = res)
      if(res .ne. 0) rc = IOerr(lun_err, err_IO,&
           'write2dat_evol_evolfunc', &
           'err open file '//etb(fname), res)
      do i = it_start, it_stop
         out_format =etb('(')!//etb(rformat)//',1x,')
         write(dataformat,'(I2,a,a,a)') &
              max(1,nonzeros(i)),'(1x,',etb(rformat),')'      
         dataformat=etb(dataformat)
         out_format=etb(etb(out_format)//etb(dataformat)//')')

         write(lun,out_format) &!time(i),  
              (rarray(j,i),j=1,max(1,nonzeros(i)))
      end  do
      close(lun_file)


    end subroutine write_real
  end subroutine write2dat_evolfun


end module Timefunctionals
