# README #

## Main structure of dmk lybrary

```mermaid
graph TD;
  ProblemSpecifics-->DMK_solver;
  TdensPotential-->DMK_solver;	
  Inputs-->DMK_solver ;
  Controls-->DMK_solver;
  DMK_solver-->TdensPotential;		
  DMK_solver-->Info;
  DMK_solver-->TimeFunctionals;	
```
where
* ProblemSpecifics : Contains varaibles describing problem considered
                     Grid and FEM space for OTP
		     Linear Operator for ADMK
* TdensPotential   : Contains Tdens (\mu) and Potential (u) varaibles
* Inputs           : Contains all inputs describing the problem
* Controls         : Contains all variable to controls algorithm
* Info             : Integer flag summarizing well/bad behaviour of the DMK_solver
* TimeFunctionals  : Collection of data stored along the simulation