module DmkSaintVenantP1P0
  use Globals
  use MuffeTiming
  use AbstractGeometry
  use P1Galerkin
  use LinearOperator
  use Matrix
  use SimpleMatrix
  use SparseMatrix
  use CombinedSparseMatrix
  use PreconditionerTuning
  use DensePreconditioner
  use DenseMatrix
  use RankOneUpdate
  use BlockMatrix
  use StdSparsePrec
  use Eigenv
  use Scratch
  use LinearSolver
  use DmkControls
  use TdensPotentialSystem
  use DmkInputsData
  use DmkDiscretization
  use BackSlashSolver


  
  implicit none
  private 
  public :: eval_trans_prime,eval_trans_second

  !>-------------------------------------------------------------------
  !> Structure Variable containg member for the discretization
  !> of DMK aproach for the Shape Optimization Problem
  !> with PO-P1 scheme, P0 for $\Tdens$, P1 for $\Pot$, with 
  !> P1 that can be defined of the same grid of $\Tdens$ or the
  !> conformal refinement.
  !> It contains all linear algebra quantities for the 
  !> computation of the time evolution.
  !>-------------------------------------------------------------------
  type, extends(dmkpair), public :: p1p0sv
     !> Number of degrees of freedom of ntdens
     integer :: ntdens
     !> Number of degrees of freedom of pot=(potx,poty,[potz])
     integer :: npot
     !> Number of degrees of freedom of system tdens + pot
     integer :: nfull
     !> Number of degrees of gradient of gradient
     integer :: ngrad
     !> Number of degrees of pot
     integer :: ambient_dimension
     !>-----------------------------------------------------------------
     !> Geometrical info
     !>-----------------------------------------------------------------
     !> Flag for two-level grid
     integer :: id_subgrid
     !> Mesh for tdens
     type(abs_simplex_mesh), pointer  :: grid_tdens
     !> Mesh for pot
     type(abs_simplex_mesh), pointer  :: grid_pot
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: nrm_grad_dyn(:)
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: grad(:)
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: grad_avg(:)
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: norm_grad(:)
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: norm_grad_avg(:)
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: gradient_compliance_tdens(:)
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: nrm2grad(:)
     !>-----------------------------------------------------------------
     !> Finite Elements Scheme 
     !> P1-Galerkin variables (built on grid_pot)
     type(p1gal)    :: p1
     !>-----------------------------------------------------------------
     !> Stiff matrix from 
     !> $ -\Div (\Tdens \epsilon(\Pot) : C \epsilon(\Pot)) = \Forcing  $
     type(spmat) :: stiff
     !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
     !> Trija pointer for assembly of B_matrix
     !> Scratch array for PCG and BICGSTAB  Procedure
     type(scrt) :: aux_bicgstab
     !> Scratch array for implicit_euler_newton
     type(scrt) :: aux_newton
     !> Work array form rhs of linear system
     real(kind=double), allocatable :: rhs(:)
     !> Work array form rhs of linear system
     real(kind=double), allocatable :: pot_permuted(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: rhs_ode(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: inc_ode(:)
     !> Dimension (ncellpot)
     !> Work array form update procedure
     real(kind=double), allocatable :: tdens_prj(:)
     !>---------------------------------------------------------------
     !> Preconditioner for sparse linear systems
     !>---------------------------------------------------------------
     !> Back up Sparse preconditioner for PCG procedure
     type(stdprec) :: standard_prec_saved
     !>-----------------------------------------------------------
     !> Scratch arrays for general porpuse
     !>-----------------------------------------------------------
     !> Dimension (ngrad)
     !> Work array 
     real(kind=double), allocatable :: scr_ngrad(:)
     !> Dimension (ntdens)p1p0_space_discretization
     !> Work array 
     real(kind=double), allocatable :: scr_ntdens(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: scr_npot(:)
     real(kind=double), allocatable :: scr_rhs(:)
     !> Dimension (ncellpot)
     !> Work array
     real(kind=double), allocatable :: scr_ncellpot(:)
	  integer, allocatable :: scr_dirichlet(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: diagonal_scale(:)
     real(kind=double), allocatable :: inv_diagonal_scale(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     integer, allocatable :: scr_integer(:)
     !> Permutation matrix for passing from 
     ! (p_x, py, pz) to 
     ! (px^1, py^1,pz^1..., px^i, py^i,pz^i, ...., px^n, py^n, pz^n)
     type(permutation) :: ordering_permutation
     !> Identity matrix of size npot
     type(scalmat) :: identity_npot
     !> Identity matrix of size npot
     type(scalmat) :: identity_ntdens
     !> Dimension (npot)
     !> Diagonal of the laplacian 
     !> Work array 
     real(kind=double), allocatable :: scr_nfull(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     real(kind=double), allocatable :: rhs_full(:)
     !> Work variable to store linear solver controls
     type(input_solver) :: ctrl_solver
     !> Info on linear solver solution
     type(output_solver) :: info_solver
     !> Info on linear solver solution
     !> Dimension(max_nonlinear_iterations)
     type(output_solver), allocatable :: sequence_info_solver(:)
     !> Dimension(max_nonlinear_iterations)
     integer, allocatable :: sequence_build_prec(:)
     !> 
     integer :: iter_first_system
     integer :: iter_last_prec
     !> It the kernel of stifness matrix
     type(densemat):: kernel_stiff
     !> Implicit Kernel transpose
     type(TransposeMatrix) :: kernel_stiff_transpose
     !> Projection matrix
     type(pair_linop) :: proj_to_kernel
   contains
     !> Static constructor 
     !> (procedure public for type p1p0sv)
     procedure, public , pass :: init => init_p1p0
     !> Static destructor
     !> (procedure public for type p1p0sv)
     procedure, public , pass :: kill => kill_p1p0
     !> Static destructor
     !> (procedure public for type p1p0sv)
     !procedure, public , pass :: tdens2pot
     !> Procedure for building
     !> $\int _{T_r} |\nrm_grad|^\Pode $ / |T_r|
     !> with \Pode=\Pflux for PP dynamic
     !>      \Pode=2      for GF dynamic
     !> (procedure public for type p1p0sv)
     !>-------------------------------------------------------------
     !>-------------------------------------------------------------
     !> Procedure for assembly the stiffness matrix 
     !> (procedure public for type p1p0sv)
     procedure, public , pass :: assembly_stiffness_matrix
     !> Procedure for computation of elastic strain energy
     !> (procedure public for type p1p0sv)
     procedure, public , pass :: build_gradient_compliance
     !> Procedure to syncronized tdens and pot varible
     !> (procedure public for type p1p0sv)
     procedure, public , pass :: syncronize_tdpot
     !> Subroutine for assembling right-hand side of 
     !> Tdens ODE
     procedure, public , pass :: assembly_rhs_ode_tdens
     !> Subroutine to get the tdens increment from the rhs_ode
     !> Tdens (or Gfvar) ODE
     procedure, public , pass :: get_increment_ode
     procedure, public , pass :: update_tdpot
     !> Update procedure for tdpotsys tpye
     !> via Explicit Euler for Tdens Dynamic(forward Euler)
     procedure, private , pass :: explicit_euler_tdens
     !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$
     !> (procedure public for type specp0_space_discretization)
     procedure, public , pass :: compute_tdpot_variation
     !> Subroutine for assembling right-hand side of 
     !> Gfvar ODE
     procedure, public , pass :: assembly_grad_lyap_gfvar
     !> Procedure to set controls (time-step, preconditioner construction
     !> etc.) for next update
     !> (procedure public for type specp0_space_discretization)
     procedure, public , pass :: set_controls_next_update
     !> Procedure to reset controls 
     !> (time-step, preconditioner construction etc.) 
     !> in case of failure in update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: reset_controls_after_update_failure
  end type p1p0sv


contains
  !>-------------------------------------------------
  !> Procedure to compute e(u):e(u)
  !> (public procedure for type p1p0sv)
  !> 
  !> usage: call var%build_gradient_compliance(&
  !>		  pot,mu,lambda,gradient_compliance)
  !>
  !> where:
  !> \param[inout] pot                 -> real (dimension=npot). 
  !> \param[in   ] mu                  -> real. First  Lam'\e
  !> \param[in   ] lambda              -> real. Second Lam'\e
  !> \param[inout] gradient_compliance -> real (dimension=ntdens)
  !<-------------------------------------------------
  subroutine build_gradient_compliance(this,&
		  pot,mu,lambda,gradient_compliance)

    use Globals
    implicit none

    ! Input and Output variables
    class(p1p0sv),     intent(inout) :: this
    real(kind=double), intent(in   ) :: pot(this%npot)
    real(kind=double), intent(in   ) :: mu, lambda
    real(kind=double), intent(inout) :: gradient_compliance(this%ntdens)

    ! Local variables
    logical :: rc
    integer :: res, i
    real(kind=double), dimension(this%grid_pot%nnode) :: potx, poty, potz
    
    ! x and y component solution Saint Venant Equations 
    potx = pot(1:this%grid_pot%nnode)
    poty = pot(1+this%grid_pot%nnode:2*this%grid_pot%nnode)
	 
	 ! z component soluzion of 3D Saint Venant Equations
	 if (this%grid_pot%logical_dimension.eq.3) then
		 potz = pot(1+2*this%grid_pot%nnode:3*this%grid_pot%nnode)
	 else
		 potz = zero
	 end if

    ! Compute energy for each cell of grid_pot
    call this%p1%eval_strain_energy(mu,lambda,this%scr_ncellpot,potx,poty,potz)
    
    ! Check if the grid for pot and tdens is different or the same
    if (this%id_subgrid .eq. 1) then  
       ! Starting from values defined on a subgrid compute 
       ! average values on the grid (weighted average)
       call this%grid_pot%avg_cell_subgrid(this%grid_tdens,&
            this%scr_ncellpot,gradient_compliance)
    else
       ! Copy values if the grid is the same
       gradient_compliance = this%scr_ncellpot
    end if

  end subroutine build_gradient_compliance


  !>------------------------------------------------------
  !> Prodeduce for the assembly of the siffness
  !> matrix for given Tdens
  !> 
  !> usage: call var%build_gradient_compliance(&
  !>		  pot,mu,lambda,gradient_compliance)
  !>
  !> where:
  !> \param[inout] pot              -> real (dimension=npot). 
  !> \param[in   ] mu               -> real. First  Lam'\e
  !> \param[in   ] lambda           -> real. Second Lam'\e
  !> \param[in   ] tdens            -> real (dimension=ntdens). 
  !> \param[inout] stiffness_matrix -> type (spmat) Stiffness matrix
  !>------------------------------------------------------
  subroutine assembly_stiffness_matrix(this,lun_err,&
       mu,lambda,tdens,stiffness_matrix)

    use Globals
    use Timing
    implicit none
    
    ! Input and Output variables
    class(p1p0sv),     intent(inout) :: this
    integer,           intent(in   ) :: lun_err
    real(kind=double), intent(in   ) :: tdens(this%ntdens)
    real(kind=double), intent(in   ) :: mu, lambda
    type(spmat),       intent(inout) :: stiffness_matrix


    if (this%id_subgrid.eq.0) then
       ! Assembly procedure when the grid for tdens
       ! and pot is the same
       call this%p1%build_stiff_saint_venant(lun_err,&
            tdens,mu,lambda,stiffness_matrix)
    else
       ! Projection of tdens over the subgrid when
       ! tdens and pot have different grids
       call this%grid_pot%proj_subgrid(tdens,this%tdens_prj)
       
       ! Assembly procedure
       call this%p1%build_stiff_saint_venant(lun_err,&
            this%tdens_prj,mu,lambda,stiffness_matrix)
    end if

  end subroutine assembly_stiffness_matrix


  !>--------------------------------------------------------------------
  !> Procedure for the syncronization and tdens potential variable
  !> (public procedure for type dmkpair)
  !> 
  !> usage: call var%syncronization(tdpot,ode_inputs,ctrl,info)
  !> 
  !> where:
  !> \param[inout] tdpot      -> tye(tdpotsys). Tdens-Potentail System
  !> \param[in   ] ode_inputs -> tye(DmkInputs). Input for Ode
  !> \param[in   ] ctrl       -> tye(DmkCtrl). Dmk controls
  !> \param[inout] info       -> integer. Flag for info 
  !>                              0 = Everything is fine
  !>                              1** = Error in linear solver
  !>                              11* = Convergence not achivied
  !>                              12* = Internal error in linear solver
  !>                              2**  = Error in non-linear solver
  !>                              21* = Non-linear convergence not achievied
  !>                              22* = Non-linear solver stoped because was divergening
  !>                              23* = Condition for non-linear solver to
  !>                                     continue failed
  !>                              3**  = Other errors
  !>---------------------------------------------------------------------
  subroutine syncronize_tdpot(this,tdpot,ode_inputs,ctrl,info)
    use DmkInputsData
    use StdSparsePrec
    use dagmg_wrap
    implicit none
    
    ! Input and Output variables
    class(p1p0sv), target, intent(inout) :: this
    class(tdpotsys),        intent(inout) :: tdpot
    class(DmkInputs),       intent(in   ) :: ode_inputs
    class(DmkCtrl),         intent(in   ) :: ctrl
    integer,                intent(inout) :: info

    ! Local variables
    logical :: endfile
    logical :: rc
    integer :: res
    integer :: icell,i,inode,j,k
    integer :: itemp=0 
    integer :: lun_err, lun_out,lun_stat
    integer :: ntdens,npot
    real(kind=double) :: pode, pflux
    real(kind=double) :: ddot, dnrm2
    real(kind=double), allocatable , dimension(:) :: x,y,xmy,Px,Py,Pxmy
    class(abs_linop), pointer :: ort_mat
    class(abs_linop), pointer :: prec_final
    type(input_solver) :: ctrl_solver
    type(input_solver) :: ctrl_multigrid
    type(input_prec) :: ctrl_prec
    type(agmg_inv), target :: multigrid_solver
    type(scalmat) :: identity_npot
    type(scalmat) :: eye_npot
    type(spmat),target :: matrix2solve,matrix2prec,matrix_temp
    type(spmat),target :: stiffxx,stiffxy,stiffyx,stiffyy
    type(array_linop) :: list(4)
    type(block_linop) :: stiff_block
    integer :: block_structure(3,4)
    type(inv), target :: inverse_stiff
    
    real(kind=double), allocatable :: kernelxrhs(:)
    real(kind=double) :: rort
   
    ! Degrees of freedom transport density
    ntdens   = tdpot%ntdens
    
    ! Degrees of freedom potential
    npot     = tdpot%npot
    
    ! shorthand for variables
    lun_err  = ctrl%lun_err
    lun_out  = ctrl%lun_out
    lun_stat = ctrl%lun_statistics

    ! Identity matrix of dimension (npot,npot)
    call identity_npot%eye(npot)

    ! call this%CPU%ALGORITHM%set('start')
    pode  = ode_inputs%pode
    pflux = ode_inputs%pflux

    ! 2.1 - Assign data 
    if (abs(pode-2.0d0) < small) then
       call tdens2gfvar(tdpot%ntdens,pode,pflux,tdpot%tdens,tdpot%gfvar)
    end if

    ! --------------------------------
    ! 1 - Solve Saint Venant Equations   
    ! --------------------------------

    ! 1.1 - Assembly matrix
    if (ctrl%debug.ge. 1) &
         write(lun_out,*) 'BUILD STIFF'
    this%scr_ntdens = tdpot%tdens
    call this%assembly_stiffness_matrix(lun_err,&
         ode_inputs%mu_lame,ode_inputs%lambda_lame,&
         this%scr_ntdens,this%stiff)

    ! Relax stiffness matrix
    if (ctrl%debug.ge. 1) &
         write(lun_out,*) 'RELAX STIFF',ctrl%relax_direct
    matrix2solve = this%stiff

    ! ------------------------------------------------------
    ! applly permution from storage order (potx,poty,[potz])
    ! into (px^1,py^1,[pz^1], ....., px^n,py^n,[pz^n]) 
    ! ------------------------------------------------------
	 this%scr_rhs = ode_inputs%rhs
	 call this%ordering_permutation%Mxv(this%scr_rhs,this%rhs)
    call this%ordering_permutation%Mxv(tdpot%pot,this%pot_permuted)
     
	 ! RHS orthogonal to the kernel for pure Neumann problem
	 if (ode_inputs%ndir .eq. 0) then

		 this%scr_rhs = ode_inputs%rhs
		 call this%ordering_permutation%Mxv(this%scr_rhs,this%rhs)

		 if (ctrl%outer_iort .ne. 0) then 

			 allocate(kernelxrhs(npot),stat=res)
			 if (res.ne.0) rc=IOerr(lun_err, err_alloc,&
					'syncronize','kernelxrhs')

			 ! Check whether the rhs is orthogonal to the kernel
			 write(*,*) 'IORT > 0: CHECK WHETHER THE RHS IS ORTHOGONAL TO KERNEL'
			 call this%proj_to_kernel%Mxv(this%rhs,kernelxrhs)
			 rort=dnrm2(npot,kernelxrhs,1)
			 write(*,*) 'KERNEL X RHS NORM: ', rort

			 ! Correction rhs if it is not orthogonal to the kernel
			 if ( rort .gt. ctrl%outer_tolerance ) then
				 call this%p1%neumann_elasticity_rhs(ode_inputs%rhs,this%scr_rhs)
				 call this%ordering_permutation%Mxv(this%scr_rhs,this%rhs)
				 call this%proj_to_kernel%Mxv(this%rhs,kernelxrhs)
				 rort=dnrm2(npot,kernelxrhs,1)
				 write(*,*) 'NEW KERNEL X RHS NORM: ', rort
			 end if 

			 deallocate(kernelxrhs,stat=res)
			 if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
				 'syncronize','kernelxrhs')

		 end if

	 end if

    if (ctrl%debug.ge. 1) &
         write(lun_out,*) 'DIRICHLET NODES',ode_inputs%ndir

    ! 1.3 Handle Neumann singularity or Dirichlet
    if ( ode_inputs%ndir > 0 ) then
       
!       write(*,*) 'Number of dirichlet nodes: ', ode_inputs%ndir
!       write(*,*) 'Dirichlet nodes:  ', ode_inputs%dirichlet_nodes(1:ode_inputs%ndir)
!       write(*,*) 'Dirichlet compon: ', ode_inputs%dirichlet_component(1:ode_inputs%ndir)
!       write(*,*) 'Dirichlet values: ', ode_inputs%dirichlet_values(1:ode_inputs%ndir)
       
       do i = 1,ode_inputs%ndir
          this%scr_dirichlet(i) = this%grid_pot%ambient_dimension*ode_inputs%dirichlet_nodes(i)
          if (ode_inputs%dirichlet_component(i).eq.1) &
               this%scr_dirichlet(i) = this%scr_dirichlet(i) - (this%grid_pot%ambient_dimension-1) 
          if (ode_inputs%dirichlet_component(i).eq.2) &
               this%scr_dirichlet(i) = this%scr_dirichlet(i) - (this%grid_pot%ambient_dimension-2) 
       end do

!       write(*,*) 'DIR NEW NUMBERING: ', this%scr_dirichlet(1:ode_inputs%ndir)

       call this%p1%dirichlet_bc(lun_err,&
            matrix2solve,this%rhs,this%pot_permuted,&
            ode_inputs%ndir,&
            this%scr_dirichlet,&
                                !ode_inputs%dirichlet_nodes,&
            ode_inputs%dirichlet_values)
       ort_mat => null()

    end if

    !
    ! diagonal scaling
    !
    if ( ctrl%diagonal_scaling == 1) then
       call matrix2solve%get_diagonal(this%diagonal_scale)
       this%diagonal_scale=sqrt(this%diagonal_scale)
       this%inv_diagonal_scale=one/this%diagonal_scale
       call matrix2solve%diagonal_scale(lun_err,&
            this%inv_diagonal_scale)
       this%rhs          = this%inv_diagonal_scale * this%rhs
       this%pot_permuted = this%diagonal_scale * this%pot_permuted
    end if

    if (ode_inputs%ndir == 0) call matrix2solve%aMpN(ctrl%relax_direct,identity_npot)

    ! 1.4 solve linear system
    if (ctrl%debug.ge. 1) &
         write(lun_out,*) 'SOLVING WITH ', etb(ctrl%outer_solver_approach)

    !
    ! set controls per outer solver
    !
    call ctrl_solver%init(lun_err,&
         approach=ctrl%outer_solver_approach,&
         scheme=ctrl%outer_krylov_scheme,&
         imax=ctrl%outer_imax,&
         iexit=ctrl%outer_iexit,&
         isol=ctrl%outer_iexit,&
         nrestart=ctrl%krylov_nrestart,&
         tol_sol=ctrl%outer_tolerance)
    if (ctrl%outer_solver_approach=='ITERATIVE') then
       !
       ! set inversion controls from ctrl
       !
       call ctrl_prec%init(&
            lun_err,&
            ctrl%outer_prec_type,&
            ctrl%outer_prec_n_fillin,&
            ctrl%inner_prec_tol_fillin)
       call ctrl_prec%info(6)
    end if



    !
    ! define (approximate) inverse 
    !
    call  inverse_stiff%init(&
         matrix2solve,ctrl_solver,ctrl_prec,&
         lun=lun_err)

    !
    ! solve Linear system 
    !
    call inverse_stiff%Mxv(this%rhs,this%pot_permuted)
    this%info_solver=inverse_stiff%info_solver


    !
    ! diagonal scaling back
    !  
    if ( ctrl%diagonal_scaling == 1) then
       call matrix2solve%diagonal_scale(lun_err,this%diagonal_scale)
       this%pot_permuted = this%inv_diagonal_scale * this%pot_permuted
       this%rhs  = this%diagonal_scale * this%rhs
    end if

    if (ctrl%debug .eq. 1) write(lun_out,*) 'LINEAR SYSTEM SOLVED'
    
	 if ( ode_inputs%ndir > 0 ) then
       !this%pot_permuted=zero
       do i=1,ode_inputs%ndir
          !inode=ode_inputs%dirichlet_nodes(i)
          inode=this%scr_dirichlet(i)
          this%scr_npot(i)=ode_inputs%dirichlet_values(i)-this%pot_permuted(inode)
       end do
       write(*,'(1X,A,1PE9.2)') 'Error Dirichlet =', dnrm2(ode_inputs%ndir , this%scr_npot,1)
    end if

    !
    ! inverse permutation for pot
    !
    call this%ordering_permutation%MTxv(this%pot_permuted,tdpot%pot)

    !
    ! set info flag
    !
    info = 0
    ! Info on linear solver solution
    if (this%info_solver%ierr .ne. 0) then
       info = 1
    end if
    if (ctrl%info_update .ge. 1) then
       if( lun_out >0) call this%info_solver%info(lun_out)
    end if
    if(lun_stat>0) call this%info_solver%info(lun_stat)

    ! Store linear solver info 
    this%sequence_info_solver(1) = this%info_solver
    this%sequence_build_prec(1)  = ctrl%build_prec
    this%iter_first_system = this%info_solver%iter


    tdpot%all_syncr   = .True.
    tdpot%tdpot_syncr = .True.

    !
    ! free memory
    !
    if (matrix2prec%is_initialized) call matrix2prec%kill(lun_err)
    if (matrix2solve%is_initialized)  call matrix2solve%kill(lun_err)
    if (ctrl%debug .eq. 1) write(lun_out,*) 'syncronize_tdpot finished'

  end subroutine syncronize_tdpot


  !>------------------------------------------------
  !> Procedure for computation of next state of system
  !> ( all variables ) given the preovius one
  !> ( private procedure for type dmkpair, used in update)
  !> 
  !> usage: call var%update(lun_err,itemp)
  !> 
  !> where:
  !> \param[inout] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[in   ] ctrl  -> type(DmkCtrl). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !> \param[in   ] info  -> integer. Flag with 3 digits for errors.
  !>                        If info==0 no erros occurred.
  !>                        First digit from the left describes 
  !>                        the main errors. The remaining digits can be used to
  !>                        mark specific errors. Current dictionary:
  !>                        1**  = Error in linear solver
  !>                         11* = Convergence not achivied
  !>                         111 = Max iterations 
  !>                         112 = Internal error in linear solver
  !>                         13* = Failure Preconditoner
  !>                         131 = Failure preconditioner assembly  
  !>                         132 = Failure preconditioner application
  !>                         140 = In reduced approch block22 changes sign
  !>                        2**  = Error in non-linear solver
  !>                         21* = Non-linear convergence not achievied
  !>                         22* = Non-linear solver stopped because was divergening
  !>                         23* = Condition for non-linear solver to
  !>                               continue failed
  !>                        3**  = Other errors
  !<---------------------------------------------------
  subroutine update_tdpot(this,tdpot,ode_inputs,ctrl, info)

    use Globals
    use Timing
    use DmkInputsData
    implicit none
    
	 ! Input and Output variables
    class(p1p0sv), target, intent(inout) :: this
    class(tdpotsys),        intent(inout) :: tdpot
    class(DmkInputs),       intent(in   ) :: ode_inputs
    class(DmkCtrl),         intent(in   ) :: ctrl
    integer,               intent(inout) :: info


    ! Local variables
    real(kind=double) :: decay,pflux,pmass,pode,time,tnext,deltat
    integer :: newton_initial
    integer :: ntdens, npot
    integer :: info_inter,passed_reduced_jacobian,time_iteration
    type(tim) :: wasted_temp
    character(len=256) :: str,msg
    integer :: slot
    integer :: lun_err, lun_out,lun_stat
    type(codeTim) :: CPU

	 ! Time-step
    deltat = ctrl%deltat

    !
    ! local copy
    !
	 ! Degrees of freedom transport density
    ntdens = this%ntdens
	 ! Degrees of freedom potential
    npot   = this%npot

	 ! Logic unit I/O
    lun_err  = ctrl%lun_err
    lun_out  = ctrl%lun_out
    lun_stat = ctrl%lun_statistics

    time_iteration = tdpot%time_iteration
    time = tdpot%time

	 !write(*,*) 'time 1: ', tdpot%time

    ! Update all spatial variables at itemp+1
    select case ( ctrl%time_discretization_scheme )
    case (1)
		 ! Solve ODE with explicit euler scheme
       call this%explicit_euler_tdens(tdpot,ode_inputs,ctrl,info)
    case default
       write(*,*) ' time_discretization_scheme ', &
            ctrl%time_discretization_scheme ,' not supported'
       stop 
    end select

	 !write(*,*) 'time 2: ', tdpot%time

    if ( info .eq. 0 ) then
       tdpot%deltat   = ctrl%deltat
       tdpot%time_old = tdpot%time
       tdpot%time     = tdpot%time + ctrl%deltat
    end if

	 !write(*,*) 'time 3: ', tdpot%time

  end subroutine update_tdpot

  !>----------------------------------------------------
  !> Procedure for update the system with Explicit Euler
  !> in the tdens varaible.
  !> IMPORTANT all the variables tdens pot 
  !> odein have to be syncronized at time time(itemp)
  !>
  !> usage: call var%explicit_euler_tdens(stderr,itemp)
  !> 
  !> where:
  !> \param[in ] stderr -> Integer. I/O err. unit
  !> \param[in ] itemp   -> Integer. Time iteration
  !<---------------------------------------------------    
  subroutine explicit_euler_tdens(this,tdpot,ode_inputs,&
       ctrl,info)

    use Globals 
    use LinearSolver
    implicit none
    
	 ! Input and Output 
    class(p1p0sv), target, intent(inout) :: this
    type(tdpotsys),        intent(inout) :: tdpot
    type(DmkInputs),       intent(in   ) :: ode_inputs
    type(DmkCtrl),         intent(in   ) :: ctrl
    integer,               intent(inout) :: info

    !local
    logical :: rc
    integer :: res
    integer ::  id_ode
    integer :: ntdens,npot,lun_err,lun_out,lun_stat
    real(kind=double) :: pode,pflux
    real(kind=double) :: delta
    integer :: icell, isubcell, ifather, inode, i,j,iloc
    real(kind=double) :: grad_pot(3), grad_base(3),grad_der_pot(3),der_pot(3)
    real(kind=double) :: ddot,dnrm2
    integer :: jcell, jsubcell, jfather
    real(kind=double), allocatable :: grad_w(:,:)
    character(len=256) :: fname, number,msg
    character(len=256) :: out_format

    class(abs_linop), pointer :: ort_mat
    real(kind=double), allocatable :: rhs(:)
    !> Info on linear solver solution
    type(input_solver) :: ctrl_solver
    type(input_prec)  :: ctrl_prec
    real(kind=double) :: current_time

	 ! Degrees of freedom transport density
    ntdens   = tdpot%ntdens
	 ! Degrees of freedom potential
    npot     = tdpot%npot

	 ! Logic unit I/O
    lun_err  = ctrl%lun_err
    lun_out  = ctrl%lun_out
    lun_stat = ctrl%lun_statistics

	 ! Check whether the variables are syncronized or not
    if ( .not. tdpot%all_syncr)  then
       write(lun_err,*) 'Not all varibles are syncronized'
       stop
    end if

	 ! Allocate rhs for the ODE
    allocate(rhs(npot),stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc,&
		   'explicit_euler_accelerated_gfvar', &
         'temp array rhs')

    ! Compute rhs ODE 
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'Compute RHS ODE'

    call this%assembly_rhs_ode_tdens(ode_inputs,&
         tdpot%tdens,tdpot%pot,this%rhs_ode)

    ! Get increment ODE
    call this%get_increment_ode(this%rhs_ode,this%inc_ode)
    out_format=ctrl%formatting('rar')
    write(msg,out_format) minval(this%inc_ode ), &
		 ' <= TDENS INCREMENT <= ', maxval(this%inc_ode)
    
	 ! Info update message
    if ( ctrl%info_update .ge. 1) then 
       if( lun_out >0) write(lun_out,*) etb(msg)
    end if
	 if( lun_stat>0) write(lun_stat,*) etb(msg)
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'UPDATE TDENS'

    ! Update $\Tdens$ using explicit Euler scheme
    tdpot%tdens = tdpot%tdens + ctrl%deltat*this%inc_ode
    
	 ! Transpot density not smaller than lower bound min_tdens
    do icell = 1, tdpot%ntdens
       tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
    end do

    tdpot%tdpot_syncr = .false.

    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'SYNCRONIZE TDPOT'
   
    ! Current time
    !current_time = tdpot%time

    ! we use syncronization suboroutine
    call this%syncronize_tdpot(tdpot,ode_inputs,ctrl,info)

    write(*,*) 'info syncro', info
    
    ! Set time at time+deltat
    !tdpot%time = current_time+ctrl%deltat
    

  end subroutine explicit_euler_tdens

  !>----------------------------------------------------------
  !> Subroutine to assign the pointer prec_zero to a
  !> stdprec. Two option are available
  !> prec_zero => prec_saved
  !> prec_zero => prec_local
  !>--------------------------------------------------------
  subroutine assembly_stdprec(lun_err,&
         matrix2prec,&
         build_prec,&
         ctrl_prec,&
         info_prec,&
         prec_saved)!,prec_local, prec_zero)
      use Globals
      implicit none
      integer,                intent(in   ) :: lun_err
      type(spmat),            intent(inout) :: matrix2prec
      integer,                intent(in   ) :: build_prec
      type(input_prec),    intent(in   ) :: ctrl_prec
      integer,                intent(inout) :: info_prec
      type(stdprec),          intent(inout) :: prec_saved
      !type(stdprec), target,  intent(inout) :: prec_local
      !type(stdprec), pointer, intent(inout) :: prec_zero
      ! local
      logical :: rc
      type(stdprec)  :: prec_local
      
      if ( build_prec .eq. 1 ) then
         info_prec=0
         ! assembly prec on local work prec
         call prec_local%init(lun_err, info_prec,&
              ctrl_prec, matrix2prec%nrow, matrix2prec)
         if ( info_prec .eq. 0 ) then            
            prec_saved = prec_local
         else
            !
            ! write warning 
            !
            ! prec_saved is updated with new preconditioner
            !
            rc = IOerr(lun_err, wrn_val, 'assembly_stdprec', &
                 'using saved prec. assembly failed info_prec=', info_prec)
         end if
         call prec_local%kill(lun_err)


      else
         info_prec = 0
         ! use saved prec.
         if ( .not. prec_saved%is_built) then
            ! check if prec_saved contains something
            rc = IOerr(lun_err, wrn_val, 'assembly_stdprec', &
                 ' prec. saved not built')
            info_prec = -1
         end if
         
      end if

    end subroutine assembly_stdprec


    

    !>--------------------------------------------------------------
    !> Procedure to control the delta_t of the time-stepping
    !> (private procedure used in upadte)
    !<---------------------------------------------------------------
    subroutine  control_deltat(lun_out,ntdens,&
         tdens,increment,previous_var,&
         ctrl,grid_tdens,deltat)
      use Globals
      implicit none

      integer,           intent(in   ) :: lun_out
      integer,           intent(in   ) :: ntdens
      real(kind=double), intent(in   ) :: tdens(ntdens)
      real(kind=double), intent(in   ) :: increment(ntdens)
      real(kind=double), intent(in   ) :: previous_var
      type(DmkCtrl),     intent(in   ) :: ctrl
      type(abs_simplex_mesh),        intent(in   ) :: grid_tdens
      real(kind=double), intent(inout) :: deltat

      !local
      real(kind=double) :: sup_rhs, cnst=1e-8
      real(kind=double) :: n2,n2tdens
      character(len=256):: out_format



      if (ctrl%time_discretization_scheme == 1) then
         deltat=ctrl%deltat 
         out_format=ctrl%formatting('ar')
         write(lun_out,out_format) 'time step = ',deltat
      end if
      if (ctrl%time_discretization_scheme == 2) then
         deltat=min(deltat * ctrl%deltat_expansion_rate,ctrl%deltat_upper_bound)
         out_format=ctrl%formatting('ar')
         write(lun_out,out_format) 'time step = ',deltat
      end if

      if (ctrl%time_discretization_scheme == 3) then
         sup_rhs= maxval(increment)
         if ( sup_rhs < zero ) then
            deltat = max(ctrl%deltat_lower_bound,&
                 min(deltat * ctrl%deltat_expansion_rate,ctrl%deltat_upper_bound))
         else
            deltat=(1.0d0-ctrl%deltat_lower_bound)/sup_rhs
         end if
         deltat=min(deltat,ctrl%deltat_upper_bound)
         
         out_format=ctrl%formatting('araar')
         write(lun_out,out_format) 'time step = ',deltat,&
              ' | ', ' linfty norm increment = ', sup_rhs
      end if

      if (ctrl%time_discretization_scheme == 4) then
         if (previous_var .ne. zero ) then
            deltat=2.0d-1/previous_var
            deltat=min(deltat,ctrl%deltat_upper_bound)
            deltat=max(deltat,ctrl%deltat_lower_bound)
         end if
         if ( 1-deltat*maxval(increment) < zero) then
            deltat = (1.0d0-cnst)/maxval(increment)
            deltat=max(deltat,ctrl%deltat_lower_bound)
         end if
      end if
      
      if (ctrl%time_discretization_scheme == 5) then
         sup_rhs =  maxval(increment)         
         if ( 1-deltat*maxval(increment) < zero) then
            deltat = (1.0d0-cnst)/maxval(increment)
            write(lun_out,*) 'time step = ',deltat            
         else
            if ( sup_rhs .gt. zero ) deltat = one / maxval(increment)
            if ( sup_rhs .lt. zero ) deltat = one / maxval(abs(increment))
         end if
         deltat=min(deltat,ctrl%deltat_upper_bound)
         deltat=max(deltat,ctrl%deltat_lower_bound)
      end if


    end subroutine control_deltat
    

  
  !>--------------------------------------------------------------------
  !> Static Constructor 
  !> (public procedure for type p1p0sv)
  !> 
  !> usage: call var%init(&
  !>                lun_err,lun_out,lun_stat,& 
  !>                ctrl,&
  !>                grid_tdens, grid_pot)
  !> where:
  !> \param[in] lun_err    -> integer. I/O unit for error message
  !> \param[in] lun_out    -> integer. I/O unit for output message
  !> \param[in] lun_stat   -> integer. I/O unit for statistic
  !> \param[in] ctrl       -> type(DmkCtrl). Controls variables
  !> \param[in] grid_tdens -> tyep(mesh). Mesh for tdens
  !> \param[in] grid_pot   -> tyep(mesh). Mesh for pot
  !>---------------------------------------------------------------------
  subroutine init_p1p0(this, ctrl, id_subgrid, grid_tdens, grid_pot)
    
    implicit none
    
    ! Input and Output
    class(p1p0sv), target,          intent(inout) :: this
    type(DmkCtrl),                  intent(in   ) :: ctrl
    integer,                        intent(in   ) :: id_subgrid
    type(abs_simplex_mesh), target, intent(in   ) :: grid_tdens
    type(abs_simplex_mesh), target, intent(in   ) :: grid_pot

    ! local
    logical :: rc
    integer :: res
    integer :: ntdens, npot, ngrad,i,j,k,iloc,jloc,inode
    integer :: lun_err
    integer :: lun_out
    integer :: lun_stat
    ! jacobian 
    type(array_mat) :: jacobian_list(4)
    integer :: block_structure(3,4)
    character(len=1) :: jacobian_directions(4)
    ! preconditioners
    type(array_linop) :: prec_list(2)
    integer :: prec_block_structure(3,2)
    type(input_prec) :: ctrl_prec
    type(scalmat),   target :: identity_npot
    integer ::info
    type(input_prec) :: ctrl_inverse
    real(kind=double) :: dnrm2,ddot
    integer :: ncellpot
    integer :: dim_work,nfull
    integer, allocatable :: itemp(:)
    type(array_linop) :: list(2)

    if ( ( ctrl%debug .eq. 1 ) .and. (lun_out>0) )&
         write( lun_out, *) 'Init p1p0 structure'
    
    this%id_subgrid = id_subgrid
    this%grid_tdens => grid_tdens
    this%grid_pot   => grid_pot
    
    !
    ! dimension assignment + local copy
    !
    this%ntdens    = grid_tdens%ncell
    this%npot      = grid_pot%nnode*grid_pot%ambient_dimension
    this%ngrad     = grid_pot%ncell
    !this%ngrad     = grid_pot%ncell*this%ambient_dimension**2
    this%ambient_dimension = grid_tdens%ambient_dimension
    this%nfull     =  this%ntdens +  this%npot

    !
    ! short hand copy 
    !
    lun_out  = ctrl%lun_out 
    lun_err  = ctrl%lun_err
    lun_stat = ctrl%lun_statistics
 
    ntdens   = this%ntdens    
    npot     = this%npot      
    ngrad    = this%ngrad  
    
    ncellpot = grid_pot%ncell

    !
    ! construction of p1 element space
    !
    if ( ( ctrl%debug .eq. 1 ) .and. (lun_out>0) )&
         write( lun_out, *) 'Init p1 Galerkin'
    call this%p1%init(lun_err,this%grid_pot)
    
    !
    ! grad depending elements
    !
     if ( ( ctrl%debug .eq. 1 ) .and. (lun_out>0) )&
          write( lun_out, *) 'Allocation gradient variables'
    allocate(&
         this%grad(ngrad),&
         this%norm_grad(ngrad),&
         this%grad_avg(ntdens),&
         this%norm_grad_avg(ntdens),& 
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member grad norm_grad')
    
    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    if ( (ctrl%debug .ge. 1)  .and. (lun_out>0) ) &
         write(lun_out,'(a)') 'Stiffness Matrix Initiliazation'

    call this%stiff%init(lun_err,&
         npot, npot,  this%p1%nterm_saint_venant_csr,&
         storage_system='csr',is_symmetric=.true.)

    call this%kernel_stiff%init(lun_err,&
         npot,3,is_symmetric=.False.,triangular='N')
    
    this%kernel_stiff%coeff=zero
    do inode = 1,grid_pot%nnode
       this%kernel_stiff%coeff(2*inode-1,1)=one
       this%kernel_stiff%coeff(2*inode  ,2)=one
       this%kernel_stiff%coeff(2*inode-1,3)=this%grid_pot%coord(2,inode)
       this%kernel_stiff%coeff(2*inode  ,3)=-this%grid_pot%coord(1,inode)
    end do
    this%kernel_stiff%coeff(:,1)=this%kernel_stiff%coeff(:,1)/&
         sqrt(one*grid_pot%nnode)
    this%kernel_stiff%coeff(:,2)=this%kernel_stiff%coeff(:,2)/&
         sqrt(one*grid_pot%nnode)

    !
    ! ortogonalization of the third kernel vector
    !
    this%kernel_stiff%coeff(:,3)=&
         this%kernel_stiff%coeff(:,3)&
         - ddot(npot,&
         this%kernel_stiff%coeff(:,3),1,&
         this%kernel_stiff%coeff(:,1),1)*this%kernel_stiff%coeff(:,1) &
         - ddot(npot,&
         this%kernel_stiff%coeff(:,3),1,&
         this%kernel_stiff%coeff(:,2),1)*this%kernel_stiff%coeff(:,2)

    this%kernel_stiff%coeff(:,3) = this%kernel_stiff%coeff(:,3)/ &
         dnrm2(npot,this%kernel_stiff%coeff(:,3),1)

    call this%kernel_stiff_transpose%init(this%kernel_stiff)
    this%kernel_stiff_transpose%name='KT'

    !
    ! init projector K * K^T
    !

    list(1)%linop => this%kernel_stiff 
    list(2)%linop => this%kernel_stiff_transpose
    call this%proj_to_kernel%init(info,6, &
         2, list,'LP')
    this%proj_to_kernel%is_symmetric=.True.

!!$    do  i=1,3
!!$       write(*,*) dnrm2(npot,this%kernel_stiff%coeff(:,i),1) 
!!$    end do
!!$    write(*,*) '1,2',&
!!$         ddot(npot,&
!!$         this%kernel_stiff%coeff(:,1),1,&
!!$         this%kernel_stiff%coeff(:,2),1)
!!$    write(*,*) '1 3',&
!!$         ddot(npot,&
!!$         this%kernel_stiff%coeff(:,1),1,&
!!$         this%kernel_stiff%coeff(:,3),1)
!!$    write(*,*) '2 3',&
!!$         ddot(npot,&
!!$         this%kernel_stiff%coeff(:,2),1,&
!!$         this%kernel_stiff%coeff(:,3),1)
!!$        
    

    

    
    !
    ! Varaibles for Linear solver procedure
    !
    allocate(&
         this%sequence_info_solver(ctrl%max_nonlinear_iterations),&
         this%sequence_build_prec(ctrl%max_nonlinear_iterations),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member sequence_info_solver sequence_build_prec')

    !
    ! create permation from ordering
    ! (p_x, py, pz) to 
    ! (px^1, py^1,pz^1..., px^i, py^i,pz^i, ...., px^n, py^n, pz^n)
    allocate(itemp(npot),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' work array itemp')

    k=0
    do j=1, grid_pot%ambient_dimension
       do i=1,grid_pot%nnode
          k=k+1
          itemp(k) = (i-1)*grid_pot%ambient_dimension + j 
       end do
    end do

    call this%ordering_permutation%init(lun_err,npot,itemp)
    deallocate(itemp,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'init_p1p0', &
         ' work array itemp')
    
    !
    ! Auxiliary var for PCG procedure
    !
    nfull=npot+ntdens
    dim_work=&
         nfull*(ctrl%krylov_nrestart+7)+&
         (ctrl%krylov_nrestart+1)*(ctrl%krylov_nrestart+2)+&
         nfull*ctrl%krylov_nrestart
    call this%aux_bicgstab%init(lun_err, 0, dim_work)

    ! Auxiliary var for PCG procedure  
    call this%aux_newton%init(lun_err,&
         0, 13*ntdens + 5 * npot + 4* (ntdens + npot))

    !           
    ! rhs scratch
    !           
    allocate(&
         this%pot_permuted(npot),&
         this%rhs(npot),&
         this%rhs_ode(ntdens),&
         this%inc_ode(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member rhs')

    ! 
    ! work array
    !           
    allocate(&
         this%tdens_prj(ngrad),&
         this%scr_npot(npot),&
			this%scr_rhs(npot),&
			this%scr_dirichlet(npot),&
         this%scr_ntdens(ntdens),&
			this%scr_ncellpot(ncellpot),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', & 
         ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad')
    allocate(&
         this%scr_ngrad(ngrad),&
         this%scr_nfull(npot+ntdens),&
         this%rhs_full(npot+ntdens),&
         this%scr_integer(npot+ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', & 
        ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad')
       

    call this%identity_npot%eye(npot)
    call this%identity_ntdens%eye(ntdens)
	 
    ! 
    ! work array
    ! 
    allocate(&
         this%gradient_compliance_tdens(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err,&
         err_alloc, 'init_p1p0', &
         ' type p1p0 member'//&
         ' D1, D2, invD2, invD2_D1')
    
  end subroutine init_p1p0


  !>---------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type p1p0sv)
  !>
  !> usage:
  !>     call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !<---------------------------------------------------------
  subroutine kill_p1p0(this,lun_err)     
    use Globals
    implicit none
    class(p1p0sv), intent(inout) :: this
    integer,                          intent(in   ) :: lun_err
    !local
    integer :: res
    logical :: rc

    this%grid_tdens => null()
    this%grid_pot   => null()
    
    !
    ! dimension reset
    !
    this%ntdens    = 0
    this%npot      = 0
    this%ngrad  = 0
    this%ambient_dimension = 0

    

    !
    ! construction of p1 element space
    !
    call this%p1%kill(lun_err)

    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    call this%stiff%kill(lun_err)
    
    !
    ! Varaibles for Linear solver procedure
    !
    ! Auxiliary var for PCG procedure  
    call this%aux_bicgstab%kill(lun_err)


    ! rhs scratch
    deallocate(&
         this%rhs,&
         this%rhs_ode,&
         this%inc_ode,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
         'kill_p1p0', &
         ' type p1p0 member rhs')

    ! 
    ! work array
    ! 
    deallocate(&
         this%tdens_prj,&
         this%scr_rhs,&
         this%scr_npot,&
         this%scr_ntdens,&
         this%scr_ngrad,&
         this%scr_nfull,&
			this%scr_ncellpot,&
         this%gradient_compliance_tdens,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')


    call this%identity_npot%kill()
    call this%identity_ntdens%kill()

  end subroutine kill_p1p0

  !>--------------------------------------------------------
  !> Assembly the rhs of the tdens ode of gfvar ode
  !>--------------------------------------------------------
  subroutine assembly_rhs_ode_tdens(this,&
       ode_inputs,&
       tdens,pot,&
       rhs_ode)
    use DmkInputsData

    implicit none
    class(p1p0sv), intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: pot(this%npot) 
    real(kind=double), intent(inout) :: rhs_ode(this%ntdens) 

    !local
    integer :: icell,j
    integer :: ntdens
    real(kind=double) :: min, max, pflux, pmass,decay
    real(kind=double) :: ptrans, gf_pflux, gf_pmass
    real(kind=double) :: penalty_factor

    ! Degrees of freedom transport density
    ntdens = this%ntdens

    pflux = ode_inputs%pflux ! one 
    decay = ode_inputs%decay ! one 
    pmass = ode_inputs%pmass ! one
    
    ! Initialize rhs ODE
    rhs_ode = zero
    
    ! Compute elastic energy density for each cell of grid_tdens
    call this%build_gradient_compliance(pot,&
         ode_inputs%mu_lame,ode_inputs%lambda_lame,&
         this%gradient_compliance_tdens)

    ! Assembly procedure rhs (1)
    do icell = 1,ntdens
       rhs_ode(icell) =  (tdens(icell)**pflux) * &
			 (two*this%gradient_compliance_tdens(icell)) - &
			 (tdens(icell)**pmass) 
    end do

	 ! Multiplication by area of the cells of grid_tdens
    rhs_ode = rhs_ode * this%grid_tdens%size_cell

  end subroutine assembly_rhs_ode_tdens
  
  subroutine assembly_grad_lyap_gfvar(this,&
       ode_inputs,&
       tdpot,&
       gfvar,tdens,pot,&
       grad_lyap)
    use DmkInputsData

    implicit none
    class(p1p0sv), intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    real(kind=double), intent(in   ) :: gfvar(this%ntdens) 
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: pot(this%npot)     
    real(kind=double), intent(inout) :: grad_lyap(this%ntdens) 
    !local
    integer :: icell,j
    integer :: ntdens
    real(kind=double) :: min,max, pflux, pmass,decay,pode
    real(kind=double) :: ptrans, gf_pflux, gf_pmass,power
    real(kind=double) :: penalty_factor

    ntdens = this%ntdens

    pflux = ode_inputs%pflux
    pode  = ode_inputs%pode
    decay = ode_inputs%decay
    pmass = ode_inputs%pmass

    ptrans   = two/ (two-pflux) 
    gf_pflux = ptrans-one 
    gf_pmass = pmass * ptrans - one  

    if( abs(pode-two) > 1e-12) then
       write(*,*) 'only pode=2.0, in assembly_grad_lyap_gfvar'
       stop
    end if
    !
    ! lyapunov is 
    !
    ! 1/2 (a(x) + b(x)*tdens |\Grad pot|^2 + 1/2 decay kappa^2 tdens^wmass/wmass
    !
    ! tdens=trans(gfvar)
    !
    ! Grad lyap = -1/2|\Grad pot|^2 trans'+decay kappa^2 tdens^(wmass-1) trans'
    !
    
    !
    ! compute -1/2|\Grad pot|^2 trans'
    !
    call this%build_gradient_compliance(pot,&
         ode_inputs%mu_lame,ode_inputs%lambda_lame,&
         this%gradient_compliance_tdens)
    !call this%build_gradient_compliance_tdens(pot,two,this%gradient_compliance_tdens)
    call eval_trans_prime(ntdens,two,pflux,gfvar,this%scr_ntdens)
    grad_lyap = -onehalf*ode_inputs%beta*this%scr_ntdens*this%gradient_compliance_tdens
    
    !
    ! +decay kappa^2 tdens^(wmass-1) trans'
    !    
    power=tdpot%wmass_exponent(two,pflux,pmass)

    grad_lyap = grad_lyap + &
         onehalf * decay * &
         (ode_inputs%kappa**2)* (tdens ** (power - one ))  * this%scr_ntdens

    !
    ! penalization 
    !
    penalty_factor = ode_inputs%penalty_factor
    !write(*,*) 'penalty_factor',penalty_factor
    if ( abs( penalty_factor) .gt. small) then
       grad_lyap = grad_lyap + &
            penalty_factor * ode_inputs%penalty_weight * &
            ( gfvar - ode_inputs%penalty_function ) 
    end if

    ! multiply by size_cell
    grad_lyap=grad_lyap * this%grid_tdens%size_cell


  end subroutine assembly_grad_lyap_gfvar

  !>------------------------------------------------------------
  !> Assembly the rhs of the tdens ode of gfvar ode
  !<-------------------------------------------------------------
  subroutine get_increment_ode(this, rhs_ode,inc_ode)
    implicit none
    class(p1p0sv), intent(in   ) :: this
    real(kind=double),                intent(in   ) :: rhs_ode(this%ntdens)
    real(kind=double),                intent(inout) :: inc_ode(this%ntdens)

	 ! Compute the increment for the solution of ODE
    inc_ode = rhs_ode/this%grid_tdens%size_cell

  end subroutine get_increment_ode
  

  !>----------------------------------------------------------------
  !> Function to eval.
  !> $ var(\TdensH^k):=\frac{
  !>                     \| \tdens^{k+1} - \tdens^{k} \|_{L2} 
  !>                   }{ 
  !>                     \| \tdens^{k+1} \|_{L2}}  
  !>                   } $
  !> (procedure public for type specp0) 
  !> 
  !> usage: call var%err_tdesn()
  !>    
  !> where:
  !> \param  [in ] var       -> type(specp0) 
  !> \result [out] var_tdens -> real. Weighted var. of tdens
  !<----------------------------------------------------------------
  function compute_tdpot_variation(this,tdpot,ctrl) result(var)
    use Globals
    use TdensPotentialSystem
    implicit none
    class(p1p0sv),    intent(in   ) :: this
    class(tdpotsys),    intent(in   ) :: tdpot
    class(DmkCtrl),     intent(in   ) :: ctrl
    real(kind=double) :: var
    !local
    real(kind=double) :: norm_old, norm_var,exponent

    ! $ var_tdens = 
    !              frac{
    !                   \|\tdens^{n+1}-\tdens^n\|_{L^2}
    !                  }{
    !                   \deltat \|\tdens^{n}\|_{L^2}
    !                  }$
    exponent = ctrl%norm_tdens
    norm_old = this%grid_tdens%normp_cell(exponent,tdpot%tdens_old)
    norm_var = this%grid_tdens%normp_cell(exponent,tdpot%tdens-tdpot%tdens_old)

    var =  norm_var / ( ctrl%deltat * norm_old )
  end function compute_tdpot_variation




  !>----------------------------------------------------
  !>
  !<---------------------------------------------------    
  subroutine reset_controls_after_update_failure(this,&
       info,&
       ode_inputs,&
       tdpot,&
       original_ctrl,&
       ctrl,&
       inputs_time,&
       ask_for_inputs)
    use DmkInputsData 
    use TdensPotentialSystem
    implicit none
    class(p1p0sv), intent(inout) :: this
    integer,           intent(in   ) :: info
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(DmkCtrl),     intent(in   ) :: original_ctrl
    type(DmkCtrl),     intent(inout) :: ctrl
    real(kind=double), intent(inout) :: inputs_time
    logical,           intent(inout) :: ask_for_inputs

    ! local
    logical :: rc
    integer :: lun_err
    integer :: digit1,digit2,digit3

    lun_err=ctrl%lun_err

    digit1=info/100 
    digit2=mod(info,100)/10
    digit3=mod(info,10)

    select case ( ctrl%time_discretization_scheme )
    case(1)
       !
       ! Explicit Euler
       !
       ctrl%deltat =  ctrl%deltat/2.0d0
       inputs_time = tdpot%time
       ask_for_inputs = .False.
    case (5)
       !
       ! NewImplicit Euler 
       !
       ctrl%deltat=ctrl%deltat/ctrl%deltat_expansion_rate
       inputs_time = tdpot%time
       ask_for_inputs = .True.

    case (7)
       !
       ! NewImplicit Euler Newton Gfvar
       !
       ctrl%deltat=ctrl%deltat/ctrl%deltat_expansion_rate
       inputs_time = tdpot%time
       ask_for_inputs = .True. 
    case (2)
       !
       ! Implicit Explicit Euler
       !

       !
       ! shrink time step
       !
       ctrl%deltat = ctrl%deltat / ctrl%deltat_expansion_rate
    end select

    
  end subroutine reset_controls_after_update_failure

  !>----------------------------------------------------
  !>
  !<---------------------------------------------------    
  subroutine  set_controls_next_update(this,&   
          ode_inputs,&
          tdpot,&
          ctrl)
    use DmkInputsData 
    use TdensPotentialSystem
    implicit none
    class(p1p0sv),     intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(DmkCtrl),     intent(inout) :: ctrl
    
    !
    !    local
    real(kind=double) :: deltat,D22max,J22min,gradmax
    real(kind=double),allocatable :: J22(:) 
    character(len=256) :: out_format
    real(kind=double) :: sup_rhs
  

    !write(*,*) 'ctrl%deltat_control '
    !
    ! time step controls
    !
    !write(*,*) 'ctrl%deltat_control ', ctrl%deltat_control ,ctrl%deltat_expansion_rate
    select case ( ctrl%deltat_control ) 
    case( 1 )
       ! constant time step
       deltat = ctrl%deltat        
		 !out_format = ctrl%formatting('ar')
		 !write(ctrl%lun_out,out_format) 'Constant TimeStep: ', deltat
    case( 2 )
       ! increasing time step
       deltat=ctrl%deltat * ctrl%deltat_expansion_rate
       deltat=min(deltat,ctrl%deltat_upper_bound)
       deltat=max(deltat,ctrl%deltat_lower_bound)
    case ( 3 )
       select case (ctrl%time_discretization_scheme)
       case (1)
          call this%assembly_rhs_ode_tdens(&
               ode_inputs,&
               tdpot%tdens,tdpot%pot,&
               this%scr_ntdens)
          
          sup_rhs= maxval(this%scr_ntdens/this%grid_tdens%size_cell)
          if ( sup_rhs < zero ) then
             ctrl%deltat = ctrl%deltat_upper_bound
          else
             ctrl%deltat=(1.0d0-ctrl%deltat_lower_bound)/sup_rhs
          end if
          ctrl%deltat=min(ctrl%deltat,ctrl%deltat_upper_bound)


          sup_rhs= maxval(abs(this%scr_ntdens/this%grid_tdens%size_cell))
          ctrl%deltat=1.0d0/sup_rhs
          ctrl%deltat=max(min(ctrl%deltat,ctrl%deltat_upper_bound),ctrl%deltat_lower_bound)
          
       case (5)
       case (7)

       case (6)
       
       end select
    end select
          


    
  end subroutine set_controls_next_update



    
  
  !>-------------------------------------------------
  !> Precedure to compute gfvar variable for GF dynamics
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine tdens2gfvar(ntdens,pode,pflux,tdens,gfvar)
    implicit none
    integer, intent(in) :: ntdens
    real(kind=double), intent(in) :: pode     
    real(kind=double), intent(in) :: pflux
    real(kind=double), intent(in) :: tdens(ntdens)
    real(kind=double), intent(out) :: gfvar(ntdens)
    !local
    real(kind=double) :: power

    !
    ! gfvar= tdens^{\frac{pflux}/{2-beta}}
    !
    if ( abs(pode-2.0d0)< 1.0d-12) then
       if ( pflux < 2.0d0) then
          power = (2.0d0 - pflux) / 2.0d0
          gfvar = tdens**power
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          gfvar = log(tdens)
       end if
    else
       write(6, *) 'In  procedure . tdens2gfvar', &
            ' transformation not defined for pode = ',pode
       stop
    end if

  end subroutine tdens2gfvar

  !>-------------------------------------------------
  !> Precedure to compute trans'(gfvar)
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine eval_trans_prime(ntdens,pode,pflux,gfvar,transformation_prime)
    use Globals
    implicit none
    integer,           intent(in ) :: ntdens
    real(kind=double), intent(in ) :: pode     
    real(kind=double), intent(in ) :: pflux
    real(kind=double), intent(in ) :: gfvar(ntdens)
    real(kind=double), intent(inout) :: transformation_prime(ntdens)
    !local
    real(kind=double) :: power

    !
    ! gfvar= tdens^{\frac{pflux}/{2-beta}}
    !
    if ( abs(pode-2.0d0)< 1.0d-12) then
       if ( pflux < 2.0d0) then
          power = 2.0d0 / (2.0d0 - pflux)
          transformation_prime = power*gfvar**(power-one)
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          transformation_prime = exp(gfvar) 
       end if
    else
       write(6, *) 'In  procedure eval_trans_prime', &
            ' transformation not defined for pode = ',pode
       stop
    end if

  end subroutine eval_trans_prime


  !>-------------------------------------------------
  !> Precedure to compute trans''(gfvar)
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine eval_trans_second(ntdens,pode,pflux,gfvar,transformation_second)
    implicit none
    integer, intent(in) :: ntdens
    real(kind=double), intent(in) :: pode     
    real(kind=double), intent(in) :: pflux
    real(kind=double), intent(in) :: gfvar(ntdens)
    real(kind=double), intent(out):: transformation_second(ntdens)
    !local
    real(kind=double) :: power

    !
    ! gfvar= tdens^{\frac{pflux}/{2-beta}}
    !
    if ( abs(pode-2.0d0)< 1.0d-12) then
       if ( pflux < 2.0d0) then
          power = 2.0d0 / (2.0d0 - pflux)
          transformation_second = power*(power-one)*gfvar**(power-two)
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          transformation_second = exp(gfvar) 
       end if
    else
       write(6, *) 'In  procedure eval_trans_second', &
            ' transformation not defined for pode = ',pode
       stop
    end if
    
  end subroutine eval_trans_second
  

  !>-------------------------------------------------
  !> Precedure to compute gfvar variable for GF dynamics
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine gfvar2tdens(ntdens,pode,pflux,gfvar,tdens)
    implicit none
    integer,           intent(in) :: ntdens
    real(kind=double), intent(in) :: pode
    real(kind=double), intent(in) :: pflux
    real(kind=double), intent(in) :: gfvar(ntdens)
    real(kind=double), intent(inout) :: tdens(ntdens)    
    !local
    real(kind=double) :: power
    
    if ( abs(pode-2.0d0)< 1.0d-12) then
       if ( pflux < 2.0d0) then
          power = 2.0d0 / (2.0d0 - pflux)
          tdens = gfvar**power
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          tdens = log(gfvar)
       end if
    else
        write(6, *) 'In  procedure . gfvar2tdens', &
            ' transformation not defined for pode = ',pode
    end if
    
  end subroutine gfvar2tdens



end module DmkSaintVenantP1P0

