module TdensPotentialSystem
  use KindDeclaration
  use Globals
  implicit none
  private 
  public :: tdpotsys, tdpotsys_constructor, tdpotsys_destructor, tdpotsys_time_evolution_info2str
  !>-------------------------------------------------------------------
  !> Structure Variable containg tdens-potential member for the discretization
  !> of tdens- ODE equation
  !>
  !> \begin{align}
  !>  \label{eq:dmk}
  !>  &\Lambda( \Tdens \Lambda^{T} \Pot) = f \\
  !>  &\Tdens'=(\Tdens )^{\Pflux}|\Lambda^{T} \Pot|^{\Pode}-decay kappa \Tdens^Pmass
  !> \end {align}
  !> 
  !> where $\Lambda$ is a linear operator.
  !> Lambda may 
  !> -dense or sparse matrix  (Sparse optimization problem)
  !> -signed incidence matrix (Sparse optimization problem of graphs)
  !> -matrices arising from the spatial discretiazion of the 
  !>  contininouos verison of equation \ref{eq:dmk}
  !>-------------------------------------------------------------------
  type :: tdpotsys
     !> Logical to check initiliazation
     logical :: is_initialized=.False.
     !> Array length of tdens and pot
     !> Length of tdens array
     integer :: ntdens
     !> Length of pot array
     integer :: npot
     !> Length of full system tdens + pot 
     integer :: nfull
     !> Number of potential to be found
     !> Default=1
     !> Continuous DMK : number_of_potentials=1
     !> Algebraic  DMK : number_of_potentials=1
     !> Graph      DMK : number_of_potentials=1
     !> Multi-commodity DMK : number_of_potentials>1
     !> Shape Optimization DMK : number_of_potentials=ambient dimension
     integer :: number_of_potentials=1
     !> Dimension(ntdens)
     !> Piecewise constant conductivity
     real(kind=double), allocatable :: tdens(:)
     !> Piecewise constant cond. at previuos time step
     !> Used in evol_time
     real(kind=double), allocatable :: tdens_old(:)
     !> Dimension(npot)
     !> Coefficient of P1-galerkin Discretization
     real(kind=double), allocatable :: pot(:)
     !> Dimension(npot)
     !> Coefficient of P1-galerkin Discretization
     real(kind=double), allocatable :: pot_old(:)
     !> Dimension(ntdens)
     !> Gradient flow variable
     real(kind=double), allocatable :: gfvar(:)
     !> Dimension(ntdens)
     !> Gradient flow variable
     real(kind=double), allocatable :: gfvar_old(:)
     !> Dimension velocity
     integer :: nvel
     !> Dimension(ntdens)
     !> Piecewise constant conductivity
     real(kind=double), allocatable :: velocity(:)
     !>----------------------------------------------------------
     logical :: first=.False.
     !> Error flag if the variable 
     !> Tdens Pot ODEin and Grad variable
     !> at the same time 
     logical :: tdpot_syncr=.false.
     !> Error flag if the variable 
     !> Tdens Pot ODEin and Grad variable
     !> at the same time 
     logical :: all_syncr=.false.
     !> Flag for reverse comunication cycel to skip update  
     !> Tdens Pot ODEin and Grad variable
     !> at the same time 
     integer :: skip_update = 0
     !>-----------------------------------------------------------------
     !> Linear system variables
     !>-----------------------------------------------------------------
     !> Number of fix point iteration
     integer :: time_iteration=0
     !> Number of updates done
     integer :: time_update=0
     !> Time where data tdens, pot, gfvar are defined
     real(kind=double) :: time=0.0d0
     !> Previous time
     real(kind=double) :: time_old=0.0d0
     !> Laste deltat update
     !> Difference in time between the time step for
     !> tdens, pot and tdens_old, tdens_pot
     real(kind=double) :: deltat=0.0d0     
     !> Flag for error in preconditioner building
     !>   info_prec=0 => no errors
     !> other flags>0 are descibed in precondioner modules
     integer :: info_prec
     !> Total number of linear system
     integer :: nlinear_system=0
     !>--------------------------------------------------
     !> Scratch varibles for storing 
     !> Averaged cgs iterations for time step
     integer :: iter_media
     !> Total cgs iterations 
     integer :: total_iteration
     !> Scratch varible to control the calulation of prec.
     integer :: iter_last_prec
     !> Scratch varible to store number of iteration
     integer :: iter_first_system
     !> Scratch varible to store number of iteration
     integer :: total_iterations_linear_system=0
     !> Scratch varible to store number of iteration
     integer :: total_number_linear_system=0
     !> Scratch varible to control the calulation of prec.
     integer :: itemp_last_prec
     !> Scratch varible to control the calulation of prec.
     integer :: ref_iter
     !> Scratch varible to store newton iteration when 
     !> Preconditioner was computed
     integer :: iter_newton_last_prec=0
     !> Scratch varible to count newton restart
     integer :: nrestart_update=0
     !> Scratch varible to count newton restart
     integer :: nrestart_invert_jacobian=0
     !> Scratch varible to store the resisual of linear system
     !> | Stiff(\Tdens) \Pot - rhs | / | rhs |
     real(kind=double) :: res_elliptic
     !> Integer for saving procedure
     integer :: int_before
     !> CPU TIMING
     real(kind=double) :: cpu_time=zero
     !> CPU TIMING
     real(kind=double) :: cpu_wasted=zero
     !> CPU TIMING EACH UDPATE
     real(kind=double) :: cpu_update=zero
     !> CPU TIMING
     real(kind=double) :: cpu_update_wasted=zero
     !>---------------------------------------------------------
     !> Real valued quantities
     !>---------------------------------------------------------
     !> Real scalar containing 
     !> $\int \Tdens \dx$
     real(kind=double) :: mass_tdens
     !> Real scalar containing 
     !> $1/2 \int \Tdens^{P(\Pflux)} \dx / P(\Pflux)$
     real(kind=double) :: weighted_mass_tdens
     !> Real scalar containing 
     !> $1/2 \int \Tdens |\Grad \Pot|^2 \dx$
     real(kind=double) :: energy
     !> Real scalar containing 
     !> $\Lyap(\Tdens)+\Wmass(Tdens)$
     real(kind=double) :: lyapunov
     !> Real scalar containing 
     !> $\min(\Tdens)$
     real(kind=double) :: min_tdens
     !> Real scalar containing 
     !> $\max(\Tdens)$
     real(kind=double) :: max_tdens
     !> Real scalar containing 
     !> $\max(|\Vel|)$
     real(kind=double) :: max_velocity
     !> Real scalar containing 
     !> $\max(|\Grad \Pot|)$
     real(kind=double) :: max_nrm_grad
     !> Real scalar containing 
     !> $\max(|\Grad \Pot|)$
     real(kind=double) :: max_nrm_grad_avg
     !> Real scalar containing 
     !> $\max(D3)=max(\Jac_{2, 2})$
     real(kind=double) :: max_D3
     !> Real scalar containing 
     !> Real scalar containing 
     !> $\int |\Vel|^{\Pvel} \dx$
     real(kind=double) :: integral_flux_pvel
     !> Real scalar containing 
     !> $\int |\Vel|^{\Pvel} - \Pot \Forcing\dx$
     real(kind=double) :: duality_gap
     !> Real scalar containing 
     !> Errors w.r.t. Tdens solution
     real(kind=double) :: err_tdens
     !> Real scalar containing 
     !> Errors w.r.t. Pot solution
     real(kind=double) :: err_pot
     !> Real scalar containing 
     !> Errors w.r.t. Pot solution
     real(kind=double) :: err_wasserstein
     !> Real scalar containing 
     !> estimated Wasserstein 1 distance
     real(kind=double) :: wasserstein_distance
     !>--------------------------------------------------------
     !> Info for non-linear solver
     !> Number of fix point iteration
     integer :: iter_nonlinear
     !>--------------------------------------------------------
     !> Scratch arrays
     !>--------------------------------------------------------
     !> Scratch var_tdens
     real(kind=double) :: system_variation=1e30
     !> Logical if steady state is achivied
     logical :: steady_state = .False.
     !>-----------------------------------------------------------
     !> Scratch arrays for general porpuse
     !>----------------------------------------------------------
     !> Dimension (ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_ntdens(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: scr_npot(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_nfull(:)
     !>---------------------------------------------------------
     !> Selcection procedure
     !>---------------------------------------------------------
     !> Number of active tdens
     integer :: ntdens_on
     !> Number of active nodes
     integer :: npot_on
     !> Number of active tdens
     integer :: ntdens_off
     !> Number of active nodes
     integer :: npot_off
     !> Dimension (tdens)
     !> List of active tdens
     logical, allocatable :: onoff_tdens(:)
     !> Dimension (npot)
     !> List of active tdens
     logical, allocatable :: onoff_pot(:)
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: active_tdens(:)
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: active_pot(:)
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: inactive_tdens(:)
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: inactive_pot(:)
   contains
     !> Static constructor 
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: init => init_tdpotsys
     !> Static destructor
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: kill => kill_tdpotsys
     !> Info procedure on evolution of tdens-potential system
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: info_time_evolution => info_time_evolution_tdpotsys
     !> Info procedure on the state of tdens-potential system
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: info_functional =>  info_functional_tdpotsys
     !> Saving procedure for data
     !> (procedure public for type tdpotsys)
     !procedure, public , pass :: write2dat => write2dat0_tdpotsys
     !> Procedure to transform tdens into gfvar
     !> (procedure public for type tdpotsys)
     procedure, public , nopass :: tdens2gfvar
     !> Procedure to transform tdens into gfvar
     !> (procedure public for type tdpotsys)
     procedure, public , nopass :: gfvar2tdens
     !> Procedure to compute exponent $\Pvel$ in
     !> min_{\vel} |\vel^\pvel| : \LinOp(\Vel) = Rhs
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: pvel_exponent
     !> Procedure to compute exponent $p$ 
     !> Weight mass funcitional
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: wmass_exponent
  end type tdpotsys
contains
  !>--------------------------------------------------------------------
  !> Static Constructor, number of tdens and pot variabele.
  !> Optimal multi-commodity. Default=1.
  !> (public procedure for type tdpotsys)
  !> 
  !> usage: call var%init(lun_err,ntdens,npot,[number_of_potentials])
  !> 
  !> where:
  !> \param[in ]            lun_err -> integer. IO unit for errors
  !> \param[in ]              ndens -> integer. Nmb. of tdens variable
  !> \param[in ]              ndens -> integer. Nmb. of pot variable
  !> \param[in,optional] number_of_potentials -> integer. Nmb. of commodity
  !>---------------------------------------------------------------------
  subroutine init_tdpotsys(this,&
       lun_err,&
       ntdens, npot, &
       number_of_potentials)
    use Globals
    implicit none
    class(tdpotsys), intent(inout) :: this
    integer,         intent(in   ) :: lun_err
    integer,         intent(in   ) :: ntdens
    integer,         intent(in   ) :: npot
    integer,optional,intent(in   ) :: number_of_potentials
    !local
    logical :: rc
    integer :: res
    integer :: i,j,start,end,icell_sub
    integer,allocatable :: ntemp(:), temp(:,:)
    real(kind=double) ::dnrm2
    integer :: flag,nzc

    
    ! free memory if already allocated
    if(this%is_initialized) call this%kill(lun_err)

    
    !
    ! dimension assignment
    !
    this%ntdens    = ntdens
    this%npot      = npot
    if (present(number_of_potentials)) this%number_of_potentials=number_of_potentials

    this%nfull     = this%ntdens + this%npot


    ! system real variables
    allocate (&
         this%tdens(ntdens),&
         this%tdens_old(ntdens),&
         this%pot(npot),&
         this%pot_old(npot),&
         this%gfvar(ntdens),&
         this%gfvar_old(ntdens),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, &
         'init_tdpotsys', &
         'member tdens, tdens_old, pot, pot_old, gfvar, gfvar_old')
   
    this%tdpot_syncr = .false.
    this%all_syncr = .false.

    ! 
    ! work array
    ! 
    allocate(&
         this%scr_npot(npot),&
         this%scr_ntdens(ntdens),&
         this%scr_nfull(npot+ntdens),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, &
         ' init_tdpotsys', &
         ' member tdens_prj'//&
         ' scr_ntdens scr_npot')

    ! 
    ! work array
    ! 
    allocate(&
         this%onoff_tdens(ntdens),&
         this%onoff_pot(npot),&
         this%active_tdens(ntdens),&
         this%active_pot(npot),&
         this%inactive_tdens(ntdens),&
         this%inactive_pot(npot),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, &
         ' init_tdpotsys', &
         ' member tdens_prj'//&
         ' onoff_tdens, onoff_pot, active_tdens, '//&
         'active_pot, inactive_tdens, inactive_pot ')


    !
    ! initilaze with all nodes and cell active
    !
    this%ntdens_on = ntdens
    do i=1,ntdens
       this%active_tdens(i) = i
    end do
    this%ntdens_off = 0
    this%inactive_tdens(:)=0
    
    this%npot_on = npot*this%number_of_potentials
    do i=1,npot
       this%active_pot(i) = i
    end do
    this%npot_off = 0
    this%inactive_pot(:)=0
       
    this%is_initialized=.True.
    
  end subroutine init_tdpotsys

  subroutine tdpotsys_constructor(this,&
       lun_err,&
       ntdens, npot,&
       number_of_potentials)  
    implicit none
    type(tdpotsys), target,intent(inout) :: this
    integer,         intent(in   ) :: lun_err
    integer,         intent(in   ) :: ntdens
    integer,         intent(in   ) :: npot
    integer,optional,intent(in   ) :: number_of_potentials


    call this%init(lun_err,ntdens,npot,number_of_potentials=number_of_potentials)
  end subroutine tdpotsys_constructor


  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type tdpotsys)
  !>
  !> usage:
  !>     call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !<-----------------------------------------------------------    
  subroutine kill_tdpotsys(this,lun_err)     
    implicit none
    class(tdpotsys), intent(inout) :: this
    integer,         intent(in   ) :: lun_err
    !local
    logical :: rc
    integer :: res
    
    !
    ! free array tdens pot
    !
    deallocate(&        
         this%tdens,&
         this%tdens_old,&
         this%pot,&
         this%pot_old,&
         this%gfvar,&
         this%gfvar_old,&
         stat=res)
    if(res .ne. 0) write(lun_err,*) &
         ' Error in procdedure kill_tdpotsys '//&
         ' Failure in deallocation of tpye tdpotsys '// &
         ' member tdens, tdens_old, pot, pot_old'

    !
    ! free work arrays
    !
    deallocate(&
         this%scr_npot,&
         this%scr_ntdens,&
         this%scr_nfull,&
         stat=res)
    if(res .ne. 0) write(lun_err,*) &
         'Error in procdedure kill_tdpotsys '//&
         ' Failure in allocation of tpye tdpotsys '// &
         ' member rhs, rhs_ode, tdens_prj,'//&
         ' scr_npot'
  
    this%is_initialized=.False.
  end subroutine kill_tdpotsys

  subroutine tdpotsys_destructor(this,lun_err)     
    implicit none
    type(tdpotsys), intent(inout) :: this
    integer,         intent(in   ) :: lun_err

    call this%kill(lun_err)

  end subroutine tdpotsys_destructor

  !>-------------------------------------------------------------
  !> Subroutine to print current time evolution 
  !> state of sytem. Currente time iteration, current time and current
  !> varaition of the system.
  !<-----------------------------------------------------------   
  subroutine info_functional_tdpotsys(this,lun)
    implicit none
    class(tdpotsys), intent(in) :: this
    integer,         intent(in) :: lun
    !local
    character(len=256) :: out_format
    character(len=3)   :: sep=' | '
    
    out_format = '(a,a,a,1pe9.2,a,a,a,1pe9.2,a,a,a,1pe9.2)'
    write(lun,out_format) &
         'tdens : ',&
         'min  ',' = ', this%min_tdens, sep,&
         'max  ',' = ', this%max_tdens,sep,&
         'wmass',' = ', this%weighted_mass_tdens

    out_format = '(a,a,a,1pe9.2,a,a,a,1pe9.2,a,a,a,1pe9.2)'
    write(lun,out_format) &
         'funct : ',&
         'ene  ',' = ', this%energy, sep,&
         'lyap ',' = ', this%lyapunov, sep, &
         'dgap ',' = ', this%duality_gap

  end subroutine info_functional_tdpotsys
  


  !>-------------------------------------------------------------
  !> Subroutine to print current time evolution 
  !> state of sytem. Currente time iteration, current time and current
  !> varaition of the system.
  !<-----------------------------------------------------------       
  subroutine info_time_evolution_tdpotsys(this,lun,orderout)
    use Globals
    implicit none
    class(tdpotsys),  intent(in) :: this
    integer,          intent(in) :: lun
    logical, optional, intent(in) :: orderout
    !local
    character(len=256) :: out_format
    character(len=256) :: strout
    character(len=3)   :: sep=' | '
    character(len=3)   :: order
    !
    ! Print iteration number, time and var_tdens
    !
    if ( .not. present(orderout)) then
       order='yes'
    else
       if ( orderout ) then
          order='no'
       else
          order='yes'
       end if
    end if
    if ( this%time_iteration .gt. 0 ) then
       out_format='(a,I5,a,a,a,1pe9.2,a,a,a,1pe9.2,a,I3,a,a,a,a,1pe9.2)'
       write(strout,out_format) &
            'iter =', this%time_iteration,&
            sep,'time',' = ' , this%time,&
            sep,'cpu',' = ', this%cpu_time,&
            's (',int(100*(one-this%cpu_wasted/this%cpu_time)),'%) ',&
            sep,'var',' = ' , this%system_variation
    else
       out_format='(a,I5,a,a,a,1pe9.2,a,a,a,1pe9.2)'
       write(strout,out_format) &
            'iter. =', this%time_iteration,&
            sep,'time ',' = ', this%time,&
            sep,'cpu',' = ', this%cpu_time
    end if
    
    write(lun,'(1x,a)') etb(strout)

  end subroutine info_time_evolution_tdpotsys

  !>------------------------------------------------------------
  !> Procedure evaluating $pvel$ exponent wihich is the candidate
  !> branch exponent for give pflux and pmass
  !>-------------------------------------------------------------
  function pvel_exponent(this,pode,pflux,pmass) result(exponent)
    implicit none
    class(tdpotsys),   intent(in   ) :: this
    real(kind=double), intent(in   ) :: pode
    real(kind=double), intent(in   ) :: pflux
    real(kind=double), intent(in   ) :: pmass
    real(kind=double) :: exponent
    !local 

    if ( ( abs(pode-2.0d0)>1e-12)) then
       if ( ( abs(2 * pmass - pflux ) > 1.0d-10 ) .and. &
            ( abs(pmass             ) > 1.0d-10 ) ) then
          exponent = (2 * pmass - pflux ) / pmass
       end if
    else
       exponent = zero
    end if

    if ( abs(pode-2.0d0)<1e-12 ) then
       if ( ( abs(1 - pflux + pmass ) > 1.0d-10 ) .and. &
            ( abs(2 - pflux + pmass ) > 1.0d-10 ) ) then
          exponent = 2 * (1 - pflux + pmass ) / (2 - pflux + pmass )
       else
          exponent = zero
       end if
    end if

  end function pvel_exponent

  !>------------------------------------------------------------
  !> Procedure evaluating $pvel$ exponent wihich is the candidate
  !> branch exponent for give pflux and pmass
  !>-------------------------------------------------------------
  function wmass_exponent(this,pode,pflux,pmass) result(exponent)
    implicit none
    class(tdpotsys),   intent(in   ) :: this
    real(kind=double), intent(in   ) :: pode
    real(kind=double), intent(in   ) :: pflux
    real(kind=double), intent(in   ) :: pmass
    real(kind=double) :: exponent
   

    exponent = zero

    if ( ( abs(pode-2.0d0)>1e-12)) then
       if ( ( abs(2 * pmass - pflux ) > 1.0d-10 ) .and. &
            ( abs(pflux             ) > 1.0d-10 ) ) then
          exponent = (2.0d0 * pmass - pflux ) / pflux
       else
          exponent = zero
       end if
    end if

    if ( ( abs(pode-2.0d0)<1e-12)) then
       if (  abs( pmass + one - pflux  ) > 1.0d-10 ) then 
          exponent = 1.0d0 + pmass - pflux 
       else
          exponent = zero
       end if
    end if

  end function wmass_exponent


  !>-------------------------------------------------
  !> Precedure to compute gfvar variable for GF dynamics
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine tdens2gfvar(ntdens,pode,pflux,tdens,gfvar)
    implicit none
    integer, intent(in) :: ntdens
    real(kind=double), intent(in) :: pode     
    real(kind=double), intent(in) :: pflux
    real(kind=double), intent(in) :: tdens(ntdens)
    real(kind=double), intent(out) :: gfvar(ntdens)
    !local
    real(kind=double) :: power
    
    !
    ! gfvar= tdens^{\frac{pflux}/{2-beta}}
    !
    if ( abs(pode-2.0d0)< 1.0d-12) then
       if ( pflux < 2.0d0) then
          power = (2.0d0 - pflux) / 2.0d0
          gfvar = tdens**power
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          gfvar = log(tdens)
       end if
    else
       write(6, *) 'In  procedure . tdens2gfvar', &
            ' transformation not defined for pode = ',pode
       stop
    end if
    
  end subroutine tdens2gfvar

  !>-------------------------------------------------
  !> Precedure to compute gfvar variable for GF dynamics
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine gfvar2tdens(ntdens,pode,pflux,gfvar,tdens)
    implicit none
    integer,           intent(in) :: ntdens
    real(kind=double), intent(in) :: pode
    real(kind=double), intent(in) :: pflux
    real(kind=double), intent(in) :: gfvar(ntdens)
    real(kind=double), intent(inout) :: tdens(ntdens)    
    !local
    real(kind=double) :: power
    
    if ( abs(pode-2.0d0)< 1.0d-12) then
       if ( pflux < 2.0d0) then
          power = 2.0d0 / (2.0d0 - pflux)
          tdens = gfvar**power
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          tdens = log(gfvar)
       end if
    else
        write(6, *) 'In  procedure . tdens2gfvar', &
            ' transformation not defined for pode = ',pode
    end if
    
  end subroutine gfvar2tdens

  function tdpotsys_time_evolution_info2str(this) result(str)
    use Globals
    implicit none
    type(tdpotsys),     intent(in   ) :: this
    character(len=256)  :: str
    !
    character(len=256)  :: out_format
    character(len=3)   :: sep=' | '
    
    if ( this%time_iteration .gt. 0 ) then
       out_format='(a,I5,a,a,a,1pe9.2,a,a,a,1pe9.2,a,I3,a,a,a,a,1pe9.2)'
       write(str,out_format) &
            'iter =', this%time_iteration,&
            sep,'time',' = ' , this%time,&
            sep,'cpu',' = ', this%cpu_time,&
            's (',int(100*(one-this%cpu_wasted/this%cpu_time)),'%) ',&
            sep,'var',' = ' , this%system_variation
    else
       out_format='(a,I5,a,a,a,1pe9.2,a,a,a,1pe9.2)'
       write(str,out_format) &
            'iter. =', this%time_iteration,&
            sep,'time ',' = ', this%time,&
            sep,'cpu',' = ', this%cpu_time
    end if

  end function tdpotsys_time_evolution_info2str
 
end module TdensPotentialSystem

