!>----------------------------
!> Test Comments
!<----------------------------
subroutine otpdmk(&
     nnodeincell,nnode,ncell, &
     topol,coord,&
     pflux,forcing,&
     tdens,pot,&
     simple_ctrl,info)
  use KindDeclaration
  use Globals
  use TdensPotentialSystem
  use DmkInputsData
  use DmkP1P0Discretization
  use DmkControls
  use AbstractGeometry
  use TimeInputs, only : write_steady
      
  implicit none
  integer,           intent(in   ) :: nnodeincell,nnode,ncell
  integer,           intent(in   ) :: topol(nnodeincell,ncell)
  real(kind=double), intent(in   ) :: coord(3,nnode)
  real(kind=double), intent(in   ) :: pflux
  real(kind=double), intent(in   ) :: forcing(ncell)
  real(kind=double), intent(inout) :: tdens(ncell)
  real(kind=double), intent(inout) :: pot(nnode)
  type(DmkCtrl),     intent(inout) :: simple_ctrl
  integer,           intent(inout) :: info
  ! local variables
  type(file) :: fgrid,fsubgrid,fout
  type(DmkInputs), target :: ode_inputs
  type(tdpotsys), target :: tdpot
  type(dmkp1p0), target :: p1p0
  type(DmkInputs), target :: inputs_data
  type(abs_simplex_mesh), target :: grid,subgrid
  ! reverse communication varaibles
  integer :: flag,flag_task
  real(kind=double) :: current_time
  ! shorthand copies
  integer :: lun_err
  integer :: lun_out
  integer :: lun_stat
  integer :: ntdens
  integer :: npot
  ! working variables
  logical :: rc
  integer :: res
  integer :: i,j,k,icell, inode
  real(kind=double), allocatable :: forcing_subgrid(:)
  real(kind=double), allocatable :: dirac_subgrid(:)
  type(DmkCtrl)  :: ctrl
  character(len=256) :: msg,fname
  type(file) :: fmat
  real(kind=double) :: normp

  write(*,*) 'pflux=',pflux,nnodeincell,ncell,nnode



  
  !
  ! copy controls
  !
  call simple_ctrl%init()
  ctrl=simple_ctrl
  


  lun_err  = ctrl%lun_err
  lun_out  = ctrl%lun_out
  lun_stat = ctrl%lun_statistics

  !
  ! buils grid and subgrid
  !
  call data2grids(&
     lun_err,&
     nnodeincell,nnode,ncell, &
     coord,topol,&
     grid,subgrid)
  write(*,*) 'ctrl definition '



  call fgrid%init(lun_err,'gridin.dat',14,'out')
  


  call grid%write_mesh(lun_err,fgrid)
  call fgrid%kill(lun_err)

  call fsubgrid%init(lun_err,'subgridin.dat',15,'out')
  call subgrid%write_mesh(lun_err,fsubgrid)
  call fsubgrid%kill(lun_err)

  call fsubgrid%init(lun_err,'subgridin_parent.dat',15,'out')
  call subgrid%write_parent(lun_err,fsubgrid)
  call fsubgrid%kill(lun_err)


  ntdens=grid%ncell
  npot=subgrid%nnode
  write(*,*) 'grid done',ntdens,npot


  !
  ! inputs data
  !
  call inputs_data%init(lun_err, ntdens,npot,set2default=.True.)
  inputs_data%pflux=pflux

  !
  ! build p1p0 FEM space
  !
  call p1p0%init(&
       ctrl,&
       1,grid, subgrid)


  !
  ! build rhs integrated and check imbalance
  !
  call subgrid%info(6)
    write(*,*) 'pflux=',pflux,nnodeincell,ncell,nnode


  allocate(&
       forcing_subgrid(subgrid%ncell),&
       dirac_subgrid(subgrid%nnode),&
       stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_alloc, 'otpdmk', &
       ' work arrays forcing_subgrid dirac_subgrid ', res)

  call subgrid%proj_subgrid(forcing,forcing_subgrid)
  fname='forcringsubgrid.dat'
  call fmat%init(lun_err,fname,10000,'out')
  call fmat%info(6)
  call write_steady(lun_err, 10000, subgrid%ncell,forcing_subgrid)
  call fmat%kill(lun_err)

  call p1p0%p1%int_test(forcing_subgrid,inputs_data%rhs)
  !inputs_data%rhs=inputs_data%rhs+dirac_subgrid
  deallocate(&
       forcing_subgrid,&
       dirac_subgrid,&
       stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_dealloc, 'otpdmk', &
       ' work arrays forcing_subgrid dirac_subgrid ', res)
  write(*,*) 'inputs data done'

  
  
  fname='rhs_integrated_inside.dat'
  call fmat%init(lun_err,fname,10000,'out')
  call write_steady(lun_err, 10000, npot, inputs_data%rhs)
  call fmat%kill(lun_err)

  fname='material.dat'
  call fmat%init(lun_err,fname,10000,'out')
  call write_steady(lun_err, 10000, subgrid%ncell, subgrid%topol(4,:)*one)
  call fmat%kill(lun_err)

  fname='forcing_inside.dat'
  call fmat%init(lun_err,fname,10000,'out')
  call write_steady(lun_err, 10000, ntdens, forcing)
  call fmat%kill(lun_err)

  !
  ! 2.3 -Initialized tdens-potential type
  !
  call tdpot%init( lun_err, ntdens, npot,1)
  tdpot%tdens=tdens
  tdpot%pot=zero

  !
  ! 3 Time evolution starts
  !
  msg=ctrl%separator('Evolution begins')
  if ( ( ctrl%info_state .gt. 1).and. (lun_out>0) )  write(lun_out,'(a)') etb(msg)
  if ( (lun_stat>0) ) write(lun_stat,'(a)') etb(msg)

  !
  ! run dmk time evolution
  !
  !
  ! start time cycle
  !
  info=0
  flag=1
  current_time=inputs_data%time
  do while ( flag.ne.0)
     call dmkp1p0_cycle_reverse_communication(p1p0,&
          flag,flag_task,info,current_time,&
          inputs_data,tdpot,ctrl,simple_ctrl)

     select case (flag)
     case(2)
        !
        ! fill ode_inputs with data at current time
        ! reading data from files and copy them into inputs_data
        inputs_data%time=current_time
     case(3)
        !
        ! flag==3 R
        ! Right before new update
        ! Here tdpot and inputs_data are syncronized
        ! The user can compute print any information regarding
        ! the state of the system
        !
     case(4)
        tdpot%system_variation=p_norm(tdpot%ntdens,zero,p1p0%norm_grad_dyn-tdpot%tdens**(inputs_data%pflux-1))
     end select
     !
     ! In case of negative info, break cycle, free memory
     !
     if ( info.ne.0 )  flag=0
  end do

  !>------------------------------------------------------
  !> Save time, var_tdens, time functional
  !>------------------------------------------------------
  !
  ! free memory
  !
  call tdpot%kill(lun_err)
  call inputs_data%kill(lun_err)

  call p1p0%kill(lun_err)
  call grid%kill(lun_err)
  call subgrid%kill(lun_err)

end subroutine otpdmk

subroutine test_select(grid)
  use AbstractGeometry
  use P1Galerkin
  use SparseMatrix
  use SimpleMatrix
  use Globals
  implicit none
  type(abs_simplex_mesh), intent(inout) :: grid
  !local
  integer :: lun_err=0,m
  type(p1gal) :: p1
  type(spmat) :: stiff_head,stiff_cut,stiffy,divx,divy,projector,true_stiff
  type(diagmat) :: diag_tdens
  real(kind=double), allocatable:: tdens(:),scr_ncell(:)
  integer :: ncell,nnode,i,ntdens
  integer :: row_list(2),column_list(2)
  real(kind=double) :: dnrm2

  call grid%build_size_cell(lun_err)
  call grid%build_normal_cell(lun_err)

  ncell=grid%ncell
  ntdens=grid%ncell

  allocate(tdens(ntdens))
  
  call p1%init(lun_err,grid)

  tdens=one

  call p1%build_stiff(6, 'csr', tdens, true_stiff)

  
  row_list(1)=2
  row_list(2)=1

  column_list(1)=1
  column_list(2)=2
  call true_stiff%select_permute_rows(2,row_list,stiff_head)
  call stiff_head%select_permute_columns(2,column_list,stiff_cut)
  call true_stiff%write(666)
  call stiff_head%write(777)
  call stiff_cut%write(888)
    

end subroutine test_select



!!$subroutine test_stiff(grid,subgrid)
!!$  use AbstractGeometry
!!$  use P1Galerkin
!!$  use SparseMatrix
!!$  use SimpleMatrix
!!$  use Globals
!!$  implicit none
!!$  type(abs_simplex_mesh), intent(inout) :: grid
!!$  type(abs_simplex_mesh), intent(inout) :: subgrid
!!$  !local
!!$  integer :: lun_err=0,m
!!$  type(p1gal) :: p1
!!$  type(spmat) :: stiff,stiffx,stiffy,divx,divy,projetcor,true_stiff
!!$  type(diagmat) :: diag_tdens
!!$  real(kind=double), allocatable:: tdens(:),scr_ncell(:)
!!$  integer :: ncell,nnode,i
!!$  real(kind=double) :: dnrm2
!!$
!!$  call grid%build_size_cell(lun_err)
!!$  call grid%build_normal_cell(lun_err)
!!$  call subgrid%build_size_cell(lun_err)
!!$  call subgrid%build_normal_cell(lun_err)
!!$
!!$  ncell=subgrid%ncell
!!$  ntdens=grid%ncell
!!$
!!$  
!!$
!!$  allocate(tdens(ntdens),scr_ncell(ncell),tdens_prj(ncell))
!!$  
!!$  call p1%init(lun_err,subgrid)
!!$  
!!$  call projector%init(0, &
!!$       subgrid%ncell_parent,subgrid%ncell,subgrid%ncell,&
!!$       'csr',&
!!$       is_symmetric=.False.)
!!$  
!!$  projector%ia(1)=1
!!$  m=0
!!$  do i=1,subgrid%ncell_parent
!!$     projector%ia(i+1)=projector%ia(i)+4
!!$     do j=1,4
!!$        m=m+1
!!$        projector%ja   (projector%ia(i)+j-1)= m
!!$        projector%coeff(projector%ia(i)+j-1)= onefourth
!!$     end do
!!$  end do
!!$
!!$  !
!!$  ! 
!!$  !
!!$  scr_ncell=one
!!$  call gradx%MULT_MDN(lun_err, projector , p1%gradx, &
!!$          ncell, 100*ncell ,scr_ncell)
!!$  
!!$  call grady%MULT_MDN(lun_err, projector , p1%gray, &
!!$          ncell, 100*ncell ,scr_ncell)
!!$
!!$ 
!!$  divx=gradx
!!$  call divx%transpose(0)
!!$
!!$  divy=grady
!!$  call divy%transpose(0)
!!$  
!!$  do i=1,100
!!$     CALL RANDOM_NUMBER(tdens)
!!$     scr_ncell=tdens*grid%size_cell
!!$     call stiffx%MULT_MDN(lun_err, divx , gradx, &
!!$          ncell, 100*ncell ,scr_ncell, divx)
!!$     call stiffx%info(6)
!!$     call stiffy%MULT_MDN(lun_err, divy , grady, &
!!$          ncell, 100*ncell ,scr_ncell, divy)
!!$     call stiffy%info(6)
!!$
!!$     stiff=stiffx
!!$     stiff%coeff=stiffx%coeff+stiffy%coeff
!!$     call p1%build_stiff(6, 'csr', tdens, true_stiff)
!!$
!!$     call sugrid%proj_subgrid(tdes,scr_tdens)
!!$     call p1%build_stiff(6, 'csr', tdens, true_stiff)
!!$
!!$     
!!$  
!!$     write(*,*) dnrm2(stiff%nterm,stiff%coeff-true_stiff%coeff,1)
!!$  end do
!!$
!!$end subroutine test_stiff
  


subroutine DMKGraph_steady_data(&
     nedges,edges,weight,tdpot,inputs_data,&
     simple_ctrl,info,timefun,flux)
  use KindDeclaration
  use Globals
  use TdensPotentialSystem
  use DmkInputsData
  use TimeFunctionals
  use AlgebraicDMK
  use DmkControls
  use SparseMatrix
  use Timing
  implicit none
  integer,                intent(in   ) :: nedges
  integer,                intent(in   ) :: edges(2,nedges)
  real(kind=double),      intent(in   ) :: weight(nedges)
  type(DmkInputs),        intent(inout) :: inputs_data
  type(tdpotsys),         intent(inout) :: tdpot
  type(DmkCtrl),          intent(inout) :: simple_ctrl
  integer,                intent(inout) :: info
  type(evolfun),          intent(inout) :: timefun
  real(kind=double),      intent(inout) :: flux(nedges)
  
  ! local variables
  type(spmat) :: incidence,matrixAT
  type(admk) :: alg
  type(file) :: fout
  ! reverse communication varaibles
  integer :: flag,flag_task
  real(kind=double) :: current_time
  ! shorthand copies
  integer :: lun_err
  integer :: lun_out
  integer :: lun_stat
  integer :: ntdens
  integer :: npot
  ! working variables
  logical :: rc
  integer :: res
  integer :: i,j,k,m,icell, inode,iroot
  type(DmkCtrl)  :: ctrl
  character(len=256) :: msg,msg2,out_format
  type(tim) :: test
  !
  real(kind=double) :: err_tdens,err_pot,balance
  real(kind=double) :: dnrm2
  

  !
  ! copy controls
  !
  call simple_ctrl%init()
  ctrl=simple_ctrl  
  lun_err  = ctrl%lun_err
  lun_out  = ctrl%lun_out
  lun_stat = ctrl%lun_statistics

  ntdens=tdpot%ntdens
  npot=tdpot%npot
  

  !
  ! build signed incidence matrix and is transpose
  !
  call matrixAT%init(lun_err,&
       tdpot%ntdens,tdpot%npot,& 
       2*tdpot%ntdens,&
       'csr')
  matrixAT%name='GRAD'
  m=0
  matrixAT%ia(1)=1
  do i=1,ntdens
     !
     ! two non zeros for each edge
     ! 
     matrixAT%ia(i+1)=matrixAT%ia(i)+2

     m=m+1
     matrixAT%ja(m)=edges(1,i)
     matrixAT%coeff(m)=one

     m=m+1
     matrixAT%ja(m)=edges(2,i)
     matrixAT%coeff(m)=-one
  end do
  
  
  if ( ctrl%debug >0 ) write(lun_out,*) 'MatrixAT Matrix Done'


  incidence=matrixAT
  call incidence%transpose(0)
  incidence%name='DIV'
  if ( ctrl%debug >0 ) write(lun_out,*) 'Incidence Matrix Done'
  
  call ctrl%print_string('state',1,ctrl%separator('Graph'))
  out_format=ctrl%formatting('aiaiarar')
  write(msg,out_format) '#Node = ', npot, ' #Nedges= ', ntdens,' | ' , &
       minval(weight),' <= |w=weight| <=', maxval(weight)
  call ctrl%print_string('state',1,msg)
  if ( minval(weight) < small) then
     write(lun_err,*) 'Weight must be strictly positive'
     stop
  end if
  

  ! check data
  if ( inputs_data%ndir .eq. 0 ) then
     do i=1,tdpot%number_of_potentials
        balance=zero
        do j=1,npot
           balance = balance + inputs_data%rhs((i-1)*npot+j)
        end do
        if (balance > 1.0d-12) then
           write(lun_err,*) 'Forcing term are not balanced'
           stop
        end if
     end do
  end if
  
  !
  ! build admk pair
  ! incidence_transpose
  call alg%init( ctrl, incidence,weight)
  if ( ctrl%debug >0 ) write(lun_out,*) 'Admk var  Done'
  
  !
  ! 3 Time evolution starts
  !
  !msg=ctrl%separator('Evolution begins')
  !if ( ( ctrl%info_state .gt. 1) .and. (lun_out>0) )  write(lun_out,'(a)') etb(msg)
  !if ( ctrl%id_save_statistics>0) write(lun_stat,'(a)') etb(msg)

  !
  ! run dmk time evolution
  !
  !
  ! start time cycle
  !
  info=0
  flag=1
  inputs_data%steady=.True.
  current_time=inputs_data%time
  do while ( flag.gt.0)
     call alg%new_dmk_cycle_reverse_communication(&
          flag,flag_task,info,current_time,&
          inputs_data,tdpot,ctrl,simple_ctrl)

     select case (flag)
     case(2)
        !
        ! fill ode_inputs with data at current time
        ! reading data from files and copy them into inputs_data
        inputs_data%time=current_time
     case(3)
        call alg%set_active_tdpot(tdpot,ctrl)

        call ctrl%print_string('state',1,' ')
        msg=ctrl%separator('INFO SYSTEM')
        call ctrl%print_string('state',1,msg)
        
        if ( ctrl%info_state .ge. 1 ) then
           out_format=ctrl%formatting('arararar')
           if ( (ctrl%info_state .ge. 3) .and. (ctrl%lun_out>0) ) &
                write(ctrl%lun_out,out_format) &
                'tdens on-off:',tdpot%ntdens_on/(tdpot%ntdens/1.0d2),&
                ',',tdpot%ntdens_off/(tdpot%ntdens/1.0d2),&
                ' pot   on-off:',tdpot%npot_on/(tdpot%npot/1.0d2),&
                ',',tdpot%npot_off/(tdpot%npot/1.0d2)
        end if
           
          
        
        ! err of dual constraints
        call alg%build_grad(alg%matrixAT,tdpot%pot,tdpot%scr_ntdens, alg%inv_weight)
        if (tdpot%ntdens_off .ne. 0) tdpot%scr_ntdens(tdpot%inactive_tdens)=zero

        ! basis pursuit error 
        if ( abs(inputs_data%pflux-inputs_data%pmass)<small) then
           out_format=ctrl%formatting('rarar')
           write(msg,out_format) &
                minval(abs(tdpot%scr_ntdens)),' <= |grad| <=', maxval(abs(tdpot%scr_ntdens)),&
                ' max(|grad|)-1.0 =',maxval(abs(tdpot%scr_ntdens))-one
           call ctrl%print_string('state',1,msg)
        end if
           
        ! compute all functionals storing results in tdpot and alg
        call alg%compute_functionals(inputs_data,tdpot,ctrl)
        
        ! copy current state informations into timefun
        call alg%store_evolution(timefun,tdpot%time_iteration,&
             tdpot,inputs_data,ctrl)

        ! comparison with respect to reference solution
        if ( inputs_data%optimal_tdens_exists) then
           tdpot%scr_ntdens=tdpot%tdens-inputs_data%optimal_tdens          
           err_tdens=p_norm(ntdens,one,&
                tdpot%scr_ntdens,&
                alg%weight)/&
                p_norm(ntdens,one,&
                inputs_data%optimal_tdens,&
                alg%weight)
           write(msg,*) 'err_tdens=',err_tdens
           call ctrl%print_string('state',1,msg)
           timefun%err_tdens(tdpot%time_iteration)=err_tdens
        end if

        
        if (inputs_data%optimal_pot_exists) then
           iroot=minloc(inputs_data%optimal_pot,npot)
           tdpot%scr_npot=(tdpot%pot-tdpot%pot(iroot)+inputs_data%optimal_pot)   
           err_pot=dnrm2(npot, tdpot%scr_npot,1)/dnrm2(npot,inputs_data%optimal_pot,1)
           write(msg,*) 'err_pot=',err_pot
           call ctrl%print_string('state',1,msg)
           timefun%err_pot(tdpot%time_iteration)=err_pot
        end if
        
        msg=ctrl%separator('')
        call ctrl%print_string('state',1,msg)
        

        
        !
        ! flag==3 R
        ! Right before new update
        ! Here tdpot and inputs_data are syncronized
        ! The user can compute print any information regarding
        ! the state of the system
        !
     end select
  end do

  ! compute optimal flux
  call alg%build_grad(alg%matrixAT,tdpot%pot,flux, alg%inv_weight)
  flux=-flux*tdpot%tdens
  
  !>------------------------------------------------------
  !> Save time, var_tdens, time functional
  !>------------------------------------------------------
  !
  ! free memory
  !
  call alg%kill(lun_err)
end subroutine DMKGraph_steady_data


subroutine boundarynodes(&
     lun_err,&
     nnodeincell,nnode,ncell, &
     coord,topol,nboundarynode,boundarynode)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  implicit none
  integer,           intent(in   ) :: lun_err
  integer,           intent(in   ) :: nnodeincell,nnode,ncell
  integer,           intent(in   ) :: topol(nnodeincell,ncell)
  real(kind=double), intent(in   ) :: coord(3,nnode)
  integer,           intent(inout) :: nboundarynode
  integer,           intent(inout) :: boundarynode(nnode)
  ! local
  type(abs_simplex_mesh) :: grid

  call grid%init_from_data(lun_err,&
       nnode,ncell,nnodeincell,'triangle', &
       topol,coord)

  call grid%build_edge_connection(lun_err)
  call grid%build_nodebc()

  nboundarynode=grid%nnode_bc
  boundarynode(1:nboundarynode)=grid%node_bc

  call grid%kill(lun_err)
  
end subroutine boundarynodes
  
subroutine data2grids(&
     lun_err,&
     nnodeincell,nnode,ncell, &
     coord,topol,&
     grid,subgrid)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  use SparseMatrix
  implicit none
  integer,           intent(in   ) :: lun_err
  integer,           intent(in   ) :: nnodeincell,nnode,ncell
  integer,           intent(in   ) :: topol(nnodeincell,ncell)
  real(kind=double), intent(in   ) :: coord(3,nnode)
  type(abs_simplex_mesh),    intent(inout) :: grid,subgrid
  ! local
  type(file) :: fgrid,fparent
  logical :: rc
  integer :: res
  type(spmat) :: connection_matrix
  character(len=256) :: cell_type
  integer, allocatable :: perm(:),iperm(:)

  if (nnodeincell==3) then
     cell_type='triangle'
  else
     cell_type='tetrahedron'
  end if

  !
  ! init grid
  !
  call grid%init_from_data(lun_err,&
       nnode,ncell,nnodeincell,cell_type, &
       topol,coord)
  write(*,*)'grid from data'
  call grid%build_size_cell(lun_err)
  write(*,*)'grid sizecell'
  call grid%build_normal_cell(lun_err)
  call grid%build_bar_cell(lun_err)  
  write(*,*)'grid done'


  !
  ! build subgrid
  !
  call subgrid%refine(lun_err,input_mesh=grid)
  write(*,*)'subgrid refined'
  call subgrid%build_nodenode_matrix(lun_err, .False.,connection_matrix)
  write(*,*)'subgrid nodenode'
  allocate(perm(subgrid%nnode),iperm(subgrid%nnode),stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_inp, 'data2grids', &
       ' work arrays perm iperm', res)
  call connection_matrix%genrcm(6,perm,iperm)
  write(*,*)'subgrid genrcm'
  call connection_matrix%kill(lun_err)
  call subgrid%renumber(lun_err, subgrid%nnode,perm,iperm)
  write(*,*)'subgrid renumber'
  call subgrid%build_size_cell(lun_err)
  call subgrid%build_normal_cell(lun_err)

  deallocate(perm,iperm,stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_dealloc, 'data2grids', &
       ' work arrays perm iperm', res)

end subroutine data2grids

subroutine data2grid(&
     lun_err,&
     nnodeincell,nnode,ncell, &
     coord,topol,grid)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  use SparseMatrix
  implicit none
  integer,           intent(in   ) :: lun_err
  integer,           intent(in   ) :: nnodeincell,nnode,ncell
  integer,           intent(in   ) :: topol(nnodeincell,ncell)
  real(kind=double), intent(in   ) :: coord(3,nnode)
  type(abs_simplex_mesh),    intent(inout) :: grid
  ! local
  type(file) :: fgrid
  logical :: rc
  integer :: res
  type(spmat) :: connection_matrix
  character(len=256) :: cell_type
  integer, allocatable :: perm(:),iperm(:)

  if (nnodeincell==3) then
     cell_type='triangle'
  else
     cell_type='tetrahedron'
  end if

  !
  ! init grid
  !
  call grid%init_from_data(lun_err,&
       nnode,ncell,nnodeincell,cell_type, &
       topol,coord)
  write(*,*)'grid from data'
  call grid%build_size_cell(lun_err)
  write(*,*)'grid sizecell'
  call grid%build_normal_cell(lun_err)
  call grid%build_bar_cell(lun_err)  
  write(*,*)'grid done'

end subroutine data2grid

subroutine write_parent(subgrid,filepath)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  type(abs_simplex_mesh), intent(in) :: subgrid
  character(len=256), intent(in  ) :: filepath
  ! local
  type(file) :: fsubgrid

  write(*,*)  filepath
  
  call fsubgrid%init(0,filepath,15,'out')
  call subgrid%write_parent(0,fsubgrid)
  call fsubgrid%kill(0)

end subroutine write_parent

subroutine build_refinement(&
     lun_err,&
     grid,subgrid)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  use SparseMatrix
  implicit none
  integer,                intent(in   ) :: lun_err
  type(abs_simplex_mesh), intent(in   ) :: grid
  type(abs_simplex_mesh), intent(inout) :: subgrid
  ! local
  type(file) :: fgrid
  logical :: rc
  integer :: res
  type(spmat) :: connection_matrix
  character(len=256) :: cell_type
  integer, allocatable :: perm(:),iperm(:)

  !
  ! build subgrid
  !
  call subgrid%refine(lun_err,input_mesh=grid)
  write(*,*)'subgrid refined'
  call subgrid%build_nodenode_matrix(lun_err, .False.,connection_matrix)
  write(*,*)'subgrid nodenode'
  allocate(perm(subgrid%nnode),iperm(subgrid%nnode),stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_inp, 'data2grids', &
       ' work arrays perm iperm', res)
  call connection_matrix%genrcm(6,perm,iperm)
  write(*,*)'subgrid genrcm'
  call connection_matrix%kill(lun_err)
  call subgrid%renumber(lun_err, subgrid%nnode,perm,iperm)

  deallocate(perm,iperm,stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_dealloc, 'data2grids', &
       ' work arrays perm iperm', res)

end subroutine build_refinement


subroutine forcing2rhs(grid,forcing,rhs)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  implicit none
  type(abs_simplex_mesh), intent(in   ) :: grid
  real(kind=double),      intent(in   ) :: forcing(grid%ncell)
  real(kind=double),      intent(inout) :: rhs(grid%nnode)
  ! local
  integer :: icell, iloc, inode
  real(kind=double) ::factorsize

  factorsize=one/grid%nnodeincell
  
  rhs = zero
  do icell = 1, grid%ncell
     do iloc = 1,grid%nnodeincell
        
        inode = grid%topol(iloc,icell)
        rhs(inode) =  rhs(inode) + &
             factorsize * forcing(icell) * grid%size_cell(icell)
     end do
  end do
  
end subroutine forcing2rhs


subroutine build_subgrid_rhs(subgrid,rhs,forcing, dirac)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  implicit none
  type(abs_simplex_mesh),      intent(in   ) :: subgrid
  real(kind=double),           intent(inout) :: rhs(subgrid%nnode)
  real(kind=double),  intent(in   ) :: forcing(subgrid%ncell_parent)
  real(kind=double),  intent(in   ) :: dirac(subgrid%nnode_parent)
  
  ! local
  logical :: rc
  integer :: res,n1,n2,inode
  real(kind=double), allocatable :: forcing_subgrid(:)
  real(kind=double), allocatable :: dirac_subgrid(:)

  write(*,*) 'project',subgrid%ncell,subgrid%nnode
  allocate(forcing_subgrid(subgrid%ncell),dirac_subgrid(subgrid%nnode),stat=res)
  if (res.ne.0) rc = IOerr(0, err_alloc, 'build_subgrid_rhs', &
       ' work arrays forcing_subgrid,dirac_subgrid', res)

  rhs=zero
  !
  ! interpolate data to subgrid
  !
  forcing_subgrid=zero
  write(*,*) 'project'
  call subgrid%proj_subgrid( forcing, forcing_subgrid)
  write(*,*) 'forcing'
  call forcing2rhs(subgrid,forcing_subgrid,rhs)
  
  
  write(*,*) 'dirac'
  dirac_subgrid=zero
  do inode=1,subgrid%nnode
     n1=subgrid%node_parent(1,inode)
     n2=subgrid%node_parent(2,inode)
     if (n1 .eq. n2 ) then
        dirac_subgrid(inode)=dirac(n1)
     end if
  end do


  !
  ! buils rhs 
  !
  rhs=rhs+dirac_subgrid
  
  deallocate(forcing_subgrid,dirac_subgrid,stat=res)
  if (res.ne.0) rc = IOerr(0, err_dealloc, 'build_subgrid_rhs', &
       ' work arrays forcing_subgrid,dirac_subgrid', res)

  
end subroutine build_subgrid_rhs


subroutine solve_sparse_linear_system(matrix, rhs, sol)
  use SparseMatrix
  use Globals
  use LinearSolver
  use StdSparsePrec

  implicit none
  type(spmat),       intent(inout) :: matrix
  real(kind=double), intent(in   ) :: rhs(matrix%nrow)
  real(kind=double), intent(inout) :: sol(matrix%ncol)
  ! local variable
  type(input_solver) :: ctrl_solver
  type(output_solver) :: info_solver
  type(input_prec) :: ctrl_prec
  integer :: info_prec
  type(stdprec) :: prec


  ! set controls
  call ctrl_prec%init(6,'IC', 30, 1.0d-4)
  call ctrl_solver%init(&
       6, &
       scheme='PCG', lun_err=6, lun_out=6,&
       iexit=0, imax=1000, iprt=0, isol=0, &
       tol_sol=1d-13,iort=0)

  ! build preconditioner
  call prec%init(0, info_prec,ctrl_prec, matrix%nrow, matrix)

  ! solve via PCG
  call linear_solver(matrix,&
               rhs,sol,&
               info_solver, &
               ctrl_solver,&
               prec_left=prec)
  
  ! free memory
  call prec%kill(0)

end subroutine solve_sparse_linear_system

subroutine solve_laplacian(grid,id_test,ctrl)
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use TimeInputs, only : write_steady    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use DmkControls
  use, intrinsic :: iso_fortran_env, only : &
       stdin=>input_unit, &
       stdout=>output_unit, &
       stderr=>error_unit  
  implicit none
  type(abs_simplex_mesh), target,intent(inout) :: grid
  integer,       intent(in  ) :: id_test
  type(DmkCtrl), intent(inout) :: ctrl


  type(file)     :: fgrid,fgridout,fctrl,fout
  
  type(p1gal)    :: lapl_p1
  type( agmg_inv) :: multigrid_inverse

  ! 
  real(kind=double), allocatable :: diffusion_coeff(:),rhs(:),sol(:)
  
  ! drichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  !  p1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:)

  !  exact solution and its  gradient
  real(kind=double), allocatable :: pot(:), gradpot(:,:)

  !  error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  ! errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before 

  ! euclidian norm
  real(kind=double) :: dnrm2,ddot

  character(len=256) :: input,fngrid,fnctrl,fnctrl_in,fname,tail



  type(spmat)    :: stiff_matrix
  type(scalmat) :: eye1,eye2
  type(input_solver) :: ctrl_solver
  type(output_solver):: info,info1
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! local vars
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir
  integer :: nnode, ncell
  integer :: nref
  integer :: igrid,id_test_read

  write(stdout,*) '*****************TEST CASE ***********************'
  write(stdout,*) 'id_test    = ', id_test
  write(stdout,*) etb(example_description(id_test))
  write(stdout,*) 'total nref =',nref
  write(stdout,*) '**************************************************'

  write(stdout,*) '*************INFO LINEAR SOLVER ******************'
  ctrl_solver=ctrl%ctrl_outer_solver
  call ctrl_solver%info(0)
  write(stdout,*) '**************************************************'
  
  call grid%build_size_cell(0)
  call grid%build_normal_cell(0)
  call grid%build_edge_connection(0)
  call grid%build_nodebc()
  call grid%build_bar_cell()
  call grid%info(0)

 
  
 
  nnode=grid%nnode
     ncell=grid%ncell

     allocate(diffusion_coeff(ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! give a value to cond
     diffusion_coeff=one

     ! p1 galerkin for laplace
     write(*,*) ' build_p1'
     call lapl_p1%init(6, grid)

     nterm = lapl_p1%nterm_csr
     ndof = lapl_p1%grid%nnode

     call stiff_matrix%init(6,&
          ndof, ndof, nterm,&
          storage_system='csr',&
          is_symmetric=.true.)

 
    write(*,*) ' build stiff'
     call lapl_p1%build_stiff(6, 'csr', diffusion_coeff, stiff_matrix)


     !
     ! allocate space for rhs, sol, gradsol
     !
     allocate(rhs(ndof),sol(ndof),gradsol(grid%logical_dimension,ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays rhs,sol,gradsol ',res)
     rhs=zero
     sol=one
     gradsol=zero
     !
     ! create rhs $rhs(i)=\int f \phi_i$
     !
     do i=1,ndof
        rhs(i) = lapl_p1%basis_integral(i)* &
             forcing(id_test,grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do
     
     !
     ! set dirichlet boundary conditions
     !
     ndir=grid%nnode_bc
     allocate(noddir(ndir),soldir(ndir),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays noddir soldir',res)
     noddir=grid%node_bc
     do i=1,grid%nnode_bc
        j = grid%node_bc(i)
        soldir(i) = pot_exact(id_test, grid%coord(1,j),grid%coord(2,j),grid%coord(3,j))
     end do

     !
     ! preprocess stiffness_matrix rhs and solution 
     ! to obtain dirichlet boundary condition
     !
     call lapl_p1%dirichlet_bc(stderr,&
          stiff_matrix,rhs,sol,&
          ndir,noddir,soldir)
     sol=zero

    
     write(*,*) ' solve linear system',etb(ctrl_solver%approach)
     sol=zero

     select case (ctrl_solver%approach)
     case('MG') 
        call multigrid_inverse%init(stderr,stiff_matrix,ctrl_solver)
        call multigrid_inverse%Mxv(rhs,sol)
        call multigrid_inverse%info_solver%info(stdout)
        call multigrid_inverse%kill(stderr)
     case ('ITERATIVE')
        stiff_matrix%name='A'
        
        call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
        call prec_stiff%init(stderr, i,ctrl_prec,ndof, stiff_matrix)
        call info%init()
        call linear_solver(stiff_matrix,rhs,sol,info,ctrl_solver,prec_stiff)
        call ctrl_prec%kill()
        call prec_stiff%kill(stderr)
        call info%info(stdout)
        call info%time(stdout)
        call info%kill()

     end select
     
     write(*,*) ' write solution'
     fname=etb('pot'//etb(tail))
     call fout%init(0,fname,10000,'out')
     call write_steady(0, 10000, ndof, sol)
     call fout%kill(0)
    
     !
     ! allocate space for reference solution and work arrays 
     !
     allocate(&
          pot(ndof),&
          gradpot(grid%logical_dimension,ncell),&
          err_pot(ndof),&
          err_gradpot(grid%logical_dimension,ncell),&
          err_nrmgradpot(ncell),&
          stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays pot gradpot',res)

     call stiff_matrix%mxv(sol,pot)
     pot=pot-rhs

     !
     ! compute gradient of the solution 
     ! exact solution its gradient
     ! errors arrays
     !
     call lapl_p1%eval_grad(sol,gradsol)
     do i=1,ndof
        pot(i) = pot_exact(id_test, grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do
     do i=1,ncell
        call gradpot_exact(id_test,&
             grid%bar_cell(1,i),&
             grid%bar_cell(2,i),&
             grid%bar_cell(3,i),&
             grid%logical_dimension, gradpot(1:grid%logical_dimension,i))
     end do
     fname=etb('exact_pot'//etb(tail))
     call fout%init(0,fname,10000,'out')
     call write_steady(0, 10000, ndof, pot)
     call fout%kill(0)

     
     err_pot=sol-pot
     err_gradpot=gradsol-gradpot
     do i=1,grid%ncell
        err_nrmgradpot(i)=dnrm2(grid%logical_dimension,err_gradpot(1:grid%logical_dimension,i),1)
     end do

     !
     ! compute l2 and print error w.r.t exact solution 
     !
     if (igrid >1 ) then
        l2err_pot_before = l2err_pot 
        l2err_gradpot_before = l2err_gradpot
     end if
     err_pot=err_pot**2
     l2err_pot=sqrt(ddot(ndof, lapl_p1%basis_integral, 1, err_pot,1))
     l2err_gradpot=grid%normp_cell(2.0d0,err_nrmgradpot)
     write(stdout,'(I4,a,3(1pe15.5))') grid%grid_level, ' | ',grid%meshpar(0), &
          l2err_pot, l2err_gradpot

     !
     ! free memory 
     !
     deallocate(noddir,soldir,stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays noddir soldir',res)

     deallocate(&
          pot,&
          gradpot,&
          err_pot,&
          err_gradpot,&
          err_nrmgradpot,&
          stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays pot gradpot',res)

     deallocate(diffusion_coeff,rhs,sol,gradsol,stat=res)
     if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
          'arrays diffusion_coeff, rhs, sol',res)
     call stiff_matrix%kill(6)
     call lapl_p1%kill(6)
contains

  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if(id_test.eq.1) str='Rectangle [0,1][0,1] with zero dirichlet bc'
  
  end function example_description

  function forcing(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=pigreco**2 * sin(pigreco*x) * cos(pigreco*y*onehalf)*5.0d0*onefourth
  
  end function forcing

  function pot_exact(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=sin(pigreco*x) * cos(pigreco*y*onehalf)

  end function pot_exact

  subroutine gradpot_exact(id_test,x,y,z,logical_dimension,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    integer,           intent(in):: logical_dimension
    real(kind=double), intent(out) :: func(logical_dimension)

    if ( id_test .eq. 1 ) then
       func(1)=pigreco*cos(pigreco*x) * cos(pigreco*y*onehalf)
       func(2)=-pigreco*onehalf*sin(pigreco*x) * sin(pigreco*y*onehalf)
       if (logical_dimension .eq. 3 ) func(3)=zero
    end if

  end subroutine gradpot_exact
  
  
end subroutine solve_laplacian



subroutine solve_pure_neumann(grid,ctrl,forcing)
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use TimeInputs, only : write_steady    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use DmkControls
  use, intrinsic :: iso_fortran_env, only : &
       stdin=>input_unit, &
       stdout=>output_unit, &
       stderr=>error_unit  
  implicit none
  type(abs_simplex_mesh), target,intent(inout) :: grid
  type(DmkCtrl), intent(inout) :: ctrl
  real(kind=double), intent(in  ) :: forcing(grid%ncell)
  !local  
  type(file)     :: fgrid,fgridout,fctrl,fout
  type(p1gal)    :: lapl_p1
  type( agmg_inv) :: multigrid_inverse

  ! 
  real(kind=double), allocatable :: diffusion_coeff(:),rhs(:),sol(:)
  
  ! drichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  !  p1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:)

  !  exact solution and its  gradient
  real(kind=double), allocatable :: pot(:), gradpot(:,:)

  !  error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  ! errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before 

  ! euclidian norm
  real(kind=double) :: dnrm2,ddot

  character(len=256) :: input,fngrid,fnctrl,fnctrl_in,fname,tail



  type(spmat)    :: stiff_matrix
  type(scalmat) :: eye1,eye2
  type(input_solver) :: ctrl_solver
  type(output_solver):: info,info1
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! local vars
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir
  integer :: nnode, ncell
  integer :: nref
  integer :: igrid,id_test_read

  write(stdout,*) '*************INFO LINEAR SOLVER ******************'
  ctrl_solver=ctrl%ctrl_outer_solver
  call ctrl_solver%info(0)
  write(stdout,*) '**************************************************'
  
  call grid%build_size_cell(0)
  call grid%build_normal_cell(0)
  call grid%build_edge_connection(0)
  call grid%build_nodebc()
  call grid%build_bar_cell()
  call grid%info(0)

 
  
 
  nnode=grid%nnode
  ncell=grid%ncell
     
  allocate(diffusion_coeff(ncell),stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
       'array diffusion_coeff',res)
  ! give a value to cond
  diffusion_coeff=one

  ! p1 galerkin for laplace
  write(*,*) ' build_p1'
  call lapl_p1%init(6, grid)

  nterm = lapl_p1%nterm_csr
  ndof = lapl_p1%grid%nnode

  call stiff_matrix%init(6,&
       ndof, ndof, nterm,&
       storage_system='csr',&
       is_symmetric=.true.)


  write(*,*) ' build stiff'
  call lapl_p1%build_stiff(6, 'csr', diffusion_coeff, stiff_matrix)


  !
  ! allocate space for rhs, sol, gradsol
  !
  allocate(rhs(ndof),sol(ndof),gradsol(grid%logical_dimension,ncell),stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
       'arrays rhs,sol,gradsol ',res)
  rhs=zero
  sol=one
  gradsol=zero
  !
  ! create rhs $rhs(i)=\int f \phi_i$
  !
  call lapl_p1%int_test(forcing,rhs)

  sol=zero

    
  write(*,*) ' solve linear system',etb(ctrl_solver%approach)
  sol=zero

  select case (ctrl_solver%approach)
  case('MG') 
     call multigrid_inverse%init(stderr,stiff_matrix,ctrl_solver)
     call multigrid_inverse%Mxv(rhs,sol)
     call multigrid_inverse%info_solver%info(stdout)
     call multigrid_inverse%kill(stderr)
  case ('ITERATIVE')
     stiff_matrix%name='A'

     call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
     call prec_stiff%init(stderr, i,ctrl_prec,ndof, stiff_matrix)
     call info%init()
     call linear_solver(stiff_matrix,rhs,sol,info,ctrl_solver,prec_stiff)
     call ctrl_prec%kill()
     call prec_stiff%kill(stderr)
     call info%info(stdout)
     call info%time(stdout)
     call info%kill()

  end select

    
  !
  ! allocate space for reference solution and work arrays 
  !
  allocate(&
       pot(ndof),&
       gradpot(grid%logical_dimension,ncell),&
       err_pot(ndof),&
       err_gradpot(grid%logical_dimension,ncell),&
       err_nrmgradpot(ncell),&
       stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
       'arrays pot gradpot',res)

  call stiff_matrix%mxv(sol,pot)
  pot=pot-rhs

  !
  ! compute gradient of the solution 
  ! exact solution its gradient
  ! errors arrays
  !
  call lapl_p1%eval_grad(sol,gradsol)
  do i=1,ndof
     pot(i) = zero!pot_exact(id_test, grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
  end do
!!$  do i=1,ncell
!!$     call gradpot_exact(id_test,&
!!$          grid%bar_cell(1,i),&
!!$          grid%bar_cell(2,i),&
!!$          grid%bar_cell(3,i),&
!!$          grid%logical_dimension, gradpot(1:grid%logical_dimension,i))
!!$  end do
!!$   

     
  err_pot=sol-pot
  err_gradpot=gradsol-gradpot
  do i=1,grid%ncell
     err_nrmgradpot(i)=dnrm2(grid%logical_dimension,err_gradpot(1:grid%logical_dimension,i),1)
  end do

  !
  ! compute l2 and print error w.r.t exact solution 
  !
  if (igrid >1 ) then
     l2err_pot_before = l2err_pot 
     l2err_gradpot_before = l2err_gradpot
  end if
  err_pot=err_pot**2
  l2err_pot=sqrt(ddot(ndof, lapl_p1%basis_integral, 1, err_pot,1))
  l2err_gradpot=grid%normp_cell(2.0d0,err_nrmgradpot)
  write(stdout,'(I4,a,3(1pe15.5))') grid%grid_level, ' | ',grid%meshpar(0), &
       l2err_pot, l2err_gradpot


  deallocate(&
       pot,&
       gradpot,&
       err_pot,&
       err_gradpot,&
       err_nrmgradpot,&
       stat=res)
  if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
       'arrays pot gradpot',res)

  deallocate(diffusion_coeff,rhs,sol,gradsol,stat=res)
  if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
       'arrays diffusion_coeff, rhs, sol',res)
  call stiff_matrix%kill(6)
  call lapl_p1%kill(6)
end subroutine solve_pure_neumann



  
!!$subroutine simple(ctrl,pathgrid,pathforcing,pflux)
!!$  use KindDeclaration
!!$  use Globals
!!$  use AbstractGeometry
!!$  use TimeInputs  
!!$  use DmkControls
!!$  use TdensPotentialSystem
!!$  use DmkP1P0
!!$  use DmkInputsData
!!$  implicit none
!!$  type(DmkCtrl),target,intent(inout) :: ctrl
!!$  character (len=512) ,intent(in ):: pathgrid
!!$  character (len=512) ,intent(in ):: pathforcing
!!$  real(kind=double),intent(in) :: pflux  
!!$  !local 
!!$  character (len=512) :: input,fname
!!$  
!!$  type(abs_simplex_mesh),target :: grid,grid0,subgrid
!!$  type(file) :: fgrid,fforcing
!!$  type(TimeData) :: forcing
!!$
!!$  integer :: lun_err=6,i,info
!!$  integer, allocatable :: topol(:,:)
!!$
!!$  real(kind=double), allocatable :: forcing_cell(:),tdens(:),pot(:)
!!$  logical :: endfile
!!$
!!$
!!$  type(file) :: fsubgrid
!!$  type(tdpotsys), target :: tdpot
!!$  !type(dmkp1p0), target :: p1p0
!!$  type(DmkInputs), target :: inputs_data
!!$  
!!$  real(kind=double), allocatable :: forcing_subgrid(:)
!!$  real(kind=double), allocatable :: dirac(:)
!!$  logical :: rc
!!$  integer :: res
!!$  type(file) :: fmat
!!$  integer :: ntdens,npot
!!$
!!$  !
!!$  ! get grid path
!!$  !
!!$  !CALL getarg(1, input)
!!$  !read(input,*,iostat=res) pathgrid
!!$  !write(*,*) 'Grid from', etb(pathgrid)
!!$
!!$  ! 
!!$  ! get forcing path
!!$  !
!!$  !CALL getarg(2, input)
!!$  !read(input,*,iostat=res) pathforcing
!!$  !write(*,*) 'Forcing from', etb(pathforcing)
!!$
!!$  !
!!$  ! get pflux
!!$  !
!!$  !CALL getarg(3, input)
!!$  !read(input,*,iostat=res) pflux
!!$  !write(*,*) 'Pflux =', pflux
!!$
!!$  !
!!$  ! read grid
!!$  ! 
!!$  call fgrid%init(lun_err,etb(pathgrid),10,'in')
!!$  call grid%read_mesh(lun_err,fgrid)
!!$  call fgrid%kill(lun_err)
!!$
!!$  !
!!$  ! read forcing
!!$  !
!!$  call fforcing%init(lun_err,etb(pathforcing),11,'in')
!!$  call forcing%init(lun_err, fforcing, 1,grid%ncell)
!!$  call forcing%set(lun_err, fforcing, 0.0d0,endfile)
!!$  allocate(forcing_cell(grid%ncell),tdens(grid%ncell),pot(grid%nnode),topol(3,grid%ncell))
!!$  topol=grid%topol(1:3,1:grid%ncell)
!!$  do i=1,grid%ncell
!!$     forcing_cell(i) =forcing%TDactual(1,i)
!!$  end do
!!$
!!$
!!$  !
!!$  ! using optdmk
!!$  !
!!$  !tdens=one
!!$  !pot=zero
!!$  !call otpdmk(grid%nnodeincell,grid%nnode,grid%ncell,&
!!$  !     topol,grid%coord,&
!!$  !     pflux,forcing_cell,&
!!$  !     tdens,pot,&
!!$  !     ctrl,info)
!!$
!!$  !
!!$  ! using steay
!!$  !
!!$  call data2grids(&
!!$       lun_err,&
!!$       grid%nnodeincell,grid%nnode,grid%ncell, &
!!$       grid%coord,topol,&
!!$       grid0,subgrid)
!!$
!!$  ntdens=grid%ncell
!!$  npot=subgrid%nnode
!!$
!!$  !
!!$  ! inputs data
!!$  !
!!$  call inputs_data%init(lun_err, ntdens,npot,set2default=.True.)
!!$  inputs_data%pflux=pflux
!!$
!!$  !
!!$  ! init set controls
!!$  !
!!$  ! globals controls
!!$  ctrl%selection=0
!!$  ctrl%threshold_tdens=1.d-10
!!$  ctrl%debug=1
!!$  ctrl%min_tdens = 1.0d-13
!!$
!!$
!!$  ! linear solver ctrl
!!$  ctrl%linear_solver='ITERATIVE'
!!$  !ctrl%linear_solver='MG'
!!$  ctrl%krylov_scheme='PCG'
!!$  ctrl%prec_type='MG'
!!$  ctrl%n_fillin=30
!!$  ctrl%tol_fillin=1d-3
!!$  ctrl%relax4prec=0d-09
!!$  ctrl%relax_direct=1d-09
!!$  ctrl%iprt=1
!!$
!!$  ctrl%imax=500
!!$  ctrl%iprt=0
!!$  ctrl%tolerance_linear_solver=1d-12
!!$  ctrl%imax_internal=5
!!$  ctrl%tol_internal=1d-12
!!$
!!$  ! time  ctrl
!!$  ! evolution controls
!!$  ctrl%max_time_iterations=10
!!$  ctrl%tolerance_system_variation=1d-04
!!$  ! time stepping scheme
!!$  ctrl%time_discretization_scheme=5
!!$  ! time stepping scheme controls
!!$  ctrl%newton_method=100
!!$  ctrl%max_nonlinear_iterations=20
!!$  ctrl%max_restart_update=10
!!$  ctrl%epsilon_W22=1d-8
!!$
!!$  ! time step size controls
!!$  ctrl%deltat = 0.5
!!$  ctrl%deltat_control=2
!!$  ctrl%deltat_expansion_rate=2.0d0
!!$  ctrl%deltat_lower_bound=0.01
!!$  ctrl%deltat_upper_bound=100
!!$
!!$
!!$  ! info , saving ctrl
!!$  ctrl%info_update=3
!!$  ctrl%info_state=2
!!$  ctrl%id_save_dat=3
!!$  ctrl%lun_statistics=10
!!$  ctrl%lun_out=6
!!$  ctrl%lun_tdens = 1234
!!$  ctrl%fn_tdens='tdens.dat'
!!$  ctrl%lun_pot=1235
!!$  ctrl%fn_pot='pot.dat'
!!$
!!$
!!$  ctrl%max_nonlinear_iterations=20
!!$  ctrl%tolerance_nonlinear=1d-09
!!$  ctrl%relax4prec=0.0d-10
!!$  ctrl%tolerance_system_variation=1d-10
!!$
!!$
!!$  allocate(&
!!$       dirac(grid%nnode),&
!!$       stat=res)
!!$  if (res.ne.0) rc = IOerr(lun_err, err_alloc, 'otpdmk', &
!!$       ' work arrays forcing_subgrid dirac_subgrid ', res)
!!$
!!$
!!$  dirac=zero
!!$  call build_subgrid_rhs(subgrid,inputs_data%rhs,forcing_cell, dirac)
!!$
!!$  deallocate(&
!!$       dirac,&
!!$       stat=res)
!!$  if (res.ne.0) rc = IOerr(lun_err, err_dealloc, 'otpdmk', &
!!$       ' work arrays forcing_subgrid dirac_subgrid ', res)
!!$  fname='rhs.dat'
!!$  call fmat%init(0,fname,10000,'out')
!!$  call write_steady(0, 10000, npot,inputs_data%rhs )
!!$  call fmat%kill(0)
!!$
!!$
!!$  !
!!$  ! init tdpot
!!$  !
!!$  call tdpot%init( lun_err, ntdens, npot,1)
!!$  tdpot%tdens=one
!!$  tdpot%pot=zero
!!$
!!$
!!$
!!$  call dmkp1p0_steady_data(grid0,subgrid,tdpot,inputs_data,ctrl,info)
!!$
!!$
!!$end subroutine simple

subroutine dmkp1p0_steady_data(&
     grid,subgrid,id_subgrid,&
     tdpot,inputs_data,&
     simple_ctrl,info, &
     timefun)
  use KindDeclaration
  use Globals
  use TdensPotentialSystem
  use DmkInputsData
  use DmkP1P0Discretization
  use DmkControls
  use AbstractGeometry
  use TimeInputs, only : write_steady
  use Timing
  use TimeFunctionals
  use TimeInputs
  implicit none
  type(abs_simplex_mesh),target, intent(in   ) :: grid
  type(abs_simplex_mesh),target, intent(in   ) :: subgrid
  integer,                       intent(in   ) :: id_subgrid
  type(tdpotsys),        target, intent(inout) :: tdpot
  type(DmkInputs),       target, intent(inout) :: inputs_data
  type(DmkCtrl),         target, intent(inout) :: simple_ctrl
  integer,                       intent(inout) :: info
  type(evolfun), optional,       intent(inout) :: timefun
  ! local variables
  type(dmkp1p0), target :: p1p0
  ! reverse communication varaibles
  integer :: flag,flag_task
  real(kind=double) :: current_time
  ! shorthand copies
  integer :: lun_err
  integer :: lun_out
  integer :: lun_stat
  integer :: ntdens
  integer :: npot
  ! working variables
  logical :: rc
  integer :: res
  integer :: i,j,k,icell, inode
  real(kind=double), allocatable :: forcing_subgrid(:)
  real(kind=double), allocatable :: dirac_subgrid(:)
  type(DmkCtrl)  :: ctrl
  character(len=256) :: msg,msg2,fname,outformat
  type(file) :: fmat,fstat
  type(Tim) :: total
  real(kind=double) :: balance,err_tdens=huge,err_pot=huge

  call total%init()
  call total%set('start')

  !
  ! init ctrl_outer_solver and outer variables
  !
  call simple_ctrl%init()
  
  
  !
  ! copy controls
  !
  ctrl=simple_ctrl

  !
  ! shorthands
  !
  lun_err  = ctrl%lun_err
  lun_out  = ctrl%lun_out
  lun_stat = ctrl%lun_statistics
  ntdens=grid%ncell
  npot=subgrid%nnode

  ! check data
  if ( inputs_data%ndir .eq. 0 ) then
     do i=1,tdpot%number_of_potentials
        balance=zero
        do j=1,npot
           balance = balance + inputs_data%rhs((i-1)*npot+j)
        end do
        if (balance > 1.0d-12) then
           write(lun_err,*) 'Forcing term are not balanced. Sum rhs=', balance
           stop
        end if
     end do
  end if
  write(*,*) 'timefun',  present(timefun)
  !
  ! build p1p0 FEM space
  !
  msg=ctrl%separator('Init p1-p0 discretization')
  call ctrl%print_string('state', 1, msg)
  call p1p0%init(&
       ctrl,&
       id_subgrid,grid, subgrid)
  
  !
  ! open files
  !
  call fstat%init(0,ctrl%fn_statistics,lun_stat,'out')
  

  !
  ! Time evolution starts
  !
  msg=ctrl%separator('Evolution begins')
  call ctrl%print_string('state', 1, msg)

  !
  ! Run dmk time evolution
  !

  ! start time cycle
  info=0
  flag=1
  tdpot%time_update=0
  tdpot%time_iteration=0
  inputs_data%steady=.True.
  current_time=zero

  do while ( flag .gt. 0 )

     call p1p0%new_dmk_cycle_reverse_communication(&
          flag,flag_task,info,current_time,&
          inputs_data,tdpot,ctrl,simple_ctrl)
     select case (flag)
     case(2)
        !
        ! fill ode_inputs with data at current time
        ! reading data from files and copy them into inputs_data
        inputs_data%time=current_time
     case(3)
        call p1p0%set_active_tdpot(tdpot,ctrl)
        !
        ! flag==3 R
        ! Right before new update
        ! Here tdpot and inputs_data are syncronized
        ! The user can compute print any information regarding
        ! the state of the system
        !

        !
        ! compute, print and store funcionals
        !
        call p1p0%compute_functionals(tdpot,inputs_data,ctrl)
        write(msg,'(a)') ctrl%separator('INFO STATE') 
        if (ctrl%info_state .ge. 2) then
           if (lun_out>0) then
              write(lun_out,*)  ' '
              write(lun_out,*) etb(msg)
              call tdpot%info_functional(lun_out)
              msg=ctrl%separator()
              write(lun_out,*) etb(msg)

           end if
        end if
        if (ctrl%id_save_statistics>0) then
           write(lun_stat,*)  ' '
           write(lun_stat,*) etb(msg)
           call tdpot%info_functional(lun_stat)
           msg=ctrl%separator()
           write(lun_stat,*) etb(msg)
        end if

        
        if ( inputs_data%optimal_tdens_exists) then
           err_tdens=grid%normp_cell(&
                two,tdpot%tdens-inputs_data%optimal_tdens)/&
                grid%normp_cell(two,inputs_data%optimal_tdens)
           outformat=ctrl%formatting('ar')
           write(msg, outformat) 'ErrTdens : ', &
                err_tdens
           call ctrl%print_string('state', 2, msg)
        end if

        if ( inputs_data%optimal_pot_exists) then
           p1p0%scr_npot=tdpot%pot-inputs_data%optimal_pot
           err_pot=p_norm(npot,two,&
                p1p0%scr_npot,&
                p1p0%p1%basis_integral)/&
                p_norm(npot,two,&
                tdpot%pot,&
                p1p0%p1%basis_integral)
           outformat=ctrl%formatting('ar')
           write(msg, outformat) 'ErrPot : ', &
                err_pot
           call ctrl%print_string('state', 2, msg)
        end if

        if ( abs(inputs_data%pflux-inputs_data%pmass)<small ) then
           call p1p0%build_norm_grad_dyn(tdpot%pot,inputs_data%pode,p1p0%norm_grad_dyn)
           outformat=ctrl%formatting('ar')
           write(msg, outformat) 'max(|\nabla u|^Pode-inputs_data%kappa^Pode) : ', &
                maxval(p1p0%norm_grad_dyn-inputs_data%kappa**inputs_data%pode)
           call ctrl%print_string('state', 2, msg)
        end if
        
        
        !
        ! if passed, store the informations in timefun
        !
        if ( present(timefun) ) then
           call p1p0%store_evolution(timefun,tdpot%time_iteration,&
                tdpot,inputs_data,ctrl)
           timefun%err_tdens(tdpot%time_iteration)=err_tdens
           timefun%err_pot(tdpot%time_iteration)=err_pot
        end if
     case(4)
        call p1p0%build_norm_grad_dyn(tdpot%pot,inputs_data%pode,tdpot%scr_ntdens)
        write(*,*) 'max norm grad ', maxval(tdpot%scr_ntdens)-1.0
        ! tdpot%scr_ntdens=tdpot%tdens**inputs_data%pflux*tdpot%scr_ntdens-tdpot%tdens
        !tdpot%scr_ntdens=tdpot%scr_ntdens-tdpot%tdens**(1-inputs_data%pflux)
        !tdpot%system_variation=p_norm(tdpot%ntdens,zero,tdpot%scr_ntdens,grid%size_cell)!/ctrl%deltat
        tdpot%scr_ntdens=-tdpot%scr_ntdens+one
        write(*,*) 'new stoppoing criteria'
        do i=1,tdpot%ntdens
           if (tdpot%tdens(i)>1e-14) then
              tdpot%scr_ntdens(i)=abs(tdpot%scr_ntdens(i))
           else
              tdpot%scr_ntdens(i)=max(zero,-tdpot%scr_ntdens(i))
           end if
        end do
        tdpot%system_variation=p_norm(tdpot%ntdens,zero,tdpot%scr_ntdens,grid%size_cell)
        write(*,*) 'new stoppoing criteria', tdpot%system_variation
     end select
  end do
  call p1p0%build_norm_grad_dyn(tdpot%pot,inputs_data%pode,tdpot%scr_ntdens)
  tdpot%scr_ntdens=tdpot%scr_ntdens-tdpot%tdens**(1-inputs_data%pflux)
         
  call fmat%init(0,'gradient.dat',10000,'out')
  call writearray2file(6, 'whole',&
       zero, tdpot%ntdens,  tdpot%scr_ntdens,&
       10000,'gradient.dat')
  call fmat%kill(0)

  !>------------------------------------------------------
  !> Save time, var_tdens, time functional
  !>------------------------------------------------------
  !
  ! free memory
  !
  call p1p0%kill(lun_err)
  msg=ctrl%separator(' MEMORY RELEASED  ')
  if ( ( ctrl%info_state .gt. 1) .and. (lun_out>0) )  write(lun_out,'(a)') etb(msg)
  if (ctrl%id_save_statistics>0) write(lun_stat,'(a)') etb(msg)
  call fstat%kill(0)

  
  call total%set('stop')
  call total%info(lun_out)
end subroutine dmkp1p0_steady_data

