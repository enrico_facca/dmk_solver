!> Defines vars and procs for cpu timing of code sections
module MuffeTiming
  use Globals
  use Timing
  implicit none
  private
  type, public :: CodeTim
     !> total execution time
     type(Tim) :: TOT
     !> Overhead time (var init, I/O, cycle handling etc)
     type(Tim) :: OVH
     !> Matrix stiffness assembly
     type(Tim) :: ASSEMBLY
     !> Preconditioner assembly 
     type(Tim) :: PREC
     !> Linear solver 
     type(Tim) :: LINEAR_SOLVER
     !> Numercal calculation of norm etc 
     type(Tim) :: EXTRACOMP
     !>  Saving time
     type(Tim) :: SAVE
     !>  Wasted time
     type(Tim) :: WASTED
     !>  Algorithm Time
     type(Tim) :: ALGORITHM
   contains
     !> static constructor
     !> (public for type CodeTim)
     procedure, public, pass :: init => CodeTim_construct
     !> static destructor
     !> (public for type Tim)
     procedure, public, pass :: kill => CodeTim_destroy
     !> outputs the content of the variable
     !> (public for type Tim)
     procedure, public, pass :: info => CodeTim_print
  end type CodeTim
contains
  !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type CodeTim)
  !> Instantiate (allocate if necessary)
  !> and initilize (by also reading from input file)
  !> variable of type CodeTim
  !>
  !> usage:
  !>     call 'var'%init()
  !>
  !<-------------------------------------------------------------
  subroutine CodeTim_construct(this)
    use Globals
    implicit none
    !vars
    class(CodeTim), intent(out) :: this
    ! local vars

    call this%TOT%init()
    call this%OVH%init()
    call this%ASSEMBLY%init()
    call this%PREC%init()
    call this%LINEAR_SOLVER%init()
    call this%EXTRACOMP%init()
    call this%SAVE%init()
    call this%WASTED%init()
    call this%ALGORITHM%init()

  end subroutine CodeTim_construct

  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type CodeTim)
  !> only scalars, does nothing
  !>
  !> usage:
  !>     call 'var'%kill(lun)
  !>
  !> where:
  !> \param[in] lun -> integer. I/O unit for error message output
  !<-----------------------------------------------------------
  subroutine CodeTim_destroy(this)
    implicit none
    class(CodeTim), intent(inout) :: this
    
    call this%TOT%kill()
    call this%OVH%kill()
    call this%ASSEMBLY%kill()
    call this%PREC%kill()
    call this%LINEAR_SOLVER%kill()
    call this%EXTRACOMP%kill()
    call this%SAVE%kill()
    call this%WASTED%kill()
    call this%ALGORITHM%kill()

  end subroutine CodeTim_destroy

  !>-------------------------------------------------------------
  !> Info procedure.
  !> (public procedure for type CodeTim)
  !> Prints content of a variable of type CodeTim
  !> 
  !> usage:
  !>     call 'var'%info(lun [, add_msg])
  !>
  !> where:
  !> \param[in] lun: integer. output unit
  !> \param[in] add_msg: character, optional.
  !>            additional message to be printed.
  !>
  !<-------------------------------------------------------------
  subroutine CodeTim_print(this, lun, add_msg)
    use Globals
    implicit none
    ! vars
    class(CodeTim), intent(in) :: this
    integer, intent(in) :: lun
    character(len=*), optional, intent(in) :: add_msg
    !local vars
    character(len=256) :: rdwr1,rdwr2
    real(kind=double) :: OVHperc_wct,OVHperc_usr
    real(kind=double) :: ASSEMBLYperc_wct,ASSEMBLYperc_usr
    real(kind=double) :: PRECperc_wct,PRECperc_usr
    real(kind=double) :: LINEAR_SOLVERperc_wct,LINEAR_SOLVERperc_usr
    real(kind=double) :: EXTRACOMPperc_wct,EXTRACOMPperc_usr
    real(kind=double) :: SAVEperc_wct,SAVEperc_usr
    real(kind=double) :: WASTEDperc_wct,WASTEDperc_usr
    real(kind=double) :: ALGORITHMperc_wct,ALGORITHMperc_usr

    write(lun,*)
    if (present(add_msg)) write(lun,'(a)') add_msg

    ! calculate perrcentages
    OVHperc_wct = this%OVH%CUMwct/this%TOT%CUMwct*100
    OVHperc_usr = this%OVH%CUMusr/this%TOT%CUMusr*100
    ASSEMBLYperc_wct = this%ASSEMBLY%CUMwct/this%TOT%CUMwct*100
    ASSEMBLYperc_usr = this%ASSEMBLY%CUMusr/this%TOT%CUMusr*100
    PRECperc_wct = this%PREC%CUMwct/this%TOT%CUMwct*100
    PRECperc_usr = this%PREC%CUMusr/this%TOT%CUMusr*100
    LINEAR_SOLVERperc_wct = this%LINEAR_SOLVER%CUMwct/this%TOT%CUMwct*100
    LINEAR_SOLVERperc_usr = this%LINEAR_SOLVER%CUMusr/this%TOT%CUMusr*100
    EXTRACOMPperc_wct = this%EXTRACOMP%CUMwct/this%TOT%CUMwct*100
    EXTRACOMPperc_usr = this%EXTRACOMP%CUMusr/this%TOT%CUMusr*100
    SAVEperc_wct = this%SAVE%CUMwct/this%TOT%CUMwct*100
    SAVEperc_usr = this%SAVE%CUMusr/this%TOT%CUMusr*100
    WASTEDperc_wct = this%WASTED%CUMwct/this%TOT%CUMwct*100
    WASTEDperc_usr = this%WASTED%CUMusr/this%TOT%CUMusr*100
    ALGORITHMperc_wct = this%ALGORITHM%CUMwct/this%TOT%CUMwct*100
    ALGORITHMperc_usr = this%ALGORITHM%CUMusr/this%TOT%CUMusr*100

    call this%TOT%info          (lun, ' Total sim. cpu  : ','100.%','100.%')  
     write(rdwr1,'(F4.0,a1)') OVHperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') OVHperc_usr,'%'
    call this%OVH%info          (lun, ' Overhead        : ',rdwr1,rdwr2)
    write(rdwr1,'(F4.0,a1)') SAVEperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') SAVEperc_usr,'%'
    call this%SAVE%info         (lun, ' Saving time     : ',rdwr1,rdwr2)
    write(rdwr1,'(F4.0,a1)') WASTEDperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') WASTEDperc_usr,'%'
    call this%WASTED%info       (lun, ' Wasted time     : ',rdwr1,rdwr2)
    write(rdwr1,'(F4.0,a1)') ALGORITHMperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') ALGORITHMperc_usr,'%'
    call this%ALGORITHM%info    (lun, ' Algorithm time  : ',rdwr1,rdwr2)
    write(lun,*) '*****************************************************'
    write(rdwr1,'(F4.0,a1)') ASSEMBLYperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') ASSEMBLYperc_usr,'%'
    call this%ASSEMBLY%info     (lun, ' matrix assembly : ',rdwr1,rdwr2)
    write(rdwr1,'(F4.0,a1)') PRECperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') PRECperc_usr,'%'
    call this%PREC%info     (lun, ' prec.  assembly : ',rdwr1,rdwr2)
    write(rdwr1,'(F4.0,a1)') LINEAR_SOLVERperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') LINEAR_SOLVERperc_usr,'%'
    call this%LINEAR_SOLVER%info(lun, ' Linear solver   : ',rdwr1,rdwr2)
    write(rdwr1,'(F4.0,a1)') EXTRACOMPperc_wct,'%'
    write(rdwr2,'(F4.0,a1)') EXTRACOMPperc_usr,'%'
    call this%EXTRACOMP%info    (lun, ' Extra comput.   : ',rdwr1,rdwr2)


  end subroutine CodeTim_print

end module MuffeTiming
