module AdmkWrap4Python
  use KindDeclaration
  use Globals
  use AlgebraicDMK
  use SparseMatrix
  use DmkControls
  use DmkInputsData
  use TdensPotentialSystem
  implicit none

  private
  public :: admkwrap, graph_admk_constructor, admk_destructor,admk_cycle_reverse_communication
  public :: store_evolution_info
  !>--------------------------------------------------------
  !> Derived type created to wrap admk
  !> (f90wrap returns errors where class(abs_linop) pointer
  !> are member of the type).
  !> This tpye stores:
  !> * the (transposed) signed incidence matrix of a graph
  !<--------------------------------------------------------
  type :: admkwrap
     type(admk)  :: admk_var
     type(spmat) :: transpose_signed_incidence_matrix
   contains
  end type admkwrap

  !>--------------------------------------------------------
  !> Derived type for passing in and out
  !> integer and real flags in the reverrse communication
  !> porcedure.
  !> (f90wrap does not changed their value if they are not
  !> encapsulated in a derived type).
  !<--------------------------------------------------------
  public :: flags
  type :: flags
     integer :: flag=1
     integer :: flag_task=0
     integer :: info=0
     real(kind=double) :: current_time=zero
     integer :: max=0
   contains
  end type flags
     
contains
  !>--------------------------------------------------------
  !> Admkwrap destructor
  !<--------------------------------------------------------
  subroutine admk_destructor(this,lun_err)     
    use Globals
    implicit none
    type(admkwrap), intent(inout) :: this
    integer,        intent(in   ) :: lun_err

    call this%admk_var%kill(lun_err)
    call this%transpose_signed_incidence_matrix%kill(lun_err)
  end subroutine admk_destructor

  !>--------------------------------------------------------
  !> Admkwrap constructor for graph problem
  !<--------------------------------------------------------
  subroutine graph_admk_constructor(&
       this,ctrl,nedges,edges,weight)
    use SparseMatrix
    implicit none
    type(admkwrap), target,      intent(inout) :: this
    type(DmkCtrl),               intent(in   ) :: ctrl
    integer,                     intent(in   ) :: nedges
    integer,                     intent(in   ) :: edges(2,nedges)
    real(kind=double), optional, intent(in   ) :: weight(nedges)
    ! local 
    integer :: lun_err=0
    integer :: ntdens, npot
    integer :: i,m
    type(spmat) :: matrixAT

    ntdens=nedges
    npot=maxval(edges)

    write(*,*) ntdens, npot
    ! init matrix inside admk tpye
    call matrixAT%init(lun_err,&
         ntdens,npot,& 
         2*ntdens,&
         'csr')
    
    m=0
    matrixAT%ia(1)=1
    do i=1,ntdens
       !
       ! two non zeros for each edge
       ! 
       matrixAT%ia(i+1)=matrixAT%ia(i)+2

       m=m+1
       matrixAT%ja(m)=edges(1,i)
       matrixAT%coeff(m)=one

       m=m+1
       matrixAT%ja(m)=edges(2,i)
       matrixAT%coeff(m)=-one
    end do

    this%transpose_signed_incidence_matrix=matrixAT
    call this%transpose_signed_incidence_matrix%transpose(0)
    this%transpose_signed_incidence_matrix%name='G'

    !
    ! build admk pair
    ! incidence_transpose
    call this%admk_var%init( ctrl, this%transpose_signed_incidence_matrix,weight)

    
  end subroutine graph_admk_constructor

 
  !>--------------------------------------------------------
  !> Reverse cummunication subroutine for Admkwrap tpye
  !<--------------------------------------------------------
  subroutine admk_cycle_reverse_communication(&
       this,&
       rc_flags,&
       inputs_data,tdpot,ctrl,original_ctrl)
    implicit none
    type(admkwrap),    intent(inout) :: this
    type(flags),       intent(inout) :: rc_flags
    type(DmkInputs),   intent(in   ) :: inputs_data
    type(tdpotsys),    intent(inout) :: tdpot
    type(DmkCtrl),     intent(inout) :: ctrl
    type(DmkCtrl),     intent(in   ) :: original_ctrl

    call this%admk_var%new_dmk_cycle_reverse_communication(&
         rc_flags%flag,rc_flags%flag_task,rc_flags%info,rc_flags%current_time,&
         inputs_data,tdpot,ctrl,original_ctrl)

  end subroutine admk_cycle_reverse_communication

  !>--------------------------------------------------------
  !> Subroutine for storing amdk time evolution 
  !<--------------------------------------------------------  
  subroutine store_evolution_info(this, tdpot,inputs_data,ctrl,timefun)
    use TimeFunctionals
    implicit none
    type(admkwrap),    intent(inout) :: this
    type(tdpotsys),    intent(inout) :: tdpot ! intent(inout) because we use some work arrays
    type(DmkInputs),   intent(in   ) :: inputs_data
    type(DmkCtrl),     intent(in   ) :: ctrl
    type(evolfun),     intent(inout) :: timefun

    
    ! compute all functionals storing results in tdpot and alg
    call this%admk_var%compute_functionals(inputs_data,tdpot,ctrl)
        
    ! copy current state informations into timefun
    call this%admk_var%store_evolution(timefun,tdpot%time_iteration,&
         tdpot,inputs_data,ctrl)

  end subroutine store_evolution_info
  
end module AdmkWrap4Python
