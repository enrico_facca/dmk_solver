module DmkDiscretization
  use Globals
  use LinearSolver
  use TdensPotentialSystem
  use DmkInputsData
  use Timing
  implicit none
  private
  public :: dmkpair, destructor, syncronization, update, variation
  public :: new_dmk_cycle_reverse_communication,dictionary_update_errors
  public :: problem, problem_destructor
  public :: solution, solution_destructor
  public :: iterative_solver, solver_destructor
  public :: iterative_algorithm, iterate_algorithm
  !>---------------------------------------------------
  !> Abstract type contains the minimal procedure to 
  !> compute DMK time evolution, either in the continuous
  !> and the discrte case.
  !> The 3 fundamental procedures, for which the abstract interface,
  !> is defined are :
  !> 1 - syncronized_tdpot
  !> 2 - udpate_tdpot
  !> 3 - compute_variation_tdpot
  !> There are 2 other procedures with  
  !> the actual implementation. Procedure that may be 
  !> overrriden if requires by a specific procedure.
  !<-------------------------------------------------
  type, abstract, public :: dmkpair
   contains
     !> Abstract procedure to override
     !> Static destructor
     !> (procedure public for type dmkpair)
     procedure(destructor), deferred :: kill
     !> Abstract procedure to be overrided
     !> Procedure for syncronize the whole system
     !> Tdens Potential and derived varaibles
     !> (procedure public for type dmkpair)
     procedure(syncronization), deferred :: syncronize_tdpot
     !> Abstract procedure to override
     !> Procedure for upate the whole system
     !> to the next time step
     !> (procedure public for type dmkpair)
     procedure(update), deferred :: update_tdpot
     !> Abstract procedure to override
     !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$
     !> (procedure public for type dmkpair_space_discretization)
     procedure(variation), deferred :: compute_tdpot_variation
     !> Procedure to reset controls 
     !> (time-step, preconditioner construction etc.) 
     !> in case of failure in update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: reset_controls_after_update_failure
     !> Procedure to set controls (time-step, preconditioner construction
     !> etc.) for next update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: set_controls_next_update
     !> Procedure to set controls (time-step, preconditioner construction
     !> etc.) for next update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: new_dmk_cycle_reverse_communication
  end type dmkpair

  abstract interface
     !>--------------------------------------------------------------------
     !> Static destructor
     !> 
     !> (public procedure for type dmkpair)
     !> 
     !> usage: call var%kill(lun_err)
     !> 
     !> where:
     !> \param[in ] lun_err -> integer. Logical unit for errors message
     !>---------------------------------------------------------------------
     subroutine destructor(this,lun_err)
       use Globals
       import dmkpair
       implicit none
       class(dmkpair), intent(inout) :: this
       integer,       intent(in   ) :: lun_err

     end subroutine destructor

     !>--------------------------------------------------------------------
     !> Procedure for the syncronization od tdens potential variable
     !> 
     !> (public procedure for type dmkpair)
     !> 
     !> usage: call var%syncronization(tdpot,ode_inputs,ctrl,info)
     !> 
     !> where:
     !> \param[inout] tdpot      -> tye(tdpotsys). Tdens-Potentail System
     !> \param[in   ] ode_inputs -> tye(DmkInputs). Input for Ode
     !> \param[in   ] ctrl       -> tye(CtrlPrm). Dmk controls
     !> \param[inout] info       -> integer. Flag for info 
     !>                              0 = Everything is fine
     !>                              1** = Error in linear solver
     !>                              11* = Convergence not achivied
     !>                              12* = Internal error in linear solver
     !>                              2**  = Error in non-linear solver
     !>                              21* = Non-linear convergence not achievied
     !>                              22* = Non-linear solver was divergening
     !>                              23* = Condition for non-linear solver to
     !>                                     continue failed
     !>                              3**  = Other errors
     !>---------------------------------------------------------------------
     subroutine syncronization( this,&
          tdpot,&
          ode_inputs,&
          ctrl,&
          info)
       use TdensPotentialSystem
       use DmkInputsData
       use DmkControls
      
       import dmkpair
       implicit none
       class(dmkpair), target,     intent(inout) :: this
       class(tdpotsys),     intent(inout) :: tdpot
       class(DmkInputs),    intent(in   ) :: ode_inputs
       class(DmkCtrl),      intent(in   ) :: ctrl
       integer,             intent(inout) :: info


     end subroutine syncronization

     !>------------------------------------------------
     !> Procedure for computation of next state of system
     !> ( all variables ) given the preovius one
     !> ( private procedure for type dmkpair, used in update)
     !> 
     !> usage: call var%update(lun_err,itemp)
     !> 
     !> where:
     !> \param[inout] tdpot -> type( tdpotsys ). Tdens-pot system
     !>                          syncronized at time $t^{k}$
     !>                          to be updated at time $t^{k+1}$
     !> \param[in   ] odein -> type(DmkInputs). Input data
     !>                          at time t^k or t^{k+1}, according
     !>                          time discretization scheme
     !> \param[in   ] ctrl  -> type(CtrlPrm). Controls of DMK
     !>                        evolution (time-step, linear system approach)
     !> \param[in   ] info  -> integer. Flag with 3 digits for errors.
     !>                        If info==0 no erros occurred.
     !>                        First digit from the left describes 
     !>                        the main errors. The remaining digits can be used to
     !>                        mark specific errors. Current dictionary:
     !>                        * 1**   = Error in linear solver
     !>                         * 11*  = Convergence not achivied
     !>                          * 111 = Max iterations 
     !>                          * 112 = Internal error in linear solver
     !>                         * 13*  = Failure Preconditoner
     !>                          * 131 = Failure preconditioner assembly  
     !>                          * 132 = Failure preconditioner application
     !>                        * 2**   = Error in non-linear solver
     !>                         *21*   = Non-linear convergence not achievied
     !>                         *22*   = Non-linear solver stopped because was divergening
     !>                         *23*   = Condition for non-linear solver to
     !>                                 continue failed
     !>                        * 3**   = Other errors
     !<-----------------------------------------------------------------------------
     subroutine update(this,&
          tdpot,&
          ode_inputs,&
          ctrl,&
          info)
       use TdensPotentialSystem
       use DmkControls
       use DmkInputsData  
       import dmkpair

       implicit none
       class(dmkpair), target, intent(inout) :: this    
       class(tdpotsys),         intent(inout) :: tdpot
       class(DmkInputs),        intent(in   ) :: ode_inputs
       class(DmkCtrl),          intent(in   ) :: ctrl
       integer,                intent(inout) :: info

     end subroutine update


     !>----------------------------------------------------------------
     !> Function to eval.
     !> $ var(\TdensH^k):=\frac{
     !>                     \| \tdens^{k+1} - \tdens^{k} \|_{L2} 
     !>                   }{ 
     !>                     \| \tdens^{k+1} \|_{L2}}  
     !>                   } $
     !> (procedure public for type dmkpair) 
     !> 
     !> usage: call var%err_tdesn()
     !>    
     !> where:
     !> \param  [in ] var       -> type(dmkpair) 
     !> \result [out] var_tdens -> real. Weighted var. of tdens
     !<----------------------------------------------------------------
     function variation(this,tdpot,ctrl) result(var)
       use Globals
       use TdensPotentialSystem
       use DmkControls
       import dmkpair

       implicit none
       class(dmkpair),    intent(in   ) :: this
       class(tdpotsys),    intent(in   ) :: tdpot
       class(DmkCtrl),     intent(in   ) :: ctrl
       real(kind=double) :: var

     end function variation


  end interface

  !>---------------------------------------------------
  !> Abstract type contains the problem inputs
  !<-------------------------------------------------
  type, abstract, public :: problem
   contains
     !> Abstract procedure to override
     !> Static destructor
     !> (procedure public for type dmkpair)
     procedure(problem_destructor), deferred :: kill
  end type problem

  abstract interface
     !>--------------------------------------------------------------------
     !> Static destructor
     !> 
     !> (public procedure for type dmkpair)
     !> 
     !> usage: call var%kill(lun_err)
     !> 
     !> where:
     !> \param[in ] lun_err -> integer. Logical unit for errors message
     !>---------------------------------------------------------------------
     subroutine problem_destructor(this,lun_err)
       use Globals
       import problem
       implicit none
       class(problem), intent(inout) :: this
       integer,       intent(in   ) :: lun_err

     end subroutine problem_destructor
  end interface


  !>---------------------------------------------------
  !> Abstract type contains the problem solution
  !<-------------------------------------------------
  type, abstract, public :: solution
   contains
     !> Abstract procedure to override
     !> Static destructor
     !> (procedure public for type dmkpair)
     procedure(solution_destructor), deferred :: kill
  end type solution

  abstract interface

     !>--------------------------------------------------------------------
     !> Static destructor
     !> 
     !> (public procedure for type dmkpair)
     !> 
     !> usage: call var%kill(lun_err)
     !> 
     !> where:
     !> \param[in ] lun_err -> integer. Logical unit for errors message
     !>---------------------------------------------------------------------
     subroutine solution_destructor(this,lun_err)
       use Globals
       import solution
       implicit none
       class(solution), intent(inout) :: this
       integer,         intent(in   ) :: lun_err

     end subroutine solution_destructor
  end interface

  
  !>---------------------------------------------------
  !> Abstract type contains the minimal procedure to 
  !> compute DMK time evolution, either in the continuous
  !> and the discrte case.
  !> The 3 fundamental procedures, for which the abstract interface,
  !> is defined are :
  !> 1 - syncronized_tdpot
  !> 2 - udpate_tdpot
  !> 3 - compute_variation_tdpot
  !> There are 2 other procedures with  
  !> the actual implementation. Procedure that may be 
  !> overrriden if requires by a specific procedure.
  !<-------------------------------------------------
  type, abstract, public :: iterative_solver
   contains
     !> Abstract procedure to override
     !> Static destructor
     !> (procedure public for type dmkpair)
     procedure(solver_destructor), deferred :: kill
  end type iterative_solver

  abstract interface  
     !>--------------------------------------------------------------------
     !> Static destructor
     !> 
     !> (public procedure for type dmkpair)
     !> 
     !> usage: call var%kill(lun_err)
     !> 
     !> where:
     !> \param[in ] lun_err -> integer. Logical unit for errors message
     !>---------------------------------------------------------------------
     subroutine solver_destructor(this,lun_err)
       use Globals
       import iterative_solver
       implicit none
       class(iterative_solver), intent(inout) :: this
       integer,       intent(in   ) :: lun_err

     end subroutine solver_destructor

  end interface


  !>---------------------------------------------------
  !> Abstract type contains the minimal procedure to 
  !> compute DMK time evolution, either in the continuous
  !> and the discrte case.
  !> The 3 fundamental procedures, for which the abstract interface,
  !> is defined are :
  !> 1 - syncronized_tdpot
  !> 2 - udpate_tdpot
  !> 3 - compute_variation_tdpot
  !> There are 2 other procedures with  
  !> the actual implementation. Procedure that may be 
  !> overrriden if requires by a specific procedure.
  !<-------------------------------------------------
  type, abstract, public :: iterative_algorithm
     integer :: flag=0
     integer :: ierr=0
     integer :: iterations=0
     integer :: max_iterations=100
     integer :: restarts=0
     integer :: max_restarts=10
     integer :: verbose=0
     type(tim) :: CPU_update
     real(kind=double) :: cpu_time=zero
     real(kind=double) :: cpu_wasted=zero
   contains
     !> Abstract procedure to override
     !> Procedure for upate the whole system
     !> to the next time step
     !> (procedure public for type dmkpair)
     procedure(iterate_algorithm), deferred :: iterate     
  end type iterative_algorithm

  abstract interface
     subroutine iterate_algorithm(this,&
          info)
       import iterative_algorithm
       implicit none
       class(iterative_algorithm),intent(inout) :: this
       integer,                   intent(inout) :: info       
       
     end subroutine iterate_algorithm

  end interface
contains
  
  !>----------------------------------------------------
  !> Procedure to reset controls after failure of update_tdpot
  !> procedure within the dmk_cycle_reverse_communication
  !> procedure.
  !> Derived type  this(dmkpair), ode_inputs and tdpot
  !> are used to extimate best controls for next update.
  !> ( public procedure for type dmkpair,
  !> used in dmk_cycle_reverse_communication)
  !> 
  !> usage: call var%update(lun_err,itemp)
  !> 
  !> where:
  !> \param[in   ] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[inout] ctrl  -> type(CtrlPrm). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !>
  !> COMMENTS:
  !> This is passed with intent inout for using scratch
  !> arrays and avoid allocation/deallocation
  !> of temporary arrays
  !<---------------------------------------------------   
  subroutine reset_controls_after_update_failure(this,&
       info,&
       ode_inputs,&
       tdpot,&
       original_ctrl,&
       ctrl,&
       input_time,&
       ask_for_inputs)
    
    use DmkInputsData 
    use TdensPotentialSystem
    use DmkControls


    implicit none
    class(dmkpair),    intent(inout) :: this
    integer,           intent(in   ) :: info
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(DmkCtrl),     intent(in   ) :: original_ctrl
    type(DmkCtrl),     intent(inout) :: ctrl
    real(kind=double), intent(inout) :: input_time
    logical,           intent(inout) :: ask_for_inputs
    ! local
    integer :: digit1,digit2,digit3

    digit1=info/100
    digit2=mod(info,100)/10
    digit3=mod(info,10)

    select case ( digit1 )
           
    case (1)
       ! failure of update due failure in linear system
       ! solution 
       ctrl%deltat=max(ctrl%deltat_lower_bound,&
            min(ctrl%deltat_upper_bound,ctrl%deltat/ctrl%deltat_expansion_rate))
    case (2)
       ! failure of non linear solver for update 
       ! (convergence not achivied, diverging residua, etc) 
       ! 
       ctrl%deltat=max(ctrl%deltat_lower_bound,&
            min(ctrl%deltat_upper_bound,ctrl%deltat/ctrl%deltat_expansion_rate))
    end select

    !
    ! select time asked depending on
    ! either explicit or implicit scheme are used
    select  case (ctrl%time_discretization_scheme)
    case (1)
       input_time=tdpot%time
    case (2)
       input_time=tdpot%time+ctrl%deltat
    case (3)
       input_time=tdpot%time
    case (4)
       input_time=tdpot%time+ctrl%deltat
    end select
       
    
       


  end subroutine reset_controls_after_update_failure

  !>----------------------------------------------------
  !> Procedure to set controls for next update within
  !> the dmk_cycle_reverse_communication subroutine.
  !> Derived type  this(dmkpair), ode_inputs and tdpot
  !> are used to extimate best controls for next update.
  !> ( public procedure for type dmkpair,
  !> used in dmk_cycle_reverse_communication)
  !> 
  !> usage: call var%update(lun_err,itemp)
  !> 
  !> where:
  !> \param[in   ] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[inout] ctrl  -> type(CtrlPrm). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !>
  !> COMMENTS:
  !> This is passed with intent inout for using scratch
  !> arrays and avoid allocation/deallocation
  !> of temporary arrays
  !<---------------------------------------------------    
  subroutine set_controls_next_update(this,&   
       ode_inputs,&
       tdpot,&
       ctrl,input_time)
    use DmkInputsData 
    use TdensPotentialSystem
    use DmkControls
    implicit none
    class(dmkpair),    intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(DmkCtrl),     intent(inout) :: ctrl
    real(kind=double), intent(inout) :: input_time


    write(*,*) 'Here dmkpair'

  end subroutine set_controls_next_update

  
  !>----------------------------------------------------
  !> Procedure for time evolution of dmk dynamic with
  !> reverse communication. This programming approch
  !> is based on passing in-and-out from the main
  !> subroutine a flag (the integer flag in our) case
  !> that say what the user must do on the data.
  !> In this case we have:
  !> *0 : Exit from cycle 
  !> *1 : Initial value
  !> *2 : Use must se tthe inputs at the given time 
  !> *3 : User can study the tdpot state
  !> *4 : User must compute if steady state is achieved
  !> *5 : User must set the controls for the next update
  !> *6 : User must reset the controls due to update failure
  !>
  !> 
  !> usage: call var%new_dmk_cycle_reverse_communication(flag,&
  !>                    flag_task, info, input_time,&
  !>                    ode_inputs,tdpot,ctrl,original_ctrl)
  !> 
  !> where:
  !> \param[inout] flag       -> integer. Comunication flag
  !> \param[inout] flag_taks  -> integer. Comunication flag for tell
  !>                             syncronization or or update
  !> \param[inout] info       -> integer. Error flag
  !>                             * info==0 No errors  
  !>                             * info>0  Some errors occured
  !>                               dictionary_update_errors
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[in   ] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[inout] ctrl  -> type(CtrlPrm). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !> \param[inout] original_ctrl  -> type(CtrlPrm). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !>
  !> COMMENTS:
  !> This is passed with intent inout for using scratch
  !> arrays and avoid allocation/deallocation
  !> of temporary arrays
  !<---------------------------------------------------   
  subroutine new_dmk_cycle_reverse_communication(&
          this,&
          flag,flag_task,info,input_time,&
          ode_inputs,tdpot,ctrl,original_ctrl)
       use Globals
       use DmkInputsData
       use TdensPotentialSystem
       use DmkControls
       use TimeInputs
       use Timing
       implicit none
       class(dmkpair),    intent(inout) :: this
       integer,           intent(inout) :: flag
       integer,           intent(inout) :: flag_task
       integer,           intent(inout) :: info
       real(kind=double), intent(inout) :: input_time
       type(DmkInputs),   intent(in   ) :: ode_inputs
       class(tdpotsys),    intent(inout) :: tdpot
       type(DmkCtrl),     intent(inout) :: ctrl
       type(DmkCtrl),     intent(in   ) :: original_ctrl
       !
       logical :: rc
       logical :: save_test
       logical :: ask_for_inputs
       integer :: res
       integer :: lun_err
       integer :: lun_out
       integer :: lun_stat
       character(len=256) :: cst,msg,msg_out,out_format,str,str1
       real(kind=double) :: cpu_time
       type(tim) :: CPU

      
       ! shorthand  
       lun_err  = ctrl%lun_err
       lun_out  = ctrl%lun_out
       lun_stat = ctrl%lun_statistics

       if (ctrl%debug .ge. 2) then
          write(lun_out,*) ' __________________'
          write(str,*) 'IN REVERSE COMMUNICATION : flag, flag_task=',flag, flag_task
          call ctrl%print_string('update',1,str)
          write(lun_out,*) 'tdpot%time_iteration, tdpot%time, input_time , ode_inputs%time'
          write(lun_out,*) tdpot%time_iteration, tdpot%time, input_time , ode_inputs%time
       end if

       
       select case (flag)
       case (1) ! initialization case
          ! open files where to write data ( if ctrl is settled in this way)
          if (tdpot%time_iteration .eq. 0) call ctrl%write_data2file('head',tdpot)

          
          ! return and read inputs at time zero
          flag=2
          flag_task=1
          input_time = tdpot%time
          tdpot%first = .True.
          return 
       case( 2 ) 
          if ( flag_task .eq. 1) then
             msg=ctrl%separator('INPUTS')
             call ctrl%print_string('update',1, msg)
             ! print problem info before first update
             if ( tdpot%first) then
                if ( (ctrl%info_state .ge. 3) .or. (ctrl%debug .ge. 1) ) then
                   call ode_inputs%info(ctrl%lun_out)
                end if
                call ode_inputs%info(ctrl%lun_statistics)
                tdpot%first=.False.
             end if

             ! SYNCRONIZED
             msg=ctrl%separator('SYNCRONIZATION TDPOT')
             call ctrl%print_string('update',1, msg)
             call CPU%init()
             call CPU%set('start')
             call this%syncronize_tdpot(tdpot,ode_inputs,ctrl,info)
             call CPU%set('stop')
             tdpot%cpu_time=tdpot%cpu_time+CPU%WCT
             call CPU%kill()

             msg=ctrl%separator('')
             call ctrl%print_string('update',1, msg)
             

             !
             ! Evalutate variation of tdens-potential system.
             !
             if ( tdpot%time_iteration >0 ) then
                
                if (ctrl%user_system_variation .eq. 0 ) then
                   ! use default procedure and go to study session
                   tdpot%system_variation = &
                        this%compute_tdpot_variation(tdpot,ctrl)
                   flag      = 3
                   info = 0
                   return
                else
                   ! ask the user to evalute variation
                   flag = 4
                   info = 0
                   return
                end if
             else
                ! go to study session
                flag = 3
                info = 0
                return
             end if
          end if
          
          if (flag_task .eq. 2) then
             info=0
             !
             ! Update cycle. 
             ! If it succees goes to the evaluation of
             ! system varaition (flag ==4 ).
             ! In case of failure, reset controls
             ! and ask new inputs or ask the user to
             ! reset controls (flag == 6 ).
             !
             if ( tdpot%nrestart_update == 0) then
                tdpot%cpu_wasted=zero
                tdpot%cpu_update_wasted=zero
             end if

             !
             ! update member pot, tdens, gfvar of type tdpotsys
             !
             ! copy before update
             tdpot%tdens_old = tdpot%tdens
             tdpot%gfvar_old = tdpot%gfvar
             tdpot%pot_old   = tdpot%pot
             tdpot%time_old  = tdpot%time
             
             ! print info 
             info=0
             if (  tdpot%nrestart_update .eq. 0) then
                msg=' '
                call ctrl%print_string('update',1,msg)
                
                out_format='(a,I5.5,a,1pe8.2,a,I2)'
                write(str,out_format) 'UPDATE ',tdpot%time_iteration+1,' | TIME STEP = ', ctrl%deltat
                
                write(msg,'(a)') ctrl%separator(str)
                call ctrl%print_string('update',1,msg)
             else
                out_format='(a,I5.5,a,1pe8.2,a,I2)'
                write(str,out_format) 'UPDATE ',tdpot%time_iteration+1,' | TIME STEP = ', ctrl%deltat,&
                     ' RESTART ', tdpot%nrestart_update
                write(msg,'(a)') ctrl%separator(str)
                call ctrl%print_string('update',1,msg)
             end if
                
             
             !
             ! actual update
             !
             call CPU%init()
             call CPU%set('start')
             call this%update_tdpot(&
                  tdpot,&
                  ode_inputs,&
                  ctrl,&
                  info)
             call CPU%set('stop')
             cpu_time=CPU%WCT
             call CPU%kill()

             tdpot%cpu_time=tdpot%cpu_time+cpu_time
             tdpot%cpu_update = tdpot%cpu_update+cpu_time
             
             if ( info .eq. 0) then
                ! info for succesfull update
                !tdpot%cpu_time=tdpot%cpu_tim+cpu_time
                out_format=ctrl%formatting('r')
                write(str1,out_format) tdpot%cpu_wasted+cpu_time
                if ( tdpot%nrestart_update .eq. 0) then
                   write(msg,*) ctrl%separator('UPDATE SUCCEED - CPU:'//etb(str1))
                else
                   write(str,'(I3)') tdpot%nrestart_update
                   write(msg,*) ctrl%separator('UPDATE SUCCEED with '//etb(str)//' restarts- CPU:'//etb(str1))
                end if
                call ctrl%print_string('update',1,msg)

             

                !
                ! syncronized tdpot and ode_inputs
                ! For example explicit Euler use the Inputs
                ! at t^{k} while at this point tdens-pot are 
                ! at t^{k+1}
                !
                if ( (.not. ode_inputs%steady) .and. (abs(tdpot%time-ode_inputs%time) > small) ) then

                   flag        = 2          ! asking for inputs
                   input_time = tdpot%time ! syncronized at tdpot%time
                   flag_task   = 1          ! syncronize
                   info        = 0 
                   
                   out_format=ctrl%formatting('arra')
                   write(str,out_format) 'NOT STEADY INPUTS and (TDPOT TIME),(INPUT TIME)= ',&
                        tdpot%time,ode_inputs%time, ' - SYNCRONIZATE'
                   call ctrl%print_string('update',1,etb(str))
                   ! return to user 
                   return
                else                   
                   !
                   ! Evalutate variation of tdens-potential system
                   ! with type procedure compute_tdpot_variation
                   ! or ask the user to compute it sending flag = 4.
                   ! 
                   !
                   if (ctrl%user_system_variation .eq. 0 ) then
                      tdpot%system_variation = &
                           this%compute_tdpot_variation(tdpot,ctrl)
                      flag = 3
                      info = 0
                      return
                   else
                      flag = 4
                      info = 0
                      return
                   end if
                end if
             else
                !
                ! update failed
                !

               
                write(msg,*) 'tdpot%nrestart_update', tdpot%nrestart_update,&
                     'ctrl%max_restart_update', ctrl%max_restart_update
                call ctrl%print_string('update',1,msg)
                
                tdpot%cpu_wasted = tdpot%cpu_wasted + cpu_time
                tdpot%cpu_update_wasted = tdpot%cpu_update_wasted + cpu_time

                ! try one restart more
                tdpot%nrestart_update = tdpot%nrestart_update + 1

                ! stop if number max restart update is passed 
                if (tdpot%nrestart_update .ge. ctrl%max_restart_update) then
                   ! print 
                   write(msg,*) ctrl%separator('UPDATE FAILURE')
                   call ctrl%print_string('update',1,msg)
                   ! print to error unit
                   write(ctrl%lun_err,*) etb(msg)
                   
                   flag=-2
                   info=-1
                else
                   !
                   ! reset controls
                   !
                   if ( ctrl%user_control_reset .eq. 0) then
                      ! reset controls. If deltat changes return  
                      ! flag==2 to get the data at the right time
                      call this%reset_controls_after_update_failure(&
                           info,&
                           ode_inputs,&
                           tdpot,&
                           original_ctrl,&
                           ctrl,&
                           input_time,&
                           ask_for_inputs)

                      ! recall update session 
                      flag =  2
                      info = -1
                   else
                      ! use will reset controls at its wish
                      flag =  6
                      info = -1
                   end if
                end if

                ! restores previous data
                tdpot%tdens = tdpot%tdens_old 
                tdpot%gfvar = tdpot%gfvar_old 
                tdpot%pot   = tdpot%pot_old
                tdpot%time  = tdpot%time_old

                ! break cycle
                return

             end if
             
             
          end if
       case (3)
          !
          ! optional saving procedure
          !
          call ctrl%write_data2file('body',tdpot)

          !
          ! optional print info
          !
          if (ctrl%info_state .ge. 1) call tdpot%info_time_evolution(ctrl%lun_out)
          call tdpot%info_time_evolution(ctrl%lun_statistics)
          if (ctrl%info_state .ge. 2) then
             out_format=ctrl%formatting('rar')
             write(msg,out_format) minval(tdpot%tdens),'<=TDENS<=' ,maxval(tdpot%tdens)
             call ctrl%print_string('state',1,msg)
             if ( ctrl%time_discretization_scheme >=3 )  then
                write(msg,out_format) minval(tdpot%gfvar),'<=GFVAR<=' ,maxval(tdpot%gfvar)
                call ctrl%print_string('state',1,msg)
             end if
          end if
          !
          ! Evalute if simulation has to continue.
          ! If yes flag = 0 is settled
          !

          ! If number of iterations exceeds maximum break 
          ! time cycle setting flag=0 with info=-1
          if ( tdpot%time_update .ge. ctrl%max_time_iterations) then
             flag = 0
             info = -1
             out_format=ctrl%formatting('ar')
             write(msg,*) ' Update Number exceed limits' ,ctrl%max_time_iterations
             call ctrl%print_string('state', 1, msg)
             call ctrl%write_data2file('tail',tdpot)
             return 
          end if

          ! set info to zero if no update will be done
          if (ctrl%max_time_iterations == 0 ) then
             info = 0
             flag = 0
             call ctrl%write_data2file('tail',tdpot)
             return 
          end if
             
          ! check convergence
          if (tdpot%time_update > 0 ) then
             ! check if convergence was achieved
             if ( tdpot%system_variation .le. ctrl%tolerance_system_variation) then
                ! convergence achieved. Set flag for exit
                flag = 0
                info = 0
                call ctrl%write_data2file('tail',tdpot)
                return 
             else
                ! convergence not achieved.
                ! New update is required
                tdpot%nrestart_update = 0
             end if
          end if

          if (flag .ne. 0 ) then  
             ! set the controls for the next update at time for next inputs
             if (ctrl%user_control_set .eq. 0 ) then
                ! default procedure
                call this%set_controls_next_update(&
                     ode_inputs,&
                     tdpot,&
                     ctrl,input_time)
                if (ctrl%debug .ge. 2) write(lun_out,*) &
                     'NEXT DELTAT',ctrl%deltat, &
                     'ASKING FOR ',input_time
                flag      = 2 ! get input flag
                flag_task = 2 ! update task
                info = 0
                return
             else
                ! user defined procedure
                flag=5        
                info = 0
                return
             end if
          end if
          
       case (4)
          !
          ! System variation has been evaluated
          ! by the user. 
          !
          flag = 3
          info = 0
          return
       case (5)
          !
          ! Controls are settled by the user.
          ! Ask for inputs next time and run update.
          !
          flag = 2
          flag_task = 2
          info = 0
          return
       case (6)
          !
          ! Controls have been resettled by the user.
          ! Ask for inputs next time and run update.
          !
          flag = 2
          flag_task = 2
          info = 0
          return
       end select


     end subroutine new_dmk_cycle_reverse_communication

     !>----------------------------------------------------
  !> Procedure for time evolution of dmk dynamic with
  !> reverse communication. This programming approch
  !> is based on passing in-and-out from the main
  !> subroutine a flag (the integer flag in our) case
  !> that say what the user must do on the data.
  !> In this case we have:
  !> *0 : Exit from cycle 
  !> *1 : Initial value
  !> *2 : Use must se tthe inputs at the given time 
  !> *3 : User can study the tdpot state
  !> *4 : User must compute if steady state is achieved
  !> *5 : User must set the controls for the next update
  !> *6 : User must reset the controls due to update failure
  !>
  !> 
  !> usage: call var%new_dmk_cycle_reverse_communication(flag,&
  !>                    flag_task, info, input_time,&
  !>                    ode_inputs,tdpot,ctrl,original_ctrl)
  !> 
  !> where:
  !> \param[inout] flag       -> integer. Comunication flag
  !> \param[inout] flag_taks  -> integer. Comunication flag for tell
  !>                             syncronization or or update
  !> \param[inout] info       -> integer. Error flag
  !>                             * info==0 No errors  
  !>                             * info>0  Some errors occured
  !>                               dictionary_update_errors
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[in   ] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[inout] ctrl  -> type(CtrlPrm). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !> \param[inout] original_ctrl  -> type(CtrlPrm). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !>
  !> COMMENTS:
  !> This is passed with intent inout for using scratch
  !> arrays and avoid allocation/deallocation
  !> of temporary arrays
  !<---------------------------------------------------   
  subroutine core_dmk_cycle_reverse_communication(&
          this,&
          flag,flag_task,info,input_time,&
          ode_inputs,tdpot,ctrl)
       use Globals
       use DmkInputsData
       use TdensPotentialSystem
       use DmkControls
       use TimeInputs
       use Timing
       implicit none
       class(dmkpair),    intent(inout) :: this
       integer,           intent(inout) :: flag
       integer,           intent(inout) :: flag_task
       integer,           intent(inout) :: info
       real(kind=double), intent(inout) :: input_time
       type(DmkInputs),   intent(in   ) :: ode_inputs
       class(tdpotsys),   intent(inout) :: tdpot
       type(DmkCtrl),     intent(inout) :: ctrl
       !
       logical :: rc
       logical :: save_test
       logical :: ask_for_inputs
       integer :: res
       integer :: lun_err
       integer :: lun_out
       integer :: lun_stat
       character(len=256) :: cst,msg,msg_out,out_format,str,str1
       real(kind=double) :: cpu_time
       type(tim) :: CPU
       !class(tdpotsys) :: tdpot_copy

      
       ! shorthand  
       lun_err  = ctrl%lun_err
       lun_out  = ctrl%lun_out
       lun_stat = ctrl%lun_statistics

       if (ctrl%debug .ge. 2) then
          write(lun_out,*) ' __________________'
          write(str,*) 'IN REVERSE COMMUNICATION : flag, flag_task=',flag, flag_task
          call ctrl%print_string('update',1,str)
          write(lun_out,*) 'tdpot%time_iteration, tdpot%time, input_time , ode_inputs%time'
          write(lun_out,*) tdpot%time_iteration, tdpot%time, input_time , ode_inputs%time
       end if

       
       select case (flag)
       case (1) ! initialization case          
          ! return and read inputs at time zero
          flag=2
          flag_task=1
          input_time = tdpot%time
          tdpot%first = .True.
          return 
       case( 2 ) 
          if ( flag_task .eq. 1) then
             msg=ctrl%separator('INPUTS')
             call ctrl%print_string('update',1, msg)
             ! print problem info before first update
             if ( tdpot%first) then
                if ( (ctrl%info_state .ge. 3) .or. (ctrl%debug .ge. 1) ) then
                   call ode_inputs%info(ctrl%lun_out)
                end if
                call ode_inputs%info(ctrl%lun_statistics)
                tdpot%first=.False.
             end if

             ! SYNCRONIZED
             msg=ctrl%separator('SYNCRONIZATION TDPOT')
             call ctrl%print_string('update',1, msg)
             call CPU%init()
             call CPU%set('start')
             call this%syncronize_tdpot(tdpot,ode_inputs,ctrl,info)
             call CPU%set('stop')
             tdpot%cpu_time=tdpot%cpu_time+CPU%WCT
             call CPU%kill()

             msg=ctrl%separator('')
             call ctrl%print_string('update',1, msg)
             

             !
             ! Evalutate variation of tdens-potential system.
             !
             if ( tdpot%time_iteration >0 ) then
                ! ask the user to evalute variation
                flag = 4
                info = 0
                return
             else
                ! go to study session
                flag = 3
                info = 0
                return
             end if
          end if
          
          if (flag_task .eq. 2) then
             !
             ! Update cycle. 
             ! If it succees goes to the evaluation of
             ! system varaition (flag ==4 ).
             ! In case of failure, reset controls
             ! and ask new inputs or ask the user to
             ! reset controls (flag == 6 ).
             !
             if ( info == 0) then
                tdpot%nrestart_update=0
                tdpot%cpu_wasted=zero
                tdpot%cpu_update_wasted=zero
                ! copy before update
                !tdpot_copy = tdpot
             end if

             
             ! print info 
             info=0
             if (  tdpot%nrestart_update .eq. 0) then
                msg=' '
                call ctrl%print_string('update',1,msg)
                
                out_format='(a,I5.5,a,1pe8.2,a,I2)'
                write(str,out_format) 'UPDATE ',tdpot%time_iteration+1,&
                     ' | TIME STEP = ', ctrl%deltat
                
                write(msg,'(a)') ctrl%separator(str)
                call ctrl%print_string('update',1,msg)
             else
                out_format='(a,I5.5,a,1pe8.2,a,I2)'
                write(str,out_format) 'UPDATE ',tdpot%time_iteration+1,&
                     ' | TIME STEP = ', ctrl%deltat,&
                     ' RESTART ', tdpot%nrestart_update
                write(msg,'(a)') ctrl%separator(str)
                call ctrl%print_string('update',1,msg)
             end if
                
             
             !
             ! actual update
             !
             call CPU%init()
             call CPU%set('start')
             call this%update_tdpot(&
                  tdpot,&
                  ode_inputs,&
                  ctrl,&
                  info)
             call CPU%set('stop')
             cpu_time=CPU%WCT
             call CPU%kill()

             tdpot%cpu_time=tdpot%cpu_time+cpu_time
             tdpot%cpu_update = tdpot%cpu_update+cpu_time
             
             if ( info .eq. 0) then
                ! info for succesfull update
                !tdpot%cpu_time=tdpot%cpu_tim+cpu_time
                out_format=ctrl%formatting('r')
                write(str1,out_format) tdpot%cpu_wasted+cpu_time
                if ( tdpot%nrestart_update .eq. 0) then
                   write(msg,*) ctrl%separator('UPDATE SUCCEED - CPU:'//etb(str1))
                else
                   write(str,'(I3)') tdpot%nrestart_update
                   write(msg,*) ctrl%separator('UPDATE SUCCEED with '//etb(str)//&
                        ' restarts- CPU:'//etb(str1))
                end if
                call ctrl%print_string('update',1,msg)

             

                !
                ! syncronized tdpot and ode_inputs
                ! For example explicit Euler use the Inputs
                ! at t^{k} while at this point tdens-pot are 
                ! at t^{k+1}
                !
                if ( (.not. ode_inputs%steady) .and. &
                     (abs(tdpot%time-ode_inputs%time) > small) ) then

                   flag        = 2          ! asking for inputs
                   input_time  = tdpot%time ! syncronized at tdpot%time
                   flag_task   = 1          ! syncronize
                   info        = 0 
                   
                   out_format=ctrl%formatting('arra')
                   write(str,out_format) 'NOT STEADY INPUTS and (TDPOT TIME),(INPUT TIME)= ',&
                        tdpot%time,ode_inputs%time, ' - SYNCRONIZATE'
                   call ctrl%print_string('update',1,etb(str))
                   ! return to user 
                   return
                else                   
                   !
                   ! Evalutate variation of tdens-potential system
                   ! with type procedure compute_tdpot_variation
                   ! or ask the user to compute it sending flag = 4.
                   ! 
                   flag = 4
                   info = 0
                   return
                end if
             else
                !
                ! update failed
                !
                tdpot%cpu_wasted = tdpot%cpu_wasted + cpu_time
                tdpot%cpu_update_wasted = tdpot%cpu_update_wasted + cpu_time

                ! try one restart more
                tdpot%nrestart_update = tdpot%nrestart_update + 1

                ! stop if number max restart update is passed 
                if (tdpot%nrestart_update .ge. ctrl%max_restart_update) then
                   ! print 
                   write(msg,*) ctrl%separator('UPDATE FAILURE')
                   call ctrl%print_string('update',1,msg)
                   ! print to error unit
                   write(ctrl%lun_err,*) etb(msg)
                   
                   flag=-2
                   info=-1
                else
                   !
                   ! ask the user to reset controls
                   !
                   flag =  6
                   info = -1
                end if

                ! restores previous data
                !tdpot = tdpot_copy

                ! break cycle
                return

             end if
             
             
          end if
       case (3)
          !
          ! Evalute if simulation has to continue.
          ! If yes flag = 0 is settled
          !

          ! If number of iterations exceeds maximum break 
          ! time cycle setting flag=0 with info=-1
          if ( tdpot%time_update .ge. ctrl%max_time_iterations) then
             flag = 0
             info = -1
             out_format=ctrl%formatting('ar')
             write(msg,*) ' Update Number exceed limits' ,ctrl%max_time_iterations
             call ctrl%print_string('state', 1, msg)
             return 
          end if

          ! set info to zero if no update will be done
          if (ctrl%max_time_iterations == 0 ) then
             info = 0
             flag = 0
             return 
          end if
             
          ! check convergence
          if (tdpot%time_update > 0 ) then
             ! check if convergence was achieved
             ! convergence not achieved.
             ! New update is required
             tdpot%nrestart_update = 0
          end if

          if (flag .ne. 0 ) then  
             ! ask the user the to set controls for
             ! the next udpate
             flag=5        
             info = 0
             return
          end if
          
       case (4)
          !
          ! System variation has been evaluated
          ! by the user. 
          !
          flag = 3
          info = 0
          return
       case (5)
          !
          ! Controls are settled by the user.
          ! Ask for inputs next time and run update.
          !
          flag = 2
          flag_task = 2
          info = 0
          return
       case (6)
          !
          ! Controls have been resettled by the user.
          ! Ask for inputs next time and run update.
          !
          flag = 2
          flag_task = 2
          info = 0
          return
       end select


     end subroutine core_dmk_cycle_reverse_communication


     

     
     
     subroutine dictionary_update_errors(info,lun)
       implicit none
       integer, intent(in) :: info
       integer, intent(in) :: lun
       !
       integer :: digit1,digit2,digit3
       
       digit1=info/100 
       digit2=mod(info,100)/10
       digit3=mod(info,10)

       select case (digit1)
       case (1)
          write(lun,*) 'Error in linear solver'
          select case ( digit2)
          case (1)
             write(lun,*) 'Convergence not achieved in iterative method'
             select case ( digit3)
             case (1)
                write(lun,*) 'Maximum iterations reached'
             case (2)
                write(lun,*) 'Internal error in linear solver'
             end select
          case (3)
             write(lun,*) 'Preconditoner Failure'  
             select case ( digit3)
             case (1)
                write(lun,*) 'Failure in assembly'
             case (2)
                write(lun,*) 'Failure in application'
             end select
          end select
       case (2)
          write(lun,*) 'Error in non-linear solver'
          select case(digit2)
          case (1)
             write(lun,*) 'Non-linear convergence not achievied'
          case (2)
             write(lun,*) 'Non-linear solver stopped because was divergening'
          case (3)
             write(lun,*) 'Condition for non-linear solver to continue failed'
          end select
       case (3)
          write(lun,*) 'Other errors'
       case default
          write(lun,*) 'Unknown info'
       end select
       
     end subroutine dictionary_update_errors

       ! """
       !  Subroutine to run reverse communition approach
       !  of iterative solver.

       !  Args:
       !      solver (Solver): Class with iterate method
       !      problem: Problem description
       !      solution: Problem solution

       !  Returns:
       !      self (CycleControls): Returning changed class.
       !          Counters and statistics are changed.
       !      solver (Solver): Class modified with statistics
       !                       of application.
       !      solution: Updated solution.
       !  """
     subroutine reverse_cummunication_iterative_solver(this,info)
       implicit none
       class(iterative_algorithm), intent(inout) :: this
       integer, intent(inout) :: info
       !local
       real(kind=double) :: start_time
      
       if (this%flag == 0) then
          !"""Begin cycle. User can now study the system"""
          this%flag = 2
          this%ierr = 0
          return
       end if

       if (this%flag == 1) then
          ! """An iteration was completed and user checked if converge was
          !      achieved and decided to continue.  We check if iteration
          !      number exceeded the maximum. If yes we break the cycle
          !      passing a negative flag. Otherwise we let the user studing
          !      the stystem.
          !"""
          if (this%iterations >= this%max_iterations) then
             this%flag = -1
             if (this%verbose >= 1) then
                write(*,*) "Update Number exceed limits",  this%max_iterations
                return

                ! we tell the user that he/she can studies the Let the use
                ! study the system
                this%restarts = 0  ! we reset the count of restart
                this%flag = 2
                this%ierr = 0
             end if
             return
          end if
       end if


       if (this%flag == 2) then
          !"""User studied the updated system.
          !Now, we need solver controls for next update."""
          this%flag = 3
          this%ierr = 0
          return
       end if

       if (this%flag == 4) then
          !"""And error occured after update.  Now, user must change the solver
          !controls for trying further iteration"""
          this%flag = 5
          this%ierr = 0
          return 
       end if

       if ( (this%flag == 3) .or.(this%flag == 5) ) then
          !"""User set or reset solver controls.  Now, use must set new problem
          !inputs, if required"""
          this%flag = 6
          this%ierr = 0
          return
       end if

       if ( this%flag == 6) then
          !""User set/reset solver controls and problem inputs
          !ow we update try to iterate"""
          this%ierr = 0

          ! Update cycle.
          ! If it succees goes to the evaluation of
          ! system varaition (flag ==4 ).
          ! In case of failure, reset controls
          ! and ask new problem inputs or ask the user to
          ! reset controls (flag == 2 + ierr=-1 ).
          if ( this%verbose >= 1) then
             if (this%restarts == 0) then
                write(*,*)(" ")
                write(*,*)"UPDATE ", this%iterations + 1
             else
                write(*,*)&
                     "UPDATE ",&
                     this%iterations + 1,&
                     " | RESTART = ",&
                     this%restarts
             end if
          end if

          ! update solution
          call this%CPU_update%set('start')
          call this%iterate(this%ierr)
          call this%CPU_update%set('stop')
          

          
          ! different action according to ierr
          if (this%ierr == 0)then
             !"""Succesfull update"""

             this%iterations = this%iterations + 1
             this%cpu_time = this%cpu_time + this%CPU_update%WCT
             if (this%verbose >= 1)then
                if (this%restarts == 0)then
                   write(*,*)"UPDATE SUCCEED CPU = ", this%cpu_time
                else
                   write(*,*)"UPDATE SUCCEED ",&
                        (this%restarts),&
                        " RESTARTS CPU =",&
                        this%cpu_time
                   write(*,*)(" ")
                end if
             end if

             !""" Ask to the user to evalute if stop cycling """
             this%flag = 1
             this%ierr = 0

             return

          else if ( this%ierr > 0) then
             !"""Update failed"""
             if (this%verbose >= 1)then
                write(*,*)("UPDATE FAILURE")
                this%cpu_wasted = this%cpu_wasted + this%CPU_update%WCT
                ! Try one restart more
                this%restarts = this%restarts + 1

                ! Stop if number max restart update is passed
                if (this%restarts >= this%max_restarts) then
                   this%flag = -1  ! breaking cycle
                else
                   ! Ask the user to reset controls and problem inputs
                   this%flag = 4                   
                end if
             end if
          else if (this%ierr < 0)then
             ! Solver return negative ierr to ask more inputs
             this%flag = 5
          end if

          return 
       end if

     end subroutine reverse_cummunication_iterative_solver

end module DmkDiscretization

