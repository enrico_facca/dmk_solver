module DmkP1P0Discretization
  use Globals
  use MuffeTiming
  use AbstractGeometry
  use P1Galerkin
  use LinearOperator
  use Matrix
  use SimpleMatrix
  use SparseMatrix
  use CombinedSparseMatrix
  use PreconditionerTuning
  use DensePreconditioner
  use DenseMatrix
  use RankOneUpdate
  use BlockMatrix
  use StdSparsePrec
  use Eigenv
  use Scratch
  use LinearSolver
  use DmkControls
  use TdensPotentialSystem
  use DmkInputsData
  use DmkDiscretization
  use BackSlashSolver
  use SaddlePointMatrix
  
  implicit none
  private 
  type, extends(abs_linop),public :: spkernel
     integer :: nkernel=0
     real(kind=double), allocatable :: base_kernel(:)
     integer, allocatable :: indeces(:)
   contains
     !> Procedure to initialized
     !> (public procedure for type spmat)
     procedure, public, pass :: init => spkernel_init
     !> Procedure to initialized
     !> (public procedure for type spmat)
     procedure, public, pass :: kill => spkernel_kill
     !> Procedure to initialized
     !> (public procedure for type spmat)
     procedure, public, pass :: set => spkernel_set
     !> Procedure to compute 
     !>         y = M * x 
     !> (public procedure for type spmat)
     procedure, public, pass :: matrix_times_vector => spkernel_Mxv
  end type spkernel

  public :: eval_trans_prime,eval_trans_second, dmkp1p0_constructor,dmkp1p0_destructor
  public :: dmkp1p0_cycle_reverse_communication, dmkp1p0_store_evolution_info
  public :: dmk_p1p0_build_norm_grad_dyn,write4matlab

!>-------------------------------------------------------------------
!> Structure Variable containg member for the discretization
!> of Physarum Polycephalum ODE equation 
!> or the gradient flow equation in tdens variable
!> or the gradient flow equation in gfvar varible
!> with PO-P1 scheme, P0 for $\Tdens$, P1 for $\Pot$, with 
!> P1 that can be defined of the same grid of $\Tdens$ or the
!> conformal refinement.
!> It contains all linear algebra quantities for the 
!> computation of the time evolution.
!>-------------------------------------------------------------------
type, extends(dmkpair), public :: dmkp1p0
   !> Number of degrees of freedom of ntdens
  integer :: ntdens
  !> Number of degrees of freedom of pot
  integer :: npot
  !> Number of degrees of freedom of system tdens + pot
  integer :: nfull
  !> Number of degrees of gradient of gradient
  integer :: ngrad
  !> Number of degrees of pot
  integer :: ambient_dimension
  !>-----------------------------------------------------------------
  !> Geometrical info
  !>-----------------------------------------------------------------
  !> Flag for two-level grid
  integer :: id_subgrid
  !> Mesh for tdens
  type(abs_simplex_mesh), pointer  :: grid_tdens
  !> Mesh for pot
  type(abs_simplex_mesh), pointer  :: grid_pot
  !> Dimension(ncell_tdens)
  !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
  !> with $T_r$ triangle on grid_tdens
  real(kind=double), allocatable :: nrm_grad_dyn(:)
  !> Dimension(ncell_tdens)
  !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
  !> with $T_r$ triangle on grid_tdens
  real(kind=double), allocatable :: grad(:,:)
  !> Dimension(ncell_tdens)
  !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
  !> with $T_r$ triangle on grid_tdens
  real(kind=double), allocatable :: grad_avg(:,:)
  !> Dimension(ncell_tdens)
  !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
  !> with $T_r$ triangle on grid_tdens
  real(kind=double), allocatable :: norm_grad(:)
  !> Dimension(ncell_tdens)
  !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
  !> with $T_r$ triangle on grid_tdens
  real(kind=double), allocatable :: norm_grad_avg(:)
  !> Dimension(ncell_tdens)
  !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
  !> with $T_r$ triangle on grid_tdens
  real(kind=double), allocatable :: norm_grad_dyn(:)
  !> Dimension(ncell_tdens)
  !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
  !> with $T_r$ triangle on grid_tdens
  real(kind=double), allocatable :: nrm2grad(:)
  !>-----------------------------------------------------------------
  !> Finite Elements Scheme 
  !> P1-Galerkin variables (built on grid_pot)
  type(p1gal)    :: p1
  !>-----------------------------------------------------------------
  !> Stiff matrix from 
  !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
  type(spmat) :: stiff
  !> Mass matrix from 
  !> $ M_{i,j}=\int \psi_i \psi_j $
  type(spmat) :: p1_mass_matrix
  !> Mass matrix from 
  !> $ M_{i,j}=\int \psi_i \psi_j $
  type(diagmat) :: p1_lumped_mass_matrix
  !> Stiff matrix from 
  !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
  type(spkernel) :: near_kernel
  !> Kernel for pot-tdens system
  !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
  type(spkernel) :: near_kernel_full
  !> Stiff matrix from 
  !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
  type(spmat) :: stiff_laplacian
  !> Stiff matrix from 
  !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
  type(spmat) :: cell_laplacian
  !> Stiff matrix from 
  !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
  type(inverse) :: inverse_stiff
  !>-----------------------------------------------------------
  !> Block matrix type containng Jacobian
  !> ( A            BT )
  !> ( -deltat D1 C  D2 ) 
  type(blockmat)  :: jacobian_full
  !> Block matrix type containng augmented Jacobian
  !>  ( A_gamma       B_gamma^T )
  !>  ( -deltat D1 C  D2        ) 
  type(blockmat)  :: augmented_jacobian
  !> A_gamma=Stiff(\Tdens)+gamma *B
  !>  ( A_gamma       B_gamma^T )
  !>  ( -deltat D1 C  D2        ) 
  type(combspmat)  :: stiff_gamma
  !> A_gamma=Stiff(\Tdens)+gamma *B
  type(spmat)  :: stiff_gamma_assembled
  !> A_gamma=Stiff(\Tdens)+gamma *B
  !>  ( A_gamma       B_gamma^T )
  !>  ( -deltat D1 C  D2        ) 
  type(spmat)  :: BT_gamma
  !> Combined matrix A+deltat BT D2^{-1} D1 C
  !> for reduced jacobian
  type(combspmat) :: jacobian_reduced
  !> Block matrix type containng Jacobian
  !> ( A  BT )
  !> ( B  G  ) 
  type(blockmat) :: sym_jacobian_full 
  !> Block matrix type containng Jacobian
  !> ( A            BT   diag(\Gfvar)  )
  !> ( diag(\Gfvar) B    1/deltat - D2 ) 
  type(blockmat)  :: gf_jacobian_full     
  ! kernel of full jacobian
  real(kind=double), allocatable  :: kernel_full(:,:)
  !>---------------------------------------------------------
  !> Jacobian component
  !>---------------------------------------------------------
  !> Diagonal matrix use to define G action
  !> Compenent of symmmetic jacobian
  type(diagmat)  :: G_matrix
  !> Diagonal matrix use to define D2 action
  type(diagmat) :: matrixC
  !> Diagonal matrix use to define D2 action
  type(diagmat) :: inv_matrixC
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: D1(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: D2(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: D3(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: invD2(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: invD2_D1(:)     
  !> Dimension (ntdens)
  !> Tdens-part of Newton function 
  real(kind=double), allocatable :: fnewton_tdens(:)
  !> Dimension (ntdens)
  !> Gfvar-part of Newton function 
  real(kind=double), allocatable :: fnewton_gfvar(:)
  !> Dimension (npot)
  !> Pot-part of Newton function 
  real(kind=double), allocatable :: fnewton_pot(:)
  !> Dimension (ntdens)
  !> Tdens-part of Newton function 
  real(kind=double), allocatable :: fnewton_tdens_old(:)
  !> Dimension (ntdens)
  !> Gfvar-part of Newton function 
  real(kind=double), allocatable :: fnewton_gfvar_old(:)
  !> Dimension (npot)
  !> Pot-part of Newton function 
  real(kind=double), allocatable :: fnewton_pot_old(:)
  !> Dimension (npot)
  !> Pot-part of Newton function 
  real(kind=double), allocatable :: rhs_reduced(:)
  !> Dimension (ntdens)
  !> Weight use in the augmented jacobian
  real(kind=double), allocatable :: diagonal_weight(:)
  !> Dimension (ntdens)
  !> Weight use in the augmented jacobian
  real(kind=double), allocatable :: inv_diagonal_weight(:)
  !> Dimension (ntdens)
  !> Weight use in the augmented jacobian
  real(kind=double), allocatable :: diagonal_scale(:)
  !> Dimension (ntdens)
  !> Weight use in the augmented jacobian
  real(kind=double), allocatable :: inv_diagonal_scale(:)
  !> Dimension (ntdens)
  !> Weight use in the augmented jacobian
  real(kind=double), allocatable :: hatS(:)
  !>-----------------------------------------------------------------
  !> Logical Flag to mark equality between
  !> Matric B and C
  logical ::  BequalC
  !> Dimension(ntdens,npot)
  !> Matrix B for ei_newton for two-level grids
  !> B_{k,i}=\int_{T_k} \Grad \Pot \Psi_i
  type(spmat) :: B_matrix
  !> Dimension(ntdens,npot)
  !> Matrix BT for ei_newton for two-level grids
  !> B_{i,k}=\int_{T_k} \Grad \Pot \Psi_i
  type(spmat) :: BT_matrix
  !> Dimension(ntdens,npot)
  !> Matrix B for ei_newton for two-level grids
  !> B_{k,i}=\int_{T_k} gf_{k}\Grad \Pot \Psi_i
  type(spmat) :: DB_matrix
  !> Dimension(ntdens,npot)
  !> Matrix BT for ei_newton for two-level grids
  !> B_{i,k}=\int_{T_k} gf_{k}\Grad \Pot \Psi_i
  type(spmat) :: BTmatrixC
  !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
  !> Trija pointer for assembly of B_matrix
  !> Dimension(ntdens,npot)
  !> Matrix B for ei_newton for two-level grids
  !> B_{k,i}=\int_{T_k} \Grad \Pot \Psi_i
  type(spmat) :: B1T_matrix
  !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
  !> Trija pointer for assembly of B_matrix
  !> Dimension(ntdens,npot)
  !> Matrix B for ei_newton for two-level grids
  !> B_{k,i}=\int_{T_k} \Grad \Pot \Psi_i
  type(spmat) :: B2_matrix
  integer, allocatable :: assembler_Bmatrix_subgrid(:,:)
  !> Dimension(grid_tdens%nnodeincell, grid_tdens%ncell ) 
  !> Trija pointer for assembly of B_matrix 
  !> with no subgrid
  integer, allocatable :: assembler_Bmatrix_grid(:,:)
  !> Dimension(B_matrix%nterm) 
  !> Integer pointer to redirect non-zero term of B_matrix
  !> into non-zero term of BT_matrix = (B_matrix)^T
  integer, allocatable :: transposer(:)
  !>-----------------------------------------------------------------
  !> Dimension(ntdens,npot)
  !> Matrix C for ei_newton for two-level grids
  !> $C_{k,i}=\int_{T_k} \Grad \Pot \Psi_i * weight_{k}$
  !> where  $weight_{k}$ depends on the pflux exponent
  type(spmat) :: C_matrix
  !> Dimension(npot,npot)
  !> Transpose Matrix C 
  type(spmat) :: CT_matrix
  !> Dimension(ntdens,npot)
  !> Transpose Matrix M
  !>   M = deltat D1 * C_matrix
  !> It is the block 1,2 of the jacobian
  type(spmat) :: deltatD1C_matrix
  !> Dimension(npot,npot)
  !> Matrix conating the ia ja pattern for matrix
  !> BTB or BTC for ei_newton 
  type(spmat) :: BTDC_matrix
  !> Dimension(ntdens,ntdens)
  !> Matrix coanting the ia ja pattern for matrix
  !> B diagonal BT 
  type(spmat) :: BDBT
  !> Dimension(stiff%nterm) 
  !> Redirector of stiff matrix into BTDC
  integer, allocatable :: stiff2BTDC(:)
  !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
  !> Trija pointer for assembly of B_matrix
  !> Scratch array for PCG and BICGSTAB  Procedure
  type(scrt) :: aux_bicgstab
  !> Scratch array for implicit_euler_newton
  type(scrt) :: aux_newton
  !> Work array form rhs of linear system
  real(kind=double), allocatable :: rhs(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: rhs_ode(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: trans_prime(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: trans_second(:)
  !> Dimension (ntdens)
  !> Work array form update procedure
  real(kind=double), allocatable :: inc_ode(:)
  !> Dimension (ncellpot)
  !> Work array form update procedure
  real(kind=double), allocatable :: tdens_prj(:)
  !>---------------------------------------------------------------
  !> Preconditioner for sparse linear systems
  !>---------------------------------------------------------------
  !> Back up Sparse preconditioner for PCG procedure
  type(stdprec) :: standard_prec_saved
  type(densemat) :: der_pot_tdens_jacobian
  type(densemat) :: tdens_jacobian

  !> BFGS update
  type(bfgs) :: bfgs_prec
  !> 
  type(densemat) :: matrix_V
  type(densemat) :: matrix_AV
  type(densemat) :: matrix_VTAV
  type(denseprec) :: matrix_PI
  !> Sparse preconditioner stifnees of laplacian
  type(stdprec) :: prec_laplacian
  !> Back up Sparse preconditioner for PCG procedure
  type(inverse) :: approx_inverse
  !> Back up of sparse preconditioner of Schur complement BTDT+G
  type(stdprec) :: prec_Schur
  ! Array involded in symmetrization of jacobian
  ! Dimension (ntdens)
  real(kind=double), allocatable :: symmetrizer(:)
  !> Diagonal precondtioner with diag(A)^{-1}
  type(diagmat)  :: inverse_diag_stiff
  !> Block diagonal precondittioner
  !> P ~= diag(A,D2)^{-1}
  type(block_linop) :: prec_full
  !> Diagonal precondittioner
  !> P = D2^{-1}
  type(diagmat) :: inverse_D2
  !>-----------------------------------------------------------
  !> Scratch arrays for general porpuse
  !>-----------------------------------------------------------
  !> Dimension (ngrad)
  !> Work array 
  real(kind=double), allocatable :: scr_ngrad(:)
  !> Dimension (ntdens)
  !> Work array 
  real(kind=double), allocatable :: scr_ntdens(:)
  !> Dimension (npot)
  !> Work array 
  real(kind=double), allocatable :: scr_npot(:)
  !> Dimension (npot+ntdens)
  !> Work array 
  integer, allocatable :: scr_integer(:)
  !> Identity matrix of size npot
  type(scalmat) :: identity_npot
  !> Identity matrix of size npot
  type(scalmat) :: identity_ntdens
  !> Dimension (npot)
  !> Diagonal of the laplacian 
  real(kind=double), allocatable :: diagonal_laplacian(:)
  !> Dimension (npot)
  !> Work array 
  real(kind=double), allocatable :: norm_rows_stiff(:)
  !> Dimension (npot)
  !> Work array 
  real(kind=double), allocatable :: norm_rows_BT(:)
  !> Dimension (npot+ntdens)
  !> Work array 
  real(kind=double), allocatable :: scr_nfull(:)
  !> Dimension (npot+ntdens)
  !> Work array 
  real(kind=double), allocatable :: rhs_full(:)
  !> Dimension (npot)ode_inputs%pode%number_of_potentials
  !> Work array to store D(A)^{-1/2} in diagonal scaling procedure
  real(kind=double), allocatable :: sqrt_diag(:)
  !> Scratch diagonal matrix
  type(diagmat) :: scr_diagmat_ntdens
  !>-----------------------------------------------------------
  !> Eigen. conteiner for DACG procedure
  type(eigen) :: spectral_info
  !> Work variable to store linear solver controls
  type(input_solver) :: ctrl_solver
  !> Info on linear solver solution
  type(output_solver) :: info_solver
  !> Info on linear solver solution
  !> Dimension(max_nonlinear_iterations)
  type(output_solver), allocatable :: sequence_info_solver(:)
  !> Dimension(max_nonlinear_iterations)
  integer, allocatable :: sequence_build_prec(:)
  !> 
  integer :: iter_first_system
  integer :: iter_last_prec
contains
  !> Static constructor 
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: init => init_p1p0
  !> Static destructor
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: kill => kill_p1p0
  !>-----------------------------------------------------
  !> PROCEDURE OVERRIDED FROM DMKPAIR
  !>-----------------------------------------------------
  !> Procedure for syncronize the whole system
  !> Tdens Potential and derived varaibles
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: syncronize_tdpot
  !> Subroutine for updating tdpotsys variables
  !> to next time step. Inferface defined in
  !> dmkpair
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: update_tdpot
  !> Abstract procedure to override
  !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: compute_tdpot_variation
  !> Procedure to reset controls 
  !> (time-step, preconditioner construction etc.) 
  !> in case of failure in update
  !> (procedure public for type specp0_space_discretization)
  procedure, public , pass :: reset_controls_after_update_failure
  !> Procedure to set controls (time-step, preconditioner construction
  !> etc.) for next update
  !> (procedure public for type specp0_space_discretization)
  procedure, public , pass :: set_controls_next_update
  !>--------------------------------------------------------
  !> GENERAL PRORPUSE ASSEMBLY PROCEDURES
  !> Procedure to build all grad-like varaibles
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: build_grad_vars
  !> Procedure to build all norm_grad_dynamics
  !> (procedure public for type dmkp1p0)
  procedure, public, pass :: build_norm_grad_dyn
  !> Procedure for assembly the stiffness matrix 
  !> A_{i,j} = \int \Tdens \Grad_i \Grad j
  !> array tdens
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: assembly_stiffness_matrix
  !>-------------------------------------------------------
  !> UPDATE ASSEMBLER
  !>------------------------------------------------------
  !> Subroutine for assembling right-hand side of 
  !> Tdens ODE
  procedure, public , pass :: assembly_rhs_ode_tdens
  !> Subroutine for assembling right-hand side of 
  !> Gfvar ODE
  procedure, public , pass :: assembly_grad_lyap_gfvar
  !> Subroutine to get the tdens increment from the rhs_ode
  !> Tdens (or Gfvar) ODE
  procedure, public , pass :: get_increment_ode
  !> Procedure for assembly the  matrix 
  !> $\Matr[\Itd,\Ipot]{B} =
  !> \int_{\Domain} |\Grad \Pot|^{\Pode-2} \testx_{\Itd} \Grad \Pot \Grad \testp _{\Ipot}$
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: assembly_BC_matrix  
  !> Subroutine for writing to file all varaibles 
  !> involved in the implicit euler time-stepping
  !> via Newton method
  procedure, private , nopass :: assembly_J22_gfvar
  procedure, private , pass :: write_newton => write_newton_p1p0
  !>----------------------------------------------------
  !> TIME STEPPING PROCEDURES
  !>----------------------------------------------------
  !> Update procedure for tdpotsys tpye
  !> via Explicit Euler for Tdens Dynamic(forward Euler)
  procedure, private , pass :: explicit_euler_tdens
  !> Update procedure for tdpotsys tpye
  !> via Explicit Euler (forward Euler)
  !> for Gradient FLow Dynamics
  !> Gfvar' = -\grad \Lyap (\Gfvar)
  procedure, private , pass :: explicit_euler_gfvar
  !> Update procedure for tdpotsys tpye
  !> via Implicit Euler for Gradient FLow Dynamics
  !> \Tdens' = RHS (\Tdens)
  !> \mu^{k+1} = \mu^{k} + \deltat RHS (\mu^{k})
  procedure, private , pass :: implicit_euler_newton_tdens
  !> Update procedure for tdpotsys tpye
  !> via Implicit Euler for Gradient FLow Dynamics
  !> Gfvar' = -\grad \Lyap (\Gfvar)
  !> Gf^{k+1} = Gf^{k} - \deltat \grad \Lyap (\Gfvar^{k+1})
  procedure, private , pass :: implicit_euler_newton_gfvar
  !>----------------------------------------------------------------
  !> Scalar and integer fucntional descibing important
  !> caracteristic of the state of tdens-pot system
  !>---------------------------------------------------------------
  !> Subroutine for computation of scalar and integer
  !> functionals like energy, lyapunov, etc
  procedure, public , pass :: compute_functionals
  !> Procedure setting functional at
  !> given time iteration and state of tdpot
  !> (procedure public for type evolfun)
  procedure, public, pass :: set_evolfun
  !>--------------------------------------------------------------
  !> Scalar functions
  !>--------------------------------------------------------------
  !> Function eval $\int \tdens $
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: mass
  !> Function eval $\int \tdens |\nabla u |^2 $
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: energy
  !> Scalar functions
  !> Function eval $\int \tdens^{\WPower{\Pflux,\Pmass}$
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: weighted_mass
  !> Function eval $\Ene(\tdens) + \Wmass(\tdens)$
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: lyapunov
  !> Function evaluating
  !> $\int_{\Omega}|q|^{power}$
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: integral_flux
  !> Procedure to tell active and frezeed regions
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: set_active_tdpot
  !> Procedure setting functional at
  !> given time iteration and state of tdpot
  !> (procedure public for type evolfun)
  procedure, public, pass :: store_evolution !=> set_evolfun
end type dmkp1p0

type, public :: p1p0pair
  !>-----------------------------------------------------------------
  !> Geometrical info
  !>-----------------------------------------------------------------
  !> Flag for two-level grid
  integer :: id_subgrid
  !> Mesh for tdens
  type(abs_simplex_mesh), pointer  :: grid_tdens
  !> Mesh for pot
  type(abs_simplex_mesh), pointer  :: grid_pot
  !>-----------------------------------------------------------------
  !> Finite Elements Scheme 
  !> P1-Galerkin variables (built on grid_pot)
  type(p1gal)    :: p1
  !> Sparse Matrix with the sparsity pattern of
  !> matrix $B\in\REAL^{\Nnode_subgrid,\Ncell_grid} 
  !> B_{i,j}=\int_{\Cell_j} \Grad \Pot \cdot \Grad \Phi_i
  !> P1-Galerkin variables (built on grid_pot)
  type(spmat)    :: B_matrix
  !> Auxiliary integer for asssembly B matrix
  integer, allocatable :: assembler_Bmatrix_subgrid(:,:)
  !> Dimension(grid_tdens%nnodeincell, grid_tdens%ncell ) 
  !> Trija pointer for assembly of B_matrix 
  !> with no subgrid
  integer, allocatable :: assembler_Bmatrix_grid(:,:)
  !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
  !> Trija pointer for assembly of B_matrix
  !> Scratch array for PCG and BICGSTAB  Procedure
  type(scrt) :: aux
contains
  !> Static constructor 
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: init => init_p1p0pair
  !> Static destructor
  !> (procedure public for type dmkp1p0)
  !procedure, public , pass :: kill => kill_p1p0pair
  !> GENERAL PRORPUSE ASSEMBLY PROCEDURES
  !> Procedure to build all grad-like varaibles
  !> (procedure public for type dmkp1p0)
  !procedure, public , pass :: build_grad_vars
  !> Procedure to build all norm_grad_dynamics
  !> (procedure public for type dmkp1p0)
  !procedure, public, pass :: build_norm_grad_prj
  !> Procedure for assembly the stiffness matrix 
  !> A_{i,j} = \int \Tdens \Grad_i \Grad j
  !> array tdens
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: assembly_stiffness_matrix_p1p0pair
  !> Procedure for assembly the  matrix 
  !> $\Matr[\Itd,\Ipot]{B} =
  !> \int_{\Domain} |\Grad \Pot|^{\Pode-2} \testx_{\Itd} \Grad \Pot \Grad \testp _{\Ipot}$
  !> (procedure public for type dmkp1p0)
  procedure, public , pass :: assembly_nodecell_matrix_subgrid
end type p1p0pair


  
contains
  !>----------------------------------------------------
  !> Real function evaluates the energy 
  !> Joule dissipeted energy $\int \tdens |\nabla u|^2$
  !> (procedure public for type p10p0)
  !> 
  !> usage:    call var%energy(tdpot)
  !>    
  !> where:
  !> \param  [in ] tdpot  -> type(tdpotsys). Tdens/potential system
  !> \result [out] energy -> real. Lyap. functional
  !<----------------------------------------------------
  function energy(this,tdens,norm_grad) result(sum)
    use Globals
    implicit none
    class(dmkp1p0), intent(in) :: this
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: norm_grad(this%grid_pot%ncell) 
    real(kind=double) :: sum
    !local 
    integer :: icell


    if (this%id_subgrid .eq. 1) then
       sum = zero
       do icell = 1, this%grid_pot%ncell
          sum = sum + &
               tdens( this%grid_pot%cell_parent(icell) ) * &
               norm_grad(icell)**2* &
               this%grid_pot%size_cell(icell)
       end do
       sum = onehalf * sum
    else
       sum = zero
       do icell = 1, this%grid_pot%ncell
          sum = sum + &
               tdens( icell ) * &
               norm_grad(icell)**2 * &
               this%grid_pot%size_cell(icell)
       end do
       sum = onehalf * sum
    end if

  end function energy


  !>----------------------------------------------------
  !> Real function evaluates the tdens mass 
  !> $\int \tdens \dx $ 
  !> (procedure public for type p10p0)
  !> 
  !> usage:    call var%mass(tdpot)
  !>    
  !> where:
  !> \param  [in ] tdpot -> type(tdpotsys). Tdens/potential system
  !> \result [out] mass  -> real. Tdens integeral
  !<----------------------------------------------------
  function mass(this,tdens) result(sum)
    use Globals
    implicit none
    class(dmkp1p0), intent(in) :: this
    real(kind=double),                intent(in) :: tdens(this%ntdens) 
    real(kind=double):: sum
    !real(kind=double), intent(out) :: mass

    !local 
    real(kind=double) :: ddot

    sum = ddot( this%ntdens, tdens,1, this%grid_tdens%size_cell,1)

  end function mass

  !>---------------------------------------------------------
  !> It evaluates the lyap. fun. 
  !> $\int 1/2 \Tdens |\Grad \Pot|^2+1/2\int \Tdens^{P(\Pflux)}/^{P(\Pflux)}$
  !> (procedure public for type fun)
  !> 
  !> usage: var%s_lyap()
  !>    
  !> where:
  !> \param  [in ] tdpot      -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs -> type(DmkInputs)  Ode's inputs
  !> \result [out] lyap       -> real. Lyapunov functional
  !<------------------------------------------------------
  function lyapunov(this,tdens,norm2grad,ode_inputs) result(res)
    implicit none
    class(dmkp1p0), intent(in   ) :: this
    real(kind=double),                intent(in   ) :: tdens(this%ntdens) 
    real(kind=double),                intent(in   ) :: norm2grad(this%grid_pot%ncell)
    type(DmkInputs),                  intent(in   ) :: ode_inputs
    ! out variable
    real(kind=double) :: res

    res = this%energy(tdens,norm2grad) + this%weighted_mass(tdens,ode_inputs)

  end function lyapunov

  !>------------------------------------------------------------
  !> Procedure evaluating $pvel$ exponent wihich is the candidate
  !> branch exponent for give pflux and pmass
  !>-------------------------------------------------------------
  function exponent_wmass(ode_inputs) result(exponent)
    implicit none
    type(DmkInputs),  intent(in   ) :: ode_inputs
    real(kind=double) :: exponent
    !local 
    real(kind=double) :: pflux
    real(kind=double) :: pmass
    real(kind=double) :: pode

    pflux = ode_inputs%pflux
    pmass = ode_inputs%pmass
    pode = ode_inputs%pode

    exponent = zero
    if ( abs(pode-two) < small  ) then
       !pode = 2 pflux
       if (  abs(1 - pflux + pmass ) > 1.0d-10 ) then 
          exponent = 1.0d0 + pmass - pflux 
       else
          exponent = zero                  
       end if
    else
       !pode = pflux
       if ( ( abs(2 * pmass - pflux ) > 1.0d-10 ) .and. &
            ( abs(pflux             ) > 1.0d-10 ) ) then
          exponent = (2 * pmass - pflux ) / pflux 
       else
          exponent = zero
       end if
    end if

  end function exponent_wmass

  
  !>---------------------------------------------------------
  !> It evaluates the lyap. fun. 
  !> $1/2 \int \Tdens^{P(\Pflux)}/^{P(\Pflux)}$
  !> (procedure public for type fun)
  !> 
  !> usage: var%w_mass(tdpot,ode_inputs)
  !>    
  !> where:
  !> \param  [in ] tdpot      -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs -> type(DmkInputs)  Ode's inputs
  !> \result [out] lyap       -> real. Lyapunov functional
  !<------------------------------------------------------
  function weighted_mass(this,tdens,ode_inputs) result(w_mass)
    implicit none
    class(dmkp1p0), intent(in   ) :: this
    real(kind=double),                intent(in   ) :: tdens(this%ntdens) 
    type(DmkInputs),                  intent(in   ) :: ode_inputs
    ! results
    real(kind=double) :: w_mass
    !local 
    integer           :: icell
    real(kind=double) :: power,pflux,pmass,pode

    pflux=ode_inputs%pflux
    pmass=ode_inputs%pmass
    pode =ode_inputs%pode

    if ( ( pode-two) > small ) then
       power = exponent_wmass(ode_inputs)
       if ( abs(power) .gt. small ) then
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) *&
                  tdens(icell) ** power * &
                  this%grid_tdens%size_cell(icell)
          end do
          w_mass = onehalf*w_mass/(power) 
       else
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) * &
                  log(tdens(icell))/log(exp(1.0d0))* &
                  this%grid_tdens%size_cell(icell)
          end do
          w_mass = onehalf * w_mass 
       end if
    else
       power = exponent_wmass(ode_inputs)
       if ( abs(power) .gt. small ) then
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) * &
                  tdens(icell) ** power            * &
                  this%grid_tdens%size_cell(icell)
          end do
          w_mass =  onehalf*w_mass/(power) 
       else
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) * &
                  log(tdens(icell))/log(exp(1.0d0))* &
                  this%grid_tdens%size_cell(icell)
          end do
          w_mass = onehalf * w_mass 
       end if
    end if


  end function weighted_mass

  !>---------------------------------------------------------
  !> It evaluates $\int_{\Omega}|\Vel|^{power}$ for
  !> a giben power
  !> (procedure public for type fun)
  !> 
  !> usage: var%w_mass(tdpot,ode_inputs)
  !>    
  !> where:
  !> \param  [in ] tdpot      -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs -> type(DmkInputs)  Ode's inputs
  !> \result [out] integral   -> real. Weighted flux integrated
  !<------------------------------------------------------
  function integral_flux(this,tdens,norm_grad_avg,power) result(integral)
    implicit none
    class(dmkp1p0), intent(in   ) :: this
    real(kind=double),                intent(in   ) :: tdens(this%ntdens) 
    real(kind=double),                intent(in   ) :: norm_grad_avg(this%grid_pot%ncell)
    real(kind=double),                intent(in   ) :: power
    ! result
    real(kind=double) :: integral
    !local 
    integer :: icell

    integral=zero
    do icell=1,this%ntdens
       integral = integral + &
            ( tdens(icell) * norm_grad_avg(icell) )** power * & 
            this%grid_tdens%size_cell(icell)
    end do
    integral = integral/power

  end function integral_flux


  !>---------------------------------------------------------
  !> It evaluates $\int_{\Omega}\Pot \Forcing $ for
  !> a giben power
  !> (procedure public for type fun)
  !> 
  !> usage: var%integral_potforcing(tdpot,ode_inputs)
  !>    
  !> where:
  !> \param  [in ] tdpot               -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs          -> type(DmkInputs)  Ode's inputs
  !> \result [out] integral_potforcing -> real. Weighted flux integrated
  !<------------------------------------------------------
  function integral_potforcing(this,pot,rhs) result(integral)
    implicit none
    class(dmkp1p0), intent(in   ) :: this
    real(kind=double),                intent(in   ) :: pot(this%npot) 
    real(kind=double),                intent(in   ) :: rhs(this%npot)
    ! result
    real(kind=double) :: integral
    !local 
    real(kind=double) :: ddot

    integral=ddot(this%npot,pot,1,rhs,1)

  end function integral_potforcing
  

  !>-------------------------------------------------
  !> Precedure for grads variables and grad-depending variables
  !> including nrm_grad_dyn defiend as
  !> nrm_grad_dyn(r) = \int _{T_r} |\nabla u| /{T_r}
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call var%build_norm_grad_vars(ctrl)
  !>
  !<-------------------------------------------------
  subroutine build_grad_vars(this,tdpot,stderr)
    use Globals
    implicit none
    class(dmkp1p0), intent(inout) :: this
    class(tdpotsys),   intent(in   ) :: tdpot
    integer,           intent(in   ) :: stderr
    !local
    integer :: icell,icell_sub,i,j
    real(kind=double) :: ddot,pflux

    if ( .not. tdpot%tdpot_syncr )  then
       write(stderr,*) 'Tdens and Pot are not syncronized'
       stop
    end if


    ! evaluation of gradx, grady ,gradz
    call this%p1%eval_grad(tdpot%pot,this%grad)

    ! evaluation norm_grad   
    call this%p1%eval_nrm_grad(tdpot%pot,this%norm_grad)

    ! average on subgrid or copy 
    if (this%id_subgrid .eq. 1 ) then
       call this%grid_pot%avg_cell_subgrid(&
            this%grid_tdens,&
            this%norm_grad,this%norm_grad_avg)
       do i =1,this%grid_pot%logical_dimension
          this%scr_ngrad(:) = this%grad(i,:)
          call this%grid_pot%avg_cell_subgrid(&
               this%grid_tdens,&
               this%scr_ngrad,this%scr_ntdens)
          do j=1,this%ntdens
             this%grad_avg(i,j) = this%scr_ntdens(j)
          end do
       end do

    else if (this%id_subgrid .eq. 0 ) then
       this%norm_grad_avg = this%norm_grad
       this%grad_avg  = this%grad
    end if

  end subroutine build_grad_vars

  !>-------------------------------------------------
  !> Precedure for grads variables and grad-depending variables
  !> including norm_grad_dyn defiend as
  !> norm_grad_dyn(r) = \int _{T_r} |\nabla u| /{T_r}
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call var%build_norm_grad_vars(ctrl)
  !>
  !<-------------------------------------------------
  subroutine build_norm_grad_dyn(this,pot,pode,norm_grad_dyn)
    use Globals
    implicit none

    class(dmkp1p0),    intent(inout) :: this
    real(kind=double), intent(in   ) :: pot(this%npot)
    real(kind=double), intent(in   ) :: pode
    real(kind=double), intent(inout) :: norm_grad_dyn(this%ntdens)

    !local
    logical rc


    ! eval norm_grad**pode   
    call this%p1%eval_nrm_grad(pot,this%scr_ngrad)
    this%scr_ngrad = this%scr_ngrad**pode

    ! eval norm_grad_dyn and norm_grad_power
    if (this%id_subgrid .eq. 1) then  
       ! average norm_grad^pode
       call this%grid_pot%avg_cell_subgrid(&
            this%grid_tdens,&
            this%scr_ngrad, &
            norm_grad_dyn)

    else
       ! copy values
       norm_grad_dyn = this%scr_ngrad
    end if

    norm_grad_dyn=abs(norm_grad_dyn)
    !write(*,*) 'pot(1)=',pot(1),'pot(n)=',pot(this%grid_pot%nnode)
    !write(*,*) 'grad(1)=',norm_grad_dyn(1)-one,'grad(m)=',norm_grad_dyn(this%grid_tdens%ncell)-one
    !write(*,*) maxval(norm_grad_dyn)-1.0
   end subroutine build_norm_grad_dyn


  !>------------------------------------------------------
  !> Prodeduce for the assembly of the siffness
  !> matrix for given Tdens
  !>------------------------------------------------------
  subroutine assembly_stiffness_matrix(p1p0,lun_err,tdens,stiffness_matrix)
    use Globals
    use Timing
    implicit none
    class(dmkp1p0), intent(inout) :: p1p0
    integer,           intent(in   ) :: lun_err
    real(kind=double), intent(in   ) :: tdens(p1p0%ntdens)
    type(spmat),       intent(inout) :: stiffness_matrix

    if ( p1p0%id_subgrid .eq. 0 ) then
       ! assembly
       call p1p0%p1%build_stiff(lun_err, 'csr', tdens(1:p1p0%ntdens), stiffness_matrix)

    else
       ! projection and assembly
       call p1p0%grid_pot%proj_subgrid(tdens,p1p0%tdens_prj)
       call p1p0%p1%build_stiff(lun_err,&
            'csr', p1p0%tdens_prj(1:p1p0%grid_pot%ncell), stiffness_matrix)
    end if


  end subroutine assembly_stiffness_matrix


  !>--------------------------------------------------------------------
  !> Procedure for the syncronization od tdens potential variable
  !> 
  !> (public procedure for type dmkpair)
  !> 
  !> usage: call var%syncronization(tdpot,ode_inputs,ctrl,info)
  !> 
  !> where:
  !> \param[inout] tdpot      -> tye(tdpotsys). Tdens-Potentail System
  !> \param[in   ] ode_inputs -> tye(DmkInputs). Input for Ode
  !> \param[in   ] ctrl       -> tye(DmkCtrl). Dmk controls
  !> \param[inout] info       -> integer. Flag for info 
  !>                              0 = Everything is fine
  !>                              1** = Error in linear solver
  !>                              11* = Convergence not achivied
  !>                              12* = Internal error in linear solver
  !>                              2**  = Error in non-linear solver
  !>                              21* = Non-linear convergence not achievied
  !>                              22* = Non-linear solver stoped because was divergening
  !>                              23* = Condition for non-linear solver to
  !>                                     continue failed
  !>                              3**  = Other errors
  !>---------------------------------------------------------------------
  subroutine syncronize_tdpot( this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use DmkInputsData
    use StdSparsePrec
    use dagmg_wrap
    implicit none
    class(dmkp1p0),target, intent(inout) :: this
    class(tdpotsys),                         intent(inout) :: tdpot
    class(DmkInputs),                        intent(in   ) :: ode_inputs
    class(DmkCtrl),                          intent(in   ) :: ctrl
    integer,                                 intent(inout) :: info

    ! local
    logical :: endfile
    integer :: icell,i,inode
    integer :: itemp=0 
    integer :: lun_err, lun_out,lun_stat
    integer :: ntdens,npot
    real(kind=double) :: pode, pflux,relax
    class(abs_linop), pointer :: ort_mat
    type(input_solver) :: ctrl_outer_solver, ctrl_inner_solver
    type(input_prec) :: ctrl_outer_prec,ctrl_inner_prec
    type(input_solver) :: ctrl_multigrid
    type(agmg_inv),target :: multigrid_solver
    type(scalmat) :: identity_npot
    class(abs_linop), pointer :: prec_final
    type(scalmat) :: eye_npot
    real(kind=double) :: ddot, dnrm2
    real(kind=double), allocatable , dimension(:) :: x,y,xmy,Px,Py,Pxmy
    type(inv), target :: inverse_stiff
    type(new_linop) :: matrix2solve,matrix2prec
    type(spmat) :: formed_matrix2solve,formed_matrix2prec
    type(inverse),target :: krylov_inverse
    character(len=256) :: msg= ' '
    !
    ! shorthand for variables
    !
    ntdens=tdpot%ntdens
    npot  =tdpot%npot
    lun_err=ctrl%lun_err
    lun_out=ctrl%lun_out
    lun_stat=ctrl%lun_statistics

    ! local initializations
    call krylov_inverse%init(lun_err,npot)
    call identity_npot%eye(npot)


    ! call this%CPU%ALGORITHM%set('start')
    pode=ode_inputs%pode
    pflux=ode_inputs%pflux
    if ( abs( pode-2.0d0 ) < small ) then
       call tdens2gfvar(tdpot%ntdens,pode,&
            pflux,tdpot%tdens,tdpot%gfvar)
    end if

    ! print info
    if (  ode_inputs%steady) then
       write(msg,*) ' Steady Inputs'
       call ctrl%print_string('update',2, msg)
    else
       if ( (ctrl%debug .ge. 1) .or. &
            (ctrl%info_update .ge. 2) ) then
          call ode_inputs%info(ctrl%lun_out)
       end if
       call ode_inputs%info(ctrl%lun_statistics)
    end if



    !
    ! 1 - solve $-\Div(\Tdens+lambda \Pot) = \Forcing$    
    !

    ! 1.1 - assembly matrix
    ! use tdens + lambda
    if (ctrl%debug.ge. 1) &
         write(lun_out,*) 'BUILD STIFF'
    this%scr_ntdens = (tdpot%tdens+ctrl%relax_tdens)*ode_inputs%beta + ode_inputs%lambda_lift  ! use tdens*beta + lambda
    call this%assembly_stiffness_matrix(lun_err,this%scr_ntdens, this%stiff)
    
    ! rhs=forcing term + Neumann boundary terms  
    this%rhs=ode_inputs%rhs

    ! impose dirichelt boundary condition or handle singularity
    if ( ode_inputs%ndir > 0 ) then
       !
       ! act on rhs
       !
       if (ctrl%debug.ge. 1) then
          write(lun_out,*) 'DIRICHLET NODES',ode_inputs%ndir
       end if
       call this%p1%dirichlet_bc(lun_err,&
            this%stiff,this%rhs, tdpot%pot,&
            ode_inputs%ndir,&
            ode_inputs%dirichlet_nodes,&
            ode_inputs%dirichlet_values)
       ort_mat=>null()
    else
       call ortogonalize(npot,&
            1,&
            this%kernel_full(1:npot,1),tdpot%pot)
       ort_mat=>this%near_kernel
    end if


    
    ! 
    matrix2solve = this%stiff   + ctrl%relax_direct * identity_npot
    call formed_matrix2solve%form_new_linop(info,lun_err,matrix2solve)
    if (ctrl%selection >0 ) then
      

       
    end if

    matrix2prec  = formed_matrix2solve + ctrl%relax4prec * identity_npot
    call formed_matrix2prec%form_new_linop(info,lun_err,matrix2prec)


    !
    ! define (approximate) inverse 
    !
    call set_linear_algebra_ctrls(ctrl,&
          ctrl_outer_solver,ctrl_outer_prec,&
          ctrl_inner_solver,ctrl_inner_prec)
    !ctrl_outer_solver=ctrl%ctrl_outer_solver
    ctrl_outer_solver%tol_sol=ctrl%tolerance_nonlinear
    call ctrl_outer_solver%info2str(msg)
    call ctrl%print_string('update', 3, 'OUTER SOLVER: '// msg)
       
    call ctrl%ctrl_inner_solver%info2str(msg)
    call ctrl%print_string('update', 3, 'INNER SOLVER: '// msg)

    
    !
    ! diagonal scaling
    !
    if ( ctrl%diagonal_scaling == 1) then
       call formed_matrix2solve%get_diagonal(this%diagonal_scale)
       this%diagonal_scale=sqrt(this%diagonal_scale)
       this%inv_diagonal_scale=one/this%diagonal_scale
       call formed_matrix2solve%diagonal_scale(lun_err,this%inv_diagonal_scale)

       this%rhs  = this%inv_diagonal_scale * this%rhs
       tdpot%pot = this%diagonal_scale * tdpot%pot

    end if

    ! 1.4 solve linear system
    if (ctrl%debug.ge. 1) &
         write(lun_out,*) 'SOLVING WITH ', etb(ctrl%outer_solver_approach)

    

    if ( ctrl%outer_solver_approach .eq. 'ITERATIVE') then       
       if ( ctrl%outer_prec_type .eq. 'INNER_SOLVER') then
          call inverse_stiff%init(formed_matrix2prec,&
               ctrl%ctrl_inner_solver,&
               ctrl%ctrl_inner_prec,lun=lun_err)       
       else
          call inverse_stiff%init(formed_matrix2prec,&
               ctrl_prec=ctrl%ctrl_inner_prec,lun=lun_err)
       end if
       call krylov_inverse%set(matrix2solve,ctrl_outer_solver,prec_left=inverse_stiff)

       !
       ! solve Linear system with ITERATIVE + PREC 
       !
       call krylov_inverse%matrix_times_vector(this%rhs,tdpot%pot,info,lun_err)
       this%info_solver=krylov_inverse%info_solver
       
       info=0
       if ( this%info_solver%ierr .ne. 0) then
          info=1
          return
       end if
       call this%info_solver%info2str(msg)
       call ctrl%print_string('update',1,msg)
       this%sequence_info_solver(1)=this%info_solver
    else
       call inverse_stiff%init(formed_matrix2solve,&
            ctrl_outer_solver,lun=lun_err)
       !
       ! solve Linear system with AGMG 
       !
       call inverse_stiff%matrix_times_vector(&
            this%rhs,&
            tdpot%pot,&
            info,lun_err)
       this%info_solver=inverse_stiff%info_solver
       info=0
       if ( this%info_solver%ierr .ne. 0) then
          info=1
          return
       end if
       call this%info_solver%info2str(msg)
       call ctrl%print_string('update',1,msg)
       this%sequence_info_solver(1)=this%info_solver
       
    end if


   

    !
    ! diagonal scaling back
    !  
    if ( ctrl%diagonal_scaling == 1) then
       call formed_matrix2solve%diagonal_scale(lun_err,this%diagonal_scale)

       this%rhs  = this%inv_diagonal_scale * this%rhs
       tdpot%pot = this%diagonal_scale * tdpot%pot
       
    end if
    if (ctrl%debug .eq. 1) write(lun_out,*) 'LINEAR SYSTEM SOLVED'
    if ( ode_inputs%ndir > 0 ) then
       this%scr_npot=zero
       do i=1,ode_inputs%ndir
          inode=ode_inputs%dirichlet_nodes(i)
          this%scr_npot(i)=ode_inputs%dirichlet_values(i)-tdpot%pot(inode)
       end do
       write(*,*) 'Error Dirichlet=', dnrm2(ode_inputs%ndir , this%scr_npot,1)
    end if


    ! store linear solver info 
    !this%sequence_info_solver(1)=this%info_solver
    this%sequence_build_prec(1)=ctrl%build_prec
    tdpot%iter_nonlinear = 1
    this%iter_first_system = this%info_solver%iter


    tdpot%all_syncr=.True.
    tdpot%tdpot_syncr = .True.

    !
    ! free memory
    !
    call formed_matrix2prec%kill(lun_err)
    call formed_matrix2solve%kill(lun_err)
    if (ctrl%debug .eq. 1) write(lun_out,*) 'syncronize_tdpot finished'

    call krylov_inverse%kill(lun_err)
    call identity_npot%kill()

  end subroutine syncronize_tdpot


  !>------------------------------------------------
  !> Procedure for computation of next state of system
  !> ( all variables ) given the preovius one
  !> ( private procedure for type dmkpair, used in update)
  !> 
  !> usage: call var%update(lun_err,itemp)
  !> 
  !> where:
  !> \param[inout] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[in   ] ctrl  -> type(DmkCtrl). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !> \param[in   ] info  -> integer. Flag with 3 digits for errors.
  !>                        If info==0 no erros occurred.
  !>                        First digit from the left describes 
  !>                        the main errors. The remaining digits can be used to
  !>                        mark specific errors. Current dictionary:
  !>                        1**  = Error in linear solver
  !>                         11* = Convergence not achivied
  !>                         111 = Max iterations 
  !>                         112 = Internal error in linear solver
  !>                         13* = Failure Preconditoner
  !>                         131 = Failure preconditioner assembly  
  !>                         132 = Failure preconditioner application
  !>                         140 = In reduced approch block22 changes sign
  !>                        2**  = Error in non-linear solver
  !>                         21* = Non-linear convergence not achievied
  !>                         22* = Non-linear solver stopped because was divergening
  !>                         23* = Condition for non-linear solver to
  !>                               continue failed
  !>                        3**  = Other errors
  !<---------------------------------------------------
  subroutine update_tdpot(this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use Globals
    use Timing
    use DmkInputsData
    implicit none
    class(dmkp1p0),  target,intent(inout) :: this
    class(tdpotsys),                           intent(inout) :: tdpot
    class(DmkInputs),                          intent(in   ) :: ode_inputs
    class(DmkCtrl),                           intent(in   ) :: ctrl
    integer,                                  intent(inout) :: info


    ! local
    real(kind=double) :: decay,pflux,pmass,pode,time,tnext,deltat
    integer :: newton_initial
    integer :: ntdens, npot
    integer :: info_inter,passed_reduced_jacobian,time_iteration
    type(tim) :: wasted_temp
    character(len=256) :: str,msg
    integer :: slot
    integer :: lun_err, lun_out,lun_stat
    type(codeTim) :: CPU

    deltat=ctrl%deltat


    !
    ! local copy
    !
    ntdens = this%ntdens
    npot   = this%npot

    lun_err=ctrl%lun_err
    lun_out=ctrl%lun_out
    lun_stat=ctrl%lun_statistics

    time_iteration = tdpot%time_iteration
    time=tdpot%time

    !
    ! we select, before updating, then active parts of the 
    ! domain
    ! 
    call this%set_active_tdpot(tdpot,ctrl)
    
    ! update all spatial variables at itemp+1
    select case ( ctrl%time_discretization_scheme )
    case (1)
       call this%explicit_euler_tdens(tdpot,ode_inputs,ctrl,info)
    case (2) 
       call this%implicit_euler_newton_tdens(&
            tdpot, &
            ode_inputs,&
            ctrl, &
            info,&
            CPU&
            )
    case (3)
       call this%explicit_euler_gfvar(tdpot,ode_inputs,ctrl,info)

    case (4)
       call this%implicit_euler_newton_gfvar(&
            tdpot, &
            ode_inputs,&
            ctrl, &
            info,&
            CPU&
            )

    case (5) 
       call explicit_euler_accelerated_gfvar(this,tdpot,&
            lun_err,info,&
            ctrl,&
            time_iteration,&
            deltat,&
            time,&
            CPU,&
            ode_inputs) 

    case default
       info=-1
       stop

    end select

    
 
  end subroutine update_tdpot


  !>----------------------------------------------------
  !> Procedure for update the system with
  !> Explicit Euler time-stepping in the tdens varaible.
  !>
  !> \mu^{k+1}= \mu^{k+1} + \deltat * RHS_ODE(\mu^k)
  !>
  !> IMPORTANT: all the variables tdens pot 
  !> odein have to be syncronized at time time(itemp)
  !>
  !> usage: call var%explicit_euler_tdens(tdpot,&
  !>                                      ode_inputs,&
  !>                                      ctrl,&
  !>                                      info)
  !> 
  !> where:
  !> \param[inout] tdpot      -> type(tdpotsys):
  !>                              Tdens/Pot pair
  !> \param[in   ] ode_inputs -> type(DmkInputs):
  !>                              Parameters and constants
  !>                              of the problem
  !> \param[in   ] ctrl        -> type(DmkCtrl):
  !>                              Algorithm controls
  !> \param[inout] info        -> integer: Flag to return
  !>                               error in the procedure.
  !>                               If no error occured
  !>                               info=0
  !<---------------------------------------------------    
  subroutine explicit_euler_tdens(this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use Globals 
    use LinearSolver

    implicit none
    class(dmkp1p0), target, intent(inout) :: this
    type(tdpotsys),                           intent(inout) :: tdpot
    type(DmkInputs),                          intent(in   ) :: ode_inputs
    type(DmkCtrl),                            intent(in   ) :: ctrl
    integer,                                  intent(inout) :: info



    !local
    logical :: rc
    integer :: res
    integer ::  id_ode
    integer :: ntdens,npot,lun_err,lun_out,lun_stat
    real(kind=double) :: pode,pflux
    real(kind=double) :: delta
    integer :: icell, isubcell, ifather, inode, i,j,iloc
    real(kind=double) :: grad_pot(3), grad_base(3),grad_der_pot(3),der_pot(3)
    real(kind=double) :: ddot,dnrm2
    integer :: jcell, jsubcell, jfather
    real(kind=double), allocatable :: grad_w(:,:)
    character(len=256) :: fname, number,msg,out_format
    class(abs_linop), pointer :: ort_mat
    real(kind=double), allocatable :: rhs(:)
    !> Info on linear solver solution
    type(input_solver) :: ctrl_solver
    type(input_prec)  :: ctrl_prec


    ntdens = tdpot%ntdens
    npot   = tdpot%npot
    lun_err=ctrl%lun_err
    lun_out=ctrl%lun_out
    lun_stat=ctrl%lun_statistics

    if ( .not. tdpot%all_syncr)  then
       write(lun_err,*) 'Not all varibles are syncronized'
       stop
    end if


    allocate(rhs(npot),stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'explicit_euler_accelerated_gfvar', &
         'temp array rhs')


    if ( ( ctrl%selection .gt. 0) .and. (ctrl%threshold_tdens>1e-20 ) ) then
       call this%near_kernel%set(tdpot%npot_off, tdpot%inactive_pot)
    end if

    !
    ! PP ODE
    ! $ (\Tdens^Pflux |\Grad Pot|)^Pode-\Kappa*Annealing*\Tdens^\Pmass + penalization$
    !
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'Compute RHS ODE'
    call this%assembly_rhs_ode_tdens(&
         ode_inputs,&
         tdpot%tdens,tdpot%pot,&
         this%rhs_ode)

    ! scale by the mass matrix con the Finite Elements used for tdens
    call this%get_increment_ode(this%rhs_ode,this%inc_ode)
    out_format=ctrl%formatting('rar')
    write(msg,out_format) minval (abs(this%inc_ode) ), &
         '<= |TDENS INCREMENT| <=', maxval(abs(this%inc_ode))
    call ctrl%print_string('update',2,msg)


    
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'UPDATE TDENS'
    !
    ! Update $\Tdens$ and $Gfvar$ 
    !
    if (ctrl%selection >0 ) then
       do i=1,tdpot%ntdens_off
          this%inc_ode(tdpot%active_tdens(i))=zero
       end do
    end if
    if ( abs(ode_inputs%direction-one)> small ) write(*,*) 'ASCENDING DYNAMICS'
    call daxpy(ntdens, ode_inputs%direction*ctrl%deltat, this%inc_ode,1,tdpot%tdens,1)

    do icell = 1, tdpot%ntdens
       tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
    end do
    tdpot%tdpot_syncr = .false.

    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'SYNCRONIZE TDPOT'
    !
    ! we use syncronization suboroutine
    ! snd then set time at time+deltat
    !
    call this%syncronize_tdpot(tdpot,ode_inputs,ctrl,info)

    ! update tdpot time related quantities
    if ( info == 0 ) then
       tdpot%deltat=ctrl%deltat
       tdpot%time_old = tdpot%time
       tdpot%time=tdpot%time+ctrl%deltat
       tdpot%iter_nonlinear = 1
       tdpot%time_iteration = tdpot%time_iteration + 1
       tdpot%time_update    = tdpot%time_update + 1
    end if
    
  end subroutine explicit_euler_tdens

  
  !>----------------------------------------------------
  !> Procedure for update the system with
  !> Implicit Euler time-stepping in the tdens varaible.
  !> Non-lineariry is solved via Newton method.
  !>
  !> \mu^{k+1}= \mu^{k+1} + \deltat * RHS_ODE(\mu^k+1)
  !>
  !> IMPORTANT: all the variables tdens pot 
  !> odein have to be syncronized at time time(itemp)
  !>
  !> usage: call var%implicit_euler_newton_tdens(&
  !>                                      tdpot,&
  !>                                      ode_inputs,&
  !>                                      ctrl,&
  !>                                      info,&
  !>                                      CPU)
  !> 
  !> where:
  !> \param[inout] tdpot      -> type(tdpotsys):
  !>                              Tdens/Pot pair
  !> \param[in   ] ode_inputs -> type(DmkInputs):
  !>                              Parameters and constants
  !>                              of the problem
  !> \param[in   ] ctrl        -> type(DmkCtrl):
  !>                              Algorithm controls
  !> \param[inout] info        -> integer: Flag to return
  !>                               error in the procedure.
  !>                               If no error occured
  !>                               info=0
  !> \param[inout] CPU         -> type(codeTim):
  !>                               CPU-timing
  !<---------------------------------------------------
  subroutine implicit_euler_newton_tdens(p1p0,&
       tdpot, &
       ode_inputs,&
       ctrl, &
       info,&
       CPU )
    use Globals
    use TimeInputs, only : write_steady
    use CombinedSparseMatrix
    use Timing
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use DmkInputsData
    use LinearSolver
    use SaddlePointMatrix
    implicit none
    class(dmkp1p0), target, intent(inout) :: p1p0
    type(tdpotsys),                           intent(inout) :: tdpot
    type(DmkInputs),                          intent(in   ) :: ode_inputs
    type(DmkCtrl),                            intent(in   ) :: ctrl
    integer,                                  intent(inout) :: info
    type(codeTim),                            intent(inout) :: CPU

    ! local
    integer :: lun_err,lun_out,lun_stat
    integer :: flag,info_newton
    integer :: current_newton_step
    real(kind=double),  allocatable :: xvar(:)
    real(kind=double),  allocatable :: increment(:)
    real(kind=double),  allocatable :: FNEWTON(:)
    real(kind=double),  allocatable :: D22(:)
    real(kind=double),  allocatable :: rhs_full(:)
    real(kind=double),  allocatable :: tdens(:), pot(:)
    real(kind=double),  allocatable :: tdensnew(:), potnew(:)
    
    real(kind=double) :: tolerance_linear_solver,alpha
    real(kind=double) :: fnewton_norm, initial_fnewton_norm, previuous_fnewton_norm   
    logical :: rc,  test,test_cycle
    integer :: res,info_prec
    integer :: max_nonlinear_step

    integer :: i,j,icell,ntdens,npot,nfull,inode
    character(len=256) :: msg=' ',msg1=' ', msg2=' '
    character(len=256) :: str=' ',sep=' ',out_format=' ',sepa=' ', directory,tail,fname
    real(kind=double) :: dnrm2,relax
    type(file) :: fmat
    real(kind=double) :: discr,MaxC,MinC,r
    type(spmat),target :: B1_matrix
    type(block_linop),target :: matrixM
    type(diagmat), target :: invC
    type(new_linop),target :: ApBTinvCB,ApBTinvCBpR,relaxed_SchurAC
    type(spmat),target   :: formed_ApBTinvCB,matrix2prec
    type(inv),target :: inv_ApBTinvCB
    type(saddleprec),target :: jacobian_prec
    type(scalmat),target :: identity_npot_on
    type(block_linop),target :: jacobian_matrix 
    type(inverse),target :: krylov_inverse
    class(abs_linop), pointer :: inverse_SchurAC
    type(newton_input) :: newton_ctrl
    type(newton_output) :: newton_info
    character(len=5) :: head_string

    type(input_solver),target :: ctrl_outer_solver, ctrl_inner_solver
    type(input_prec),target :: ctrl_outer_prec,ctrl_inner_prec

    ntdens=p1p0%ntdens
    npot=p1p0%npot
    nfull=ntdens+npot

    lun_out=ctrl%lun_out
    lun_err=ctrl%lun_err
    lun_stat=ctrl%lun_statistics

    allocate( &
         xvar(nfull), &
         FNEWTON(nfull),&
         increment(nfull),&
         rhs_full(nfull),&
         D22(ntdens),&
         tdens(ntdens),&
         pot(npot),&
         tdensnew(ntdens),&
         potnew(npot),&
         stat=res)
    if (res .ne. 0) &
         rc = IOerr(lun_err, err_alloc , 'implicit_euler_newton', &
         ' work arrays',res)

    call invC%init(lun_err,ntdens)
    call identity_npot_on%eye(npot)
    call krylov_inverse%init(lun_err,npot)

    ! include dirichlet boundary conditione
    if ( ctrl%info_update > 3) then
       call ode_inputs%info(ctrl%lun_out)
    end if

    if ( ctrl%selection > 0) then
       out_format='(a,f4.0,a,af4.0,a)'                
       write(msg,out_format) &
            'Active nodes=',100*tdpot%npot_on/(one*tdpot%npot),'% - ',&
            'active cell=',100*tdpot%ntdens_on/(one*tdpot%ntdens),'%'
       call ctrl%print_string('update', 2,msg)
    end if

    
    
    !
    ! set initial data 
    !
    xvar(1:npot)       = tdpot%pot
    xvar(npot+1:nfull) = tdpot%tdens

    

    !
    ! set controls
    !
    newton_ctrl%max_nonlinear_iterations = ctrl%max_nonlinear_iterations
    newton_ctrl%tolerance_nonlinear      = ctrl%tolerance_nonlinear
    call set_linear_algebra_ctrls(ctrl,&
         ctrl_outer_solver,ctrl_outer_prec,&
         ctrl_inner_solver,ctrl_inner_prec)
    call ctrl_outer_solver%info2str(msg)
    call ctrl%print_string('update', 2,msg)


    !
    ! start newton cycle
    !
    flag=1
    info_newton=0
    do while ( ( flag .gt. 0 ) .and. (info_newton .eq. 0) )
       select case ( flag)
       case (1)
          ! print head string
          out_format='(I2,a3)'
          write(head_string,out_format) newton_info%current_newton_step,' | '

          
          !
          ! assembly Fnewton
          !
          pot   = xvar(1:npot)
          tdens = xvar(npot+1:nfull)


          !
          ! FNEWTON POT= stiff*pot - rhs
          !
          
          ! assembly stiffness matrix 
          p1p0%scr_ntdens=ode_inputs%beta*tdens+ode_inputs%lambda_lift
          p1p0%stiff%is_symmetric=.True.
          call p1p0%assembly_stiffness_matrix(lun_err,&
               p1p0%scr_ntdens,p1p0%stiff)
          call p1p0%stiff%Mxv(pot, FNEWTON(1:npot) )
          FNEWTON(1:npot) = FNEWTON(1:npot) - ode_inputs%rhs

          ! include dirichlet boundary conditione
          if ( ode_inputs%ndir>0) then
             do i=1,ode_inputs%ndir
                inode=ode_inputs%dirichlet_nodes(i)
                r=p1p0%stiff%coeff(p1p0%stiff%idiag(inode))
                call p1p0%stiff%set_row(inode,zero)
                p1p0%stiff%coeff(p1p0%stiff%idiag(inode))=r
             end do
             p1p0%stiff%is_symmetric=.False.
             do i=1,ode_inputs%ndir
                inode=ode_inputs%dirichlet_nodes(i)
                r=p1p0%stiff%coeff(p1p0%stiff%idiag(inode))
                FNEWTON(inode)=r*(pot(inode)-ode_inputs%dirichlet_values(i))
             end do           
          end if
          
          
          !
          ! FNEWTON TDENS = 1/dt * W * (tdens - tdens_old) - RHS_ODE 
          !
          call p1p0%assembly_rhs_ode_tdens(&
               ode_inputs,&
               tdens,pot,&
               tdpot%scr_ntdens)
          
          FNEWTON(npot+1:nfull) = &
               - 1/ctrl%deltat * p1p0%grid_tdens%size_cell * &
               (tdens-tdpot%tdens_old) &
               + tdpot%scr_ntdens

          FNEWTON(npot+1:nfull)=FNEWTON(npot+1:nfull)!/p1p0%grid_tdens%size_cell

          !
          ! freeze data on non variables
          !
          !if ( ctrl%selection > 0 ) then
             ! potential
             if ( tdpot%npot_on  < tdpot%npot ) then
                do i=1,tdpot%npot_off
                   j=tdpot%inactive_pot(i)
                   FNEWTON(j)=zero
                end do
             end if

             ! tdens
             if ( ( tdpot%ntdens_on<tdpot%ntdens) ) then
                do i=1,tdpot%ntdens_off
                   j=tdpot%inactive_tdens(i)
                   FNEWTON(npot+j)=zero
                end do
             end if
          !end if
          
       case (2)
          ! print info
          write(head_string,'(I2,a3)') newton_info%current_newton_step,' | '
          
          !
          ! compute norm of F 
          !
          p1p0%scr_nfull=FNEWTON
          newton_ctrl%fnewton_norm=dnrm2(npot+ntdens,p1p0%scr_nfull,1)

          ! print info
          out_format=ctrl%formatting('aaraaraar')
          write(msg,out_format) &
               etb(head_string),&
               'NEWTON FUNC=',newton_ctrl%fnewton_norm,&
               ' | ',&
               'POT =', dnrm2(npot,FNEWTON(1:npot),1),&
               ' | ',&
               'TDENS =', dnrm2(ntdens,FNEWTON(npot+1:nfull),1)
               
          call ctrl%print_string('update',2,msg)

          if ( ode_inputs%ndir>0) then
             ! err dirichlet nodes
             p1p0%scr_npot=zero
             do i=1,ode_inputs%ndir
                inode=ode_inputs%dirichlet_nodes(i)
                p1p0%scr_npot(inode)= ode_inputs%dirichlet_values(i)-xvar(inode)
             end do

             ! err not dirichlet nodes
             tdpot%scr_npot=FNEWTON(1:npot)
             do i=1,ode_inputs%ndir
                inode=ode_inputs%dirichlet_nodes(i)
                tdpot%scr_npot(inode)= zero
             end do

             ! print
             out_format=ctrl%formatting('aarar')
             write(msg,*) etb(head_string),&
                  'err diri',dnrm2(npot, p1p0%scr_npot,1),&
                  'err not diri',dnrm2(npot, tdpot%scr_npot,1)
          end if
       case (3)
          !
          ! 1 - assembly jacobian J 
          ! 2 - solve J s = -F 
          !        
          pot   = xvar(1:npot)
          tdens = xvar(npot+1:nfull)
          
          ! assembly J(1,1) = d FNEWTON POT  / d pot   = matrix_A
          ! DONE becasue J(1,1) is p1p0%stiff
          
          ! assembly J(1,2) = d FNEWTON POT  / d tdens = matrix_B1T
          call p1p0%assembly_BC_matrix(pot, ode_inputs%pode ,p1p0%B_matrix)
         

          ! assembly J(2,1)
          do i=1,p1p0%B_matrix%nterm
             p1p0%B1T_matrix%coeff(p1p0%transposer(i)) = p1p0%B_matrix%coeff(i)
          end do
          call p1p0%B1T_matrix%MxD(lun_err, ode_inputs%beta)
          

          ! assembly J(1,2)= d FNEWTON TDENS  / d pot = matrix_B2
          if ( abs(ode_inputs%pode-two)< small ) then
             p1p0%B2_matrix%coeff=p1p0%B_matrix%coeff            
          else
             call p1p0%assembly_BC_matrix(tdpot%pot, ode_inputs%pode ,p1p0%B2_matrix)
          end if
          call p1p0%B2_matrix%DxM(lun_err, ode_inputs%beta*two*tdens**ode_inputs%pflux)

          ! assembly J(2,2)= d FNEWTON TDENS  / d tdens = - matrix_C
          call assembly_D2(p1p0,ode_inputs,tdens, pot, D22)
          p1p0%matrixC%diagonal = -(1/ctrl%deltat)*p1p0%grid_tdens%size_cell +&
               D22
          p1p0%matrixC%diagonal=-p1p0%matrixC%diagonal!/p1p0%grid_tdens%size_cell

          !
          ! F^i(pot,tdens)=u^i-u^i_dirichlet
          ! \partial_{Tdens_{k}} F^{i}=0 \forall k
          !
          if ( ode_inputs%ndir>0) then
             do i=1,ode_inputs%ndir
                inode=ode_inputs%dirichlet_nodes(i)
                call p1p0%B1T_matrix%set_row(inode,zero)
             end do
          end if
             

          ! info hessian 
          out_format=ctrl%formatting('arar')
          MinC=minval(p1p0%matrixC%diagonal(tdpot%active_tdens(1:tdpot%ntdens_on)))
          MaxC=maxval(p1p0%matrixC%diagonal(tdpot%active_tdens(1:tdpot%ntdens_on)))
          write(msg1,out_format) etb(head_string) , MinC,'<=C<=',MaxC
          call ctrl%print_string('update',3,msg1)

          out_format=ctrl%formatting('arar')
          MinC=minval(D22(tdpot%active_tdens(1:tdpot%ntdens_on)))
          MaxC=maxval(D22(tdpot%active_tdens(1:tdpot%ntdens_on)))
          write(msg2,out_format) etb(head_string), minC,'<=D22<=',MaxC
          call ctrl%print_string('update',3,msg2)

          !
          ! solve linear system
          !

          ! set initial data and rhs
          increment=zero
          rhs_full = -FNEWTON


          ! freeze data on non variables
          ! corresponding FNEWTON are already zero
          !if ( ctrl%selection > 0 ) then
             if ( ( tdpot%ntdens_on<tdpot%ntdens) .or. &
                  ( tdpot%npot_on  <tdpot%npot  ) ) then
                ! reduced block J_11
                do i=1,tdpot%npot_off
                   inode=tdpot%inactive_pot(i)
                   call p1p0%stiff%set_row(inode,zero)
                   p1p0%stiff%coeff(p1p0%stiff%idiag(inode))=one
                   call p1p0%B1T_matrix%set_row(inode,zero)
                end do

             
                do i=1,tdpot%ntdens_off
                   icell=tdpot%inactive_tdens(i)
                   call p1p0%B2_matrix%set_row(icell,zero)
                   p1p0%matrixC%diagonal(icell)=one
                end do
             end if
          !end if

          ! 
          ! solve J s = -F 
          !
          
          ! init saddle point matrix J  
          call jacobian_matrix%saddle_point(&
               lun_err,&
               p1p0%stiff,&
               p1p0%B1T_matrix,p1p0%B2_matrix,.True.,&
               p1p0%matrixC)


          ! set intial solution
          increment= zero
          rhs_full = -FNEWTON

          
          
          ! solve linear sys 
          select case (ctrl%solve_jacobian_approach)
          case ('reduced')
             !
             ! invert with respect to C and solve reduced system
             !
             minC=1e30
             do i=1,tdpot%ntdens_on
                if ( p1p0%matrixC%diagonal(tdpot%active_tdens(i))<minC ) then
                   minC=p1p0%matrixC%diagonal(tdpot%active_tdens(i))
                   j=i
                end if
             end do
             out_format=ctrl%formatting('aaiararr')
             write(msg,out_format) etb(head_string),&
                  'j=argmin(C)= ', tdpot%active_tdens(j), &
                  'Tdens(j)=', tdens(tdpot%active_tdens(j)),&
                  ' coord  ',&
                  p1p0%grid_tdens%bar_cell(1,tdpot%active_tdens(j)),&
                  p1p0%grid_tdens%bar_cell(2,tdpot%active_tdens(j))
             call ctrl%print_string('update', 3,msg)
             if ( minC < ctrl%limit_C) then
                info=-1
                out_format=ctrl%formatting('aarar')
                write(msg,out_format) etb(head_string),&
                     'min(C)=', minC,' below limit ', ctrl%limit_C
                call ctrl%print_string('update', 1,msg)
                info=112 ! error in linear solver
                info_newton=-1 ! break newton cycle
                exit    
             else
                !
                ! form Schur Complement SAC = A+B1T(C)^{-1}B2
                !
                
                ! invert C
                invC%diagonal = one/p1p0%matrixC%diagonal             

                ! form implicitely
                p1p0%stiff%name='A'
                invC%name='invC'
                p1p0%B2_matrix%name='B2'
                p1p0%B1T_matrix%name='B1^t'

                ApBTinvCB = p1p0%stiff + &
                     p1p0%B1T_matrix * invC * p1p0%B2_matrix
                
                ! set properties 
                ApBTinvCB%is_symmetric = p1p0%stiff%is_symmetric ! false if dirichlet 
                ApBTinvCB%pair_list(ApBTinvCB%npairs)%is_symmetric  = p1p0%stiff%is_symmetric
                ! set this flag to speed assembly
                ApBTinvCB%pair_list(1)%pattern = 'AWAT'


                ! relax direct schur complement             
                ApBTinvCBpR     = ApBTinvCB + ctrl%relax_direct*identity_npot_on
                relaxed_SchurAC = ApBTinvCB + &
                     (ctrl%relax_direct+ctrl%relax4prec)*identity_npot_on


             
                
                !ctrl_outer_solver=ctrl%ctrl_outer_solver
                if (ctrl_outer_solver%approach .eq. 'ITERATIVE')  then
                   if (ctrl%debug>1) then
                      call ctrl_outer_prec%info(lun_out)
                      call ctrl_outer_solver%info(lun_out)
                   end if
                   !
                   ! init interative sovler
                   !
                   ! form explicitely A+B1TinvCB2+rI  and build approximate inverse 
                   call matrix2prec%form_new_linop(info,lun_err,relaxed_SchurAC) 
                   call inv_ApBTinvCB%init(matrix2prec,ctrl_prec=ctrl%ctrl_outer_prec,lun=lun_err)
                   call krylov_inverse%set(ApBTinvCBpR,ctrl_outer_solver,prec_left=inv_ApBTinvCB)
                   inverse_SchurAC => krylov_inverse
                else
                   call matrix2prec%form_new_linop(info,lun_err,ApBTinvCBpR)
                   call inv_ApBTinvCB%init(matrix2prec,ctrl_outer_solver,lun=lun_err)
                   inverse_SchurAC => inv_ApBTinvCB
                end if


                !
                ! initialized precondtioner of saddle point matrix 
                !
                call jacobian_prec%init(&
                     lun_err,&
                     jacobian_matrix,&
                     prec_type='SchurACFull',&
                     invSchurAC=inverse_SchurAC, invC=invC)

                !
                ! apply inverse
                !
                call jacobian_prec%matrix_times_vector(rhs_full, &
                     increment,info,lun_err)

                !
                ! copy info solver
                !
                if ( ctrl_outer_solver%approach .eq. 'ITERATIVE') then
                   p1p0%info_solver=krylov_inverse%info_solver
                else
                   p1p0%info_solver=inv_ApBTinvCB%info_solver
                end if
                call inv_ApBTinvCB%kill(lun_err)
                call matrix2prec%kill(lun_err)
             end if
          case default 
             rc = IOerr(lun_err, err_val , 'implicit_euler_newton_tdens', &
                  ' ctrl%solve_jacobian_approach :"'//&
                  etb(ctrl%solve_jacobian_approach)// '" not suppported')
             
          end select

          ! store info linear solver
          p1p0%sequence_info_solver(newton_info%current_newton_step)= p1p0%info_solver         


          ! print info
          call p1p0%info_solver%info2str(str) 
          write(msg,'(a,a,a)') &
               etb(head_string), 'LINEAR SOLVER : ',etb(str)
          call ctrl%print_string('update',2,msg)

          
          ! set info flag
          if (p1p0%info_solver%ierr .ne. 0) then
             info=112 ! error in linear solver
             info_newton=-1 ! break newton cycle
             exit         
          end if

          ! freeze data on non variables
          ! corresponding FNEWTON are already zero
          !if ( ctrl%selection > 0 ) then
             if ( ( tdpot%ntdens_on<tdpot%ntdens) .or. &
                  ( tdpot%npot_on  <tdpot%npot  ) ) then
                ! increment frezzen
                tdpot%scr_npot=zero
                do i=1,tdpot%npot_off
                   inode=tdpot%inactive_pot(i)
                   tdpot%scr_npot(inode)=increment(inode)
                   !increment(inode)=zero
                end do
                out_format=ctrl%formatting('aar')
                write(msg,out_format) &
                     etb(head_string), 'Inc. frezzed nodes : ', dnrm2(npot,tdpot%scr_npot,1)
                call ctrl%print_string('update',2,msg)

                ! increment frezzen
                tdpot%scr_ntdens=zero
                do i=1,tdpot%ntdens_off
                   icell=tdpot%inactive_tdens(i)
                   tdpot%scr_npot(icell)=increment(npot+icell)
                   !increment(npot+icell)=zero
                end do
                out_format=ctrl%formatting('aar')
                write(msg,out_format) &
                     etb(head_string), 'Inc. frezzed cells : ', dnrm2(ntdens,tdpot%scr_ntdens,1)
                call ctrl%print_string('update',2,msg)
             end if
          !end if

          !
          ! set newton incremental step thus that
          ! block 2,2 of Jacobian Matrix is stricly positive/negative 
          !
          newton_ctrl%alpha=one
          test_cycle=.True.
          do while (test_cycle)
             potnew     = pot   + newton_ctrl%alpha * increment(1:npot)
             tdensnew   = tdens + newton_ctrl%alpha * increment(npot+1:nfull)

             call assembly_D2(p1p0,ode_inputs,tdensnew, potnew, D22)
             p1p0%scr_ntdens=-1/ctrl%deltat+D22/p1p0%grid_tdens%size_cell

             r=-1.0d30
             do i=1,tdpot%ntdens_on
                icell=tdpot%active_tdens(i)
                !p1p0%scr_ntdens(icell)=-1d30
                r=max( p1p0%scr_ntdens(icell),r)
                if ( p1p0%scr_ntdens(icell) >= r) j=icell
             end do             
             
             !r=maxval(p1p0%scr_ntdens)
             test = (r < -ctrl%epsilon_W22)
             if ( test ) exit
            newton_ctrl%alpha= newton_ctrl%alpha/1.05d0
             test_cycle = ( newton_ctrl%alpha > ctrl%relax_limit) 
             if ( .not. test_cycle ) then
                info_newton=230
             end if
          end do
          !tdens = tdensnew
          !pot   = potnew

          ! print dumping 
          out_format=ctrl%formatting('aar')
          write(msg,out_format) etb(head_string),&
               'Alpha Dumping =', newton_ctrl%alpha 
          call ctrl%print_string('update',2,msg)

          ! print separator
          write(sep,'(a)') ctrl%separator()              
          call ctrl%print_string('update',2,sep)
          
       end select
       call newton_raphson_reverse_with_dt(&
            flag,info_newton,&
            nfull, xvar, increment,&
            newton_ctrl,newton_info)

       !do i=1,ntdens
          
       
    end do

    call ctrl%print_string('update',2,ctrl%separator())

    if (info_newton .eq. 0) then
       ! copy new results
       tdpot%pot   = xvar(1:npot)
       tdpot%tdens = xvar(npot+1:nfull)
       do i=1,ntdens
          tdpot%tdens=max(tdpot%tdens,ctrl%min_tdens)
       end do
       
       ! print newton resume info
       out_format='(a,1x,I2,1x,a,'//etb(ctrl%rformat_info)//')'
       write(msg,out_format) &
            'NEWTON CONVERGED IN', &
            newton_info%current_newton_step,&
            'ITERATIONS WITH RESIDUA=',&
            newton_ctrl%fnewton_norm
       call ctrl%print_string('update',1,msg) 
    else
       ! set info flag according to update_tdpot description
       select case (info_newton)
       case (-1)
          ! maximum number of iterations reached
          info=210
       case (-2)
          ! newton diverged
          info=220
       case (230)
          ! dumping alpha 
          info=info_newton
       end select

       ! print info
       out_format='(a,1x,I2,1x,a,'//etb(ctrl%rformat_info)//',a,I5)'
       write(msg,out_format) &
            'NEWTON FAILED AFTER', &
            newton_info%current_newton_step,&
            'ITERATIONS WITH RESIDUA=',&
            newton_ctrl%fnewton_norm ,&
            ' ERRFLAG= ',info
       call ctrl%print_string('update',1,msg)
    end if


    ! update tdpot time related quantities
    if ( info == 0 ) then
       tdpot%deltat=ctrl%deltat
       tdpot%time_old = tdpot%time
       tdpot%time=tdpot%time+ctrl%deltat
       tdpot%iter_nonlinear = newton_info%current_newton_step - 1
       tdpot%time_iteration = tdpot%time_iteration + 1
       tdpot%time_update    = tdpot%time_update + 1
    end if

    ! free memeory
    deallocate( &
         xvar, &
         FNEWTON,&
         increment,&
         rhs_full,&
         D22,&
         tdens,&
         pot,&
         tdensnew,&
         potnew,&
         stat=res)
    if (res .ne. 0) &
         rc = IOerr(lun_err, err_dealloc , 'implicit_euler_newton_tdens', &
         ' work arrays',res)

    call invC%kill(lun_err)
    call identity_npot_on%kill()
    call krylov_inverse%kill(lun_err)



    
  end subroutine implicit_euler_newton_tdens

  
  !>----------------------------------------------------
  !> Procedure for update the system with
  !> Explicit Euler time-stepping in the gfvar varaible.
  !> Non-lineariry is solved via Newton method.
  !>
  !> gfvar = Trans(\tdens)
  !>
  !> \gf^{k+1}= \gf^{k+1} + \deltat * RHS_ODE(\gf^k)
  !>
  !>
  !> IMPORTANT: all the variables tdens pot 
  !> odein have to be syncronized at time time(itemp)
  !>
  !> usage: call var%explicit_euler_gfvar(&
  !>                                   tdpot,&
  !>                                   ode_inputs,&
  !>                                   ctrl,&
  !>                                   info)
  !> 
  !> where:
  !> \param[inout] tdpot      -> type(tdpotsys):
  !>                              Tdens/Pot pair
  !> \param[in   ] ode_inputs -> type(DmkInputs):
  !>                              Parameters and constants
  !>                              of the problem
  !> \param[in   ] ctrl        -> type(DmkCtrl):
  !>                              Algorithm controls
  !> \param[inout] info        -> integer: Flag to return
  !>                               error in the procedure.
  !>                               If no error occured
  !>                               info=0
  !<---------------------------------------------------   
  subroutine explicit_euler_gfvar(this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use Globals 
    use LinearSolver
    use dagmg_wrap

    implicit none
    class(dmkp1p0), target, intent(inout) :: this
    type(tdpotsys),                           intent(inout) :: tdpot
    type(DmkInputs),                          intent(in   ) :: ode_inputs
    type(DmkCtrl),                            intent(in   ) :: ctrl
    integer,                                  intent(inout) :: info



    !local
    logical :: rc
    integer :: res
    integer ::  id_ode
    integer :: ntdens,npot,lun_err,lun_out,lun_stat
    real(kind=double) :: pode,pflux
    real(kind=double) :: delta
    integer :: icell, isubcell, ifather, inode, i,j,iloc
    real(kind=double) :: grad_pot(3), grad_base(3),grad_der_pot(3),der_pot(3)
    real(kind=double) :: ddot,dnrm2,current_time
    integer :: jcell, jsubcell, jfather
    real(kind=double), allocatable :: grad_w(:,:)
    character(len=256) :: fname, number
    class(abs_linop), pointer :: ort_mat
    real(kind=double), allocatable :: rhs(:)
    !> Info on linear solver solution
    type(input_solver) :: ctrl_solver,ctrl_multigrid
    !> Linear solver for laplacian smoothing
    type(spmat) :: matrix2solve
    type(scalmat) :: identity_ntdens
    type(input_prec)  :: ctrl_prec
    type(agmg_inv)    :: multigrid_solver

    type(input_solver),target :: ctrl_outer_solver, ctrl_inner_solver
    type(input_prec),target :: ctrl_outer_prec,ctrl_inner_prec


    ntdens = tdpot%ntdens
    npot   = tdpot%npot
    lun_err=ctrl%lun_err
    lun_out=ctrl%lun_out
    lun_stat=ctrl%lun_statistics

    if ( .not. tdpot%all_syncr)  then
       write(lun_err,*) 'Not all varibles are syncronized'
       stop
    end if


    if (( ctrl%selection .gt. 0) .and. (ctrl%threshold_tdens>1e-20 )) then
       call this%near_kernel%set(tdpot%npot_off, tdpot%inactive_pot)
    end if

    !
    ! convert to gfvar 
    !
    
    call tdens2gfvar(tdpot%ntdens,ode_inputs%pode,ode_inputs%pflux,tdpot%tdens,tdpot%gfvar)

    !
    ! Compute rhs ode 
    !
    call this%assembly_grad_lyap_gfvar(&
         ode_inputs,&
         tdpot,&
         tdpot%gfvar,tdpot%tdens,tdpot%pot,&
         this%rhs_ode,this%aux_bicgstab)

    ! scale by the mass matrix con the Finite Elements used for tdens
    call this%get_increment_ode(this%rhs_ode,this%inc_ode)

    ! apply laplacian smoothing
    if (abs(ode_inputs%sigma_laplacian_smoothing)>small ) then
       !
       ! build matrix (I-sigma Lapl) 
       !
       matrix2solve=this%cell_laplacian
       call identity_ntdens%eye(ntdens)
       matrix2solve%coeff=ode_inputs%sigma_laplacian_smoothing*matrix2solve%coeff
       call matrix2solve%aMpN(one,identity_ntdens)

       !
       ! set controls and solve via multigrid
       !
       call set_linear_algebra_ctrls(ctrl,&
          ctrl_outer_solver,ctrl_outer_prec,&
          ctrl_inner_solver,ctrl_inner_prec)
       call multigrid_solver%init(lun_err,matrix2solve,ctrl_outer_solver)
       tdpot%scr_ntdens=-this%inc_ode;
       call multigrid_solver%Mxv(this%inc_ode,tdpot%scr_ntdens)
       this%inc_ode=tdpot%scr_ntdens;
       call ctrl_multigrid%kill()
       
       if ( ctrl%info_update .ge. 1) then 
          if( lun_out >0) call multigrid_solver%info_solver%info(lun_out)
       end if
       if( lun_stat>0) call multigrid_solver%info_solver%info(lun_stat)
       
    end if
    
    

    !
    ! Update $\Tdens$ and $Gfvar$ 
    !
    call daxpy(ntdens, -ctrl%deltat, this%inc_ode,1,tdpot%gfvar,1)
    call gfvar2tdens(ntdens,ode_inputs%pode,ode_inputs%pflux,tdpot%gfvar, tdpot%tdens)
    do icell = 1, tdpot%ntdens
       tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
    end do
    call tdens2gfvar(tdpot%ntdens,ode_inputs%pode,ode_inputs%pflux,tdpot%tdens,tdpot%gfvar)
    tdpot%tdpot_syncr = .false.

    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'SYNCRONIZE TDPOT'

    !
    ! we use syncronization subroutine
    ! and then set time at time+deltat
    !
    call this%syncronize_tdpot(tdpot,ode_inputs,ctrl,info)

    ! update tdpot time related quantities
    if ( info == 0 ) then
       tdpot%deltat=ctrl%deltat
       tdpot%time_old = tdpot%time
       tdpot%time=tdpot%time+ctrl%deltat
       tdpot%iter_nonlinear = 1
       tdpot%time_iteration = tdpot%time_iteration + 1
       tdpot%time_update    = tdpot%time_update + 1
    end if

  end subroutine explicit_euler_gfvar

    
  !>------------------------------------------------------------------
  !> Procedure building the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> (ntdens)-rows and (npot)-columns. 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes (10 nodes in 3d)
  !> of a conformally refined grid.
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(icell):ia(icell+1)-1) [with icell the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine set_B_matrix_general(lun_err,subgrid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: subgrid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(subgrid%nnodeincell,subgrid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, icell_sub, inode, inode_sub,j,k,ind
    integer :: nsubnode, nsubcell, ncell_parent
    integer :: nrow, ncol, nterm, nnodeincell, nsubnode_in_cell
    integer :: start, finish
    integer , allocatable :: count_subnode_in_cell(:),ia(:),ja(:)


    nsubcell     = subgrid%ncell
    nsubnode     = subgrid%nnode
    nnodeincell  = subgrid%nnodeincell 
    ncell_parent = subgrid%ncell_parent


    ! triangle
    if ( nnodeincell .eq. 3 ) nsubnode_in_cell = 6
    ! tetrahedron
    if ( nnodeincell .eq. 4 ) nsubnode_in_cell = 10

    nrow  = ncell_parent
    ncol  = nsubnode
    nterm = nsubnode_in_cell * ncell_parent


    !
    ! Set B_matrix dimensions and work arrays
    ! we use members ia,ja in B_matrix as work array
    !
    call B_matrix%init(lun_err, &
         nrow, ncol, nterm,&
         storage_system='csr',&
         is_symmetric =.false.)

    ! allocate work array 
    allocate(&
                                ! count the number of added node for each cell
         count_subnode_in_cell(nrow),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'set_B_matrix_general', &
         'alloc fail for temp array nsubnode_in_cell')

    B_matrix%ia=0
    B_matrix%ja=0
    count_subnode_in_cell=0



    !
    ! set ia pointer
    !
    do icell=1,ncell_parent+1
       B_matrix%ia(icell)=1+(icell-1) * nsubnode_in_cell
    end do

    do icell_sub = 1, nsubcell
       ! cycle all subcell 
       ! find parent cell
       ! find bound in the array ja
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle all nodes in subcell
       !
       if ( count_subnode_in_cell(icell) < nsubnode_in_cell ) then
          do k = 1, nnodeincell
             inode_sub=subgrid%topol(k,icell_sub)

             ! search subnode index in previously added subnodes
             found=.false.
             do j = 1, count_subnode_in_cell(icell)
                ind = start + j -1
                if ( B_matrix%ja( ind ) .eq. inode_sub ) then 
                   found=.true.
                end if
             end do
             ! add subnode index if not added yet
             if ( .not. found ) then
                ind     = start + count_subnode_in_cell(icell)
                B_matrix%ja(ind) = inode_sub
                ! add plus one to nodes  counted for each row
                count_subnode_in_cell(icell) = count_subnode_in_cell(icell) + 1
             end if
          end do
       end if
    end do

    !
    ! sort matrix column-index 
    !
    call B_matrix%sort()

    !
    ! assembly of trija
    !
    do icell_sub = 1, nsubcell
       !
       ! icell father and bounds
       !
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle subnodes
       !
       do k = 1,nnodeincell
          inode_sub = subgrid%topol(k,icell_sub)            
          found=.false.
          j=0
          do while ( .not. found )
             j=j+1
             ind   = start+j-1
             found = ( B_matrix%ja(ind) .eq. inode_sub )
          end do
          trija(k,icell_sub) = ind
       end do
    end do

    deallocate(count_subnode_in_cell,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'set_B_matrix_general', &
         'dealloc fail for temp array count_nsubnode_in_tria ')

  end subroutine set_B_matrix_general


  



  !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(icell):ia(icell+1)-1) [with icell the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine set_B_matrix_grid(lun_err,grid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: grid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(3,grid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, inode, j,k,ind,icol
    integer :: nnode, ncell, nnodeincell
    integer :: start, finish,nodes(3)
    integer , allocatable :: nnode_in_cell(:),ia(:),ja(:)


    ncell        = grid%ncell
    nnode        = grid%nnode
    nnodeincell  = grid%nnodeincell

    ! allocate work array 
    allocate(&
         nnode_in_cell(ncell),&
         ia(ncell+1),&
         ja(ncell*3),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'subnode_topol', &
         'alloc fail for temp array nnode_in_cell ia ja')


    call B_matrix%init(lun_err, &
         ncell, nnode, ncell*nnodeincell,&
         storage_system='csr',&
         is_symmetric =.false.)
    
    nnode_in_cell=0
    ia=0
    ja=0

    do icell=1,ncell+1
       ia(icell)=1+(icell-1)*nnodeincell
    end do

    do icell = 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       ja(start:finish) = grid%topol(1:nnodeincell,icell)
       call isort(nnodeincell,ja(start:finish))
    end do

    !
    ! assign ia,ja
    !      
    B_matrix%ia=ia
    B_matrix%ja=ja

    !
    ! assembly of trija
    !
    do icell= 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       nodes=grid%topol(1:nnodeincell,icell)
       do ind=start,finish
          icol=ja(ind)
          k=1
          do while ( icol .ne. nodes(k) )
             k=k+1
          end do
          trija(k,icell) = ind
       end do
    end do



    deallocate(nnode_in_cell,ia,ja,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'subnode_topol', &
         'dealloc fail for temp array nnode_in_cell ia ja')

  end subroutine set_B_matrix_grid

   
  !>----------------------------------------------------------
  !> Subroutine to assign the pointer prec_zero to a
  !> stdprec. Two option are available
  !> prec_zero => prec_saved
  !> prec_zero => prec_local
  !>--------------------------------------------------------
  subroutine assembly_stdprec(lun_err,&
         matrix2prec,&
         build_prec,&
         ctrl_prec,&
         info_prec,&
         prec_saved)!,prec_local, prec_zero)
      use Globals
      implicit none
      integer,                intent(in   ) :: lun_err
      type(spmat),            intent(inout) :: matrix2prec
      integer,                intent(in   ) :: build_prec
      type(input_prec),    intent(in   ) :: ctrl_prec
      integer,                intent(inout) :: info_prec
      type(stdprec),          intent(inout) :: prec_saved
      !type(stdprec), target,  intent(inout) :: prec_local
      !type(stdprec), pointer, intent(inout) :: prec_zero
      ! local
      logical :: rc
      type(stdprec)  :: prec_local
      
      if ( build_prec .eq. 1 ) then
         info_prec=0
         ! assembly prec on local work prec
         call prec_local%init(lun_err, info_prec,&
              ctrl_prec, matrix2prec%nrow, matrix2prec)
         if ( info_prec .eq. 0 ) then            
            prec_saved = prec_local
         else
            !
            ! write warning 
            !
            ! prec_saved is updated with new preconditioner
            !
            rc = IOerr(lun_err, wrn_val, 'assembly_stdprec', &
                 'using saved prec. assembly failed info_prec=', info_prec)
         end if
         call prec_local%kill(lun_err)


      else
         info_prec = 0
         ! use saved prec.
         if ( .not. prec_saved%is_built) then
            ! check if prec_saved contains something
            rc = IOerr(lun_err, wrn_val, 'assembly_stdprec', &
                 ' prec. saved not built')
            info_prec = -1
         end if
         
      end if

    end subroutine assembly_stdprec

   

    !>--------------------------------------------------------------
    !> Procedure to control the delta_t of the time-stepping
    !> (private procedure used in upadte)
    !<---------------------------------------------------------------
    subroutine  control_deltat(lun_out,ntdens,&
         tdens,increment,previous_var,&
         ctrl,grid_tdens,deltat)
      use Globals
      implicit none

      integer,           intent(in   ) :: lun_out
      integer,           intent(in   ) :: ntdens
      real(kind=double), intent(in   ) :: tdens(ntdens)
      real(kind=double), intent(in   ) :: increment(ntdens)
      real(kind=double), intent(in   ) :: previous_var
      type(DmkCtrl),     intent(in   ) :: ctrl
      type(abs_simplex_mesh),        intent(in   ) :: grid_tdens
      real(kind=double), intent(inout) :: deltat

      !local
      real(kind=double) :: sup_rhs, cnst=1e-8
      real(kind=double) :: n2,n2tdens
      character(len=256):: out_format



      if (ctrl%time_discretization_scheme == 1) then
         deltat=ctrl%deltat 
         out_format=ctrl%formatting('ar')
         write(lun_out,out_format) 'time step = ',deltat
      end if
      if (ctrl%time_discretization_scheme == 2) then
         deltat=min(deltat * ctrl%deltat_expansion_rate,ctrl%deltat_upper_bound)
         out_format=ctrl%formatting('ar')
         write(lun_out,out_format) 'time step = ',deltat
      end if

      if (ctrl%time_discretization_scheme == 3) then
         sup_rhs= maxval(increment)
         if ( sup_rhs < zero ) then
            deltat = max(ctrl%deltat_lower_bound,&
                 min(deltat * ctrl%deltat_expansion_rate,ctrl%deltat_upper_bound))
         else
            deltat=(1.0d0-ctrl%deltat_lower_bound)/sup_rhs
         end if
         deltat=min(deltat,ctrl%deltat_upper_bound)
         
         out_format=ctrl%formatting('araar')
         write(lun_out,out_format) 'time step = ',deltat,&
              ' | ', ' linfty norm increment = ', sup_rhs
      end if

      if (ctrl%time_discretization_scheme == 4) then
         if (previous_var .ne. zero ) then
            deltat=2.0d-1/previous_var
            deltat=min(deltat,ctrl%deltat_upper_bound)
            deltat=max(deltat,ctrl%deltat_lower_bound)
         end if
         if ( 1-deltat*maxval(increment) < zero) then
            deltat = (1.0d0-cnst)/maxval(increment)
            deltat=max(deltat,ctrl%deltat_lower_bound)
         end if
      end if
      
      if (ctrl%time_discretization_scheme == 5) then
         sup_rhs =  maxval(increment)         
         if ( 1-deltat*maxval(increment) < zero) then
            deltat = (1.0d0-cnst)/maxval(increment)
            write(lun_out,*) 'time step = ',deltat            
         else
            if ( sup_rhs .gt. zero ) deltat = one / maxval(increment)
            if ( sup_rhs .lt. zero ) deltat = one / maxval(abs(increment))
         end if
         deltat=min(deltat,ctrl%deltat_upper_bound)
         deltat=max(deltat,ctrl%deltat_lower_bound)
      end if


    end subroutine control_deltat
    
    !>---------------------------------------------------------
    !> Procedure to write into file the variables involved in the
    !> newotn procedure.
    !>---------------------------------------------------------
    subroutine write_newton_p1p0(p1p0,&
         lun_err,lun_work,&
         directory,tail,&
         deltat)
      use Globals
      implicit none
      class(dmkp1p0), intent(in   ) :: p1p0
      integer,                          intent(in   ) :: lun_err
      integer,                          intent(in   ) :: lun_work
      character(len=*),                 intent(in   ) :: directory
      character(len=*),                 intent(in   ) :: tail
      real(kind=double),                intent(in   ) :: deltat
      ! local 
      integer :: i
      character(len=256) :: fname
      type(file) :: fmat
      integer :: ntdens, npot
      
      ntdens = p1p0%ntdens
      npot   = p1p0%npot


      fname=etb(etb(directory)//'/stiff_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%stiff%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/b_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%B_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/bt_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%BT_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/c_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%C_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/deltatd1c_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%deltatD1C_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/d1_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,ntdens
         write(fmat%lun,*) p1p0%D1(i)
      end do
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/d2_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,ntdens
         write(fmat%lun,*) p1p0%D2(i)
      end do
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/deltat_'//etb(tail))
      call fmat%init(lun_err,fname,10000,'out')
      write(fmat%lun,*) deltat 
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/rhs_pot_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,npot
         write(fmat%lun,*) -p1p0%fnewton_pot(i)
      end do
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/rhs_tdens_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,ntdens
         write(fmat%lun,*) -p1p0%fnewton_tdens(i)
      end do
      call fmat%kill(lun_err) 


    end subroutine write_newton_p1p0
  



  
  !>--------------------------------------------------------------------
  !> Static Constructor 
  !> (public procedure for type dmkp1p0)
  !> 
  !> usage: call var%init(&
  !>                lun_err,lun_out,lun_stat,& 
  !>                ctrl,&
  !>                grid_tdens, grid_pot)
  !> where:
  !> \param[in] lun_err    -> integer. I/O unit for error message
  !> \param[in] lun_out    -> integer. I/O unit for output message
  !> \param[in] lun_stat   -> integer. I/O unit for statistic
  !> \param[in] ctrl       -> type(DmkCtrl). Controls variables
  !> \param[in] grid_tdens -> tyep(mesh). Mesh for tdens
  !> \param[in] grid_pot   -> tyep(mesh). Mesh for pot
  !>---------------------------------------------------------------------
  subroutine init_p1p0(this,&
       ctrl,&
       id_subgrid, grid_tdens, grid_pot)
    implicit none
    class(dmkp1p0),target, intent(inout) :: this
    type(DmkCtrl),                    intent(in   ) :: ctrl
    integer,                          intent(in   ) :: id_subgrid
    type(abs_simplex_mesh), target,               intent(in   ) :: grid_tdens
    type(abs_simplex_mesh), target,               intent(in   ) :: grid_pot
    ! local
    logical :: rc
    integer :: res
    integer :: ntdens, npot, ngrad,i,iloc,jloc
    integer :: lun_err
    integer :: lun_out
    integer :: lun_stat
    ! jacobian 
    type(array_mat) :: jacobian_list(4)
    integer :: block_structure(3,4)
    character(len=1) :: jacobian_directions(4)
    real(kind=double), allocatable :: kernel_full(:,:) 
    ! preconditioners
    type(array_linop) :: prec_list(2)
    integer :: prec_block_structure(3,2)
    type(input_prec) :: ctrl_prec
    type(scalmat),   target :: identity_npot
    integer ::info
    type(input_prec) :: ctrl_inverse
    real(kind=double) :: dnrm2
    integer :: dim_work,nfull

    if ( ( ctrl%debug .eq. 1 ) .and. (lun_out>0) )&
         write( lun_out, *) 'Init p1p0 structure'

    
    
    this%id_subgrid = id_subgrid
    this%grid_tdens => grid_tdens
    this%grid_pot   => grid_pot

    !write(*,*) this%id_subgrid
    
    !
    ! dimension assignment + local copy
    !
    this%ntdens    = grid_tdens%ncell
    this%npot      = grid_pot%nnode
    this%ngrad     = grid_pot%ncell
    this%ambient_dimension = grid_tdens%ambient_dimension
    this%nfull     =  this%ntdens +  this%npot

    !
    ! short hand copy 
    !
    lun_out  = ctrl%lun_out 
    lun_err  = ctrl%lun_err
    lun_stat = ctrl%lun_statistics
 
    ntdens   = this%ntdens    
    npot     = this%npot      
    ngrad    = this%ngrad  
    
    !
    ! construction of p1 element space
    !
    if ( ( ctrl%debug .eq. 1 ) .and. (lun_out>0) )&
         write( lun_out, *) 'Init p1 Galerkin'
    call this%p1%init(lun_err,this%grid_pot)
    
    !
    ! grad depending elements
    !
    if ( ( ctrl%debug .eq. 1 ) .and. (lun_out>0) )&
         write( lun_out, *) 'Allocation gradient variables'
    allocate(&
         this%grad(this%grid_pot%logical_dimension,ngrad),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member grad')
    
    allocate(&
         this%grad_avg(this%grid_pot%logical_dimension,ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member grad_avg')

    allocate(&
         this%norm_grad_avg(ntdens),& 
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member norm_grad_avg')
    
    allocate(&
         this%norm_grad(ngrad),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member grad norm_grad')
    
    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    if ( (ctrl%debug .ge. 1)  .and. (lun_out>0) ) &
         write(lun_out,'(a)') 'Stiffness Matrix Initiliazation'
    call this%stiff%init(lun_err,&
         npot, npot,  this%p1%nterm_csr,&
         storage_system='csr',&
         is_symmetric=.true.)
    this%stiff%ia=this%p1%ia_csr
    this%stiff%ja=this%p1%ja_csr
    
    
    


    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'Stiffness Matrix initiliazed'
    call this%near_kernel%init(lun_err,npot)
    call this%near_kernel_full%init(lun_err,npot+ntdens)
    this%near_kernel_full%base_kernel(1:npot) = one
    this%near_kernel_full%base_kernel(1+npot: npot+ntdens) = zero
    this%near_kernel_full%base_kernel(:) = &
         this%near_kernel_full%base_kernel(:)/dnrm2(npot+ntdens,this%near_kernel_full%base_kernel(:),1)
    

    !
    ! Variables for Newton Method in Implicit Euler Procedure
    !
    ! B_matrix and assembler_Bmatrix_subgrid
    ! (the pointer ia and ja are stored directly in B_matrix)
    allocate(&
         this%assembler_Bmatrix_subgrid(&
         this%grid_pot%nnodeincell,this%grid_pot%ncell),&
         this%assembler_Bmatrix_grid(&
         this%grid_tdens%nnodeincell,this%grid_tdens%ncell),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member assembler_Bmatrix_subgrid')
    
    if (id_subgrid .eq. 1) then
       call assembly_B_matrix_general(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_subgrid) 
    else
       call assembly_B_matrix_grid(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_grid)
    end if
    
    
    

    !
    ! copy and transpose matrix_B
    !
    this%BT_matrix = this%B_matrix
    allocate (this%transposer(this%B_matrix%nterm),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member transposer')
    call this%BT_matrix%transpose(lun_err,this%transposer)

    this%B2_matrix = this%B_matrix
    this%B1T_matrix = this%BT_matrix

    allocate(&
         this%sequence_info_solver(ctrl%max_nonlinear_iterations),&
         this%sequence_build_prec(ctrl%max_nonlinear_iterations),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member sequence_info_solver sequence_build_prec')


    !
    ! copies of B matrix to copy its structure
    !
    this%C_matrix=this%B_matrix
    this%deltatD1C_matrix = this%C_matrix
    !
    ! copy and transpose matrix_C
    !
    this%CT_matrix=this%C_matrix
    call this%CT_matrix%transpose(lun_err)
    
    !
    ! gf matrices
    !
    this%DB_matrix = this%B_matrix
    this%BTmatrixC = this%BT_matrix

    ! copy and transpose matrix_C
    !
    this%CT_matrix=this%C_matrix
    call this%CT_matrix%transpose(lun_err)
    
    !
    ! set dimensions for compunent od block matrix
    !
    call this%matrixC%init(lun_err, ntdens)
    call this%inv_matrixC%init(lun_err, ntdens)


    ! set Kernel
    allocate(this%kernel_full(npot+ntdens,1),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' temporary member')
    this%kernel_full(1:npot,1)             = one
    this%kernel_full(npot+1:npot+ntdens,1) = zero

    ! set jacobian block structure (only dimensions)
    !
    ! (A          B^T )
    ! (-dt D1 C   D2  )
    !
    block_structure(:,1) = (/1,1,1/) ! A        matrix in 1,1
    jacobian_list(1)%mat => this%stiff 
    jacobian_directions(1) = 'N'

    block_structure(:,2) = (/2,1,2/) ! B^T      matrix in 1,2 
    jacobian_list(2)%mat => this%BT_matrix
    jacobian_directions(2) = 'N'

    block_structure(:,3) = (/3,2,1/) ! -dt D1 B matrix in 2,1
    jacobian_list(3)%mat => this%deltatD1C_matrix
    jacobian_directions(3) = 'N'

    block_structure(:,4) = (/4,2,2/) ! D2       matrix in 2,2
    jacobian_list(4)%mat => this%matrixC
    jacobian_directions(4) = 'N'

    call this%jacobian_full%init(&
         lun_err,&
         4, jacobian_list,&
         2 , 2, 4, block_structure,jacobian_directions, .false. ) 


    ! EFAMLB
    ! set symmetric jacobian block structure
    !
    if ( ( ctrl%debug .eq. 1 ) .and. (lun_out>0) )&
         write( lun_out, *) 'Init symmetric  jacobian '
    
    allocate(this%symmetrizer(ntdens),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, &
         'init_p1p0', &
         'symmetrizer' ) 
   
    call this%G_matrix%init(lun_err, ntdens)
    
       


   
    !
    ! Varaibles for Linear solver procedure
    !
    ! Auxiliary var for PCG procedure
    nfull=npot+ntdens
    dim_work=&
         nfull*(ctrl%krylov_nrestart+7)+&
         (ctrl%krylov_nrestart+1)*(ctrl%krylov_nrestart+2)+&
         nfull*ctrl%krylov_nrestart
    call this%aux_bicgstab%init(lun_err, 0, dim_work)

    ! Auxiliary var for PCG procedure  
    call this%aux_newton%init(lun_err,&
         0, 13*ntdens + 5 * npot + 4* (ntdens + npot))

    ! rhs scratch
    allocate(&
         this%trans_second(ntdens),&
         this%trans_prime(ntdens),&
         this%rhs(npot),&
         this%rhs_ode(ntdens),&
         this%inc_ode(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member rhs')

    ! 
    ! work array
    !           
    allocate(&
         this%tdens_prj(ngrad),&
         this%scr_npot(npot),&
         this%scr_ntdens(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', & 
         ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')
    allocate(&
         this%scr_ngrad(ngrad),&
         this%scr_nfull(npot+ntdens),&
         this%rhs_full(npot+ntdens),&
         this%sqrt_diag(npot),&
         this%scr_integer(npot+ntdens),&
         this%diagonal_laplacian(npot),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', & 
        ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')
       

    call this%identity_npot%eye(npot)
    call this%identity_ntdens%eye(ntdens)
    ! 
    ! work array
    ! 
    allocate(&
         this%D1(ntdens),&
         this%D2(ntdens),&
         this%D3(ntdens),&
         this%invD2(ntdens),&
         this%invD2_D1(ntdens),&
         this%norm_rows_stiff(npot),&
         this%norm_grad_dyn(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member'//&
         ' D1, D2, invD2, invD2_D1')
   
    this%scr_ntdens=one
    call this%assembly_stiffness_matrix(lun_err,this%scr_ntdens,this%stiff)
    call this%stiff%get_diagonal(this%diagonal_laplacian )

    ! 
    ! newton function
    ! 
    allocate(&
         this%fnewton_tdens(ntdens),&
         this%fnewton_gfvar(ntdens),&
         this%fnewton_pot(npot),&
         this%fnewton_tdens_old(ntdens),&
         this%fnewton_gfvar_old(ntdens),&
         this%fnewton_pot_old(npot),&
         this%rhs_reduced(npot),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member'//&
         ' fnewton_pot,  fnewton_tdens'//&
         ' fnewton_pot_old,  fnewton_tdens_old'//&
         ' rhs_reduced')


     !
    ! reduced jacobian A +deltat BT D2^{-1} D1 C 
    !
    !
    !  assembly system matrix 
    !     M = A + deltat + B^T invD2 D1 C ( PP ode)  
    !        or 
    !     M = A + deltat + B^T invD2 D1 B ( GF ode)
    
    ! for GF ode
    ! M = A + 2*deltat B^T invD2 D1 B
    call this%jacobian_reduced%init(&
         lun_err,&
         .true.,&
         this%stiff,&
         this%B_matrix,&
         this%B_matrix,&
         this%invD2_D1)

    ! for PP ode
    ! M = A+deltat B^T invD2 D1 C
    call this%stiff_gamma%init(&
         lun_err,&
         .True.,&
         this%stiff,&
         this%B_matrix,&
         this%B_matrix,&
         this%scr_ntdens)

    !
    ! build sparsity pattern of matrix BTDC
    !
    call this%stiff_gamma_assembled%mult_MDN(lun_err,&
         this%BT_matrix,this%C_matrix,&
         100,100*this%npot,&
         this%invD2_D1)

    this%BT_gamma = this%BT_matrix 


    
    !
    ! augemented lagrangian approach
    !
    ! set jacobian block structure (only dimensions)
    !
    ! (A_gamma    B_gamma^T )
    ! (-dt D1 C   D2        )
    !
    block_structure(:,1) = (/1,1,1/) ! A        matrix in 1,1
    jacobian_list(1)%mat => this%stiff !_gamma_assembled 
    jacobian_directions(1) = 'N'

    block_structure(:,2) = (/2,1,2/) ! B^T      matrix in 1,2 
    jacobian_list(2)%mat => this%BT_matrix !gamma
    jacobian_directions(2) = 'N'

    block_structure(:,3) = (/3,2,1/) ! -dt D1 B matrix in 2,1
    jacobian_list(3)%mat => this%deltatD1C_matrix
    jacobian_directions(3) = 'N'

    block_structure(:,4) = (/4,2,2/) ! D2       matrix in 2,2
    jacobian_list(4)%mat => this%matrixC
    jacobian_directions(4) = 'N'

    call this%augmented_jacobian%init(&
         lun_err,&
         4, jacobian_list,&
         2 , 2, 4, block_structure,jacobian_directions, .false. ) 


     allocate (&
          this%diagonal_weight(ntdens),&
          this%inv_diagonal_weight(ntdens),&
          this%diagonal_scale(npot),&
          this%inv_diagonal_scale(npot),&
          this%hatS(ntdens),&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
          ' member diagonal weight ')


    if ((ctrl%debug .ge. 1) .and. (lun_out>0 ) ) &
         write(lun_out,'(a)') 'init scratch'

    !
    ! build sparsity pattern of matrix BTDC
    !
    call this%BTDC_matrix%mult_MDN(lun_err,&
         this%BT_matrix,this%C_matrix,&
         100,100*this%npot,&
         this%invD2_D1)

    !
    ! assembly redirector of stiffness  matrix into BTDC 
    !
    allocate(this%stiff2BTDC(this%p1%nterm_csr),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' member stiff2BTDC ')
    call this%stiff%assembly_redirector(lun_err,&
         this%BTDC_matrix,this%stiff2BTDC)


        ! (A          B^T            )
    ! (B   -D2 (deltat D1)^-1 =G )
    !
    block_structure(:,1) = (/1,1,1/) ! A        matrix in 1,1
    jacobian_list(1)%mat => this%stiff 
    jacobian_directions(1) = 'N'
    
    block_structure(:,2) = (/2,1,2/) ! B^T      matrix in 1,2 
    jacobian_list(2)%mat => this%BT_matrix
    jacobian_directions(2) = 'N'
    
    block_structure(:,3) = (/3,2,1/) ! B matrix in 2,1
    jacobian_list(3)%mat => this%B_matrix
    jacobian_directions(3) = 'N'
    
    block_structure(:,4) = (/4,2,2/) ! D2 / ( -dt D1 )  matrix in 2,2
    jacobian_list(4)%mat => this%G_matrix
    jacobian_directions(4) = 'N'
    
    call this%sym_jacobian_full%init(&
         lun_err,&
         4, jacobian_list,&
         2 , 2, 4, block_structure, jacobian_directions, .True.)




    ! Predonditioner and back_up
    call this%spectral_info%init(lun_err,&
         10,this%npot)


    
    !call this%der_pot_tdens_jacobian%init(lun_err, npot, ntdens)
    !call this%tdens_jacobian%init(lun_err, ntdens,ntdens)

    !
    ! bfgs 
    !
    call this%bfgs_prec%init(lun_err,npot)
    call this%matrix_V%init(lun_err, npot, ctrl%max_bfgs,&
         is_symmetric= .False.)
    call this%matrix_AV%init(lun_err, npot, ctrl%max_bfgs,&
        is_symmetric= .False.)
    call this%matrix_VTAV%init(lun_err, ctrl%max_bfgs, ctrl%max_bfgs,&
         is_symmetric= .False.)
    !call this%matrix_VTAV%info(6)
    ctrl_inverse%prec_type = 'ICHOL'
    call this%matrix_PI%init(lun_err,this%matrix_VTAV,ctrl_inverse)
    
    !
    ! component (2,2)
    !
    call this%inverse_D2%init(lun_err,ntdens)
       
    call this%inverse_diag_stiff%init(lun_err,npot)

    prec_block_structure(:,1) = (/1,1,1/)
    prec_list(1)%linop => this%inverse_diag_stiff

    prec_block_structure(:,2) = (/2,2,2/)
    prec_list(2)%linop=> this%inverse_D2

    
    call this%prec_full%init(lun_err, &
         2, prec_list,&
         2, 2, &
         2, prec_block_structure,.True.)
        
    
   
    call this%scr_diagmat_ntdens%init(lun_err,ntdens)
    
    call this%approx_inverse%init(lun_err,npot)

    call this%inverse_stiff%init(lun_err,npot)
    
    call build_cell_laplacian(lun_err,this%grid_tdens,this%cell_laplacian)

    this%scr_ngrad = 1.0
    call this%p1%build_mass(lun_err,'csr',this%scr_ngrad,this%p1_mass_matrix)
    call this%p1%build_mass_lumped(lun_err,this%scr_ngrad,this%p1_lumped_mass_matrix)


  contains

      !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(icell):ia(icell+1)-1) [with icell the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine assembly_B_matrix_general(lun_err,subgrid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: subgrid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(subgrid%nnodeincell,subgrid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, icell_sub, inode, inode_sub,j,k,ind
    integer :: nsubnode, nsubcell, ncell_parent
    integer :: nrow, ncol, nterm, nnodeincell, nsubnode_in_cell
    integer :: start, finish
    integer , allocatable :: count_subnode_in_cell(:),ia(:),ja(:)


    nsubcell     = subgrid%ncell
    nsubnode     = subgrid%nnode
    nnodeincell  = subgrid%nnodeincell 
    ncell_parent = subgrid%ncell_parent


    ! triangle
    if ( nnodeincell .eq. 3 ) nsubnode_in_cell = 6
    ! tetrahedron
    if ( nnodeincell .eq. 4 ) nsubnode_in_cell = 10

    nrow  = ncell_parent
    ncol  = nsubnode
    nterm = nsubnode_in_cell * ncell_parent


    !
    ! Set B_matrix dimensions and work arrays
    ! we use members ia,ja in B_matrix as work array
    !
    call B_matrix%init(lun_err, &
         nrow, ncol, nterm,&
         storage_system='csr',&
         is_symmetric =.false.)

    ! allocate work array 
    allocate(&
         ! count the number of added node for each cell
         count_subnode_in_cell(nrow),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'assembly_B_matrix_general', &
         'alloc fail for temp array nsubnode_in_cell')

    B_matrix%ia=0
    B_matrix%ja=0
    count_subnode_in_cell=0



    !
    ! set ia pointer
    !
    do icell=1,ncell_parent+1
       B_matrix%ia(icell)=1+(icell-1) * nsubnode_in_cell
    end do

    do icell_sub = 1, nsubcell
       ! cycle all subcell 
       ! find parent cell
       ! find bound in the array ja
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle all nodes in subcell
       !
       if ( count_subnode_in_cell(icell) < nsubnode_in_cell ) then
          do k = 1, nnodeincell
             inode_sub=subgrid%topol(k,icell_sub)

             ! search subnode index in previously added subnodes
             found=.false.
             do j = 1, count_subnode_in_cell(icell)
                ind = start + j -1
                if ( B_matrix%ja( ind ) .eq. inode_sub ) then 
                   found=.true.
                end if
             end do
             ! add subnode index if not added yet
             if ( .not. found ) then
                ind     = start + count_subnode_in_cell(icell)
                B_matrix%ja(ind) = inode_sub
                ! add plus one to nodes  counted for each row
                count_subnode_in_cell(icell) = count_subnode_in_cell(icell) + 1
             end if
          end do
       end if
    end do

    !
    ! sort matrix column-index 
    !
    call B_matrix%sort()

    !
    ! assembly of trija
    !
    do icell_sub = 1, nsubcell
       !
       ! icell father and bounds
       !
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle subnodes
       !
       do k = 1,nnodeincell
          inode_sub = subgrid%topol(k,icell_sub)            
          found=.false.
          j=0
          do while ( .not. found )
             j=j+1
             ind   = start+j-1
             found = ( B_matrix%ja(ind) .eq. inode_sub )
          end do
          trija(k,icell_sub) = ind
       end do
    end do

    deallocate(count_subnode_in_cell,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'assembly_B_matrix_general', &
         'dealloc fail for temp array count_nsubnode_in_tria ')

  end subroutine assembly_B_matrix_general



  subroutine build_cell_laplacian(lun_err,grid,cell_laplacian)
    implicit none
    integer,                intent(in   ) :: lun_err
    type(abs_simplex_mesh), intent(in   ) :: grid
    type(spmat),            intent(inout) :: cell_laplacian
    !local
    logical :: rc
    integer :: res
    real(kind=double),allocatable :: invW(:)
    type(abs_simplex_mesh) :: graph
    type(spmat) :: grad_operator,div_operator
    
    !
    ! build the laplacian as the weigthed laplacian L
    ! of the graph describing the cell-cell connection
    ! the Laplacian matrix is 
    ! L=G^T W^-1 G
    ! with
    ! G=signed incidence matrix of graph (is the gradient operator)
    ! G^T=divergence operator
    ! W =diagonal matrix with t
    !
    !call graph%build_cellcell_graph(lun_err,this%grid_tdens)
    ! build G
    call graph%build_connection_matrix(lun_err,grad_operator)
    do i=1,graph%ncell
       grad_operator%coeff(grad_operator%ia(i))=one
       grad_operator%coeff(grad_operator%ia(i)+1)=-one
    end do

    ! build G^T
    div_operator=grad_operator
    call div_operator%transpose(lun_err)
    ! build W^-1
    call graph%build_size_cell(lun_err)
    allocate(invW(graph%ncell),stat=res)
    if (res.ne. 0) rc = IOerr(lun_err, err_alloc,&
         ' build_cell_laplacian', &
         ' temp array invW' ) 

    invW=one/graph%size_cell

    ! build laplacian
    call cell_laplacian%mult_MDN(lun_err,&
         div_operator,grad_operator,&
         20,20*grid%ncell,invW,&
         div_operator)

    deallocate(invW,stat=res)
    if (res.ne. 0) rc = IOerr(lun_err, err_dealloc,&
         ' build_cell_laplacian', &
         ' temp array invW' )
    call grad_operator%kill(lun_err)
    call div_operator%kill(lun_err)




  end subroutine build_cell_laplacian


    
  end subroutine init_p1p0


  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type dmkp1p0)
  !>
  !> usage:
  !>     call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !<-----------------------------------------------------------    
  subroutine kill_p1p0(this,lun_err)     
    use Globals
    implicit none
    class(dmkp1p0), intent(inout) :: this
    integer,                          intent(in   ) :: lun_err
    !local
    integer :: res,i
    logical :: rc

    this%grid_tdens => null()
    this%grid_pot   => null()
    
    !
    ! dimension reset
    !
    this%ntdens    = 0
    this%npot      = 0
    this%ngrad  = 0
    this%ambient_dimension = 0

    

    !
    ! construction of p1 element space
    !
    call this%p1%kill(lun_err)

    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    call this%stiff%kill(lun_err)
    call this%near_kernel%kill(lun_err)
    call this%near_kernel_full%kill(lun_err)
    deallocate(this%kernel_full,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member this%kernel_full')

    !
    ! Variables for Newton Method in Implicit Euler Procedure
    !
    ! B_matrix and assembler_Bmatrix_subgrid
    ! (the pointer ia and ja are stored directly in B_matrix)
    deallocate(&
         this%assembler_Bmatrix_subgrid,&
         this%assembler_Bmatrix_grid,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member assembler_Bmatrix_subgrid')

    !
    ! kill matrix_B and its transpose 
    !
    call this%B_matrix%kill(lun_err)
    call this%BT_matrix%kill(lun_err)
    deallocate (this%transposer,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member transposer')


    !
    ! kill matrix_C and its transpose 
    !
    call this%C_matrix%kill(lun_err)
    call this%CT_matrix%kill(lun_err)
    call this%deltatD1C_matrix%kill(lun_err)


    
    !
    ! Varaibles for Linear solver procedure
    !
    ! Auxiliary var for PCG procedure  
    call this%aux_bicgstab%kill(lun_err)
    ! Auxiliary var for PCG procedure  
    call this%aux_newton%kill(lun_err)
    ! rhs scratch
    deallocate(&
         this%trans_second,&
         this%trans_prime,&
         this%rhs,&
         this%rhs_ode,&
         this%inc_ode,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member rhs')

   

    ! 
    ! work array
    ! 
    deallocate(&
         this%tdens_prj,&
         this%scr_npot,&
         this%scr_ntdens,&
         this%scr_ngrad,&
         this%scr_nfull,&
         this%sqrt_diag,&
         this%norm_grad_dyn,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')


    call this%identity_npot%kill()
    call this%identity_ntdens%kill()

    ! 
    ! work array
    ! 
    deallocate(&
         this%D1,&
         this%D2,&
         this%D3,&
         this%invD2,&
         this%invD2_D1,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member'//&
         ' D1, D2, invD2, invD2_D1')


    deallocate(&
         this%grad,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member grad')
    
    deallocate(&
         this%grad_avg,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member grad_avg')

    deallocate(&
         this%norm_grad_avg,& 
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member norm_grad_avg')
    
    deallocate(&
         this%norm_grad,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
     ' type p1p0 member grad norm_grad')

    

    ! 
    ! newton function
    ! 
    deallocate(&
         this%fnewton_tdens,&
         this%fnewton_gfvar,&         
         this%fnewton_pot,&
         this%fnewton_tdens_old,&
         !this%fnewton_gfvar_old,&         
         !this%fnewton_pot_old,&
         !this%rhs_reduced,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member'//&
         ' fnewton_pot,  fnewton_tdens')



    !
    ! kill matrix BTDC
    !
    call this%BTDC_matrix%kill(lun_err)


    !
    ! assembly redirector of stiffness  matrix into BTDC 
    !
    deallocate(this%stiff2BTDC,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' member stiff2BTDC ')


    call this%scr_diagmat_ntdens%kill(lun_err)

    call this%approx_inverse%kill(lun_err)

    do i=1,size(this%sequence_info_solver)
       call this%sequence_info_solver(i)%kill()
    end do
    
    deallocate(&
         this%sequence_info_solver,&
         this%sequence_build_prec,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member sequence_info_solver sequence_build_prec')


  end subroutine kill_p1p0

  !>------------------------------------------------------------
  !> Assembly the rhs of the tdens ode of gfvar ode
  !<-------------------------------------------------------------
  subroutine assembly_rhs_ode_tdens(this,&
       ode_inputs,&
       tdens,pot,&
       rhs_ode)
    use DmkInputsData

    implicit none
    class(dmkp1p0), intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: pot(this%npot) 
    real(kind=double), intent(inout) :: rhs_ode(this%ntdens) 

    !local
    integer :: icell,j
    integer :: ntdens
    real(kind=double) :: min,max, pflux, pmass,decay
    real(kind=double) :: ptrans, gf_pflux, gf_pmass
    real(kind=double) :: penalty_factor

    ntdens = this%ntdens


    pflux = ode_inputs%pflux
    decay = ode_inputs%decay
    pmass = ode_inputs%pmass
    
    ! used rhs_ode as scratch
    rhs_ode = zero
    call this%build_norm_grad_dyn(pot,ode_inputs%pode, this%norm_grad_dyn)
    do icell = 1,ntdens
       rhs_ode(icell) = rhs_ode(icell) + &
            ode_inputs%beta(icell)*tdens(icell) ** pflux * this%norm_grad_dyn(icell)
    end do

    do icell = 1,ntdens     
       rhs_ode(icell) = rhs_ode(icell) - &
            ode_inputs%kappa(icell)**ode_inputs%pode * decay * &
            tdens(icell) ** pmass 
    end do

    rhs_ode = rhs_ode + ode_inputs%psi  

    !
    ! penalization
    !
    penalty_factor = ode_inputs%penalty_factor
    if ( abs( penalty_factor ) .gt. small) then
       do icell = 1,ntdens
          rhs_ode(icell) = rhs_ode(icell) - &
               penalty_factor * &
               ode_inputs%penalty_weight(icell) * &
               (tdens(icell) - &
               ode_inputs%penalty_function(icell) * &
               tdens(icell)**pflux ) 
       end do
    end if

    rhs_ode = rhs_ode * this%grid_tdens%size_cell
    


  end subroutine assembly_rhs_ode_tdens
  
  subroutine assembly_grad_lyap_gfvar(this,&
       ode_inputs,&
       tdpot,&
       gfvar,tdens,pot,&
       grad_lyap,aux_global)
    use DmkInputsData

    implicit none
    class(dmkp1p0), intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    real(kind=double), intent(in   ) :: gfvar(this%ntdens) 
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: pot(this%npot)     
    real(kind=double), intent(inout) :: grad_lyap(this%ntdens)
    type(scrt), optional,target,intent(inout) :: aux_global
    !local
    logical :: rc
    integer :: icell,j,ibegin,iend
    integer :: ntdens
    real(kind=double) :: min,max, pflux, pmass,decay,pode
    real(kind=double) :: ptrans, gf_pflux, gf_pmass,power
    real(kind=double) :: penalty_factor
    type(scrt), target :: aux_local
    type(scrt), pointer  :: aux
    real(kind=double), pointer :: trans_prime(:), gfvar_penalty(:), gfvar_weight(:)
    
    ntdens = this%ntdens

    !
    ! set work space using raux vector in scrt type
    !
    if (present(aux_global)) then
       ! check if there is enough space
       if( .not. aux_global%check(0,3*ntdens) ) &
            rc = IOerr(0, wrn_inp, 'assembly_grad_lyap_gfvar',&
            ' aux array too small')
       aux=>aux_global   
    else
       ! allocate local space
       call aux_local%init(0,0,3*ntdens)
       aux=>aux_local
    end if
    ! set array bounds with range procedure of scrt type
    iend=0
    call aux%range(ntdens,ibegin,iend)
    trans_prime =>aux%raux(ibegin:iend)
    call aux%range(ntdens,ibegin,iend)
    gfvar_penalty =>aux%raux(ibegin:iend)
    call aux%range(ntdens,ibegin,iend)
    gfvar_weight =>aux%raux(ibegin:iend)

    
    
    
    
    pflux = ode_inputs%pflux
    pode  = ode_inputs%pode
    decay = ode_inputs%decay
    pmass = ode_inputs%pmass

    if( abs(pode-two) > 1e-12) then
       write(*,*) 'only pode=2.0, in assembly_grad_lyap_gfvar'
       stop
    end if
    !
    ! lyapunov is 
    !
    ! 1/2 (a(x) + b(x)*tdens |\Grad pot|^2 + 1/2 decay kappa^2 tdens^wmass/wmass
    !
    ! tdens=trans(gfvar)
    !
    ! Grad lyap = -1/2|\Grad pot|^2 trans'+decay kappa^2 wmass tdens^(wmass-1) trans'
    !
    
    !
    ! compute -1/2|\Grad pot|^2 trans'
    !
    call this%build_norm_grad_dyn(pot,two,this%norm_grad_dyn)
    call eval_trans_prime(ntdens,two,pflux,gfvar,this%trans_prime)
    grad_lyap = -onehalf*ode_inputs%beta*this%trans_prime*this%norm_grad_dyn
    
    !
    ! +decay kappa^2 1/wmass tdens^(wmass-1) trans'
    !    
    power=tdpot%wmass_exponent(two,pflux,pmass)

    grad_lyap = grad_lyap + &
         onehalf * decay * &
         (ode_inputs%kappa**2) * (tdens ** (power - one ))  * this%trans_prime

    !
    ! penalization 
    !
    penalty_factor = ode_inputs%penalty_factor
    if ( abs( penalty_factor) .gt. small) then
       call tdens2gfvar(ntdens,pode,pflux,ode_inputs%penalty_weight,  gfvar_weight)       
       call tdens2gfvar(ntdens,pode,pflux,ode_inputs%penalty_function,gfvar_penalty)
       !write(*,*) minval(gfvar_penalty),' <=gfvar penalty <=', maxval(gfvar_penalty) 
       grad_lyap = grad_lyap + &
            penalty_factor * gfvar_weight * &
            ( gfvar - gfvar_penalty ) 
    end if

    ! multiply by size_cell
    grad_lyap=grad_lyap * this%grid_tdens%size_cell


    ! free memory (if required)
    aux=>null()
    if ( aux_local%is_initialized) call aux_local%kill(0)
    

  end subroutine assembly_grad_lyap_gfvar

  !>------------------------------------------------------------
  !> Assembly the rhs of the tdens ode of gfvar ode
  !<-------------------------------------------------------------
  subroutine get_increment_ode(this, rhs_ode,inc_ode)
    implicit none
    class(dmkp1p0), intent(in   ) :: this
    real(kind=double),                intent(in   ) :: rhs_ode(this%ntdens)
    real(kind=double),                intent(inout) :: inc_ode(this%ntdens)

    inc_ode = rhs_ode/this%grid_tdens%size_cell

  end subroutine get_increment_ode
  


  

 subroutine set_threshold(tdpot,p1p0,ctrl)
    implicit none
    type(tdpotsys), intent(in  ) :: tdpot
    type(dmkp1p0), intent(inout) :: p1p0
    type(DmkCtrl),  intent(inout) :: ctrl
    
    ! threshold is the minimum between the 
    ! number of iterations 
    ! required just after preconditioning and
    ! reference thereshold
    
    select case (ctrl%id_buffer_prec)
    case(1)
       ! cosntant threshold given in controls file
       ctrl%threshold_prec  = ctrl%ref_iter
    case (2)
       !
       ! threshold is average of the linear solver iterations
       ! 
       ctrl%threshold_prec  = &
            int( tdpot%total_iterations_linear_system  / &
            tdpot%total_number_linear_system ) 
    case (3)
       ! threshold is the number of iterations 
       ! required using the optimal preconditioner
       ctrl%threshold_prec =  tdpot%iter_last_prec
    case (4)
       ! threshold is the minimum between the 
       ! number of iterations 
       ! required just after preconditioning and
       ! reference thereshold
       ctrl%threshold_prec =  min(ctrl%ref_iter, tdpot%iter_last_prec)
    case (5)
       ! iteration for solution of first elliptic 
       ctrl%threshold_prec = tdpot%iter_first_system
    end select

    if ( ctrl%id_buffer_prec .ne. 0) then
       if ( p1p0%info_solver%iter > & 
            int(ctrl%prec_growth  * ctrl%threshold_prec ) ) then
          ctrl%build_prec=1
       else
          ctrl%build_prec   = 0

       end if
    else
       ctrl%build_prec=1
    end if
  end subroutine set_threshold

  !>--------------------------------------------------------
  !> Procedure to assembly sparse matrix
  !> 
  !> $\Matr[\Itd, IPot]{BC} = 
  !>   \int_{\Domain} \testx_{\Itd} |\Grad \Pot|^{\Pgrad-2}< \Grad \Pot, \Grad \testp_{\Ipot}>$ 
  !<--------------------------------------------------------
  subroutine assembly_BC_matrix(p1p0,pot, pgrad ,BC_matrix)
      use Globals
      use SparseMatrix
      implicit none
      
      class(dmkp1p0), intent(inout) :: p1p0
      real(kind=double),                intent(in   ) :: pot(p1p0%npot)
      real(kind=double),                intent(in   ) :: pgrad
      type(spmat),                      intent(inout) :: BC_matrix
      !local
      integer :: icell_sub,icell_parent,iloc,ind
      integer :: nnodeincell,ndim
      real(kind=double) :: pot_cell(4),grad_cell(3),grad_base(3), area_subgrid
      real(kind=double) :: weight_grad_norm      
      real(kind=double) :: ddot,dnrm2

      nnodeincell = p1p0%p1%grid%nnodeincell
      ndim        = p1p0%p1%grid%logical_dimension
      
      ! evaluation of gradx, grady ,gradz
      call p1p0%p1%eval_grad(pot,p1p0%grad)

      BC_matrix%coeff  = zero
      
      if ( p1p0%id_subgrid .eq. 1 ) then
         do icell_sub=1,p1p0%p1%grid%ncell
            ! copy area of triangle in subgrid            
            area_subgrid = p1p0%p1%grid%size_cell(icell_sub)
            weight_grad_norm = dnrm2(ndim, p1p0%grad(1:ndim,icell_sub),1)**(pgrad-2.0d0) 

            ! get index of triangle of coaser grid     
            do iloc = 1,nnodeincell
               ! get gradient of local base function
               call p1p0%p1%get_gradbase(iloc,icell_sub, grad_base)

               ! add contribution
               ind=p1p0%assembler_Bmatrix_subgrid(iloc,icell_sub)
               BC_matrix%coeff(ind) = BC_matrix%coeff(ind) + &
                    ddot(ndim, p1p0%grad(1:ndim,icell_sub),1, grad_base(1:ndim), 1 ) * &
                    weight_grad_norm * area_subgrid
            end do
         end do
      else
         do icell_sub=1,p1p0%p1%grid%ncell
            ! copy area of triangle in subgrid            
            area_subgrid = p1p0%p1%grid%size_cell(icell_sub)
            weight_grad_norm = dnrm2(ndim, p1p0%grad(1:ndim,icell_sub),1)**(pgrad-2.0d0) 

            ! get index of triangle of coaser grid     
            do iloc = 1,nnodeincell
               ! get gradient of local base function
               call p1p0%p1%get_gradbase(iloc,icell_sub, grad_base)

               ! add contribution
               ind=p1p0%assembler_Bmatrix_grid(iloc,icell_sub)
               BC_matrix%coeff(ind) = BC_matrix%coeff(ind) + &
                    ddot(ndim, p1p0%grad(1:ndim,icell_sub),1, grad_base(1:ndim), 1 ) * &
                    weight_grad_norm * area_subgrid
            end do
         end do
      end if
         
      BC_matrix%is_symmetric = ( abs(pgrad-2.0d0)<small )

    end subroutine assembly_BC_matrix


    subroutine set_active_tdpot(p1p0,tdpot,ctrl)
      use Globals
      implicit none
      class(dmkp1p0), intent(inout) :: p1p0
      type(tdpotsys), intent(inout) :: tdpot
      type(DmkCtrl),  intent(in   ) :: ctrl
      ! local 
      integer :: icell,inode,iloc,ifather
      logical :: on

      select case (ctrl%selection )
         case (1)
            
            tdpot%ntdens_on    = 0
            tdpot%ntdens_off   = 0
            tdpot%active_tdens = 0
            tdpot%onoff_tdens  = .False.
            do icell = 1,p1p0%grid_tdens%ncell 
               on = ( tdpot%tdens(icell) .ge. ctrl%threshold_tdens)
               tdpot%onoff_tdens(icell) = on
               if ( on ) then
                  tdpot%ntdens_on = tdpot%ntdens_on + 1
                  tdpot%active_tdens(tdpot%ntdens_on) = icell
               else
                  tdpot%ntdens_off = tdpot%ntdens_off + 1 
                  tdpot%inactive_tdens(tdpot%ntdens_off) = icell
                  !tdpot%tdens(tdpot%ntdens_off) = ctrl%threshold_tdens
               end if
            end do

            !
            ! in order to switch off a node all tdens 
            ! surrunding it must be inactive
            !      
            tdpot%onoff_pot(:)  = .False.
            do icell=1,p1p0%grid_pot%ncell
               ifather = p1p0%grid_pot%cell_parent(icell) 
               do iloc = 1, p1p0%grid_pot%nnodeincell
                  inode =  p1p0%grid_pot%topol(iloc,icell)
                  tdpot%onoff_pot(inode) = &
                       tdpot%onoff_pot(inode) .or.  tdpot%onoff_tdens(ifather) 
               end do
            end do

            tdpot%npot_on  =0
            tdpot%npot_off = 0
            do inode = 1,p1p0%grid_pot%nnode
               if ( tdpot%onoff_pot(inode) ) then
                  tdpot%npot_on = tdpot%npot_on + 1
                  tdpot%active_pot(tdpot%npot_on) = inode
               else
                  tdpot%npot_off = tdpot%npot_off + 1 
                  tdpot%inactive_pot(tdpot%npot_off) = inode
               end if
            end do
         end select
            
    end subroutine set_active_tdpot


 
      
    subroutine spkernel_init(this,lun_err,ncol)
       use Globals
       implicit none
       class(spkernel), intent(inout) :: this
       integer,         intent(in   ) :: lun_err
       integer,         intent(in   ) :: ncol
       ! local
       logical :: rc
       integer :: res
       
       this%nrow = ncol
       this%ncol = ncol
       this%is_symmetric =.True.
       allocate(this%indeces(ncol),this%base_kernel(ncol),stat=res)
       if (res.ne.0) rc=IOerr(lun_err,err_dealloc, 'spkernel kill', &
                     ' type spkernel member indeces ')
       this%nkernel=0
       
     end subroutine spkernel_init

     subroutine spkernel_kill(this,lun_err)
       use Globals
       implicit none
       class(spkernel), intent(inout) :: this
       integer,         intent(in ) :: lun_err
       ! local
       logical :: rc
       integer :: res

       
       this%nrow = 0
       this%nkernel=0
       this%ncol = 0
       this%is_symmetric =.false.
       deallocate(this%indeces,this%base_kernel,stat=res)
       if (res.ne.0) rc=IOerr(lun_err,err_dealloc, 'spkernel kill', &
                     ' type spkernel member indeces ')
       
     end subroutine spkernel_kill

     subroutine spkernel_set(this,nkernel, indeces)
       use Globals
       implicit none
       class(spkernel), intent(inout) :: this
       integer,  intent(in ) :: nkernel
       integer,  intent(in ) :: indeces(nkernel)
       ! local
       integer :: i
       
       this%indeces(1:nkernel) = indeces(1:nkernel)
       this%nkernel = nkernel
       this%base_kernel = one
       do i=1,nkernel
          this%base_kernel(i) = zero
       end do
       this%base_kernel = this%base_kernel / sqrt(one*(this%ncol-nkernel))
       
     end subroutine spkernel_set

       
    subroutine spkernel_Mxv(this,vec_in,vec_out,info,lun_err)
       implicit none
       class(spkernel), intent(inout) :: this
       real(kind=double), intent(in   ) :: vec_in(this%ncol)
       real(kind=double), intent(inout) :: vec_out(this%nrow)
       integer,           intent(inout) :: info
       integer,           intent(in   ) :: lun_err
       ! 
       integer :: i,j
       real(kind=double) :: rort,ddot

       info=0
       vec_out=zero
       do i=1,this%nkernel
          j=this%indeces(i)
          vec_out(j)=vec_in(j)
       end do
       rort=ddot(this%ncol,this%base_kernel,1, vec_in,1)
       vec_out=vec_out + rort*this%base_kernel

     end subroutine spkernel_Mxv
            
     !>----------------------------------------------------
     !> Evalute functionals (like energy, lyapunov, etc.)
     !<---------------------------------------------------    
     subroutine compute_functionals(this,&
          tdpot,&
          ode_inputs,&
          ctrl)
       use DmkInputsData 
       use TdensPotentialSystem
       implicit none
       class(dmkp1p0), intent(inout) :: this
       type(tdpotsys),                   intent(inout) :: tdpot
       type(DmkInputs),                  intent(in   ) :: ode_inputs
       type(DmkCtrl),                    intent(in   ) :: ctrl

       !
       ! computed gradient and simliar quantities
       !
       call this%build_grad_vars(tdpot,0)

       !tdpot%system_variation     = this%compute_tdpot_variation(tdpot,ctrl)
       tdpot%energy               = this%energy(tdpot%tdens,this%norm_grad)
       tdpot%weighted_mass_tdens  = this%weighted_mass(tdpot%tdens,ode_inputs)
       tdpot%lyapunov             = tdpot%energy + tdpot%weighted_mass_tdens
       tdpot%min_tdens            = minval(tdpot%tdens)
       tdpot%max_tdens            = maxval(tdpot%tdens)
       tdpot%wasserstein_distance = this%grid_tdens%normp_cell(1.0d0,tdpot%tdens)


     end subroutine compute_functionals


     !>-------------------------------------------------
     !> Precedure to store in timefun quantities
     !> (errors, number of iterations,etc) along
     !> time evolution.
     !> 
     !> ( public procedure for type dmkp1p0)
     !> 
     !> usage: call 
     !>
     !<-------------------------------------------------
     subroutine store_evolution(this,timefun,itemp,tdpot,ode_inputs,ctrl)
       use TdensPotentialSystem
       use DmkControls
       use TdensPotentialSystem
       use DmkInputsData
       use TimeFunctionals

       implicit none
       class(dmkp1p0), target, intent(in   ) :: this
       type(evolfun),  intent(inout) :: timefun
       integer,        intent(in   ) :: itemp
       type(tdpotsys), intent(in   ) :: tdpot
       type(DmkInputs),intent(in   ) :: ode_inputs
       type(DmkCtrl),  intent(in   ) :: ctrl

       !local
       real(kind=double) :: power,w1dist

       !>----------------------------------------------------------------
       !> Integer Valued Fucntional
       !>----------------------------------------------------------------
       timefun%last_time_iteration = itemp


       timefun%iter_solver(1:tdpot%iter_nonlinear,itemp) = &
            this%sequence_info_solver(1:tdpot%iter_nonlinear)%iter
       timefun%iter_nonlinear(itemp) = tdpot%iter_nonlinear
       timefun%iter_total(itemp)=sum(timefun%iter_solver(1:tdpot%iter_nonlinear,itemp))
       timefun%iter_media(itemp)=int(timefun%iter_total(itemp)*one/tdpot%iter_nonlinear)

       !----------------------------------------------------------------
       ! Real Valued Fucntional
       !---------------------------------------------------------------
       if(itemp>0) timefun%var_tdens(itemp) = tdpot%system_variation
       timefun%time(itemp) = tdpot%time
       if(itemp>0) timefun%deltat(itemp) = tdpot%deltat

       if (itemp==0) then
          timefun%cpu_time(itemp)=tdpot%cpu_time
       else
          timefun%cpu_time(itemp)=timefun%cpu_time(itemp-1)+tdpot%cpu_time
       end if
       timefun%CPU_iterations(itemp)=tdpot%cpu_time
       timefun%CPU_iterations_wasted(itemp)=tdpot%cpu_wasted

       timefun%mass_tdens(itemp)= tdpot%mass_tdens
       timefun%weighted_mass_tdens(itemp)= tdpot%weighted_mass_tdens
       timefun%energy(itemp) = tdpot%energy
       timefun%lyapunov(itemp)  =  tdpot%lyapunov
       timefun%min_tdens(itemp)= tdpot%min_tdens
       timefun%max_tdens(itemp)= tdpot%max_tdens
       timefun%max_nrm_grad(itemp)     = tdpot%max_nrm_grad
       timefun%max_nrm_grad_avg(itemp) = tdpot%max_nrm_grad_avg 


       timefun%res_elliptic(itemp) = tdpot%res_elliptic
       timefun%deltat(itemp) = ctrl%deltat 

     end subroutine store_evolution



     !>----------------------------------------------------------------
     !> Function to eval.
     !> $ var(\TdensH^k):=\frac{
     !>                     \| \tdens^{k+1} - \tdens^{k} \|_{L2} 
     !>                   }{ 
     !>                     \| \tdens^{k+1} \|_{L2}}  
     !>                   } $
     !> (procedure public for type specp0) 
     !> 
     !> usage: call var%err_tdesn()
     !>    
     !> where:
     !> \param  [in ] var       -> type(specp0) 
     !> \result [out] var_tdens -> real. Weighted var. of tdens
     !<----------------------------------------------------------------
     function compute_tdpot_variation(this,tdpot,ctrl) result(var)
       use Globals
       use TdensPotentialSystem
       implicit none
       class(dmkp1p0),    intent(in   ) :: this
       class(tdpotsys),    intent(in   ) :: tdpot
       class(DmkCtrl),     intent(in   ) :: ctrl
       real(kind=double) :: var
       !local
       real(kind=double) :: norm_old, norm_var,exponent

       ! $ var_tdens = 
       !              frac{
       !                   \|\tdens^{n+1}-\tdens^n\|_{L^2}
       !                  }{
       !                   \deltat \|\tdens^{n}\|_{L^2}
       !                  }$
       exponent = ctrl%norm_tdens
       norm_old = this%grid_tdens%normp_cell(exponent,tdpot%tdens_old)
       norm_var = this%grid_tdens%normp_cell(exponent,tdpot%tdens-tdpot%tdens_old)

       var =  norm_var / ( ctrl%deltat * norm_old )
     end function compute_tdpot_variation




     !>----------------------------------------------------
     !>
     !<---------------------------------------------------    
     subroutine reset_controls_after_update_failure(this,&
          info,&
          ode_inputs,&
          tdpot,&
          original_ctrl,&
          ctrl,&
          input_time,&
          ask_for_inputs)
       use DmkInputsData 
       use TdensPotentialSystem
       implicit none
       class(dmkp1p0), intent(inout) :: this
       integer,           intent(in   ) :: info
       type(DmkInputs),   intent(in   ) :: ode_inputs
       type(tdpotsys),    intent(in   ) :: tdpot
       type(DmkCtrl),     intent(in   ) :: original_ctrl
       type(DmkCtrl),     intent(inout) :: ctrl
       real(kind=double), intent(inout) :: input_time
       logical,           intent(inout) :: ask_for_inputs

       ! local
       logical :: rc
       integer :: lun_err
       integer :: digit1,digit2,digit3

       lun_err=ctrl%lun_err

       digit1=info/100 
       digit2=mod(info,100)/10
       digit3=mod(info,10)

       select case ( ctrl%time_discretization_scheme )
       case(1)
          !
          ! Explicit Euler
          !
          ctrl%deltat =  ctrl%deltat/ctrl%deltat_expansion_rate
          input_time = tdpot%time
          ask_for_inputs = .False.
       case (2)
          !
          ! NewImplicit Euler 
          !
          ctrl%deltat=ctrl%deltat/ctrl%deltat_expansion_rate
          input_time = tdpot%time + ctrl%deltat
          ask_for_inputs = .True.

       case (4)
          !
          ! NewImplicit Euler Newton Gfvar
          !
          ctrl%deltat=ctrl%deltat/ctrl%deltat_expansion_rate
          input_time = tdpot%time + ctrl%deltat
          ask_for_inputs = .True. 
       end select


     end subroutine reset_controls_after_update_failure

     !>----------------------------------------------------
     !>
     !<---------------------------------------------------    
     subroutine  set_controls_next_update(this,&   
          ode_inputs,&
          tdpot,&
          ctrl,input_time)
       use DmkInputsData 
       use TdensPotentialSystem
       implicit none
       class(dmkp1p0),     intent(inout) :: this
       type(DmkInputs),   intent(in   ) :: ode_inputs
       type(tdpotsys),    intent(in   ) :: tdpot
       type(DmkCtrl),     intent(inout) :: ctrl
       real(kind=double), intent(inout) :: input_time
       !
       !    local
       real(kind=double) :: deltat,D22max,J22min,gradmax
       real(kind=double),allocatable :: J22(:) 

       !
       ! time step controls
       !
       select case ( ctrl%deltat_control ) 
       case( 1 )
          ! constant time step
          deltat=ctrl%deltat        
       case( 2 )
          ! increasing time step
          deltat=ctrl%deltat * ctrl%deltat_expansion_rate
          deltat=min(deltat,ctrl%deltat_upper_bound)
          deltat=max(deltat,ctrl%deltat_lower_bound)
       case (3 )
          select case (ctrl%time_discretization_scheme)
          case (2)
             call assembly_D2(this,ode_inputs,tdpot%tdens, tdpot%pot,this%scr_ntdens)
             D22max=maxval(this%scr_ntdens)
          case (7)
             allocate(J22(this%ntdens))
             !
             ! make J22 strictly positive
             ! J22=grid_tdens%size_cell*(M22)
             ! 1/deltat * size_cell + sizecell*M22> ctrl%epsilon_w22
             ! deltat=1/(ctrl%epsilon_w22-min(W22))
             !
             call assembly_J22_gfvar(this,ode_inputs,tdpot,tdpot%gfvar,tdpot%tdens, tdpot%pot,J22,this%aux_bicgstab )
             J22min=minval(J22)!/this%grid_tdens%size_cell)
             if ( J22min > zero ) then
                !
                ! in this case 1/deltat + 
                !
                call this%assembly_grad_lyap_gfvar(&
                     ode_inputs,&
                     tdpot,&
                     tdpot%gfvar,tdpot%tdens,tdpot%pot,&
                     J22,this%aux_bicgstab)
                gradmax=maxval(abs(J22/this%grid_tdens%size_cell))
                deltat=min(abs(one/(ctrl%epsilon_W22-J22min)),one/gradmax)
             else
                deltat=minval(this%grid_tdens%size_cell)/(ctrl%epsilon_W22-J22min)
             end if
             call assembly_J22_gfvar(this,ode_inputs,tdpot,tdpot%gfvar,tdpot%tdens, tdpot%pot,J22,this%aux_bicgstab )
             J22=one/deltat*this%grid_tdens%size_cell+J22
             deallocate(J22)

          case (3)
             !
             ! explicit euler gfvar
             !
             call this%assembly_grad_lyap_gfvar(&
                  ode_inputs,&
                  tdpot,&
                  tdpot%gfvar,tdpot%tdens,tdpot%pot,&
                  this%rhs_ode,this%aux_bicgstab)
             deltat=one/maxval(abs(this%rhs_ode/this%grid_tdens%size_cell))
             deltat=min(deltat,ctrl%deltat_upper_bound)
             deltat=max(deltat,ctrl%deltat_lower_bound)


          case (1)
             !
             ! explicit euler gfvar
             !
             call this%assembly_rhs_ode_tdens(&
                  ode_inputs,&
                  tdpot%tdens,tdpot%pot,&
                  this%rhs_ode)
             deltat=one/maxval(abs(this%rhs_ode/this%grid_tdens%size_cell))
             deltat=min(deltat,ctrl%deltat_upper_bound)
             deltat=max(deltat,ctrl%deltat_lower_bound)
          end select
       end select


       ctrl%deltat=deltat

       select case ( ctrl%time_discretization_scheme )
       case (1)
          input_time=tdpot%time
       case (2)
          input_time=tdpot%time+ctrl%deltat
       case (3)
          input_time=tdpot%time
       case (4)
          input_time=tdpot%time+ctrl%deltat
       end select

     end subroutine set_controls_next_update



     subroutine set_controls_update (&
          itemp,&
          deltat,&
          tdpot, &
          p1p0,&
          ode_inputs,&
          timefun,&
          ctrl_original,&
          ctrl)
       use Globals 
       use TdensPotentialSystem
       use TimeFunctionals
       implicit none
       integer ,            intent(in   ) :: itemp
       real(kind=double),   intent(inout) :: deltat
       type(tdpotsys),      intent(inout) :: tdpot
       type(dmkp1p0), intent(inout)  :: p1p0
       type(DmkInputs),       intent(in   ) :: ode_inputs
       type(evolfun),       intent(in   ) :: timefun
       type(DmkCtrl),       intent(in   ) :: ctrl_original
       type(DmkCtrl),       intent(inout) :: ctrl
       !local 
       integer :: i,j
       integer :: threshold,avg_iter,lun_out=6
       real(kind=double) :: sup_rhs
       character(len=256) :: out_format

       !
       ! time step controls
       !
       select case ( ctrl%deltat_control ) 
       case( 1 )
          !
          ! cosntant time step
          !
          deltat=ctrl%deltat 
          out_format=ctrl%formatting('ar')

       case( 2 )
          !
          ! increasing time step
          !
          ctrl%deltat=min(&
               ctrl%deltat * ctrl%deltat_expansion_rate,ctrl%deltat_upper_bound)
          out_format=ctrl%formatting('ar')
          !write(lun_out,out_format) 'time step = ',deltat

       case (3 )
          !
          ! time step one / rhs_ode
          !
          select case ( ctrl%time_discretization_scheme) 
          case (1)
             call p1p0%assembly_rhs_ode_tdens(&
                  ode_inputs,&
                  tdpot%tdens,tdpot%pot,&
                  tdpot%scr_ntdens)
             sup_rhs= maxval(tdpot%scr_ntdens/p1p0%grid_tdens%size_cell)
             if ( sup_rhs < zero ) then
                ctrl%deltat = min(ctrl%deltat * &
                     ctrl%deltat_expansion_rate,ctrl%deltat_upper_bound)
             else
                ctrl%deltat=(1.0d0-ctrl%deltat_lower_bound)/sup_rhs
             end if
             ctrl%deltat=min(ctrl%deltat,ctrl%deltat_upper_bound)

             out_format=ctrl%formatting('araar')
             write(lun_out,out_format) 'time step = ',deltat,&
                  ' | ', ' linfty norm increment = ', sup_rhs
          case (2)
             tdpot%scr_ntdens =  p1p0%nrm_grad_dyn - one
             sup_rhs= maxval(tdpot%scr_ntdens)
             if ( sup_rhs < zero ) then
                ctrl%deltat = min(ctrl%deltat * &
                     ctrl%deltat_expansion_rate,ctrl%deltat_upper_bound)
             else
                ctrl%deltat=(1.0d0-ctrl%deltat_lower_bound)/sup_rhs
             end if
             ctrl%deltat=min(ctrl%deltat,ctrl%deltat_upper_bound)

             out_format=ctrl%formatting('araar')
             write(lun_out,out_format) 'time step = ',ctrl%deltat,&
                  ' | ', ' linfty norm increment = ', sup_rhs
          end select

       end select

     end subroutine set_controls_update


     !
     ! define how to build precnditiner
     ! build_prec  = 0/1
     ! build_tunig = 0/1
     !
     !call tdpot%set_threshold(ctrl)

     !
     ! adaptive setting of linear solver tolerance 
     !
     ! ctrl%tol_nonlinear=min(5d-3,max(var_tdens(itemp),tol_zero))
     ! ctrl%ctrl_solver%tol_sol=max(min(1.0d-4,1.0d-2*var_tdens(itemp)),small)





     subroutine assembly_D2(p1p0,ode_inputs,tdens, pot,D22)
       use DmkInputsData
       implicit none 
       type(dmkp1p0), intent(inout) :: p1p0
       type(DmkInputs),                 intent(in   ) :: ode_inputs
       real(kind=double),               intent(in   ) :: tdens(p1p0%ntdens)
       real(kind=double),               intent(in   ) :: pot(p1p0%npot)
       real(kind=double),               intent(inout) :: D22(p1p0%ntdens)


       !local
       integer :: icell
       real(kind=double) :: min,max
       real(kind=double) ::  pflux, pmass, decay, pode,penalty_factor

       pflux = ode_inputs%pflux
       pmass = ode_inputs%pmass
       decay = ode_inputs%decay
       pode  = ode_inputs%pode

       call p1p0%build_norm_grad_dyn(pot,pode,D22)


       do icell = 1,p1p0%ntdens
          !
          ! cycle that can be used to estimate D2 and adapt deltat
          !
          D22(icell) = &
               ode_inputs%beta(icell)*pflux * tdens(icell)**(pflux-one) * &
               D22(icell) &
               - &
               decay*ode_inputs%kappa(icell)**pode * &
               pmass * tdens(icell)**(pmass-one) 
       end do

       penalty_factor = ode_inputs%penalty_factor
       if ( abs( penalty_factor ) .gt. small) then
          do icell = 1,p1p0%ntdens
             D22(icell) = D22(icell) - &
                  penalty_factor * &
                  ode_inputs%penalty_weight(icell) * &
                  (1 - &
                  ode_inputs%penalty_function(icell) * &
                  pflux*tdens(icell)**(pflux-one) ) 
          end do
       end if



       D22=D22*p1p0%grid_tdens%size_cell

     end subroutine assembly_D2



     !>----------------------------------------------------
     !> Procedure for update the system with
     !> Implicit Euler time-stepping in the gfvar varaible.
     !> Non-lineariry is solved via Newton method.
     !>
     !> \gf^{k+1}= \gf^{k} + \deltat * RHS_ODE(\gf^k+1)
     !>
     !> IMPORTANT: all the variables tdens pot 
     !> odein have to be syncronized at time time(itemp)
     !>
     !> usage: call var%implicit_euler_newton_gfvar(&
     !>                                      tdpot,&
     !>                                      ode_inputs,&
     !>                                      ctrl,&
     !>                                      info,&
     !>                                      CPU)
     !> 
     !> where:
     !> \param[inout] tdpot      -> type(tdpotsys):
     !>                              Tdens/Pot pair
     !> \param[in   ] ode_inputs -> type(DmkInputs):
     !>                              Parameters and constants
     !>                              of the problem
     !> \param[in   ] ctrl        -> type(DmkCtrl):
     !>                              Algorithm controls
     !> \param[inout] info        -> integer: Flag to return
     !>                               error in the procedure.
     !>                               If no error occured
     !>                               info=0
     !> \param[inout] CPU         -> type(codeTim):
     !>                               CPU-timing
     !<---------------------------------------------------  
     subroutine implicit_euler_newton_gfvar(p1p0,&
          tdpot, &
          ode_inputs,&
          ctrl, &
          info,&
          CPU )
       use Globals
       use TimeInputs, only : write_steady
       use CombinedSparseMatrix
       use Timing
       use SimpleMatrix
       use Matrix
       use BlockMatrix
       use DmkInputsData
       use LinearSolver
       implicit none
       class(dmkp1p0), target, intent(inout) :: p1p0
       type(tdpotsys),                           intent(inout) :: tdpot
       type(DmkInputs),                          intent(in   ) :: ode_inputs
       type(DmkCtrl),                            intent(in   ) :: ctrl
       integer,                                  intent(inout) :: info
       type(codeTim),                            intent(inout) :: CPU

       ! local
       integer :: lun_err,lun_out,lun_stat
       integer :: flag,info_newton
       integer :: current_newton_step
       real(kind=double),  allocatable :: xvar(:)
       real(kind=double),  allocatable :: increment(:)
       real(kind=double),  allocatable :: FNEWTON(:)
       real(kind=double),  allocatable :: J22(:)
       real(kind=double),  allocatable :: rhs_full(:)
       real(kind=double),  allocatable :: gfvar(:),tdens(:), pot(:),gfvar_old(:)
       real(kind=double),  allocatable :: gfvarnew(:),tdensnew(:), potnew(:)
       real(kind=double),  allocatable :: trans_prime(:), trans_second(:)
       real(kind=double),  allocatable :: total_gfvar_increment(:)
       real(kind=double),  allocatable :: cond(:),cond_sub(:)
       
       real(kind=double) :: tolerance_linear_solver,alpha,pflux,pode
       real(kind=double) :: fnewton_norm, initial_fnewton_norm, previuous_fnewton_norm
       real(kind=double) :: rtemp,rtemp2
       logical :: rc,  test,test2,test_cycle
       integer :: res
       integer :: max_nonlinear_step
       type(input_solver) :: ctrl_solver, ctrl_outer_solver
       integer :: i,j,icell,inode,ntdens,npot,nfull
       character(len=256) :: msg=' ',msg1=' ', msg2=' ',msg0=' '
       character(len=5) :: head_string
       character(len=256) :: str=' ',sep=' ',out_format=' ',sepa=' ', directory,tail,fname
       real(kind=double) :: dnrm2
       type(file) :: fmat
       real(kind=double) :: discr,MaxC,MinC,r
       type(scalmat),  target  :: identity_npot
       type(block_linop),target :: matrixM
       type(diagmat), target :: invC
       type(new_linop) :: ApBTinvCB
       type(spmat),target   :: formed_ApBTinvCB
       type(inv),target :: inv_ApBTinvCB
       type(input_solver) :: ctrl_inner_solver
       type(input_prec) :: ctrl_prec
       type(saddleprec),target :: jacobian_prec
       type(scalmat),target :: identity_npot_on
       type(block_linop),target :: jacobian_matrix 
       type(newton_input) :: newton_ctrl
       type(newton_output) :: newton_info


       type(new_linop) :: relaxed_A
       type(spmat),target   :: formed_relaxed_A
       type(input_solver) :: ctrl_inv_A
       type(inv) :: inv_A

       type(spmat) :: temp

       type(diagmat), target :: inv_M
       type(new_linop) :: B_inv_M_BT       
       type(spmat),target :: formed_B_inv_M_BT
       type(input_solver) :: ctrl_inv_B_inv_M_BT
       type(inv) :: inv_B_inv_M_BT
       type(new_linop) :: inv_Schur

       type(input_solver) :: ctrl_iterative
       

       ntdens=p1p0%ntdens
       npot=p1p0%npot
       nfull=ntdens+npot

       lun_out=ctrl%lun_out
       lun_err=ctrl%lun_err
       lun_stat=ctrl%lun_statistics

       allocate( &
            xvar(nfull), &
            FNEWTON(nfull),&
            increment(nfull),&
            total_gfvar_increment(ntdens),&
            rhs_full(nfull),&
            J22(ntdens),&
            gfvar_old(ntdens),&
            gfvar(ntdens),&
            tdens(ntdens),&
            pot(npot),&
            tdensnew(ntdens),&
            potnew(npot),&
            trans_prime(ntdens),&
            trans_second(ntdens),&
            cond(3*ntdens),&
            cond_sub(3*p1p0%grid_pot%ncell),&
            stat=res)
       if (res .ne. 0) &
            rc = IOerr(lun_err, err_alloc , 'implicit_euler_gfvar', &
            ' work arrays',res)

       call invC%init(lun_err,ntdens)
       call identity_npot_on%eye(npot)

       call inv_M%init(lun_err,npot)
       inv_M%diagonal = 1.0 / p1p0%p1_lumped_mass_matrix%diagonal

       call identity_npot%eye(npot)

       !
       ! set initial data 
       !
       call tdens2gfvar(tdpot%ntdens,&
            ode_inputs%pode,ode_inputs%pflux,tdpot%tdens,gfvar)
       gfvar_old=gfvar

       xvar(1:npot)       = tdpot%pot
       xvar(npot+1:nfull) = gfvar
       tdens              = tdpot%tdens
       
       !
       ! set controls
       !
       newton_ctrl%max_nonlinear_iterations = ctrl%max_nonlinear_iterations
       newton_ctrl%tolerance_nonlinear      = ctrl%tolerance_nonlinear
       newton_ctrl%max_tolerance_linear     = ctrl%outer_tolerance
       flag=1
       info_newton=0
       total_gfvar_increment=0

       ! run newton cycle
       do while ( ( flag .gt. 0 ) .and. (info_newton .eq. 0) )
          select case ( flag)
          case (1)
             ! print head string
             out_format='(I2,a3)'
             write(head_string,out_format) newton_info%current_newton_step,' | '
             
             if (ctrl%info_update.ge. 4) call ode_inputs%info(lun_out)
             if (lun_stat>0) call ode_inputs%info(lun_stat)

             !
             ! assembly Fnewton
             ! 
             pot   = xvar(1:npot)
             gfvar = xvar(npot+1:nfull)
             MinC=minval(gfvar(tdpot%active_tdens(1:tdpot%ntdens_on)))
             MaxC=maxval(gfvar(tdpot%active_tdens(1:tdpot%ntdens_on)))
             p1p0%scr_ntdens=tdens
             call gfvar2tdens(ntdens,ode_inputs%pode,ode_inputs%pflux, gfvar ,tdens)
             

             out_format=ctrl%formatting('ararrarrar')
             write(msg,out_format) &
                  head_string,&
                  minval(tdens(tdpot%active_tdens(1:tdpot%ntdens_on))),&
                  '<=TDENS<=', &
                  maxval(tdens(tdpot%active_tdens(1:tdpot%ntdens_on))),&
                  minval(gfvar(tdpot%active_tdens(1:tdpot%ntdens_on))),&
                  '<=GFVAR<=', &
                  maxval(gfvar(tdpot%active_tdens(1:tdpot%ntdens_on))),&
                  minval(ode_inputs%lambda_lift),&
                  '<=LIFT<=', &
                  maxval(ode_inputs%lambda_lift)
                 
                  
             call ctrl%print_string('update',2,msg)
             do i=1,ntdens
                if (isnan(tdens(i))) write(*,*) 'Nan at', i
             end do
             
             !
             ! FNEWTON POT= stiff*pot - rhs = - \Grad_{pot} Lyap
             !
             p1p0%scr_ntdens=ode_inputs%beta*tdens+ode_inputs%lambda_lift
             out_format=ctrl%formatting('arar')
             write(msg,out_format) &
                  head_string,&
                  minval(p1p0%scr_ntdens),&
                  '<=TDENS_STIFF<=', &
                  maxval(p1p0%scr_ntdens)
             call p1p0%assembly_stiffness_matrix(lun_err,&
                  p1p0%scr_ntdens,p1p0%stiff)
             call p1p0%stiff%Mxv(pot, FNEWTON(1:npot) )
             FNEWTON(1:npot) = FNEWTON(1:npot) - ode_inputs%rhs

             !
             ! FNEWTON GFVAR = - d_{\gfvar} (\distance/ 2*deltat + \Lyap)
             !               = - 1/dt * W * (gfvar-  gfvar_old) - d_{\gfvar} Lyap
             !
             ! compute d_{\gfvar} Lyap
             call p1p0%assembly_grad_lyap_gfvar(&
                  ode_inputs,&
                  tdpot,&
                  gfvar,tdens,pot,&
                  FNEWTON(npot+1:nfull),p1p0%aux_bicgstab)
             FNEWTON(npot+1:nfull)=-FNEWTON(npot+1:nfull)

             ! add relaxation term= -\grad{Relax}
             FNEWTON(npot+1:nfull) = &
                  FNEWTON(npot+1:nfull) &
                  - one/ctrl%deltat * p1p0%grid_tdens%size_cell * &
                  (gfvar - gfvar_old)
             
             !
             ! freeze data on non variables
             !
             if ( ctrl%selection > 0 ) then
                ! potential
                if ( tdpot%npot_on  < tdpot%npot ) then
                   do i=1,tdpot%npot_off
                      j=tdpot%inactive_pot(i)
                      FNEWTON(j)=zero
                   end do
                end if

                ! tdens
                if ( ( tdpot%ntdens_on<tdpot%ntdens) ) then
                   do i=1,tdpot%ntdens_off
                      j=tdpot%inactive_tdens(i)
                      FNEWTON(npot+j)=zero
                   end do
                end if
             end if

             

          case (2)
             ! print head info
             write(head_string,'(I2,a3)') newton_info%current_newton_step,' | '

             !
             ! compute norm of F 
             !
             !newton_ctrl%fnewton_norm=dnrm2(npot+ntdens,FNEWTON,1)

             ! Federico stop criteria
             ! res = | abs(F_gfvar)/ abs(gfvar increment) | _{\infty}             
             do icell = 1, ntdens
                p1p0%scr_ntdens(icell) = abs(FNEWTON(npot+icell))/max(1e-15,abs(total_gfvar_increment(icell)))
             end do
             newton_ctrl%fnewton_norm = p_norm(ntdens,zero,p1p0%scr_ntdens)
             
             

             !dnrm2(ntdens,FNEWTON(npot+1:nfull),1)/&
                  !max(1e-15,dnrm2(ntdens,total_gfvar_increment,1))

             out_format=ctrl%formatting('aarar')
             write(msg,out_format) &
                  head_string,&
                  'New res', newton_ctrl%fnewton_norm,&
                  'norm increment',dnrm2(ntdens,total_gfvar_increment,1)
             
             call ctrl%print_string('update',2,msg)
             
             ! print info
             out_format=ctrl%formatting('aaraaraarar')
             write(msg,out_format) &
                  head_string,&
                  'NEWTON FUNC=',newton_ctrl%fnewton_norm,&
                   ' | ',&
                  'POT =', dnrm2(npot,FNEWTON(1:npot),1),&
                  ' | ',&
                  'GFVAR =', dnrm2(ntdens,FNEWTON(npot+1:nfull),1),&
                  ' tol=',newton_ctrl%tolerance_nonlinear
                 
             call ctrl%print_string('update',2,msg)

             
             

          case (3)
             !
             ! 1 - assembly jacobian J
             ! 2 - solve J s = -F 
             !

             ! 1 - assembly jacobian J
             ! J=( A          \Trans' B^T               )
             !   ( \trans' B  - (relax + D^2_{\gfvar} J )
             if (ctrl%debug .ge. 2) then
                call ctrl%print_string('update',4,'GFVAR')
             end if
             pot   = xvar(1:npot)
             gfvar = xvar(npot+1:nfull)
             call gfvar2tdens(ntdens,ode_inputs%pode,ode_inputs%pflux, gfvar ,tdens)

             !
             ! assembly J
             !
             if (ctrl%debug .ge. 2) then
                call ctrl%print_string('update',4,&
                     'ASSEMBLY JACOBIAN')
             end if
             
             ! assembly J(1,1) = d FNEWTON POT  / d pot   = matrix_A
             ! DONE because J(1,1) is p1p0%stiff

             ! assembly J(2,1) = d FNEWTON gfvar  / d pot = Diag(trans') * matrix_B2
             ! assembly B_{\Icell,\Inode} = int_{\Icell} \grad pot \Grad \Base_{Inode}
             call p1p0%assembly_BC_matrix(pot, two ,p1p0%B_matrix)

             ! multiply for trans'transformation_second
             p1p0%B2_matrix%coeff=p1p0%B_matrix%coeff
             call eval_trans_prime(ntdens,two,ode_inputs%pflux,gfvar,trans_prime)
             out_format=ctrl%formatting('arar')
             !MinC=minval(ode_inputs%beta)
             !MaxC=maxval(ode_inputs%beta)
             !write(msg0,out_format)  head_string,MinC,'<=beta<=',MaxC
             !call ctrl%print_string('update',4,msg0)
             
             p1p0%scr_ntdens = trans_prime*ode_inputs%beta
             call p1p0%B2_matrix%DxM(lun_err, p1p0%scr_ntdens) 

             ! assembly J(2,1) via transposer
             p1p0%B1T_matrix=p1p0%B2_matrix
             call p1p0%B1T_matrix%transpose(lun_err)
             !do i=1,p1p0%B_matrix%nterm
             !   p1p0%B1T_matrix%coeff(p1p0%transposer(i)) = p1p0%B2_matrix%coeff(i)
             !end do
             ! matrixC = - d ( FNEWTON(gfvar) )/d_{\gfvar} 
             !         = - d ( - 1/dt * W * (gfvar-  gfvar_old) - d (Lyap) ) / d_{\gfvar} 
             !         = + 1/dt * W  + d^2 Lyap
             !do i=1,ntdens
             !   if (gfvar(i)<zero) write(*,*) 'gfar negative at', i,gfvar(i)
             !   if (tdens(i)<zero) write(*,*) 'tdens Nan at', i,tdens(i)
             !end do
             !write(*,*) 'here'
             !do i=1,ntdens
             !   if (isnan(J22(i))) write(*,*) 'j22 Nan at', i,J22(i)
             !end do
             call assembly_J22_gfvar(p1p0,ode_inputs,tdpot,&
                  gfvar,tdens, pot, J22,p1p0%aux_bicgstab)
             !do i=1,ntdens
             !   if (isnan(J22(i))) write(*,*) 'j22 Nan at', i,J22(i)
             !end do
             p1p0%matrixC%diagonal = J22 + &
                  (one/ctrl%deltat)*p1p0%grid_tdens%size_cell

             ! save components neton system
             if (ctrl%id_save_matrix >0) then
                call save_matrices(p1p0,tdpot,gfvar,tdens, pot)
             end if

             !
             ! print info
             !
             out_format=ctrl%formatting('arar')
             MinC=minval(p1p0%matrixC%diagonal(tdpot%active_tdens(1:tdpot%ntdens_on)))
             MaxC=maxval(p1p0%matrixC%diagonal(tdpot%active_tdens(1:tdpot%ntdens_on)))
             write(msg0,out_format)  head_string,MinC,'<=D<=',MaxC
             call ctrl%print_string('update',4,msg0)

             out_format=ctrl%formatting('arar')
             MinC=minval(J22(tdpot%active_tdens(1:tdpot%ntdens_on)))
             MaxC=maxval(J22(tdpot%active_tdens(1:tdpot%ntdens_on)))
             write(msg1,out_format) head_string,minC,'<=J22<=',MaxC
             call ctrl%print_string('update',4,msg1)

             out_format=ctrl%formatting('arar')
             MinC=minval(gfvar(tdpot%active_tdens(1:tdpot%ntdens_on)))
             MaxC=maxval(gfvar(tdpot%active_tdens(1:tdpot%ntdens_on)))
             write(msg2,out_format) head_string,MinC,'<=gfvar<=',MaxC
             call ctrl%print_string('update',4,msg2)
             
             

             ! 
             ! solve J s = -F 
             !
             call jacobian_matrix%saddle_point(&
                  lun_err,&
                  p1p0%stiff,&
                  p1p0%B1T_matrix,p1p0%B2_matrix,.True.,&
                  p1p0%matrixC)


             ! 
             ! solve J s = -F 
             !

             ! set intial solution
             increment= zero
             rhs_full = -FNEWTON


             ! freeze data on non variables
             ! corresponding FNEWTON are already zero
             if ( ctrl%selection > 0 ) then
                if ( ( tdpot%ntdens_on<tdpot%ntdens) .or. &
                     ( tdpot%npot_on  <tdpot%npot  ) ) then
                   ! reduced block J_11
                   do i=1,tdpot%npot_off
                      inode=tdpot%inactive_pot(i)
                      call p1p0%stiff%set_row(inode,zero)
                      p1p0%stiff%coeff(p1p0%stiff%idiag(inode))=one
                      call p1p0%B1T_matrix%set_row(inode,zero)
                   end do


                   do i=1,tdpot%ntdens_off
                      icell=tdpot%inactive_tdens(i)
                      call p1p0%B2_matrix%set_row(icell,zero)
                      p1p0%matrixC%diagonal(icell)=one
                   end do
                end if
             end if


             

             ! select invertion approach
             select case (ctrl%solve_jacobian_approach)
             case ('reduced')
                !
                ! form Schur Complement SAC = A+B1T(C)^{-1}B2
                !

                ! invert C
                invC%diagonal = one/p1p0%matrixC%diagonal

                !call p1p0%build_norm_grad_dyn(pot,pode,this%scr_ntdens)
                
                !this%scr_ntdens =  tdens + ode_inputs%lambda_lift +&
                !     one/p1p0%matrixC%diagonal * gfvar **2 * this%scr_ntdens 
                ! form implicitely
                ApBTinvCB = p1p0%stiff + &
                     p1p0%B1T_matrix * invC * p1p0%B2_matrix + &
                     (ctrl%relax_direct+ctrl%relax4prec)*identity_npot
                !ctrl%relax_direct*identity_npot_on
                ! set properties             
                ApBTinvCB%is_symmetric = .TRUE.
                ! set this flag to speed assembly
                !ApBTinvCB%pair_list(1)%pattern = 'AWAT+rho*I'
                ! form explicitely 
                call formed_ApBTinvCB%form_new_linop(info,lun_err,ApBTinvCB)

                ! do i=1,formed_ApBTinvCB%nterm
                !    if (isnan(formed_ApBTinvCB%coeff(i))) then
                !       write(*,*) 'S is nan at ', i
                !    end if
                ! end do

                do i=1,ntdens
                   if (isnan(gfvar(i))) then
                      write(*,*) 'C is nan at ', i
                   end if
                   if (isnan(invC%diagonal(i))) then
                      write(*,*) p1p0%matrixC%diagonal(i), 'C is nan at ', i,invC%diagonal(i)
                   end if
                end do
                
                
                !
                ! set controls per outer solver (solver used in preconditioner)
                !
                call ctrl_solver%init(lun_err,&
                     approach=ctrl%outer_solver_approach,&
                     scheme=ctrl%outer_krylov_scheme,&
                     imax=ctrl%outer_imax,&
                     iexit=ctrl%outer_iexit,&
                     isol=ctrl%outer_isol,&
                     nrestart=ctrl%krylov_nrestart,&
                     tol_sol=ctrl%outer_tolerance)

                call ctrl_solver%info2str(msg2)
                out_format=ctrl%formatting('aaa')
                write(msg,out_format) &
                     head_string, &
                     'CTRL : ', etb(msg2)
                call ctrl%print_string('update', 3, etb(msg))
                
                if (ctrl_solver%approach=='ITERATIVE') then
                   !
                   ! set inversion controls from ctrl
                   !
                   call ctrl_prec%init(&
                        lun_err,&
                        ctrl%outer_prec_type,&
                        ctrl%outer_prec_n_fillin,&
                        ctrl%outer_prec_tol_fillin)
                   if (ctrl%info_update .ge. 3)then
                      call ctrl_prec%info(ctrl%lun_out)
                   end if
                end if

                !
                ! define (approximate) inverse 
                !
                call inv_ApBTinvCB%init(&
                     formed_ApBTinvCB,ctrl_solver,ctrl_prec,&
                     lun=lun_err)
                if ( ctrl%debug.ge. 2) call inv_ApBTinvCB%info(6)


                !
                ! initialized precondtioner of saddle point matrix 
                !
                call jacobian_prec%init(&
                     lun_err,&
                     jacobian_matrix,&
                     prec_type='SchurACFull',&
                     invSchurAC=inv_ApBTinvCB, invC=invC)

                !
                ! apply inverse
                !
                call jacobian_prec%Mxv(rhs_full, increment,info,lun_err)

                !
                ! copy info solver
                !
                p1p0%info_solver=inv_ApBTinvCB%info_solver
                info=p1p0%info_solver%ierr

             case ('reduced_iterative')
                !
                ! form Schur Complement SAC = A+B1T(C)^{-1}B2
                !

                ! invert C
                invC%diagonal = one/p1p0%matrixC%diagonal


                if (.true.) then
                   ! build anistropic
                   if (p1p0%grid_pot%logical_dimension .eq. 3) then
                      write(*,*) 'only 2d in'
                      stop
                   end if

                   ! evaluation of gradx, grady ,gradz
                   call p1p0%p1%eval_grad(pot,p1p0%grad)

                   do i = 1, p1p0%grid_pot%logical_dimension
                      ! average gradient
                      p1p0%scr_ngrad(:) = p1p0%grad(i,:)
                      call p1p0%grid_pot%avg_cell_subgrid(&
                           p1p0%grid_tdens,&
                           p1p0%scr_ngrad,p1p0%scr_ntdens)
                      do j=1,ntdens
                         p1p0%grad_avg(i,j) = p1p0%scr_ntdens(j)
                      end do
                   end do

                   ! create vector cond describing anisotropy matrix 
                   do icell = 1, ntdens
                      rtemp = invC%diagonal(icell) * p1p0%grid_tdens%size_cell(icell) * trans_prime (icell) **2 
                      rtemp2 = tdens(icell) + ode_inputs%lambda_lift(icell)
                      cond(3*(icell-1)+1) = rtemp2 + rtemp * p1p0%grad_avg(1,icell)* p1p0%grad_avg(1,icell)
                      cond(3*(icell-1)+2) =          rtemp * p1p0%grad_avg(1,icell)* p1p0%grad_avg(2,icell)
                      cond(3*(icell-1)+3) = rtemp2 + rtemp * p1p0%grad_avg(2,icell)* p1p0%grad_avg(2,icell)
                   end do

                   do i = 1,p1p0%grid_pot%ncell
                      icell = p1p0%grid_pot%cell_parent(i)
                      cond_sub(3*(i-1)+1) = cond(3*(icell-1)+1)
                      cond_sub(3*(i-1)+2) = cond(3*(icell-1)+2)
                      cond_sub(3*(i-1)+3) = cond(3*(icell-1)+3)
                   end do

                   call formed_ApBTinvCB%kill(lun_err)
                   call p1p0%p1%build_stiff(lun_err, 'csr', cond_sub, formed_ApBTinvCB)
                   formed_ApBTinvCB%is_symmetric = .TRUE.

                   do i = 1,formed_ApBTinvCB%nrow
                      j=formed_ApBTinvCB%idiag(i)
                      formed_ApBTinvCB%coeff(j)=formed_ApBTinvCB%coeff(j)+ctrl%relax_direct+ctrl%relax4prec
                   end do
                   
                else
                   ApBTinvCB = p1p0%stiff + &
                        p1p0%B1T_matrix * invC * p1p0%B2_matrix + &
                        (ctrl%relax_direct+ctrl%relax4prec)*identity_npot
                   ApBTinvCB%is_symmetric = .TRUE.
                   call formed_ApBTinvCB%form_new_linop(info,lun_err,ApBTinvCB)
                end if
               
                do i=1,ntdens
                   if (isnan(gfvar(i))) then
                      write(*,*) 'C is nan at ', i
                   end if
                   if (isnan(invC%diagonal(i))) then
                      write(*,*) p1p0%matrixC%diagonal(i), 'C is nan at ', i,invC%diagonal(i)
                   end if
                end do
                
                
                !
                ! apply inverse
                !
                call ctrl_outer_solver%init(lun_err,&
                     approach='ITERATIVE',&
                     scheme='FGMRES',&
                     imax=ctrl%outer_imax,&
                     iprt=ctrl%outer_iprt,&
                     iexit=ctrl%outer_iexit,&
                     isol=ctrl%outer_isol,&
                     nrestart=ctrl%krylov_nrestart,&
                     tol_sol=ctrl%outer_tolerance)

                
                !
                ! set controls per outer solver (solver used in preconditioner)
                !
                call ctrl_inner_solver%init(lun_err,&
                     approach=ctrl%inner_solver_approach,&
                     scheme=ctrl%inner_krylov_scheme,&
                     imax=ctrl%inner_imax,&
                     iprt=ctrl%inner_iprt,&
                     iexit=ctrl%inner_iexit,&
                     isol=ctrl%inner_isol,&
                     nrestart=ctrl%krylov_nrestart,&
                     tol_sol=ctrl%inner_tolerance)
                ctrl_inner_solver%ignore_errors = .True.

                call ctrl_inner_solver%info2str(msg2)
                out_format=ctrl%formatting('aaa')
                write(msg,out_format) &
                     head_string, &
                     'INNER SOLVER CTRL : ', etb(msg2)
                call ctrl%print_string('update', 3, etb(msg))
                
                if (ctrl%inner_solver_approach=='ITERATIVE') then
                   !
                   ! set inversion controls from ctrl
                   !
                   call ctrl_prec%init(&
                        lun_err,&
                        ctrl%inner_prec_type,&
                        ctrl%inner_prec_n_fillin,&
                        ctrl%inner_prec_tol_fillin)
                   if (ctrl%info_update .ge. 3)then
                      call ctrl_prec%info(ctrl%lun_out)
                   end if
                end if
                
                

                !
                ! define (approximate) inverse 
                !
                call inv_ApBTinvCB%init(&
                     formed_ApBTinvCB,ctrl_inner_solver,ctrl_prec,&
                     lun=lun_err)
                if ( ctrl%debug.ge. 2) call inv_ApBTinvCB%info(6)


                !
                ! initialized precondtioner of saddle point matrix 
                !
                call jacobian_prec%init(&
                     lun_err,&
                     jacobian_matrix,&
                     prec_type='SchurACFull',&
                     invSchurAC=inv_ApBTinvCB, invC=invC)

                call ctrl_outer_solver%Info(6)
                call ctrl_inner_solver%Info(6)
                
                call linear_solver(jacobian_matrix,rhs_full,&
                     increment,&
                     p1p0%info_solver,ctrl_outer_solver,&
                     prec_right=jacobian_prec)

                write(*,*) 'total iterions',inv_ApBTinvCB%cumulative_iterations
                write(*,*) 'total applicaiton',inv_ApBTinvCB%cumulative_applications

                
                !
                ! copy info solver
                !
                info=p1p0%info_solver%ierr

             case ('lsc')
                call ctrl%print_string('update', 3, 'LEAST SQUARE COMMUTATOR')
                !
                ! set controls per outer solver (solver used in preconditioner)
                !
                call ctrl_inv_A%init(lun_err,&
                     approach='AGMG',&
                     scheme='PCG',&
                     imax=400,&
                     iexit=ctrl%outer_iexit,&
                     isol=ctrl%outer_isol,&
                     nrestart=ctrl%krylov_nrestart,&
                     tol_sol=1.0d-4)

                write(*,*) 'ctrl invA'

                relaxed_A = p1p0%stiff + (ctrl%relax4prec)*identity_npot
                write(*,*) 'A relaxed implicit'
                call formed_relaxed_A%form_new_linop(info,lun_err,relaxed_A)
                write(*,*) 'A relaxed explicit'
                call inv_A%init(formed_relaxed_A,ctrl_solver,ctrl_prec,&
                     lun=lun_err)
                write(*,*) 'invA '

                

                B_inv_M_BT = p1p0%B2_matrix * inv_M * p1p0%B1T_matrix
                write(*,*) 'B Q^{-1} BT implcit'
                call  formed_B_inv_M_BT%form_new_linop(info,lun_err,B_inv_M_BT)
                write(*,*) 'B Q^{-1} BT explicit'
                
                call ctrl_inv_B_inv_M_BT%init(lun_err,&
                     approach='AGMG',&
                     scheme='PCG',&
                     imax=400,&
                     iexit=ctrl%outer_iexit,&
                     isol=ctrl%outer_isol,&
                     nrestart=ctrl%krylov_nrestart,&
                     tol_sol=1.0d-4)
                write(*,*) 'ctrl inv B Q^{-1} BT'

                call inv_B_inv_M_BT%init(&
                     formed_B_inv_M_BT,ctrl_solver,ctrl_prec,&
                     lun=lun_err)
                write(*,*) 'inv B Q^{-1} BT'
                
                inv_Schur =  inv_B_inv_M_BT *  p1p0%B2_matrix  * p1p0%stiff *  p1p0%B1T_matrix  * inv_B_inv_M_BT

                write(*,*) 'implciit schur inverse'
                !
                ! initialized precondtioner of saddle point matrix 
                !
                call jacobian_prec%init(&
                     lun_err,&
                     jacobian_matrix,&
                     prec_type='SchurCAFull',&
                     invA=inv_A, invSchurCA = inv_Schur)

                write(*,*) 'saddle prec'
                call ctrl_iterative%init(&
                     6,&
                     approach='ITERATIVE',&
                     scheme='FGMRES',&
                     tol_sol=1.0d-5)
                
                call linear_solver(jacobian_matrix,rhs_full,&
                     increment,&
                     p1p0%info_solver,ctrl_iterative,&
                     prec_right=jacobian_prec)
                
                
                

             case default 
                rc = IOerr(lun_err, err_val , 'implicit_euler_newton_gfvar', &
                     ' ctrl%solve_jacobian_approach :"'//&
                     etb(ctrl%solve_jacobian_approach)// '" not suppported')

             end select

             if (ctrl%info_update .ge. 4) then
                call jacobian_matrix%Mxv(increment,p1p0%scr_nfull)
                p1p0%scr_nfull=p1p0%scr_nfull-rhs_full
                out_format=ctrl%formatting('aararar')
                write(msg,out_format) &
                     head_string, &
                     'RES :  ',dnrm2(nfull,p1p0%scr_nfull,1), &
                     ' REL-RES : ',dnrm2(nfull,p1p0%scr_nfull,1)/dnrm2(nfull,rhs_full,1), &
                     ' RHS : ',dnrm2(nfull,rhs_full,1)
                call ctrl%print_string('update', 4, etb(msg))

                out_format=ctrl%formatting('aaarar')
                write(msg,out_format) &
                     head_string, &
                     'INC: ' , &
                     ' POT : ',dnrm2(npot,increment(1:npot),1),&
                     ' GFVAR : ',dnrm2(ntdens,increment(1+npot:nfull),1)
                call ctrl%print_string('update', 4, etb(msg))

             end if
             

             ! store info linear solver
             p1p0%sequence_info_solver(newton_info%current_newton_step) = &
                  p1p0%info_solver      

             ! set info flag
             if (p1p0%info_solver%ierr .ne. 0) then
                info=112 ! error in linear solver
                info_newton=1 ! break newton cycle
                exit          
             end if

             !
             ! print info
             !
             call p1p0%info_solver%info2str(str) 
             write(msg,'(a,a,a)') &
                  head_string, 'LINEAR SOLVER :  ',etb(str)
             call ctrl%print_string('update', 2, etb(msg))
             

             if (info_newton .eq. 0)  then
                ! if no error occured in jacobian inversion
                ! set newton incremental step thus that
                ! block 2,2 of Jacobian Matrix is stricly positive/negative 
                !
                newton_ctrl%alpha=one
                test_cycle=.True.
                do while (test_cycle)
                   potnew   = pot   + newton_ctrl%alpha * increment(1:npot)
                   gfvarnew = gfvar + newton_ctrl%alpha * increment(npot+1:nfull)
                   call gfvar2tdens(ntdens,ode_inputs%pode,&
                        ode_inputs%pflux,gfvarnew,tdensnew)

                   call assembly_J22_gfvar(p1p0,ode_inputs,&
                        tdpot,gfvarnew, tdensnew, potnew, J22,&
                        p1p0%aux_bicgstab)
                   p1p0%scr_ntdens=J22+(p1p0%grid_tdens%size_cell/ctrl%deltat)
                   r=minval(p1p0%scr_ntdens)
                   test = r > ctrl%epsilon_W22

                   ! preserve positiveness up to machine precision
                   test2 = .True.!( minval(gfvarnew) > -small )
                   !out_format=ctrl%formatting('ar')
                   !write(*,*) 'min gfvarnew ', minval(gfvarnew)
                   if ( test .and. test2 ) exit
                   newton_ctrl%alpha=newton_ctrl%alpha/1.05d0
                   test_cycle = ( newton_ctrl%alpha > ctrl%relax_limit) 
                   if ( .not. test_cycle ) then
                      info_newton=230
                   end if
                end do
                ! set to zero values that are negative but small
                ! (those negative but big are not ammisible)
                r = minval(gfvarnew)
                if ( ( r< zero) .and. (r>-small) ) then
                   do i=1,ntdens
                      gfvar(icell)=max(zero,gfvar(icell))
                   end do
                end if


                total_gfvar_increment=total_gfvar_increment+&
                     newton_ctrl%alpha*increment(npot+1:npot+ntdens)
                

                out_format=ctrl%formatting('aar')
                write(msg,out_format) head_string, &
                     'Alpha Dumping =', newton_ctrl%alpha 
                call ctrl%print_string('update',2,msg)
             end if

             ! print separator
             write(sep,'(a)') ctrl%separator()              
             call ctrl%print_string('update',2,sep)

          end select
          ! call newton_raphson_reverse(&
          !      flag,info_newton, &
          !      current_newton_step, nfull, xvar, increment, alpha, &
          !      fnewton_norm, initial_fnewton_norm, previuous_fnewton_norm,& 
          !      ctrl%max_nonlinear_iterations,  ctrl%tolerance_nonlinear,&
          !      ctrl%absolute_growth_factor, ctrl%relative_growth_factor)
          call newton_raphson_reverse_with_dt(&
               flag,info_newton,&
               nfull, xvar, increment,&
               newton_ctrl,newton_info)
       end do

       write(sep,'(a)') ctrl%separator()
       call ctrl%print_string('update',2,sep)


       
       if (info_newton .eq. 0) then
          ! copy 
          tdpot%pot     = xvar(1:npot)
          tdpot%gfvar   = xvar(npot+1:nfull)
          call gfvar2tdens(ntdens,ode_inputs%pode,ode_inputs%pflux, tdpot%gfvar,tdpot%tdens)
          do i=1,ntdens
             tdpot%tdens=max(tdpot%tdens,ctrl%min_tdens)
          end do
          call tdens2gfvar(ntdens,&
               ode_inputs%pode,ode_inputs%pflux,tdpot%tdens, tdpot%gfvar)

          ! print newton resume info
          out_format='(a,1x,I2,1x,a,'//etb(ctrl%rformat_info)//')'
          write(msg,out_format) &
               'NEWTON CONVERGED IN', &
               newton_info%current_newton_step,&
               'ITERATIONS WITH RESIDUA=',&
               newton_ctrl%fnewton_norm
          call ctrl%print_string('update',1,msg) 
       else
          ! set info flag according to update_tdpot description
          select case (info_newton)
          case (-1)
             ! maximum number of iterations reached
             info=210
          case (-2)
             ! newton diverged
             info=220
          case (230)
             ! dumping alpha 
             info=info_newton
          end select

          ! print info
          out_format='(a,1x,I2,1x,a,'//etb(ctrl%rformat_info)//',a,I5)'
          write(msg,out_format) &
               'NEWTON FAILED AFTER', &
               newton_info%current_newton_step,&
               'ITERATIONS WITH RESIDUA=',&
               newton_ctrl%fnewton_norm ,&
               ' ERRFLAG= ',info
          call ctrl%print_string('update',1,msg)
       end if

       
       ! update tdpot time related quantities
       if ( info == 0 ) then
          tdpot%deltat=ctrl%deltat
          tdpot%time_old = tdpot%time
          tdpot%time=tdpot%time+ctrl%deltat
          tdpot%iter_nonlinear = newton_info%current_newton_step - 1
          tdpot%time_iteration = tdpot%time_iteration + 1
          tdpot%time_update    = tdpot%time_update + 1
       end if

       ! free memory
       deallocate( &
            xvar, &
            FNEWTON,&
            increment,&
            rhs_full,&
            J22,&
            tdens,&
            pot,&
            tdensnew,&
            potnew,&
            stat=res)
       if (res .ne. 0) &
            rc = IOerr(lun_err, err_dealloc , 'implicit_euler_newton_gfvar', &
            ' work arrays',res)

      

     contains
       subroutine save_matrices(p1p0,tdpot,gfvar,tdens, pot)
         type(dmkp1p0), intent(in   ) :: p1p0
         type(tdpotsys),                  intent(in   ) :: tdpot
         real(kind=double), intent(in ) :: gfvar(ode_inputs%ntdens)
         real(kind=double), intent(in ) :: tdens(ode_inputs%ntdens)
         real(kind=double), intent(in ) :: pot(ode_inputs%npot)
         !local
         character(len=256) :: fname,tail,directory
         type(file) :: fmat

         write(tail,'(I0.4,a,I0.4,a)') &
              tdpot%time_iteration,'_',current_newton_step, '.dat' 
         directory=etb('../output/linsys')

         fname=etb(etb(directory)//'/tdens_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call write_steady(lun_err, 10000, ntdens, tdens)
         call fmat%kill(lun_err)

         fname=etb(etb(directory)//'/gfvar_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call write_steady(lun_err, 10000, ntdens, gfvar)
         call fmat%kill(lun_err)


         fname=etb(etb(directory)//'/pot_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call write_steady(lun_err, 10000, npot, pot)
         call fmat%kill(lun_err)

         fname=etb(etb(directory)//'/rhs_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call write_steady(lun_err, 10000, npot, ode_inputs%rhs)
         call fmat%kill(lun_err)



         fname=etb(etb(directory)//'/rhs_newton_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call write_steady(lun_err, 10000, ntdens+npot, -FNEWTON)
         call fmat%kill(lun_err)

         fname=etb(etb(directory)//'/stiff_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call p1p0%stiff%write(10000,'matlab')
         call fmat%kill(lun_err)


         fname=etb(etb(directory)//'/Bmat_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call p1p0%B_matrix%write(10000,'matlab')
         call fmat%kill(lun_err)



         fname=etb(etb(directory)//'/D_'//etb(tail))
         call fmat%init(lun_err,fname,10000,'out')
         call write_steady(lun_err, 10000, ntdens, p1p0%matrixC%diagonal)
         call fmat%kill(lun_err)


       end subroutine save_matrices
     end subroutine implicit_euler_newton_gfvar


     !>----------------------------------------------------
     !> Procedure for update the system with Explicit Euler
     !> IMPORTANT all the variables tdens pot 
     !> odein have to be syncronized at time time(itemp)
     !>
     !> usage: call var%explicit_euler_accelarated_gfvar(stderr,itemp)
     !> 
     !> where:
     !> \param[in ] stderr -> Integer. I/O err. unit
     !> \param[in ] itemp   -> Integer. Time iteration
     !<---------------------------------------------------    
     subroutine explicit_euler_accelerated_gfvar(this,&
          tdpot,&
          lun_err, info,&
          ctrl,&
          time_iteration,&
          deltat,&
          time,&
          CPU,&
          ode_inputs) 
       use Globals 
       use Timing

       implicit none
       class(dmkp1p0), target,intent(inout) :: this
       type(tdpotsys),   intent(inout) :: tdpot
       integer,           intent(in   ) :: lun_err
       integer,           intent(inout) :: info
       type(DmkCtrl),     intent(in   ) :: ctrl
       integer,           intent(in   ) :: time_iteration
       real(kind=double), intent(in   ) :: deltat
       real(kind=double), intent(in   ) :: time
       type(codeTim),     intent(inout) :: CPU
       type(DmkInputs),   intent(in   ) :: ode_inputs


       !local
       logical :: rc
       integer :: res
       integer :: icell, id_ode
       integer :: ntdens,npot
       real(kind=double) :: pode,pflux
       real(kind=double) :: delta,dnrm2
       class(abs_linop), pointer :: ort_mat
       real(kind=double), allocatable :: rhs(:)
       type(input_prec) :: ctrl_prec
       type(input_solver) :: ctrl_solver
       type(inv) :: inverse_stiff


       if ( .not. tdpot%all_syncr)  then
          write(lun_err,*) 'Not all varibles are syncronized'
          stop
       end if

       !
       ! local variables
       !
       ntdens = tdpot%ntdens
       npot   = tdpot%npot

       allocate(rhs(npot),stat=res)
       if (res.ne.0) rc=IOerr(lun_err, err_alloc, &
            'explicit_euler_accelerated_gfvar', &
            'temp array rhs')


       ! Gradient Flow : ctrl%id_ode .eq. 2
       ! $ \Gfvar|\Grad \Pot|^2 - \Gfvar $
       !
       if ( time_iteration .eq. 1 ) then
          !
          ! at first iteration use standard explicit Euler
          !

          !
          ! compute increment
          !
          call this%build_norm_grad_dyn(tdpot%pot,2.0d0,this%norm_grad_dyn)
          tdpot%gfvar = tdpot%gfvar  +  deltat * ( tdpot%gfvar * this%norm_grad_dyn - tdpot%gfvar)
          call gfvar2tdens(ntdens,ode_inputs%pode,ode_inputs%pflux,&
               tdpot%gfvar, tdpot%tdens)
          !
          ! ensure positivity
          !
          do icell = 1, tdpot%ntdens
             tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
          end do
          call tdens2gfvar(ntdens,two,one,&
               tdpot%tdens,tdpot%gfvar)

          ! assembly matrix   
          this%scr_ntdens = tdpot%tdens + ode_inputs%lambda_lift ! use tdens + lambda_lift
          call this%grid_pot%proj_subgrid(this%scr_ntdens,this%tdens_prj)
          call this%p1%build_stiff(lun_err,&
               'csr', this%tdens_prj, this%stiff)


          rhs=ode_inputs%rhs
          ! handle Neumann singularity or Dirichlet 
          if ( ode_inputs%ndir > 0 ) then
             call this%p1%dirichlet_bc(lun_err,&
                  this%stiff,rhs, tdpot%pot,&
                  ode_inputs%ndir,&
                  ode_inputs%dirichlet_nodes,&
                  ode_inputs%dirichlet_values)
             ort_mat=>null()
          else
             call ortogonalize(this%stiff%ncol,&
                  1,&
                  this%kernel_full(1:npot,1),tdpot%pot)
             ort_mat=>this%near_kernel
          end if

          !
          ! set controls per inner solver (solver used in preconditioner)
          !
          call ctrl_solver%init(lun_err,&
               approach=ctrl%outer_solver_approach,&
               scheme=ctrl%outer_krylov_scheme,&
               imax=ctrl%outer_imax,&
               iexit=ctrl%outer_iexit,&
               isol=ctrl%outer_iexit,&
               nrestart=ctrl%krylov_nrestart,&
               tol_sol=ctrl%outer_tolerance)
          if (ctrl%outer_solver_approach=='ITERATIVE') then
             !
             ! set inversion controls from ctrl
             !
             call ctrl_prec%init(&
                  lun_err,&
                  ctrl%outer_prec_type,&
                  ctrl%outer_prec_n_fillin,&
                  ctrl%inner_prec_tol_fillin)
             call ctrl_prec%info(6)


          end if

          !
          ! define (approximate) inverse 
          !
          call  inverse_stiff%init(&
               this%stiff,ctrl_solver,ctrl_prec,&
               lun=lun_err)


          !
          ! solve LInear system 
          !
          call inverse_stiff%Mxv(this%rhs,tdpot%pot)
          this%info_solver=inverse_stiff%info_solver

          if( ctrl%lun_out >0) call this%info_solver%info(ctrl%lun_out)
          if( ctrl%lun_statistics>0) call this%info_solver%info(ctrl%lun_statistics)


          ! store linear solver info 
          this%sequence_info_solver(1)=this%info_solver
          this%sequence_build_prec(1)=ctrl%build_prec
          this%iter_first_system = this%info_solver%iter

       else
          !
          ! yk=x{k}-(k-1)/(k+2)*(x_{k}-x_{k-1})
          !
          write(*,*) ' gfvar', time_iteration, minval(tdpot%gfvar), maxval(tdpot%gfvar)
          tdpot%scr_ntdens = tdpot%gfvar + &
               ( time_iteration - 1 ) *  &
               one/ ( time_iteration + 10 + 2 ) * &
               ( tdpot%gfvar - this%D1 )
          write(*,*) ' xk-1', time_iteration, minval(this%D1), maxval(this%D1)
          write(*,*) ' yk  ', time_iteration, minval(tdpot%scr_ntdens), maxval(tdpot%scr_ntdens)
          !
          ! update tdens to compute potential drift
          !
          call gfvar2tdens(ntdens,two,one,&
               tdpot%scr_ntdens,&
               this%scr_ntdens)
          write(*,*) ' scr_ntdens', time_iteration, minval(this%scr_ntdens), maxval(this%scr_ntdens)

          this%scr_npot = ode_inputs%rhs

          !
          ! compute \Grad \lyap ( yk )
          !
          ! compute potential
          ! assembly matrix   
          this%scr_ntdens = tdpot%tdens + ode_inputs%lambda_lift ! use tdens + lambda_lift
          call this%grid_pot%proj_subgrid(this%scr_ntdens,this%tdens_prj)
          call this%p1%build_stiff(lun_err,&
               'csr', this%tdens_prj, this%stiff)


          ! handle Neumann singularity or Dirichlet 
          if ( ode_inputs%ndir > 0 ) then
             call this%p1%dirichlet_bc(lun_err,&
                  this%stiff,rhs, tdpot%pot,&
                  ode_inputs%ndir,&
                  ode_inputs%dirichlet_nodes,&
                  ode_inputs%dirichlet_values)
             ort_mat=>null()
          else
             call ortogonalize(this%stiff%ncol,&
                  1,&
                  this%kernel_full(1:npot,1),tdpot%pot)
             ort_mat=>this%near_kernel
          end if

          !
          ! set controls per inner solver (solver used in preconditioner)
          !
          call ctrl_solver%init(lun_err,&
               approach=ctrl%outer_solver_approach,&
               scheme=ctrl%outer_krylov_scheme,&
               imax=ctrl%outer_imax,&
               iexit=ctrl%outer_iexit,&
               isol=ctrl%outer_iexit,&
               nrestart=ctrl%krylov_nrestart,&
               tol_sol=ctrl%outer_tolerance)
          if (ctrl%outer_solver_approach=='ITERATIVE') then
             !
             ! set inversion controls from ctrl
             !
             call ctrl_prec%init(&
                  lun_err,&
                  ctrl%outer_prec_type,&
                  ctrl%outer_prec_n_fillin,&
                  ctrl%inner_prec_tol_fillin)
             call ctrl_prec%info(6)


          end if

          !
          ! define (approximate) inverse 
          !
          call  inverse_stiff%init(&
               this%stiff,ctrl_solver,ctrl_prec,&
               lun=lun_err)


          !
          ! solve LInear system 
          !
          call inverse_stiff%Mxv(this%scr_npot,tdpot%pot)
          this%info_solver=inverse_stiff%info_solver




          if( ctrl%lun_out >0) call this%info_solver%info(ctrl%lun_out)
          if( ctrl%lun_statistics>0) call this%info_solver%info(ctrl%lun_statistics)


          ! store linear solver info 
          this%sequence_info_solver(1)=this%info_solver
          this%sequence_build_prec(1)=ctrl%build_prec
          this%iter_first_system = this%info_solver%iter


          call this%build_norm_grad_dyn(tdpot%pot,2.0d0,this%norm_grad_dyn)
          this%rhs_ode = ( tdpot%scr_ntdens * this%norm_grad_dyn - tdpot%scr_ntdens) 
          

          !
          ! x_k+1=yk - deltat \Grad \Lyap (yk) 
          !
          tdpot%gfvar = this%scr_ntdens + deltat * this%rhs_ode

          call gfvar2tdens(ntdens,two,pflux,tdpot%gfvar,tdpot%tdens)

       end if

       deallocate(rhs,stat=res)
       if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'explicit_euler_accelerated_gfvar', &
            'temp array rhs')



     end subroutine explicit_euler_accelerated_gfvar



     !>-------------------------------------------------
     !> Precedure to compute gfvar variable for GF dynamics
     !> ( public procedure for type tdpotsys)
     !> 
     !> usage: call 
     !>
     !<-------------------------------------------------
     subroutine tdens2gfvar(ntdens,pode,pflux,tdens,gfvar)
       implicit none
       integer, intent(in) :: ntdens
       real(kind=double), intent(in) :: pode     
       real(kind=double), intent(in) :: pflux
       real(kind=double), intent(in) :: tdens(ntdens)
       real(kind=double), intent(out) :: gfvar(ntdens)
       !local
       real(kind=double) :: power

       !
       ! gfvar= tdens^{\frac{pflux}/{2-beta}}
       !
       if ( abs(pode-2.0d0)< 1.0d-12) then
          if ( pflux < 2.0d0) then
             power = (2.0d0 - pflux) / 2.0d0
             gfvar = tdens**power
          else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
             gfvar = log(tdens)
          end if
       else
          write(6, *) 'In  procedure . tdens2gfvar', &
               ' transformation not defined for pode = ',pode
          stop
       end if

     end subroutine tdens2gfvar

          !>-------------------------------------------------
     !> Precedure to compute gfvar variable for GF dynamics
     !> ( public procedure for type tdpotsys)
     !> 
     !> usage: call 
     !>
     !<-------------------------------------------------
     subroutine gfvar2tdens(ntdens,pode,pflux,gfvar,tdens)
       implicit none
       integer,           intent(in) :: ntdens
       real(kind=double), intent(in) :: pode
       real(kind=double), intent(in) :: pflux
       real(kind=double), intent(in) :: gfvar(ntdens)
       real(kind=double), intent(inout) :: tdens(ntdens)    
       !local
       real(kind=double) :: power

       !
       ! tdens=gfvar^p  p=\frac{2}{2-\pflux}
       !
       if ( abs(pode-2.0d0)< 1.0d-12) then
          if ( pflux < 2.0d0) then
             power = 2.0d0 / (2.0d0 - pflux)
             tdens = gfvar**power
          else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
             tdens = log(gfvar)
          end if
       else
          write(6, *) 'In  procedure . gfvar2tdens', &
               ' transformation not defined for pode = ',pode
       end if

     end subroutine gfvar2tdens


     
     !>-------------------------------------------------
     !> Precedure to compute trans'(gfvar)
     !> ( public procedure for type tdpotsys)
     !> 
     !> usage: call 
     !>
     !<-------------------------------------------------
     subroutine eval_trans_prime(ntdens,pode,pflux,gfvar,transformation_prime)
       use Globals
       implicit none
       integer,           intent(in ) :: ntdens
       real(kind=double), intent(in ) :: pode     
       real(kind=double), intent(in ) :: pflux
       real(kind=double), intent(in ) :: gfvar(ntdens)
       real(kind=double), intent(inout) :: transformation_prime(ntdens)
       !local
       real(kind=double) :: power

       !
       ! tdens=gfvar^p  p=2/(2-\pflux)
       !
       if ( abs(pode-2.0d0)< 1.0d-12) then
          if ( pflux < 2.0d0) then
             power = 2.0d0 / (2.0d0 - pflux)
             transformation_prime = power*gfvar**(power-one)
             !write(*,*) pflux,power, minval(transformation_prime),'<=trans prime <=',maxval(transformation_prime)
          else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
             transformation_prime = exp(gfvar) 
          end if
       else
          write(6, *) 'In  procedure eval_trans_prime', &
               ' transformation not defined for pode = ',pode
          stop
       end if

     end subroutine eval_trans_prime


     !>-------------------------------------------------
     !> Precedure to compute trans''(gfvar)
     !> ( public procedure for type tdpotsys)
     !> 
     !> usage: call 
     !>
     !<-------------------------------------------------
     subroutine eval_trans_second(ntdens,pode,pflux,gfvar,transformation_second)
       implicit none
       integer, intent(in) :: ntdens
       real(kind=double), intent(in) :: pode     
       real(kind=double), intent(in) :: pflux
       real(kind=double), intent(in) :: gfvar(ntdens)
       real(kind=double), intent(out):: transformation_second(ntdens)
       !local
       real(kind=double) :: power

       !
       ! tdens=gfvar^p  p=2/(2-\pflux)
       ! 
       !
       if ( abs(pode-2.0d0)< 1.0d-12) then
          if ( pflux < 2.0d0) then
             power = 2.0d0 / (2.0d0 - pflux)
             transformation_second = power*(power-one)*gfvar**(power-two)
             !write(*,*) power,minval(transformation_second),'<=trans second<=',maxval(transformation_second)
          else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
             transformation_second = exp(gfvar) 
          end if
       else
          write(6, *) 'In  procedure eval_trans_second', &
               ' transformation not defined for pode = ',pode
          stop
       end if

     end subroutine eval_trans_second



     !> Compute the 2nd divated w.r.t to gfvar of lyapunov
     !> NO RELAXATION is included

     subroutine assembly_J22_gfvar(p1p0,ode_inputs,tdpot,gfvar,tdens, pot,J22,aux_global)
       use DmkInputsData
       implicit none 
       type(dmkp1p0),      intent(inout) :: p1p0
       type(DmkInputs),    intent(in   ) :: ode_inputs
       type(tdpotsys),     intent(in   ) :: tdpot
       real(kind=double),  intent(in   ) :: gfvar(p1p0%ntdens)
       real(kind=double),  intent(in   ) :: tdens(p1p0%ntdens)
       real(kind=double),  intent(in   ) :: pot(p1p0%npot)
       real(kind=double),  intent(inout) :: J22(p1p0%ntdens)
       type(scrt), optional,target,intent(inout) :: aux_global


       ! local
       logical :: rc
       integer :: res
       integer :: icell,ntdens,ibegin,iend
       real(kind=double) :: min,max
       real(kind=double) ::  pflux, pmass, decay, pode,penalty_factor,power
       type(scrt), target :: aux_local
       type(scrt), pointer  :: aux
       real(kind=double), pointer :: trans_prime(:), trans_second(:), gfvar_weight(:)


       ntdens = p1p0%ntdens

       ! set work space
       if (present(aux_global)) then
          if( .not. aux_global%check(0,3*ntdens) ) &
               rc = IOerr(0, wrn_inp, 'minres_solver', &
               ' aux array too small')
          aux=>aux_global   
       else
          call aux_local%init(0,0,3*ntdens)
          aux=>aux_local
       end if
       iend=0
       call aux%range(ntdens,ibegin,iend)
       trans_prime =>aux%raux(ibegin:iend)
       call aux%range(ntdens,ibegin,iend)
       trans_second =>aux%raux(ibegin:iend)
       call aux%range(ntdens,ibegin,iend)
       gfvar_weight =>aux%raux(ibegin:iend)

       ! shorhands
       pflux = ode_inputs%pflux
       pmass = ode_inputs%pmass
       decay = ode_inputs%decay
       pode  = ode_inputs%pode
       power = tdpot%wmass_exponent(pode,pflux,pmass)


       ! TODO, remove. build_norm_grad_dyn  is already built
       J22=zero
       call p1p0%build_norm_grad_dyn(pot,two,J22)
       call eval_trans_prime (p1p0%ntdens,two,pflux,gfvar,trans_prime)
       call eval_trans_second(p1p0%ntdens,two,pflux,gfvar,trans_second)
       ! do icell=1,ntdens
       !    if (isnan(J22(icell))) write(*,*) 'j22', icell
       !    if (isnan(trans_prime(icell))) write(*,*) 'prime', icell
       !    if (isnan(trans_second(icell))) write(*,*) 'second', icell
       !    if (tdens(icell)<zero) write(*,*) 'tdens<0', icell
       !    if (gfvar(icell)<zero) write(*,*) 'gfvar <0', icell
       ! end do
       ! J22=d^2 (lyap)
       !    =d( -0.5 trans' *beta*normgrad^2 + 0.5 * decay kappa^2 * trans'* trans^(wmass-1)
       !    =-0.5 trans'' beta * normgrad^2 + 0.5* decay kappa^2 *
       !          ( trans'' * trans^(wmass-1) + trans' * (wmass-1) (trans)^{wmass-2) * trans' 
       J22 = - onehalf * ode_inputs%beta * trans_second * J22

       ! trying to avoid underflow
       ! add contrinution using gfvar_weight as scratch varaible
       if (  abs(power-one)<small) then
          gfvar_weight=one
       else
          gfvar_weight=tdens ** (power - one )
       end if
       J22 = J22 + &
            onehalf * decay * ode_inputs%kappa**2 * &
            gfvar_weight * trans_second
       ! add contribution 
       if ( abs(power-one)>small) J22=J22+&
            onehalf * decay * ode_inputs%kappa**2 * &
            (power-one)*tdens**(power-two) * trans_prime**2 
       do icell=1,ntdens
          if (isnan(J22(icell))) write(*,*) 'J22', icell
       end do

       penalty_factor = ode_inputs%penalty_factor
       !write(6,*) 'penalty_factor',penalty_factor
       if ( abs( penalty_factor ) .gt. small) then
          call tdens2gfvar(ntdens,pode,pflux,ode_inputs%penalty_weight,gfvar_weight)       
          J22 = J22 + penalty_factor * gfvar_weight
       end if

       do icell=1,ntdens
          if (isnan(J22(icell))) write(*,*) 'J22', icell
       end do


       J22=J22*p1p0%grid_tdens%size_cell

       do icell=1,ntdens
          if (isnan(J22(icell))) write(*,*) 'J22', icell
       end do

       !
       ! free memory
       !
       aux => null()
       if (aux_local%is_initialized) call aux_local%kill(0)
       
       
     end subroutine assembly_J22_gfvar


     !>-------------------------------------------------
     !> Precedure to store in timefun quantities
     !> (errors, number of iterations,etc) along
     !> time evolution.
     !> 
     !> ( public procedure for type dmkp1p0)
     !> 
     !> usage: call 
     !>
     !<-------------------------------------------------
     subroutine set_evolfun(this,timefun,itemp,tdpot,ode_inputs,ctrl)
       use TdensPotentialSystem
       use DmkControls
       use TdensPotentialSystem
       use DmkInputsData
       use TimeFunctionals

       implicit none
       class(dmkp1p0), target, intent(in   ) :: this
       type(evolfun), intent(inout) :: timefun
       integer,        intent(in   ) :: itemp
       type(tdpotsys), intent(in   ) :: tdpot
       type(DmkInputs),  intent(in   ) :: ode_inputs
       type(DmkCtrl),  intent(in   ) :: ctrl

       !local
       real(kind=double) :: power,w1dist

       !>----------------------------------------------------------------
       !> Integer Valued Fucntional
       !>----------------------------------------------------------------
       timefun%iter_solver(1:tdpot%iter_nonlinear,itemp) = &
            this%sequence_info_solver(1:tdpot%iter_nonlinear)%iter
       timefun%iter_nonlinear(itemp) = tdpot%iter_nonlinear
       timefun%iter_total(itemp)=sum(timefun%iter_solver(1:tdpot%iter_nonlinear,itemp))
       timefun%iter_media(itemp)=int(timefun%iter_total(itemp)*one/tdpot%iter_nonlinear)
!!$    timefun%prec_calc(:,itemp) = &
!!$         tdpot%sequence_build_prec(:)

       !----------------------------------------------------------------
       ! Real Valued Fucntional
       !---------------------------------------------------------------

       timefun%mass_tdens(itemp)= tdpot%mass_tdens
       timefun%weighted_mass_tdens(itemp)= tdpot%weighted_mass_tdens
       timefun%energy(itemp) = tdpot%energy
       timefun%lyapunov(itemp)  =  tdpot%lyapunov
       timefun%min_tdens(itemp)= tdpot%min_tdens
       timefun%max_tdens(itemp)= tdpot%max_tdens
       timefun%max_nrm_grad(itemp)     = tdpot%max_nrm_grad
       timefun%max_nrm_grad_avg(itemp) = tdpot%max_nrm_grad_avg 


       timefun%res_elliptic(itemp) = tdpot%res_elliptic
       timefun%deltat(itemp) = ctrl%deltat 

     end subroutine set_evolfun


     subroutine set_linear_algebra_ctrls(ctrl,&
          ctrl_outer_solver,ctrl_outer_prec,&
          ctrl_inner_solver,ctrl_inner_prec)
       implicit none
       class(DmkCtrl),     intent(in   ) :: ctrl
       type(input_solver), intent(inout) :: ctrl_outer_solver
       type(input_prec),   intent(inout) :: ctrl_outer_prec
       type(input_solver), intent(inout) :: ctrl_inner_solver
       type(input_prec),   intent(inout) :: ctrl_inner_prec
       
       !
       ! init higher level controls for linear solvers
       !
       call ctrl_outer_solver%init(0,&
            approach=ctrl%outer_solver_approach,&
            scheme=ctrl%outer_krylov_scheme,&
            imax=ctrl%outer_imax,&
            iexit=ctrl%outer_iexit,&
            isol=ctrl%outer_iexit,&
            iprt=ctrl%outer_iprt,&
            nrestart=ctrl%krylov_nrestart,&
            tol_sol=ctrl%outer_tolerance)

       call ctrl_inner_solver%init(0,&
            approach=ctrl%inner_solver_approach,&
            scheme=ctrl%inner_krylov_scheme,&
            imax=ctrl%inner_imax,&
            iexit=ctrl%inner_iexit,&
            isol=ctrl%inner_iexit,&
            nrestart=ctrl%krylov_nrestart,&
            tol_sol=ctrl%inner_tolerance)

       !
       ! set inversion controls from ctrl
       !
       if ( ctrl%outer_prec_type .ne. 'INNER_SOLVER') Then
          call ctrl_outer_prec%init(&
               0,&
               ctrl%outer_prec_type,&
               ctrl%outer_prec_n_fillin,&
               ctrl%outer_prec_tol_fillin)

       end if
       call ctrl_inner_prec%init(&
            0,&
            ctrl%inner_prec_type,&
            ctrl%inner_prec_n_fillin,&
            ctrl%inner_prec_tol_fillin)

     end subroutine set_linear_algebra_ctrls

     

     !
     ! Procedure defined for Python wrap
     !

     !> Constructor
     subroutine dmkp1p0_constructor(this,&
          ctrl,&
          id_subgrid, grid_tdens, grid_pot)
       implicit none
       type(dmkp1p0),target,          intent(inout) :: this
       type(DmkCtrl),                  intent(in   ) :: ctrl
       integer,                        intent(in   ) :: id_subgrid
       type(abs_simplex_mesh), target, intent(in   ) :: grid_tdens
       type(abs_simplex_mesh), target, intent(in   ) :: grid_pot

       call this%init(ctrl,id_subgrid,grid_tdens, grid_pot) 
       
     end subroutine dmkp1p0_constructor

     !> Destructor
     subroutine dmkp1p0_destructor(this,lun_err)     
       use Globals
       implicit none
       type(dmkp1p0), intent(inout) :: this
       integer,        intent(in   ) :: lun_err

       call this%kill(lun_err)
     end subroutine dmkp1p0_destructor


     subroutine dmkp1p0_cycle_reverse_communication(&
          this,&
          flag,flag_task,info,&
          input_time,&
          inputs_data,tdpot,ctrl,original_ctrl)

       type(dmkp1p0),     intent(inout) :: this
       integer,           intent(inout) :: flag
       integer,           intent(inout) :: flag_task
       integer,           intent(inout) :: info
       real(kind=double), intent(inout) :: input_time
       type(DmkInputs),   intent(in   ) :: inputs_data
       type(tdpotsys),    intent(inout) :: tdpot
       type(DmkCtrl),     intent(inout) :: ctrl
       type(DmkCtrl),     intent(in   ) :: original_ctrl

       
       call this%new_dmk_cycle_reverse_communication(&
            flag,flag_task,info,input_time,&
            inputs_data,tdpot,ctrl,original_ctrl)

       
     end subroutine dmkp1p0_cycle_reverse_communication

     !>-------------------------------------------------
     !> Precedure for grads variables and grad-depending variables
     !> including norm_grad_dyn defiend as
     !> norm_grad_dyn(r) = \int _{T_r} |\nabla u| /{T_r}
     !> ( public procedure for type tdpotsys)
     !> 
     !> usage: call var%build_norm_grad_vars(ctrl)
     !>
     !<-------------------------------------------------
     subroutine dmk_p1p0_build_norm_grad_dyn(this,pot,pode,norm_grad_dyn)
       use Globals
       implicit none
       
       type(dmkp1p0),     intent(inout) :: this
       real(kind=double), intent(in   ) :: pot(this%npot)
       real(kind=double), intent(in   ) :: pode
       real(kind=double), intent(inout) :: norm_grad_dyn(this%ntdens)

       call this%build_norm_grad_dyn(pot,pode,norm_grad_dyn)
     end subroutine dmk_p1p0_build_norm_grad_dyn
     !>--------------------------------------------------------
     !> Subroutine for storing amdk time evolution 
     !<--------------------------------------------------------  
     subroutine dmkp1p0_store_evolution_info(this, tdpot,inputs_data,ctrl,timefun)
       use TimeFunctionals
       implicit none
       type(dmkp1p0),     intent(inout) :: this
       type(tdpotsys),    intent(inout) :: tdpot ! intent(inout) because we use some work arrays
       type(DmkInputs),   intent(in   ) :: inputs_data
       type(DmkCtrl),     intent(in   ) :: ctrl
       type(evolfun),     intent(inout) :: timefun


       ! compute all functionals storing results in tdpot and alg
       call this%compute_functionals(tdpot,inputs_data,ctrl)

       ! copy current state informations into timefun
       call this%store_evolution(timefun,tdpot%time_iteration,&
            tdpot,inputs_data,ctrl)

     end subroutine dmkp1p0_store_evolution_info
     

       !>--------------------------------------------------------------------
  !> Static Constructor 
  !> (public procedure for type dmkp1p0)
  !> 
  !> usage: call var%init(&
  !>                lun_err,lun_out,lun_stat,& 
  !>                ctrl,&
  !>                grid_tdens, grid_pot)
  !> where:
  !> \param[in] lun_err    -> integer. I/O unit for error message
  !> \param[in] lun_out    -> integer. I/O unit for output message
  !> \param[in] lun_stat   -> integer. I/O unit for statistic
  !> \param[in] ctrl       -> type(DmkCtrl). Controls variables
  !> \param[in] grid_tdens -> tyep(mesh). Mesh for tdens
  !> \param[in] grid_pot   -> tyep(mesh). Mesh for pot
  !>---------------------------------------------------------------------
  subroutine init_p1p0pair(this,&
       lun_err,id_subgrid, grid_tdens, grid_pot)
    implicit none
    class(p1p0pair),target, intent(inout) :: this
    integer,               intent(in   ) :: lun_err
    integer,               intent(in   ) :: id_subgrid
    type(abs_simplex_mesh), target, intent(in   ) :: grid_tdens
    type(abs_simplex_mesh), target, intent(in   ) :: grid_pot
    ! local
    logical :: rc
    integer :: res
    integer :: ntdens, npot, ngrad,i,iloc,jloc
    ! jacobian 
    type(array_mat) :: jacobian_list(4)
    integer :: block_structure(3,4)
    character(len=1) :: jacobian_directions(4)
    real(kind=double), allocatable :: kernel_full(:,:) 
    ! preconditioners
    type(array_linop) :: prec_list(2)
    integer :: prec_block_structure(3,2)
    type(input_prec) :: ctrl_prec
    type(scalmat),   target :: identity_npot
    integer ::info
    type(input_prec) :: ctrl_inverse
    real(kind=double) :: dnrm2
    integer :: dim_work,nfull

    
    this%id_subgrid = id_subgrid
    this%grid_tdens => grid_tdens
    this%grid_pot   => grid_pot
    
    !
    ! dimension assignment + local copy
    !
    !this%ntdens    = grid_tdens%ncell
    !this%npot      = grid_pot%nnode
    !this%ngrad     = grid_pot%ncell
    !this%ambient_dimension = grid_tdens%ambient_dimension
    !this%nfull     =  this%ntdens +  this%npot

 
    ntdens   = grid_tdens%ncell
    npot     = grid_pot%nnode
    ngrad    = grid_pot%ncell  
    
    !
    ! construction of p1 element space
    !
    call this%p1%init(lun_err,this%grid_pot)
    
    !
    ! Variables for Newton Method in Implicit Euler Procedure
    !
    ! B_matrix and assembler_Bmatrix_subgrid
    ! (the pointer ia and ja are stored directly in B_matrix)
    allocate(&
         this%assembler_Bmatrix_subgrid(&
         this%grid_pot%nnodeincell,this%grid_pot%ncell),&
         this%assembler_Bmatrix_grid(&
         this%grid_tdens%nnodeincell,this%grid_tdens%ncell),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member assembler_Bmatrix_subgrid')
    
    if (id_subgrid .eq. 1) then
       call assembly_B_matrix_subgrid(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_subgrid) 
    else
       call assembly_B_matrix_grid(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_grid)
    end if

  end subroutine init_p1p0pair

  !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(icell):ia(icell+1)-1) [with icell the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine assembly_B_matrix_subgrid(lun_err,subgrid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: subgrid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(subgrid%nnodeincell,subgrid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, icell_sub, inode, inode_sub,j,k,ind
    integer :: nsubnode, nsubcell, ncell_parent
    integer :: nrow, ncol, nterm, nnodeincell, nsubnode_in_cell
    integer :: start, finish
    integer , allocatable :: count_subnode_in_cell(:),ia(:),ja(:)


    nsubcell     = subgrid%ncell
    nsubnode     = subgrid%nnode
    nnodeincell  = subgrid%nnodeincell 
    ncell_parent = subgrid%ncell_parent


    ! triangle
    if ( nnodeincell .eq. 3 ) nsubnode_in_cell = 6
    ! tetrahedron
    if ( nnodeincell .eq. 4 ) nsubnode_in_cell = 10

    nrow  = ncell_parent
    ncol  = nsubnode
    nterm = nsubnode_in_cell * ncell_parent


    !
    ! Set B_matrix dimensions and work arrays
    ! we use members ia,ja in B_matrix as work array
    !
    call B_matrix%init(lun_err, &
         nrow, ncol, nterm,&
         storage_system='csr',&
         is_symmetric =.false.)

    ! allocate work array 
    allocate(&
         ! count the number of added node for each cell
         count_subnode_in_cell(nrow),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'assembly_B_matrix_general', &
         'alloc fail for temp array nsubnode_in_cell')

    B_matrix%ia=0
    B_matrix%ja=0
    count_subnode_in_cell=0



    !
    ! set ia pointer
    !
    do icell=1,ncell_parent+1
       B_matrix%ia(icell)=1+(icell-1) * nsubnode_in_cell
    end do

    do icell_sub = 1, nsubcell
       ! cycle all subcell 
       ! find parent cell
       ! find bound in the array ja
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle all nodes in subcell
       !
       if ( count_subnode_in_cell(icell) < nsubnode_in_cell ) then
          do k = 1, nnodeincell
             inode_sub=subgrid%topol(k,icell_sub)

             ! search subnode index in previously added subnodes
             found=.false.
             do j = 1, count_subnode_in_cell(icell)
                ind = start + j -1
                if ( B_matrix%ja( ind ) .eq. inode_sub ) then 
                   found=.true.
                end if
             end do
             ! add subnode index if not added yet
             if ( .not. found ) then
                ind     = start + count_subnode_in_cell(icell)
                B_matrix%ja(ind) = inode_sub
                ! add plus one to nodes  counted for each row
                count_subnode_in_cell(icell) = count_subnode_in_cell(icell) + 1
             end if
          end do
       end if
    end do

    !
    ! sort matrix column-index 
    !
    call B_matrix%sort()

    !
    ! assembly of trija
    !
    do icell_sub = 1, nsubcell
       !
       ! icell father and bounds
       !
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle subnodes
       !
       do k = 1,nnodeincell
          inode_sub = subgrid%topol(k,icell_sub)            
          found=.false.
          j=0
          do while ( .not. found )
             j=j+1
             ind   = start+j-1
             found = ( B_matrix%ja(ind) .eq. inode_sub )
          end do
          trija(k,icell_sub) = ind
       end do
    end do

    deallocate(count_subnode_in_cell,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'assembly_B_matrix_general', &
         'dealloc fail for temp array count_nsubnode_in_tria ')

  end subroutine assembly_B_matrix_subgrid

  !>--------------------------------------------------------
  !> Procedure to assembly sparse matrix
  !> 
  !> $\Matr[\Itd, IPot]{BC} = 
  !>   \int_{\Domain} \testx_{\Itd} |\Grad \Pot|^{\Pgrad-2}< \Grad \Pot, \Grad \testp_{\Ipot}>$ 
  !<--------------------------------------------------------
  subroutine assembly_nodecell_matrix_subgrid(p1p0, velocity, pgrad ,BC_matrix)
    use Globals
    use SparseMatrix
    implicit none

    class(p1p0pair), intent(inout) :: p1p0
    real(kind=double),                intent(in   ) :: velocity(p1p0%grid_pot%logical_dimension,p1p0%grid_pot%ncell)
    real(kind=double),                intent(in   ) :: pgrad
    type(spmat),                      intent(inout) :: BC_matrix
    !local
    integer :: icell_sub,icell_parent,iloc,ind
    integer :: nnodeincell,ndim
    real(kind=double) :: pot_cell(4),grad_cell(3),grad_base(3), area_subgrid
    real(kind=double) :: weight_grad_norm      
    real(kind=double) :: ddot,dnrm2

    nnodeincell = p1p0%p1%grid%nnodeincell
    ndim        = p1p0%p1%grid%logical_dimension

    
    BC_matrix%coeff  = zero
    do icell_sub=1,p1p0%p1%grid%ncell
       ! copy area of triangle in subgrid            
       area_subgrid = p1p0%p1%grid%size_cell(icell_sub)
       weight_grad_norm = dnrm2(ndim, velocity(1:ndim,icell_sub),1)**(pgrad-2.0d0) 

       ! get index of triangle of coaser grid     
       do iloc = 1,nnodeincell
          ! get gradient of local base function
          call p1p0%p1%get_gradbase(iloc,icell_sub, grad_base)

          ! add contribution
          ind=p1p0%assembler_Bmatrix_subgrid(iloc,icell_sub)
          BC_matrix%coeff(ind) = BC_matrix%coeff(ind) + &
               ddot(ndim, velocity(1:ndim,icell_sub),1, grad_base(1:ndim), 1 ) * &
               weight_grad_norm * area_subgrid
       end do
    end do

    BC_matrix%is_symmetric = ( abs(pgrad-2.0d0)<small )

  end subroutine assembly_nodecell_matrix_subgrid


  !>------------------------------------------------------
  !> Prodeduce for the assembly of the siffness
  !> matrix for given Tdens
  !>------------------------------------------------------
  subroutine assembly_stiffness_matrix_p1p0pair(p1p0,lun_err,tdens,stiffness_matrix)
    use Globals
    use Timing
    implicit none
    class(p1p0pair),   intent(inout) :: p1p0
    integer,           intent(in   ) :: lun_err
    real(kind=double), intent(in   ) :: tdens(p1p0%grid_tdens%ncell)
    type(spmat),       intent(inout) :: stiffness_matrix
    
    if ( p1p0%id_subgrid .eq. 0 ) then
       ! assembly
       call p1p0%p1%build_stiff(lun_err, 'csr', tdens(1:p1p0%grid_tdens%ncell), stiffness_matrix)

    else
       ! projection and assembly
       call p1p0%grid_pot%proj_subgrid(tdens,p1p0%aux%raux(1:p1p0%grid_pot%ncell))
       call p1p0%p1%build_stiff(lun_err,&
            'csr', p1p0%aux%raux(1:p1p0%grid_pot%ncell), stiffness_matrix)
    end if


  end subroutine assembly_stiffness_matrix_p1p0pair


    !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(icell):ia(icell+1)-1) [with icell the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine assembly_B_matrix_grid(lun_err,grid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: grid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(3,grid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, inode, j,k,ind,icol
    integer :: nnode, ncell, nnodeincell
    integer :: start, finish,nodes(3)
    integer , allocatable :: nnode_in_cell(:),ia(:),ja(:)


    ncell        = grid%ncell
    nnode        = grid%nnode
    nnodeincell  = grid%nnodeincell

    ! allocate work array 
    allocate(&
         nnode_in_cell(ncell),&
         ia(ncell+1),&
         ja(ncell*3),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'subnode_topol', &
         'alloc fail for temp array nnode_in_cell ia ja')


    call B_matrix%init(lun_err, &
         ncell, nnode, ncell*nnodeincell,&
         storage_system='csr',&
         is_symmetric =.false.)
    
    nnode_in_cell=0
    ia=0
    ja=0

    do icell=1,ncell+1
       ia(icell)=1+(icell-1)*nnodeincell
    end do

    do icell = 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       ja(start:finish) = grid%topol(1:nnodeincell,icell)
       call isort(nnodeincell,ja(start:finish))
    end do

    !
    ! assign ia,ja
    !      
    B_matrix%ia=ia
    B_matrix%ja=ja

    !
    ! assembly of trija
    !
    do icell= 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       nodes=grid%topol(1:nnodeincell,icell)
       do ind=start,finish
          icol=ja(ind)
          k=1
          do while ( icol .ne. nodes(k) )
             k=k+1
          end do
          trija(k,icell) = ind
       end do
    end do



    deallocate(nnode_in_cell,ia,ja,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'subnode_topol', &
         'dealloc fail for temp array nnode_in_cell ia ja')

  end subroutine assembly_B_matrix_grid

  subroutine write4matlab(p1p0,directory)
    use Globals
    use TimeInputs, only : write_steady
    implicit none
    type(dmkp1p0),   intent(inout) :: p1p0
    character(len=256) :: directory
    !local 
    character(len=256) :: fname
    integer :: lun=1000,lun_err=6
    integer :: i,j,iloc,jloc
    type(spmat) :: gradx,grady

    fname=etb(etb(directory)//'stiff.dat')
    open(lun,file=fname)
    call p1p0%stiff%write(lun,'matlab')
    close(lun)

    fname=etb(etb(directory)//'/assembler_stiff.dat')   
    open(lun,file=fname)
    do i=1,p1p0%grid_pot%ncell
       do iloc=1,p1p0%grid_pot%nnodeincell
          do jloc=1,p1p0%grid_pot%nnodeincell
             write(lun,*) p1p0%p1%assembler_csr(jloc,iloc,i)
          end do
       end do
    end do
    close(lun)

    fname=etb(etb(directory)//'/matrix_B.dat')
    open(lun,file=fname)
    call p1p0%B_matrix%write(lun,'matlab')
    close(lun)

    fname=etb(etb(directory)//'/assembler_B.dat')
    open(lun,file=fname)
    do i=1,p1p0%grid_pot%ncell
       write(lun,*) p1p0%assembler_Bmatrix_subgrid(:,i)
    end do
    close(lun)

    fname=etb(etb(directory)//'/sizesubcell.dat')
    open(lun,file=fname)
    call write_steady(6,lun,p1p0%grid_pot%ncell,p1p0%grid_pot%size_cell)
    close(lun)

    fname=etb(etb(directory)//'/sizecell.dat')
    open(lun,file=fname)
    call write_steady(6,lun,p1p0%grid_tdens%ncell,p1p0%grid_tdens%size_cell)
    close(lun)

    call p1p0%p1%gradbase2spmat(lun_err,gradx,grady)
    fname=etb(etb(directory)//'/gradx.dat')
    open(lun,file=fname)
    call gradx%write(lun,'matlab')
    close(lun)

    fname=etb(etb(directory)//'/grady.dat')
    open(lun,file=fname)
    call grady%write(lun,'matlab')
    close(lun)

    !call gradx%DxM(lun_err, p1p0%grid_pot%size_cell)
    !call grady%DxM(lun_err, p1p0%grid_pot%size_cell)


    call gradx%kill(lun_err)
    call grady%kill(lun_err)
    
  end subroutine write4matlab

  
end module DmkP1P0Discretization

   

   
