module DmkInputsData
  use KindDeclaration
  implicit none
  private  
  public :: DmkInputs, DmkInputs_constructor, DmkInputs_destructor,set_optimal_tdens,set_optimal_pot
  !>----------------------------------------------------------------
  !> Structure variable containg the time varing quantities 
  !> (pflux, pmass, decay, kappa, rhs_integrated)
  !> and fix in time quanties 
  !> (tdens0) the ODE  
  !> \begin{gather}
  !>    \Div(\Tdens \Grad \Pot) = \Forcing 
  !>    \quad
  !>    -\Tdens \Grad \Pot \cdot \n_{\partial \Domain} = \Bdflux
  !> \\
  !> \Tdens'=|\Tdens \Grad \Pot(\Tdens)|^\Pflux-kappa*decay*\Tdens^{\Pmass}
  !> \\
  !> \Tdens(tzero) = \Tdens0
  !> \end{gather}
  !> It may contains the optional quantities 
  !> forcing, boundary_flux, optdens(fix initialized and used
  !> only if the correspondet input files exis
  !>------------------------------------------------------------------
  type :: DmkInputs
     !> Logical flag for steady_state data
     logical :: steady=.False.
     !> Identifier of ODE
     !> 1 : PP  (Physarum Policephalum dynamic)
     !> 2 : GF  (Gradient Flow dynamic in Tdens variable)
     !> 3 : GF2 (Gradient Flow dynamic in Gfvar variable)
     integer :: id_ode
     !> Time frames
     real(kind=double) ::  time=zero
     !>-------------------------------------------------------------
     !> Mandatory Input data
     !>-------------------------------------------------------------
     !> Number of variables for Tdens-variables
     !> Continuous DMK : ntdens = cells in grid
     !> Algebraic  DMK : ntdens = columns of the matrix A
     !> Graph      DMK : ntdens = edges in the graph 
     !> Multi-commodity DMK : npot = edge in the graph 
     !> Shape Optimization DMK : ntdens = cells in grid
     integer :: ntdens
     !> Number of variables for Pot-variables
     !> Continuous DMK : npot=nodes in grid or subgrid
     !> Algebraic  DMK : npot=number of rows of the matrix A
     !> Graph      DMK : npot=nodes of the graph 
     !> Multi-commodity DMK : npot=number of graph nodes * number of commodities 
     !> Shape Optimization DMK :
     !> number_of_potentials=(grid or subgrid nodes)*ambient dimension
     integer :: npot
     !> Number of potential to be found
     !> (default=1)
     !> * Continuous DMK : number_of_potentials=1
     !> * Algebraic  DMK : number_of_potentials=1
     !> * Graph      DMK : number_of_potentials=1
     !> * Multi-commodity DMK : number_of_potentials>1
     !> * Shape Optimization DMK : number_of_potentials=ambient dimension
     integer :: number_of_potentials=1
     !>--------------------------------------------------------------
     !> Dirichelet quantities
     !> Number of Dirichlet nodes 
     integer :: ndir=0
     !> Dimension= ndir
     !> Dirichlet node components (x,y,[z]) ( in subgrid ) 
     integer, allocatable :: dirichlet_component(:)
     !> Dimension= ndir
     !> Dirichlet node indeces ( in subgrid ) 
     integer, allocatable :: dirichlet_nodes(:)
     !> Dimension = ndir
     !> Dirichlet values at nodes ( in subgrid )
     real(kind=double), allocatable :: dirichlet_values(:)
     !>------------------------------------------------------------
     !> Lift of tdens in elliptic equation
     !> Dimension=ntdens
     !> -div(\tdens + lambda_lift \grad \pot) = rhs ( continouos )
     !> ( A ( diag(\tdens+lambda_lift)  )A    \pot  = rhs ( discrete )
     real(kind=double), allocatable :: lambda_lift(:)
     !> Lift of tdens in elliptic equation
     !> Dimension=ntdens
     !> -div((beta*\tdens + lambda_lift) \grad \pot) = rhs ( continouos )
     real(kind=double), allocatable :: beta(:)
     !> Lasso relaxation 
     !> (stiff(tdens) +lasso*Id)          \pot = rhs ( continouos )
     !> ( A ( diag(\Tdends) A  +lasso Id )\pot = rhs ( discrete )
     real(kind=double) :: lasso=zero
     !>--------------------------------------------------------------
     !> Sign to tell if is ascending or descending
     real(kind=double) :: direction=one
     !> Pflux power
     !> Contiains 1 real with pflux power
     real(kind=double) :: pflux=one
     !> Time decay
     !> Contiains 1 real with pflux power
     real(kind=double) :: decay=one
     !> Pmass power
     !> Contiains 1 real with pflux power
     real(kind=double) :: pmass=one
     !> ODE expoent for norm of grad
     !> pflux : PP  (Physarum Policephalum dynamic)
     !> 2.0   : GF  (Gradient Flow dynamic in Tdens variable)
     real(kind=double) :: pode=two
     !> Laplacian Smoothing parameter
     !> \sigma in the paper
     !> "Laplacian Smoothing Gradient Descent" by
     !> Osher, Stanley et al.
     real(kind=double) :: sigma_laplacian_smoothing=0.0d0
     !>-----------------------------------------------------
     !> Penalization
     !>-----------------------------------------------------
     !> True/False flag for existence penalization
     logical :: penalty_exists = .False.
     !> Scaling factor for penalty
     !> Contiains 1 real with pflux power
     real(kind=double)  :: penalty_factor=zero
     !> Optional weight $\Wpenalty$ of penalty decay
     !> Give by adding penalty 
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     real(kind=double), allocatable :: penalty_weight(:)
     !> Dimension = ntdens
     !> Optional penalty $\geq 0$ given by adding
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     !> in the minimization problem.
     real(kind=double), allocatable :: penalty_function(:)
     !>-----------------------------------------------------
     !> Dimension = ntdens
     !> Spatial decay 
     !> Contiains ntria-array with spatial decay
     real(kind=double), allocatable :: kappa(:)
     !> Dimension = ntdens
     !> Alessandro Lonardi 
     real(kind=double), allocatable :: psi(:)
     !> Dimension = npot
     !> Rhs in the linear system
     !> arising from equation 
     !> $-\div(\Tdens \Grad \Pot ) = \Forcing$
     !> Completed with proper boundary condition
     real(kind=double), allocatable :: rhs(:)
     !>-------------------------------------------------------
     !> Reference solution
     !>------------------------------------------------------
     !> Flag for existence optimal solution for tdens 
     logical :: optimal_tdens_exists = .False.
     !> Dimension = ntdens
     !> Optimal tdens solution
     !> Settled by setoptdens procedure
     real(kind=double), allocatable :: optimal_tdens(:)
     !>-------------------------------------------------------
     !> Reference solution for potential
     !>------------------------------------------------------
     !> Flag for existence optimal solution for tdens 
     logical :: optimal_pot_exists = .False.
     !> Dimension = npot*number_of_potentials
     !> Optimal tdens solution
     !> Settled by setoptdens procedure
     real(kind=double), allocatable :: optimal_pot(:)
     !> Lam\'e constants for Saint-Venant Equation
     real(kind=double) :: mu_lame=one
     real(kind=double) :: lambda_lame=one
   contains
     !> Static constructor
     !> (public for type DmkInputs)
     procedure, public, pass :: init => DmkInputs_init
     !> Static destructor
     !> (public for type DmkInputs)
     procedure, public, pass :: kill => DmkInputs_kill
     !> Info procedure
     !> (public for type DmkInputs)
     procedure, public, pass :: info=> DmkInputs_info
  end type DmkInputs
  
   type, public :: ProblemData
      !>-------------------------------------------------------------
      !> Mandatory Input data
      !>-------------------------------------------------------------
      !> Nmber of variables for Tdens-variables 
      integer :: ntdens
      !> Nmber of variables for Pot-variables
      integer :: npot
      !>-----------------------------------------------------
      !> Reference solution to compute errors
      !>-----------------------------------------------------
      !> True/False flag for existence of optdens array
      logical :: opt_tdens_exists= .False.
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: opt_tdens(:)
      !> True/False flag for existence of optdens array
      logical :: opt_pot_exists= .False.
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: opt_pot(:)
      !> Initial Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: tdens0(:)
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: pot0(:)
   end type ProblemData


contains
  !>------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type OdeInp)
  !> Instantiate (allocate if necessary)
  !> and initialize variables of type OdeInp
  !>
  !> usage:
  !>     call 'var'%init(lun_err,ntdens,npot,set2default,number_of_potentials)
  !>
  !> where:
  !> \param[in] lun_err              -> integer. Logical unit for error msg.
  !> \param[in] ntdens               -> integer. Dimension of Tdens-variables
  !> \param[in] npot                 -> integer. Dimension of Pot-variables
  !> \param[in] optional,set2defualt -> logical. Flag to set
  !>                                    innput values to defualt = l1-minimizaiton
  !> \param[in] optional, number_of_potentials -> intger. Number of commodity
  !>                                    Default=1
  !<-------------------------------------------------------------
  subroutine DmkInputs_init(this,&
         lun_err, ntdens,npot,set2default,number_of_potentials)
      implicit none
      class(DmkInputs),  intent(inout) :: this
      integer,           intent(in   ) :: lun_err
      integer,           intent(in   ) :: ntdens
      integer,           intent(in   ) :: npot
      logical, optional, intent(in   ) :: set2default
      integer, optional, intent(in   ) :: number_of_potentials

      !local
      logical :: rc
      integer :: res


      this%ntdens = ntdens
      this%npot   = npot
      if (present(number_of_potentials)) this%number_of_potentials = number_of_potentials
      
      

      allocate( &
            this%dirichlet_nodes(this%npot),&
            this%dirichlet_values(this%npot),&
            this%dirichlet_component(this%npot),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in DmkInputs_init'
      this%dirichlet_nodes=0
      this%dirichlet_values=zero
      this%dirichlet_component=0
      
      allocate( &
           this%penalty_function(ntdens),&
           this%penalty_weight(ntdens),&
           this%kappa(ntdens),&
           this%psi(ntdens),&
           this%lambda_lift(ntdens),&
           this%beta(ntdens),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in DmkInputs_init'
      this%psi   = zero 
      
      allocate( &
           this%rhs(npot),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in DmkInputs_init'

      if ( present( set2default) .and.  set2default ) then
         this%time  = zero
         this%ndir  = 0
         this%pflux = one
         this%pmass = one
         this%pode  = two
         this%decay = one
         this%kappa = one
         this%penalty_factor = zero
         this%penalty_function = zero
         this%penalty_weight = zero
         this%lambda_lift  = zero
         this%beta    = one
      end if
         
    end subroutine DmkInputs_init

    !>------------------------------------------------------------
    !> Static desconstructor for Pyhton Wrap
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_constructor(this,&
         lun_err, ntdens,npot,set2default)
      implicit none
      type(DmkInputs),     intent(inout) :: this
      integer,     intent(in   ) :: lun_err
      integer,     intent(in   ) :: ntdens
      integer,     intent(in   ) :: npot
      logical, optional, intent(in ) :: set2default

      call this%init(lun_err,ntdens,npot,set2default)

    end subroutine DmkInputs_constructor


    !>------------------------------------------------------------
    !> Static desconstructor for Pyhton Wrap
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine set_optimal_tdens(this,lun_err, optimal_tdens)
      implicit none
      type(DmkInputs),   intent(inout) :: this
      integer,           intent(in   ) :: lun_err
      real(kind=double), intent(in   ) :: optimal_tdens(this%ntdens)
      !local
      logical :: rc
      integer :: res


      
      this%optimal_tdens_exists =.True.
      allocate(this%optimal_tdens(this%ntdens),stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in setoptimal_tdens'

      
      this%optimal_tdens = optimal_tdens
      
      
    end subroutine set_optimal_tdens


    !>------------------------------------------------------------
    !> Procedure to set optimal solution for potential
    !> (optimal_pot) is available 
    !> (procedure public for type OdeInp)
    !>
    !> usage:
    !>     call set_optimal_pot(this, lun_err,optimal_pot)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !> \param[in] lun_err -> real (dimension=this%npot)
    !<-------------------------------------------------------------
    subroutine set_optimal_pot(this,lun_err, optimal_pot)
      implicit none
      type(DmkInputs),   intent(inout) :: this
      integer,           intent(in   ) :: lun_err
      real(kind=double), intent(in   ) :: optimal_pot(this%npot)
      !local
      logical :: rc
      integer :: res


      
      this%optimal_pot_exists =.True.
      allocate(this%optimal_pot(this%npot),stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in setoptimal_pot'

      
      this%optimal_pot = optimal_pot
      
      
    end subroutine set_optimal_pot
    

    !>------------------------------------------------------------
    !> Static desconstructor.
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_kill(this,lun_err )
      implicit none
      class(DmkInputs),     intent(inout) :: this
      integer,     intent(in   ) :: lun_err
      !local
      integer ::res

      this%ntdens = 0
      this%npot   = 0
      this%ndir   = 0


      deallocate( &
            this%dirichlet_nodes,&
            this%dirichlet_values,&
            this%dirichlet_component,&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'deallocation error in DmkInputs_desctructor'

      deallocate( &
           this%penalty_function,&
           this%penalty_weight,&
           this%kappa,&
           this%psi,&
           this%lambda_lift,&
           this%beta,&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'deallocation error in DmkInputs_desctructor'

      deallocate( &
           this%rhs,&
           stat=res)
      if(res .ne. 0)  write(lun_err,*) 'deallocation error in DmkInputs_desctructor'
    end subroutine DmkInputs_kill

    !>------------------------------------------------------------
    !> Static desconstructor for Pyhton Wrap
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_destructor(this,lun_err )
      implicit none
      type(DmkInputs),  intent(inout) :: this
      integer,          intent(in   ) :: lun_err

      call this%kill(lun_err)
    end subroutine DmkInputs_destructor

    !>------------------------------------------------------------
    !> Information procedure
    !> (procedure public for type DmkInputs)
    !>
    !> usage:
    !>     call 'var'%info(lun_out)
    !>
    !> where:
    !> \param[in] lun_out -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_info(this,lun_out )
      use Globals
      implicit none
      class(DmkInputs),     intent(in) :: this
      integer,              intent(in) :: lun_out
      real(kind=double) :: lb,ub
      character(len=40) :: outformat=' '
      integer :: i
      
      outformat='(2(a,1x,I12,1x))'
      write(lun_out,outformat) &
           'Ntdens =', this%ntdens , &
           'Npot=', this%npot

      if (this%steady) then
         write(lun_out,*) 'Stationary data'
      else
         write(lun_out,*) 'Not stationary data'
      end if
      
      outformat='(3(a,1x,1pe8.2,1x))'
      write(lun_out,etb(outformat)) 'Pflux(beta)=',this%pflux,'Pmass(gamma)=',this%pmass,'Pode=',this%pode

      lb=minval(this%rhs)
      ub=maxval(this%rhs)
      outformat='(1pe9.2,a,1pe8.2)'
      write(lun_out,etb(outformat)) lb,'<=rhs        <=',ub

      lb=minval(this%lambda_lift)
      ub=maxval(this%lambda_lift)
      if ((lb-ub)>small) then
         outformat='(1pe9.2,a,1pe8.2)'
         write(lun_out,etb(outformat)) lb,'<=lambda_lift<=',ub
      end if

      lb=minval(this%kappa)
      ub=maxval(this%kappa)
      !if ((lb-ub)>small) then
         outformat='(1pe9.2,a,1pe8.2)'
         write(lun_out,etb(outformat)) lb,'<=kappa      <=',ub
      !end if

      lb=minval(this%beta)
      ub=maxval(this%beta)
      if ((lb-ub)>small) then
         outformat='(1pe9.2,a,1pe8.2)'
         write(lun_out,etb(outformat)) lb,'<=beta       <=',ub
      end if

      if ( this%ndir >0 ) then
         write(lun_out,*) 'Dirichlet Nodes=', this%ndir
         outformat='(I10,1x,1pe8.2)'
         write(lun_out,*) 'Node index - Dir. value (Only first 2)'
         do i=1,min(this%ndir,2)
            write(lun_out,*) this%dirichlet_nodes(i),this%dirichlet_values(i)
         end do
      end if
    end subroutine DmkInputs_info

   
    
  end module DmkInputsData
