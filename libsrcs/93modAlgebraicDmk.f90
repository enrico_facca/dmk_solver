module AlgebraicDmk
  use Globals
  use Timing
  ! matrices
  use LinearOperator
  use Matrix
  use SimpleMatrix
  use SparseMatrix
  use StiffnessMatrix

  use DenseMatrix
  use BlockMatrix
  ! preconditioner
  use DensePreconditioner
  use StdSparsePrec
  use StiffPrecondioner
  ! 
  use Eigenv
  use Scratch
  use LinearSolver
  use MuffeTiming

  use DmkControls
  use DmkInputsData
  use DmkDiscretization
  use TdensPotentialSystem

  use BackSlashSolver
  
  implicit none
  private
  !>-------------------------------------------------------------------
  !> Structure Variable containg member for the discretization
  !> of PP ODE equation
  !> \begin{align}
  !>   & A^T diag(\Tdens) diag(weight)^{-1} A \Pot = b \\
  !>   & \Tdens'=(\Tdens |diag(weight)^{-1} A \Pot|)^{\Pflux}
  !>              -decay kappa \Tdens^Pmass
  !> \end {align}
  !> or the gradient flow equation
  ! \begin{align}
  !>   & A^T diag(\Tdens) diag(weight)^{-1} A \Pot = b \\
  !>   & \Tdens'=\Tdens^{\Pflux} |diag(weight)^{-1} A \Pot|^2
  !>              -decay kappa \Tdens^Pmass
  !> \end {align}
  !> It contains the structure variable odein with the inputs of the ODE
  !>-------------------------------------------------------------------
  public :: admk
  type, extends(dmkpair) :: admk
     !>-----------------------------------------------------------------
     !> Array length of tdens and pot
     !> (So far ntdens > npot) thus the matrix is this form
     !>   (           )
     !>   (     A     )
     !>   (           )
     !> Length of tdens array
     integer :: ntdens
     !> Length of pot array
     integer :: npot
     !> Matrix with 
     !> ntdens rows
     !> npot   columns
     class(abs_linop), pointer :: matrixAT => null()
     !> Matrix with 
     !> ntdens rows
     !> npot   columns
     class(abs_linop), pointer :: matrixA => null()
     type(TransposeMatrix) :: implicit_matrixAT
     type(spmat) :: explicit_sparse_matrixAT
     !> Weigth 
     !> Dimension(ntdens)
     real(kind=double), allocatable :: weight(:)
     !> Inverse Weigth 
     !> Dimension(ntdens)
     real(kind=double), allocatable :: inv_weight(:)
     !>----------------------------------------------------------------
     !> Derived varaibles 
     !>----------------------------------------------------------------
     !> Dimension(ntdens)
     !>  grad = W^{-1} A \Pot
     real(kind=double), allocatable :: grad(:)
     !> Dimension(ntdens)
     !> flux = \Tdens  \Grad 
     real(kind=double), allocatable :: flux(:)
     !> Dimension(ntdens)
     !> Norm the gradient over the triangles
     real(kind=double), allocatable :: nrm_grad(:)
     !> Dimension(ntdens)
     !> Norm the gradient over the triangles
     real(kind=double), allocatable :: nrm2_grad(:)
     !>-----------------------------------------------------------------
     !> Linear system variables
     !>-----------------------------------------------------------------
     !> Number of Dirichlet points
     !> Fixed to one to cope the singularity of Pure Neumann Problem
     integer :: ndir=1
     !> Dirichlet points
     !> Fixed in cope_singularity
     integer, allocatable :: noddir(:)
     !> Stiff matrix from 
     !> $ A^T diag(\Tdens) \Diag(L)^{-1} A$ 
     type(stiffmat) :: stiff
     !> Assembled stiff matrix 
     !real(kind=double), allocatable :: assembled_stiff(:,:)
     type(densemat) :: assembled_stiff
     !> Choelesky Factor of Stiff matrix
     !real(kind=double), allocatable :: assembled_stiff(:,:)
     type(denseprec) :: cholesky_stiff
     !> Preconditioner for stiffnes matrix
     !> A^T D A
     type(stiffprec)  :: prec_stiff
     !> Preconditioner for stiffnes matrix
     !> A^T D A
     type(stiffprec)  :: prec_jacobian
     !> Back up Eigen. conteiner for DACG procedure
     type(eigen) :: eigen_matrix_saved
     !> Eigen. conteiner for LANCZOS procedure
     type(eigen) :: eigen_precmatrix
     !> Back up Eigen. conteiner for LANCZOS procedure
     type(eigen):: eigen_precmatrix_saved
     !>----------------------------------------------------
     !> Local copy of linear solver  controls
     type(input_solver) :: ctrl_solver
     !> Info/outputs for linear solver 
     type(output_solver) :: info_solver
     !> Flag for error in preconditioner building
     !>   info_prec=0 => no errors
     !> other flags>0 are descibed in precondioner modules
     integer :: info_prec
     !> Flag for error in tuning building
     !>   info_prec=0 => no errors
     !> other flags>0 some errors occurs
     integer :: info_tuning
     !> Logical unit for statistic file
     integer :: lun_stat
     !> Variation of tdens
     real(kind=double) :: var_tdens=1e30
     !>----------------------------------------------------------
     !> Sycronization checks
     !> Flag for assembly or not the rhs
     logical :: rhs_assembly=.true.
     !> Error flag if the variable 
     !> Tdens Pot ODEin and Grad variable
     !> at the same time 
     logical :: tdpot_syncr=.false.
     !> Error flag if the variable 
     !> Tdens Pot ODEin and Grad variable
     !> at the same time 
     logical :: all_syncr=.false.
     !>--------------------------------------------------
     !> Scratch varibles for storing 
     integer :: temporal_iteration=0
     !> Averaged cgs iterations for time step
     integer :: iter_media
     !> Total cgs iterations 
     integer :: total_iteration
     !> Scratch varible to control the calulation of prec.
     integer :: last_iter_prec
     !> Scratch varible to control the calulation of prec.
     integer :: ref_iter
     !> Scratch variable to store number of nonlinear solver failure
     integer :: failure_nonlinear=0
     !> Scratch variable to store number of krylov iterations wasted
     integer :: waste_nonlinear=0
     !>--------------------------------------------------------
     !> Info for non-linear solver
     !> Number of fix point iteration
     integer :: iter_nonlinear
     !> Linear system solver info
     !> Dimension(max_nonlinear_iterations)
     type(output_solver), allocatable :: sequence_info_solver(:)
     !>
     integer :: iter_first_system
     !> Controls of preconditioner construction
     integer, allocatable :: sequence_build_prec(:)
     !> Controls of preconditioner construction
     integer, allocatable :: sequence_build_tuning(:)
     !> Number of fix point iteration
     integer :: nfillin_used
     !> Constant of contraction of fix point iteration
     real(kind=double) :: cnst_nonlinear
     !> Scratch var_tdens
     real(kind=double) :: loc_var_tdens
     !> Scratch array for PCG Procedure
     type(scrt) :: aux_bicgstab
     !> Dimension (npot)
     !> Work array form rhs of linear system
     real(kind=double), allocatable :: rhs(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: rhs_ode(:)
     !> Dimension (npot)
     !> Genereral Work array form update procedure
     real(kind=double), allocatable :: scr_npot(:)
     !> Dimension (ntdens)
     !> Genereral Work array form update procedure
     real(kind=double), allocatable :: scr_ntdens(:)
     !> Dimension (ntdens)
     !> Genereral Work array form update procedure
     real(kind=double), allocatable :: scr_nfull(:)
     !>--------------------------------------------------------
     !> Newton variables for Implcit Euler time-stepping
     !>--------------------------------------------------------
     !> Reduced jacobian J = stiff(tdens_tilde)
     type(stiffmat) :: jacobian_reduced
     !>--------------------------------------------------------
     !> Dense jacobian component 
     !> J_F=(A^t 0 ) (D(tdens)   InvW   D(grad)) (A 0 )
     !>     (0   Id) (-dt D1D2DG InvW    D3    ) (0 Id)
     !> Idendity of size ntdens
     type(scalmat) :: eye_ntdens
     !> Diagonal (1,1)
     type(diagmat) :: diag_tdens
     !> Diagonal (1,2)
     type(diagmat) :: diag_grad
     !> Diagonal (2,1)
     type(diagmat) :: diag_dtD1D2DG
     !> Diagonal (2,2)
     type(diagmat) :: diag_D3
     type(scalmat) :: identity_ntdens
     !>--------------------------------------------------------
     !> Left Component
     type(blockmat) :: AT_eye
     !> Central componet
     type(blockmat) :: diagonals
     !> Right Component
     type(blockmat) :: A_eye
     !>--------------------------------------------------------
     !> Dense jacobian    J = ( AT_eye diags A_eye)
     type(pair_linop) :: jac5
     !> block preconditioner 
     !>
     !> prec = ( P(stiff) 0    )
     !>        ( 0        invD3)
     type(block_linop) :: jacobian_prec
     !> Diagonal Preconditioner
     type(diagmat)  :: prec_invD3
     !> Scratch varaibles
     type(scrt) :: scr_newton
     !>xs------------------------------------------------------
     !> Full jacobian    J = ( AT_eye diags A_eye)
     type(new_linop) :: Jmat
     type(diagmat) :: W11,W12,W21,W22
     type(block_linop) :: Wmat
     type(diagmat) :: invW11,invW12,invW21,invW22
     type(block_linop) :: invW
     


     !> Preconditoner for Laplacian matrix ATA
     type(stdprec)  :: prec_sparse_stiff
     !> Laplacian matrix ATA
     type(spmat)  :: laplacian
     !> Preconditoner for Laplacian matrix ATA
     type(stdprec)  :: prec_laplacian
     !> Preconditoner for Laplacian matrix ATA
     type(inverse)  :: inverse_laplacian
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     !type(spkernel) :: near_kernel
     !> ( (ATA)^{-1} 0 )
     !> ( 0          I )
     type(block_linop) :: invLAPL
     !> ( (ATA)^{-1} 0 ) ( AT 0 ) invW ( A 0 ) ( (ATA)^{-1} 0 )
     !> ( 0          I ) ( 0  I )      ( 0 I ) ( 0          I ) 
     type(new_linop) :: tildeJ
     !> Helman prec with V^{-1}
     type(diagmat) :: V11,V12,V21,V22
     type(block_linop) :: Vmat
     type(diagmat) :: invV11,invV12,invV21,invV22
     type(block_linop) :: invV
     type(inverse)  :: inverse_stiff
     type(block_linop) :: invSTIFF
     !> ( (ATA)^{-1} 0 ) ( AT 0 ) invW ( A 0 ) ( (ATA)^{-1} 0 )
     !> ( 0          I ) ( 0  I )   
     
     !> ( sqrt(tdens) 0 )
     !> ( 0           I )
     type(diagmat) :: matrix_sqrt_tdens
     type(block_linop) :: matrix_SQTD_EYE
     
     !> ( L^{-1/2}    0       )
     !> ( 0           L^{1/2} )
     
     type(block_linop) :: matrix_mass

    
     type(diagmat) :: matrix_weight
     type(diagmat) :: matrix_inverse_weight
     type(diagmat) :: matrix_sqrt_weight
     type(diagmat) :: matrix_inverse_sqrt_weight
     
     !> Preconditioner
     !> inv_LAPL*matrix_mass*W^{-1} *matrix_mass*inv_LAPL
     type(pair_linop) :: helmanW
     !> inv_stiff*matrix_mass*SQTD_EYE*invV*SQTD_EYE*matrix_mass*inv_stiff
     type(pair_linop) :: helmanV

     type(CodeTim) :: CPU
     !>----------------------------------------------------------
     !> Cut procedure 
     !> Number of non zeros component
     integer :: nnz_tdens
     !> Non zeros element
     !> initial select=(1,2,...,ntdens)
     integer, allocatable :: selection_tdens(:)
     !> Non zeros element
     !> initial select=(1,2,...,ntdens)
     integer, allocatable :: complement_tdens(:)

     !> Krylov-based  solve for matrices for size npot
     type(inverse) :: krylov_inverse_npot
     !> Sparse stiffness
     type(inv)   :: inv_sparse_stiff
     type(spmat) :: formed_sparse_stiff
     class(abs_linop), pointer :: prec_pointer=>null()
   contains
     !> Static constructor 
     !> (procedure public for type admk)
     procedure, public , pass :: init => init_admk
     !> Static destructor
     !> (procedure public for type admk)
     procedure, public , pass :: kill => kill_admk
     !> Procedure for upate the whole system
     !> to the next time step. Interface defined in
     !> abstact type dmkpair.
     !> (procedure public for type dmkpair)
     procedure, public, pass :: syncronize_tdpot
     !> Procedure to estimate TdensPotential Variation
     !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$.
     !> Interface defined in abstact type dmkpair.
     !> (procedure public for type specp0_space_discretization)
     procedure, public , pass :: compute_tdpot_variation
     !> Abstract procedure to override
     !> Procedure for upate the whole system
     !> to the next time step.
     !> Interface defined in abstact type dmkpair.
     !> (procedure public for type dmkpair)
     procedure, public , pass :: update_tdpot
     !>-------------------------------------------------------
     !> TIME-STEPPING PROCEDURES
     !>-------------------------------------------------------
     !> Update Procedure via Forward Euler time-stepping
     !> for tdens variable. 
     !> (procedure public for type admk)
     procedure, private , pass :: explicit_euler_tdens
     !> Update Procedure via Forward Euler time-stepping
     !> for gfvar variable. Gradient Descent Dynamics.
     !> (procedure public for type admk)
     procedure, private , pass :: explicit_euler_gfvar
     !> Update Procedure via backward Euler time-stepping
     !> for gfvar variable. Non linearity is
     !> solved via Newton-Raphson method
     !> (procedure public for type admk)
     procedure, private, pass :: new_implicit_euler_newton_gfvar
     !> Update Procedure via backward Euler time-stepping
     !> for tdens variable. Non linearity is
     !> solved via Newton-Raphson method
     !> (procedure public for type admk)
     procedure, private, pass :: new_implicit_euler_newton_tdens
     !> Procedure to set active and inactive tdens and potential
     !> variable.
     !> (procedure public for type admk)
     procedure, public , pass :: set_active_tdpot
     !> Update Procedure via backward Euler time-stepping
     !> for Tdens variable. Non linearity is
     !> solved via Newton-Raphson method
     !> (procedure public for type admk)
     !procedure, private, pass :: implicit_euler_newton_tdens
     !> Update Procedure via Accelerated Gradient Descent
     !> for Tdens variable.
     !> (procedure private for type admk)
     !procedure, public , pass :: accelerated_gradient_descent_tdens
     !>--------------------------------------------------------------
     !> Scalar functions
     !>--------------------------------------------------------------
     !> Function eval $\sum \tdens $
     !> (procedure public for type admk)
     !procedure, public , pass :: mass
     !> Scalar functions
     !> Function eval $\sum \tdens^{\WPower{\Pflux,\Pmass}$
     !> (procedure public for type admk)
     procedure, public , pass :: weighted_mass
     !> Function eval $\int \tdens |\nabla u |^2 $
     !> (procedure public for type admk)
     procedure, public , pass :: energy
     !> Function eval $\Ene(\tdens) + \Wmass(\tdens)$
     !> (procedure public for type admk)
     procedure, public , pass :: lyapunov
     !> Function eval |Aq -b| /|b|
     !> (procedure public for type admk)
     !procedure, public , pass :: linear_residum
     
     !> Function eval $\int \tdens log( \tdens ) $
     !> (procedure public for type admk)
     !procedure, public , pass :: ent_tdens
     !> Function eval $\sup \tdens * |nrm_grad| $
     !> (procedure public for type admk)
     !procedure, public , pass :: supflux
     !> Function eval 
     !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$
     !> (procedure public for type admk)
     !procedure, public , pass :: eval_var_tdens
     !> Function evaluating candidate branch exponet 
     !> (procedure public for type admk)
     !procedure, public , pass :: pvel_exponent
     !> Function evaluating candidate branch exponet 
     !> (procedure public for type admk)
     !procedure, public , pass :: exponent_wmass
     !> Function evaluating
     !> $\int_{\Omega}|q|^{power}$
     !> (procedure public for type admk)
     !procedure, public , pass :: integral_flux
     !> Function evaluating
     !> $\int_{\Omega} u f $
     !> (procedure public for type admk)
     !procedure, public , pass :: integral_potforcing
     !> Function evaluating
     !> $\int_{\Omega}|q|^{power}- uf $
     !> (procedure public for type admk)
     !procedure, public , pass :: duality_gap
     !> Function evaluating
     !> PODE=pflux for PP dyanmic (id_ode .eq. 1)
     !> PODE=2.0   for GF dyanmic (id_ode .eq. 2)
     !> (procedure public for type admk)
     !procedure, public , pass :: eval_pode
     !> Procedure to convert tdens into gfvar
     !> (procedure public for type admk)
     !procedure, public , nopass :: td2gf
     !> Procedure to convert gfvar into td
     !> (procedure public for type admk)
     !procedure, public , nopass :: gf2td
     !> Procedure to compute all avalable scalr function
     !> (procedure public for type admk)
     procedure, public , pass :: compute_functionals
     !-------------------------------------------------------
     ! Subroutine for solving the elliptic equation 
     ! and related quanties
     !>-------------------------------------------------------
     !> Procedure for interfacing with linear solver subrotine
     !> with : 1 - diagonal scaling 
     !>        2 - preconditioner build and back-up
     !> (procedure public for type admk)
     !procedure, public , nopass :: my_linear_solver
     !> Procedure to obtain the solution pot that solves
     !>   $-\Div ( \tdens \nabla u) = f $
     !> once tdens is given
     !> (procedure public for type admk)
     !procedure, public , pass :: tdens2pot
     !> Procedure for building variable $\Grad \Pot$
     !> (procedure public for type admk)
     procedure, public , nopass :: build_grad
     !> Procedure for building variable $\Grad \Pot$
     !> (procedure public for type admk)
     !procedure, public , nopass :: compute_flux
     !> Procedure for building
     !> \Grad(\Pot) |\Grad(\Pot)| |\Grad(\Pot)|^2
     !> (procedure public for type admk)
     !procedure, public , pass :: build_grad_vars

     !> Subroutine assembly preconditioner for
     !> matrix A weight AT 
     !> according to prec. controls
     procedure, private , nopass :: assembly_prec
     !> Subroutine to assembly tuning for 
     !> preconditioner from sparse matrix
     !> according to tuning controls
     !procedure, private , nopass :: assembly_tuning
     !> Subroutine to assembly matrices and vectors
     !> in the jacobian arasing in the 
     !> newton procedure for implicit Euler time-stepping
     !procedure, private , pass :: assembly_newton
     !> Subroutine to assembly matrices and vectors
     !> in the newton procedure 
     !> for implicit Euler time-stepping
     !> of the Gradient Flow variable
     !procedure, private , pass :: assembly_newton_gf
     !---------------------------------------------------
     !> Procedure for upate the wholw system
     !> to the next time step
     !> (procedure public for type admk)
     !procedure, public , pass :: update
     !> Procedure for upate the wholw system
     !> to the next time step
     !> (procedure public for type admk)
     !procedure, public , nopass :: control_deltat
     !> Procedure evulate nnz zero tdens
     !> (procedure public for type admk)
     !procedure, public , nopass :: build_selection
     !> Procedure evulate lasso parameter
     !> (procedure public for type admk)
     !procedure, public , pass :: set_lasso
     !> Procedure to set controls (time-step, preconditioner construction
     !> etc.) for next update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: set_controls_next_update
     !> Procedure to reset controls 
     !> (time-step, preconditioner construction etc.) 
     !> in case of failure in update
     !> (procedure public for type specp0_space_discretization)
     !> Override procedure of type dmkpair
     procedure, public , pass :: reset_controls_after_update_failure
     !> Procedure to compute gradient w.r.t. gfvar
     !> variable of the Lyapunov unctional
     !> (procedure public for type admk)
     procedure, public, pass :: assembly_gradient_lyapunov_gfvar
     !> Procedure to compute rhs of ode
     !> of tdens dynamics
     !> (procedure public for type admk)
     procedure, public, pass :: assembly_mirrow_gradient_lyapunov_tdens
     !> Procedure to compute rhs of ode
     !> of tdens dynamics
     !> (procedure public for type admk)
     procedure, public, pass :: assembly_mirrow_hessian_lyapunov_tdens
     !> Procedure setting functional at
     !> given time iteration and state of tdpot
     !> (procedure public for type evolfun)
     procedure, public, pass :: store_evolution !=> set_evolfun
  end type admk
contains
  !>--------------------------------------------------------------------
  !> Static Constructor, given ctrl and two meshes
  !> (public procedure for type admk)
  !> 
  !> usage: call var%init(&
  !>                ctrl,&
  !>                matrixAT,&
  !>                weight)
  !> where:
  !> \param[in ] ctrl          -> type(DmkCtrl). Controls variables
  !> \param[in ] matrixAT      -> class(abs_matrix). General matrix
  !> \param[in ] (opt.) weight -> real. Dimension(matrixAT%nrow)
  !>---------------------------------------------------------------------
  subroutine init_admk(this,&
       ctrl,&
       matrixA,weight,kernel_matrixA_T)
    use Globals
    use Matrix
    
    implicit none
    class(admk),  target,         intent(inout) :: this
    type(DmkCtrl),                intent(in   ) :: ctrl
    class(abs_matrix), target,    intent(in   ) :: matrixA
    real(kind=double),            intent(in   ) :: weight(matrixA%ncol)
    class(abs_matrix), target, optional,intent(in   ) :: kernel_matrixA_T
    
    !local
    logical :: rc, endfile
    integer :: res
    integer :: lun_err,lun_out,lun_stat
    integer :: ntdens, ncell_pot, npot,nfull
    integer :: info_prec
    integer :: i,j,start,end,icell_sub
    integer,allocatable :: ntemp(:), temp(:,:)
    
    ! newton varaibles
    ! 
    type(array_mat)  :: A_eye_component(2)
    integer          :: A_eye_structure(3,2)
    character(len=1) :: A_eye_direction(2)
    type(array_mat)  :: AT_eye_component(2)
    integer          :: AT_eye_structure(3,2)
    character(len=1) :: AT_eye_direction(2)
    type(array_mat)  :: diagonals_component(4)
    integer          :: diagonals_structure(3,4)
    character(len=1) :: diagonals_direction(4)
    type(array_mat)   :: jacobian_component(3)
    character(len=1)  :: jacobian_direction(3)

    type(array_linop) :: Jmat_component(3)
    integer            :: Jmat_structure(3,4)

    type(array_linop)  :: W_component(4)
    integer            :: W_structure(3,4)

    type(array_linop)  :: blockdiag_component(2)
    integer            :: blockdiag_structure(3,2)

    type(array_linop)  :: invW_component(4)
    integer            :: invW_structure(3,4)

    type(array_linop)  :: invLAPL_component(2)
    integer            :: invLAPL_structure(3,2)

    !
    type(array_linop) :: jacobian_prec_component(2)
    integer           :: jacobian_prec_structure(3,2)

    type(array_linop)  :: tildeJ_component(5)
    integer          :: tildeJ_structure(2,5)

    type(array_linop)  :: jac5_component(5)


    type(array_linop)  :: V_component(4)
    integer            :: V_structure(3,4)

    type(array_linop)  :: invV_component(4)
    integer            :: invV_structure(3,4)

    type(array_linop)  :: helman_component(9)
    integer          :: helman_structure(2,9)
    integer :: dim_work,info
    
    type(spmat) :: AT

    type(input_solver) :: ctrl_solver

    type(spmat) :: copy_laplacian
    type(input_prec) :: ctrl_prec

    real(kind=double)  :: alphas(10)
    
    lun_err  = ctrl%lun_err
    lun_out  = ctrl%lun_out
    lun_stat = ctrl%lun_statistics

    ! point matrix A
    this%matrixA => matrixA
    
    ! set matrix AT

    
    select type( matrixA )
    type is (spmat)
       !
       ! in this case we explicitely form the transposed matrix
       !
       this%explicit_sparse_matrixAT = matrixA
       call this%explicit_sparse_matrixAT%transpose(lun_err)
       this%matrixAT => this%explicit_sparse_matrixAT
    class default
       !
       ! use implicit transposition
       !
       call this%implicit_matrixAT%init(matrixA)
       this%matrixAT => this%implicit_matrixAT
    end select
    this%matrixA%name='A'
    this%matrixAT%name='AT'
    
    
    !local size of array
    ntdens    = this%matrixAT%nrow
    npot      = this%matrixAT%ncol
    nfull     = ntdens+npot

    this%ntdens    = ntdens
    this%npot      = npot

    if ( ctrl%debug .eq. 1 ) write( lun_out, *) ' Tdpot system : Allocations'
    ! system real variables
    allocate (&
         this%weight(ntdens),&
         this%inv_weight(ntdens),&
         this%grad(ntdens),&
         this%flux(ntdens),&
         this%nrm_grad(ntdens),&
         this%nrm2_grad(ntdens),&
         stat=res)  

    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'system_tdenspot', &
         ' member tdens, pot, grad , nrm_grad',res)

    ! copy weight
    this%weight     =  weight
    this%inv_weight = one / this%weight


    this%tdpot_syncr = .false.
    this%all_syncr = .false.

    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    if ( ctrl%debug .ge. 1 ) write( lun_out, *) &
         ' Tdpot system : Init linear system variables'
        
    !call this%stiff%init(lun_err,this%matrixAT,this%inv_weight)

    allocate (&
         this%noddir(this%ndir),&
         stat=res)  
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_admk', &
         ' member noddir',res)

    !
    ! create work space for krylov inverse for matrix of size _npot
    !
    call this%krylov_inverse_npot%init(lun_err,npot)
    
    !
    ! 
    !
    call ctrl_prec%init(ctrl%lun_err,&
         ctrl%outer_prec_type,&
         ctrl%outer_prec_n_fillin, &
         ctrl%outer_prec_tol_fillin)

    
    call this%prec_stiff%init(lun_err,&
            this%stiff, ctrl_prec, info_prec)
    if ( ctrl%debug .ge. 1 ) write( lun_out, *) &
         ' Tdpot system : PREC STIFF INIT DONE '
    
    call this%prec_jacobian%init(lun_err,&
            this%stiff, ctrl_prec, info_prec)
    
    if ( ctrl%debug .ge. 1 ) write( lun_out, *) &
         ' Tdpot system : PREC JACOBIAN INIT DONE '



    !
    ! linear solver info
    !
    allocate(&
         this%sequence_info_solver(ctrl%max_nonlinear_iterations),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_admk', &
         ' type admk member sequence_info_solver')
    allocate(&
         this%sequence_build_prec(ctrl%max_nonlinear_iterations),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_admk', &
         ' type admk member sequence_info_solver')
    allocate(&
         this%sequence_build_tuning(ctrl%max_nonlinear_iterations),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_admk', &
         ' type admk member sequence_info_solver')


    !
    ! Varaibles for PCG Procedure    
    if ( ctrl%debug .eq. 1 ) write( lun_out, *) &
         ' Tdpot system : Init auxiliary vars.'
    
    ! Auxiliary var for PCG procedure
    nfull=npot+ntdens
    dim_work=&
         nfull*(ctrl%krylov_nrestart+7)+&
         (ctrl%krylov_nrestart+1)*(ctrl%krylov_nrestart+2)+&
         nfull*ctrl%krylov_nrestart
    call this%aux_bicgstab%init(lun_err, 0, dim_work)
    allocate(&
         this%rhs(npot),&
         this%rhs_ode(ntdens),&
         this%scr_npot(npot),&
         this%scr_ntdens(ntdens),&
         this%scr_nfull(npot+ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, &
         ' init_admk', &
         ' type admk member rhs')

    !this%prec_saved=this%prec
    if ( ctrl%debug .eq. 1 ) write( lun_out, *) &
         ' Tdpot system : Init Eigenv. '

    !
    ! init varaibles to avoid overhead in newton
    
    ! 
    !   jacobian reduced = stiff(\tilde{tdens})
    !
    !call this%jacobian_reduced%init(lun_err,&
    !     this%matrixAT,this%inv_weight)

    
    ! Full jacobian
    !
    ! J_F=( AT_eye diagonals A_eye) 
    ! 

    ! TODO remove AT_eye matrix and use better composed_matrix
    ! set  block structure

    call this%eye_ntdens%eye(ntdens)
    call this%eye_ntdens%eye(lun_err)
    !
    ! (AT  0)
    ! (0   I)
    !    
    ! A^T matrix in 1,1
    !AT_eye_component(1)%mat => this%matrixAT 
    !AT_eye_structure(:,1) = (/1,1,1/) 
    !AT_eye_direction(1) = 'T'

    ! I_ntdens   matrix in 2,2
   ! AT_eye_component(2)%mat => this%eye_ntdens
    !AT_eye_structure(:,2) = (/2,2,2/) 
    !AT_eye_direction(2) = 'N'

!!$    if ( ctrl%debug .eq. 1 ) &
!!$         write( lun_out, *) ' Init AT_eye block matrix '
!!$    call this%AT_eye%init(&
!!$         lun_err,&
!!$         2, AT_eye_component,&
!!$         2 , 2, 2, &
!!$         AT_eye_structure,AT_eye_direction,&
!!$         .false.)
!!$
!!$    call this%AT_eye%info(lun_err)
    
    !
    ! set  block structure
    !
    ! A_eye= (A  0)
    !        (0  I)
    !
    if ( ctrl%debug .eq. 1 ) &
         write( lun_out, *) ' Init A_eye block matrix '
    
    ! A matrix in 1,1
!!$    A_eye_component(1)%mat => this%matrixAT 
!!$    A_eye_structure(:,1) = (/1,1,1/) 
!!$    A_eye_direction(1) = 'N'
!!$
!!$    ! I_ntdens   matrix in 2,2
!!$    A_eye_component(2)%mat => this%eye_ntdens
!!$    A_eye_structure(:,2) = (/2,2,2/) 
!!$    A_eye_direction(2) = 'N'
!!$
!!$    call this%A_eye%init(&
!!$            lun_err,&
!!$            2, A_eye_component,&
!!$            2 , 2, 2, &
!!$            A_eye_structure,A_eye_direction,&
!!$            .false.)
!!$
!!$    call this%A_eye%info(lun_err)

    
    
    ! set A_eye (and jacobian) kernel
    !
!!$    if ( this%matrixAT%dim_kernel .gt. 0) then
!!$       this%A_eye%kernel(1:npot,:) = this%matrixAT%kernel(:,:)
!!$       this%A_eye%kernel(npot+1:nfull,:) = zero
!!$    end if

    !
    ! Set full diagonals block matrix
    !
    ! (D(tdens)  D(grad))
    ! (-dtD1D2DG D3     )
    !
    !
    ! initialize all componets of daigonals matrix
    !
    call this%diag_tdens%init(lun_err, ntdens)
    call this%diag_grad%init(lun_err, ntdens)
    call this%diag_dtD1D2DG%init(lun_err, ntdens)
    call this%diag_D3%init(lun_err, ntdens)

    ! D(tdens)   matrix in 1,1
    diagonals_component(1)%mat => this%diag_tdens 
    diagonals_structure(:,1) = (/1,1,1/) 
    diagonals_direction(1)='N'

    ! D(grad)    matrix in 1,2 
    diagonals_component(2)%mat => this%diag_grad
    diagonals_structure(:,2) = (/2,1,2/) 
    diagonals_direction(2)='N'

    ! -dt D1D2DG matrix in 2,1
    diagonals_component(3)%mat => this%diag_dtD1D2DG
    diagonals_structure(:,3) = (/3,2,1/) 
    diagonals_direction(3)='N'

    ! D3       matrix in 2,2
    diagonals_component(4)%mat => this%diag_D3
    diagonals_structure(:,4) = (/4,2,2/) 
    diagonals_direction(4)='N'

    if ( ctrl%debug .eq. 1 ) &
         write( lun_out, *) ' Init diagonals block matrix '
    call this%diagonals%init(&
         lun_err,&
         4, diagonals_component,&
         2 , 2, 4, &
         diagonals_structure, diagonals_direction, &
         .false.)
   
    
    if ( ctrl%debug .eq. 1 ) &
         write( lun_out, *) ' Init Jacobian tdens block matrix '


    
    !
    ! block precondioner
    !
    
!!$    ! 
!!$    ! ( P(stiffness) 0      )
!!$    ! ( 0            inv_D3 )
!!$    !
!!$    call this%prec_invD3%init(lun_err,ntdens)
!!$
!!$    jacobian_prec_structure(:,1) = (/1,1,1/)
!!$    jacobian_prec_component(1)%linop => this%prec_stiff
!!$
!!$    jacobian_prec_structure(:,2) = (/2,2,2/)
!!$    jacobian_prec_component(2)%linop => this%prec_invD3
!!$
!!$    if ( ctrl%debug .eq. 1 ) &
!!$         write( lun_out, *) ' Init Jacobian Prec. '
!!$    call this%jacobian_prec%init(lun_err, &
!!$         2, jacobian_prec_component,&
!!$         2, 2, &
!!$         2, jacobian_prec_structure,&
!!$         .True.)


    !
    !  scr_newton
    !
    call this%scr_newton%init(lun_err,0,10*ntdens+5*npot+2*nfull)

    select type(mat => this%matrixAT)
    type is (spmat)
       if ( ctrl%debug .eq. 1 ) &
         write( lun_out, *) ' Init Laplacian '
       !
       ! laplacian = div * \grad = AT L^{-1} A = E ET 
       !
       AT=mat
       call AT%transpose(lun_err)
       this%scr_ntdens=one
       call this%laplacian%MULT_MDN(lun_err, AT , mat, &
               ntdens, 100*ntdens ,this%inv_weight)

       !
       ! assembly implicit inverse of laplacian
       !
       ! assembly prec
       if ( ctrl%debug .eq. 1 ) &
            write( lun_out, *) ' init inverse laplacian '
       copy_laplacian=this%laplacian
       do i=1,npot
          j= copy_laplacian%idiag(i)
          copy_laplacian%coeff(j)=copy_laplacian%coeff(j)+1d-6
       end do       
       call this%prec_laplacian%init(lun_err, info_prec,&
              ctrl_prec, this%laplacian%nrow,copy_laplacian )
       ! init inverse 
       ctrl_solver%scheme='PCG'
       ctrl_solver%iexit=1
       ctrl_solver%isol=1
       ctrl_solver%tol_sol=1d-14
       ctrl_solver%iort=1
       !call this%near_kernel%init(lun_err,npot)
       !call this%near_kernel%set(0, (/1/))
       
    end select
    
    !
    ! selections procedure
    !
    allocate(&
         this%complement_tdens(ntdens),&
         this%selection_tdens(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_admk', &
         ' type admk member selection_tdens')
    
    this%nnz_tdens = ntdens
    do i=1, ntdens
       this%selection_tdens(i) = i
    end do
    this%complement_tdens = 0
    

  end subroutine init_admk


  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type admk)
  !>
  !> usage:
  !>     call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !<-----------------------------------------------------------    
  subroutine kill_admk(this,lun_err)     
    use Globals
    implicit none
    class(admk), intent(inout) :: this
    integer,         intent(in   ) :: lun_err
    !local
    integer :: res
    logical :: rc


    call this%aux_bicgstab%kill(lun_err)

    deallocate ( this%noddir, stat=res)  
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
         ' system_tdenspot', &
         ' member noddir',res)


    call this%stiff%kill(lun_err)
    !call this%assembled_stiff%kill(lun_err)


    deallocate(&        
         this%grad,&
         this%flux,&
         this%nrm_grad,&
         this%nrm2_grad,&
         this%rhs,&
         this%rhs_ode,&
         this%scr_npot,&
         this%scr_ntdens,&
         this%scr_nfull,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_admk', &
         ' dealloc failed for type admk meber'//&
         ' member tdens, pot, nrm_grad, nrm2_grad'//&
         ' lyap_tria'&
         ,res)

    !
    ! selections procedure
    !
    deallocate(&
         this%complement_tdens,&
         this%selection_tdens,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'init_admk', &
         ' type admk member selection_tdens')

  end subroutine kill_admk


  
  !>--------------------------------------------------------------------
  !> Procedure for the syncronization od tdens potential variable
  !> 
  !> (public procedure for type dmkpair)
  !> 
  !> usage: call var%syncronization(tdpot,ode_inputs,ctrl,info)
  !> 
  !> where:
  !> \param[inout] tdpot      -> tye(admk). Tdens-Potentail System
  !> \param[in   ] ode_inputs -> tye(DmkInputs). Input for Ode
  !> \param[in   ] ctrl       -> tye(DmkCtrl). Dmk controls
  !> \param[inout] info       -> integer. Flag for info 
  !>                              0 = Everything is fine
  !>                              1** = Error in linear solver
  !>                              11* = Convergence not achivied
  !>                              12* = Internal error in linear solver
  !>                              2**  = Error in non-linear solver
  !>                              21* = Non-linear convergence not achievied
  !>                              22* = Non-linear solver stoped because
  !>was divergening
  !>                              23* = Condition for non-linear solver to
  !>                                     continue failed
  !>                              3**  = Other errors
  !>---------------------------------------------------------------------
  subroutine syncronize_tdpot( this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use TimeInputs
    use DmkInputsData
    use StdSparsePrec
    use dagmg_wrap
    use BackSlashSolver
    implicit none
    class(admk), target, intent(inout) :: this
    class(tdpotsys),      intent(inout) :: tdpot
    class(DmkInputs),     intent(in   ) :: ode_inputs
    class(DmkCtrl),       intent(in   ) :: ctrl
    integer,             intent(inout) :: info

    ! local
    logical :: endfile
    integer :: icell,i
    integer :: itemp=0 
    integer :: lun_err, lun_out,lun_stat
    integer :: ntdens,npot
    real(kind=double) :: pode, pflux
    class(abs_linop), pointer :: ort_mat
    type(input_solver) :: ctrl_solver
    type(input_prec)   :: ctrl_prec
    type(diagmat), target :: diagonal_tdens
    type(scalmat), target :: identity_ntdens,identity_npot
    type(new_linop), target :: stiff,matrix4prec
    !class(abs_linop), pointer :: inv_stiff
    character(len=256) :: msg=' ',out_format
    real(kind=double) :: dnrm2
    
    !
    ! shorthand for variables
    !
    ntdens=tdpot%ntdens
    npot  =tdpot%npot
    lun_err=ctrl%lun_err
    lun_out=ctrl%lun_out
    lun_stat=ctrl%lun_statistics
    pode=ode_inputs%pode
    pflux=ode_inputs%pflux
    
    
    ! initiliazations
    call diagonal_tdens%init(lun_err,ntdens)
    call identity_ntdens%eye(ntdens)
    call identity_npot%eye(npot)

    !
    ! print info
    !

    ! info inputs
    if (  ode_inputs%steady) then
       write(msg,*) ' Steady Inputs'
       call ctrl%print_string('update',2,msg)
    else
       if ( (ctrl%debug .ge. 1) .or. (ctrl%info_update .ge. 2) ) then
          call ode_inputs%info(ctrl%lun_out)
       end if
       call ode_inputs%info(ctrl%lun_statistics)
       write(msg,*) minval  ( this%inv_weight ), '<= W^{-1} <=', maxval(this%inv_weight)
       call ctrl%print_string('update',2,msg)
    end if

    ! info state
    out_format=ctrl%formatting('rar')
    write(msg,out_format) minval  ( tdpot%tdens ), '<= TDENS <=', maxval(tdpot%tdens)
    call ctrl%print_string('update',2,msg)

    out_format=ctrl%formatting('ararar')
    write(msg,out_format)  &
         'CTRL: relax_tdens=', ctrl%relax_tdens,&
         ' relax_direct=', ctrl%relax_direct,&
         ' relax4prec=', ctrl%relax4prec
    call ctrl%print_string('update',2,msg)

    
    
    ! implicitely assembly stiff : = A diag(tdens + lambda ) L^{-1} A^T+(relax_direc)*Id+lasso*Id    
    diagonal_tdens%diagonal = &
         (tdpot%tdens + ctrl%relax_tdens+ode_inputs%lambda_lift) * this%inv_weight  ! use tdens + lambda
    diagonal_tdens%name='diag((tdens+lambda)/weight)'
    stiff = this%matrixA * diagonal_tdens * this%matrixAT + &
         (ctrl%relax_direct+ode_inputs%lasso)  * identity_npot
    stiff%is_symmetric = .True.

    ! relax matrix for precondiong
    matrix4prec = stiff + ctrl%relax4prec * identity_npot
    matrix4prec%is_symmetric = .True.

    
    ! set controls per outer solver
    ! tolerance is given by tolerance non linear
    ctrl_solver=ctrl%ctrl_outer_solver
    ctrl_solver%tol_sol=ctrl%tolerance_nonlinear
    if( ctrl%info_update .gt. 3) call ctrl%ctrl_outer_solver%info(6)
    
    !
    ! define (approximate) inverse 
    !
   
    if ( ctrl_solver%approach .eq. 'ITERATIVE') then
       ! update preconditioner x
       select type ( mat => this%matrixA)
       type is ( spmat)          
          if ( ctrl%build_prec .eq. 1) then
             msg= 'ctrl%build_prec ==1 : ASSEMBLING STIFF + BUILDING PREC '
             call ctrl%print_string('update',3,msg)

             ! assembly matrix explitely
             call this%formed_sparse_stiff%form_new_linop(info,lun_err,matrix4prec)
             if ( ctrl%debug .ge. 2) call this%formed_sparse_stiff%info(ctrl%lun_out)

             ! create prec.
             if ( ctrl%outer_prec_type .eq. 'INNER_SOLVER') then
                ! use inner solver
                call this%inv_sparse_stiff%init(&
                     this%formed_sparse_stiff,ctrl%ctrl_inner_solver,&
                     ctrl%ctrl_inner_prec,&
                     lun=lun_err)
                ! assign inverse
                this%prec_pointer => this%inv_sparse_stiff
             else
                ! use standard sparse preconditioner
                call this%prec_sparse_stiff%init(lun_err,info,ctrl%ctrl_outer_prec,npot, &
                     this%formed_sparse_stiff)
                if (info .ne. 0) return
                this%prec_pointer => this%prec_sparse_stiff
             end if
          end if
          ! in debug, print preconditioner info
          if ( ctrl%debug .ge. 2) call this%prec_pointer%info(ctrl%lun_out)
          

          class default
             !type is ( densemat)
          write(*,*) 'ONLY SPARSE SUPPORTED'
          stop
          !call formed_dense_stiff%form_new_linop(info,lun_err,stiff)
       end select

       ! set krylov inverse
       call this%krylov_inverse_npot%set(stiff,&
            ctrl_solver,&
            prec_left=this%prec_pointer)

       ! solve linear system
       this%rhs=ode_inputs%rhs
       ! we pass pot as vec_out as initial guess
       call this%krylov_inverse_npot%matrix_times_vector(this%rhs, tdpot%pot,info,lun_err)


       ! copy info
       this%info_solver=this%krylov_inverse_npot%info_solver
    else
       !
       ! form explicitely matrix stiff
       ! 
       select type ( mat => this%matrixA)
       type is ( spmat)
          if ( ctrl%debug .ge. 2) call stiff%info(lun_out)
          call this%formed_sparse_stiff%form_new_linop(info,lun_err,stiff)
          if ( ctrl%debug .ge. 2) call this%formed_sparse_stiff%info(6)

          !
          ! create prec.
          !
          call this%inv_sparse_stiff%init(&
               this%formed_sparse_stiff,&
               ctrl_solver,&
               ctrl%ctrl_outer_prec,&
               lun=lun_err)
          if ( ctrl%debug .ge. 2) call this%inv_sparse_stiff%info(6)

          !
          ! solve linear system
          !
          this%rhs=ode_inputs%rhs
          call this%inv_sparse_stiff%matrix_times_vector(this%rhs, tdpot%pot,info,lun_err)

          !
          ! copy info
          !
          this%info_solver = this%inv_sparse_stiff%info_solver
          call this%info_solver%info(lun_out)

          class default
             !type is ( densemat)
          write(*,*) 'ONLY SPARSE SUPPORTED'
          stop
          !call formed_dense_stiff%form_new_linop(info,lun_err,stiff)
       end select
    end if

    !
    ! st info flag
    !
    info=0
    if ( this%info_solver%ierr .ne. 0) then
       info=1
    end if
    call this%info_solver%info2str(msg)
    call ctrl%print_string('update',1,msg)

    ! store linear solver info 
    this%sequence_info_solver(1)=this%info_solver
    this%sequence_build_prec(1)=ctrl%build_prec
    tdpot%iter_nonlinear   = 1
    this%iter_first_system = this%info_solver%iter
    tdpot%total_iterations_linear_system = &
         tdpot%total_iterations_linear_system + this%info_solver%iter
    tdpot%total_number_linear_system     = tdpot%total_number_linear_system + 1 
    if (ctrl%build_prec .eq. 1 ) tdpot%iter_last_prec = this%info_solver%iter
    
    

    tdpot%all_syncr=.True.
    tdpot%tdpot_syncr = .True.

    !
    ! free memory    
    !
    call diagonal_tdens%kill(lun_err)
    call identity_ntdens%kill()
    call identity_npot%kill()
  end subroutine syncronize_tdpot

    !>----------------------------------------------------------------
  !> Function to eval.
  !> $ var(\TdensH^k):=\frac{
  !>                     \| \tdens^{k+1} - \tdens^{k} \|_{L2} 
  !>                   }{ 
  !>                     \| \tdens^{k+1} \|_{L2}}  
  !>                   } $
  !> (procedure public for type specp0) 
  !> 
  !> usage: call var%err_tdesn()
  !>    
  !> where:
  !> \param  [in ] var       -> type(specp0) 
  !> \result [out] var_tdens -> real. Weighted var. of tdens
  !<----------------------------------------------------------------
  function compute_tdpot_variation(this,tdpot,ctrl) result(var)
    use Globals
    use TdensPotentialSystem
    implicit none
    class(admk),    intent(in   ) :: this
    class(tdpotsys), intent(in   ) :: tdpot
    class(DmkCtrl),  intent(in   ) :: ctrl
    real(kind=double) :: var
    !local
    real(kind=double) :: norm_old, norm_var,exponent

    ! $ var'norm old =', norm_old,&_tdens = 
    !              frac{
    !                   \|\tdens^{n+1}-\tdens^n\|_{L^2}
    !                  }{
    !                   \deltat \|\tdens^{n}\|_{L^2}
    !                  }$
    exponent = ctrl%norm_tdens
    norm_old = p_norm(tdpot%ntdens,exponent,tdpot%tdens_old)
    norm_var = p_norm(tdpot%ntdens,exponent,tdpot%tdens-tdpot%tdens_old)
    var =  norm_var / ( ctrl%deltat * norm_old )

!!$    write(*,*) 'compute_tdpot_variation: norm=',ctrl%norm_tdens,&
!!$         'norm old =', norm_old,&
!!$         'norm var =', norm_var
    
  end function compute_tdpot_variation

  !>------------------------------------------------
  !> Procedure for computation of next state of system
  !> ( all variables ) given the preovius one
  !> ( private procedure for type dmkpair, used in update)
  !> 
  !> usage: call var%update(lun_err,itemp)
  !> 
  !> where:
  !> \param[inout] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[in   ] ctrl  -> type(DmkCtrl). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !> \param[in   ] info  -> integer. Flag with 3 digits for errors.
  !>                        If info==0 no erros occurred.
  !>                        First digit from the left describes 
  !>                        the main errors. The remaining digits can be used to
  !>                        mark specific errors. Current dictionary:
  !>                        1**  = Error in linear solver
  !>                         11* = Convergence not achivied
  !>                         12* = Internal error in linear solver
  !>                        2**  = Error in non-linear solver
  !>                         21* = Non-linear convergence not achievied
  !>                         22* = Non-linear solver stoped because was divergening
  !>                         23* = Condition for non-linear solver to
  !>                               continue failed
  !>                        3**  = Other errors
  !<---------------------------------------------------
  subroutine update_tdpot(this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use Globals
    use Timing
    use DmkInputsData
    implicit none
    class(admk),     target,intent(inout) :: this
    class(tdpotsys),         intent(inout) :: tdpot
    class(DmkInputs),        intent(in   ) :: ode_inputs
    class(DmkCtrl),          intent(in   ) :: ctrl
    integer,                intent(inout) :: info

    ! local
    real(kind=double) :: decay,pflux,pmass,pode,time,tnext,deltat
    integer :: newton_initial
    integer :: ntdens, npot
    integer :: info_inter,passed_reduced_jacobian,time_iteration
    type(tim) :: wasted_temp
    character(len=256) :: str,msg
    integer :: slot
    integer :: lun_err, lun_out,lun_stat
    type(codeTim) :: CPU

    deltat=ctrl%deltat


    !
    ! local copy
    !
    ntdens = this%ntdens
    npot   = this%npot

    lun_err=ctrl%lun_err
    lun_out=ctrl%lun_out
    lun_stat=ctrl%lun_statistics

    time_iteration = tdpot%time_iteration
    time = tdpot%time

    ! update all spatial variables at itemp+1
    select case ( ctrl%time_discretization_scheme )
    case (1)
       call this%explicit_euler_tdens(tdpot,ode_inputs,ctrl,info)
    case (2)
       call this%new_implicit_euler_newton_tdens(tdpot,ode_inputs,ctrl,info)
    case (3)
       call this%explicit_euler_gfvar(tdpot,ode_inputs,ctrl,info)
    case (4)
       call this%new_implicit_euler_newton_gfvar(tdpot,ode_inputs,ctrl,info)
    case (5)
       !call this%accelerated_gradient_descent_gfvar(tdpot,ode_inputs,ctrl,info)
    end select
   
end subroutine update_tdpot

!>----------------------------------------------------
!> Procedure to update Tdpot system via Explicit Euler.
!> Tdens^{k+1}=Tdens^{k+1} + deltat * RhsOde(Tdens^{k})
!>   Pot^{k+1}=PotOp(Tdens^{k+1})
!>
!> Same INPUT-OUTPUT of update_tdpot
!<-----------------------------------------------------
subroutine explicit_euler_tdens(this,&
     tdpot,&
     ode_inputs,&
     ctrl,&
     info)
    use Globals
    use TimeInputs
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use LinearSolver
    use dagmg_wrap
    implicit none
    class(admk),            intent(inout) :: this
    type(tdpotsys),         intent(inout) :: tdpot
    type(DmkInputs),        intent(in   ) :: ode_inputs
    type(DmkCtrl),          intent(in   ) :: ctrl
    integer,                intent(inout) :: info


  !local
  logical :: endfile,assembly
  integer :: lun_err,lun_out,lun_stat
  integer :: icell
  integer :: ntdens,npot
  real(kind=double) :: decay, pflux,pmass,pode
  real(kind=double) :: dnrm2
  character(len=256) :: outformat,str

  lun_err  = ctrl%lun_err
  lun_out  = ctrl%lun_out
  lun_stat = ctrl%lun_statistics

  ntdens = this%ntdens
  npot   = this%npot

  if ( .not. tdpot%all_syncr)  then
     write(lun_err,*) 'Not all varibles are syncronized'
     stop
  end if

  
  
  ! assembly direction
  call this%assembly_mirrow_gradient_lyapunov_tdens(&
       ode_inputs,&
       tdpot%tdens,tdpot%pot,&
       this%rhs_ode)
  this%rhs_ode = this%rhs_ode * this%inv_weight
  

  ! info update
  outformat=ctrl%formatting('rar')
  write(str,outformat) &
       minval(this%rhs_ode),' < TDENS INCREMENT < ',maxval(this%rhs_ode)
  call ctrl%print_string('update',1,str)

  
  ! Update $\Tdens$ and get new $\Tdens$
  tdpot%tdens=tdpot%tdens -ctrl%deltat * this%rhs_ode
  do icell = 1, this%ntdens
     tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
  end do

  ! syncronized Pot
  call this%syncronize_tdpot( &
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)

  ! update tdpot time related quantities
  if ( info == 0 ) then
     tdpot%deltat=ctrl%deltat
     tdpot%time_old = tdpot%time
     tdpot%time=tdpot%time+ctrl%deltat
     tdpot%iter_nonlinear = 1
     tdpot%time_iteration = tdpot%time_iteration + 1
     tdpot%time_update    = tdpot%time_update + 1
  end if

end subroutine explicit_euler_tdens

!>----------------------------------------------------
!> Procedure to update Tdpot system via Explicit Euler.
!> GF^{k+1}    = GF^{k+1} - deltat * \Grad \Lyap (GF^{k})
!> Tdens^{k+1} = \Trans(GF^{k+1})
!> Pot^{k+1}   = PotOp(\Trans(Tdens^{k+1}))
!>
!> Same INPUT-OUTPUT of update_tdpot
!<-----------------------------------------------------
subroutine explicit_euler_gfvar(this,&
     tdpot,&
     ode_inputs,&
     ctrl,&
     info)
    use Globals
    use TimeInputs
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use LinearSolver
    use dagmg_wrap
    implicit none
    class(admk),            intent(inout) :: this
    type(tdpotsys),         intent(inout) :: tdpot
    type(DmkInputs),        intent(in   ) :: ode_inputs
    type(DmkCtrl),          intent(in   ) :: ctrl
    integer,                intent(inout) :: info


  !local
  logical :: endfile,assembly
  integer :: lun_err,lun_out,lun_stat
  integer :: icell
  integer :: ntdens,npot
  real(kind=double) :: decay, pflux,pmass,pode
  real(kind=double) :: dnrm2
  character(len=256) :: outformat,str

  lun_err  = ctrl%lun_err
  lun_out  = ctrl%lun_out
  lun_stat = ctrl%lun_statistics

  ntdens = this%ntdens
  npot   = this%npot

  if ( .not. tdpot%all_syncr)  then
     write(lun_err,*) 'Not all varibles are syncronized'
     stop
  end if

  !
  ! convert in gfvar 
  !
  call tdens2gfvar(ctrl%trans,ntdens,tdpot%tdens,tdpot%gfvar)

  !
  ! compute gradient
  !
  call this%assembly_gradient_lyapunov_gfvar(&
       ode_inputs,&
       ctrl%trans,&
       tdpot%gfvar,tdpot%tdens,tdpot%pot,&
       this%scr_ntdens)
  
  ! print info 
  outformat=ctrl%formatting('rar')
  write(str,outformat) &
       minval(this%scr_ntdens),' < GFVAR INCREMENT < ',maxval(this%scr_ntdens)
  call ctrl%print_string('update',1,str)
     
     
  
  !
  ! Update $\Gfvar$ and get new $\Tdens$
  !
  this%scr_ntdens = this%scr_ntdens * this%inv_weight
  call daxpy(ntdens, -ctrl%deltat, this%scr_ntdens,1,tdpot%gfvar,1)
  call gfvar2tdens(ctrl%trans,ntdens,tdpot%gfvar,tdpot%tdens)
  do icell = 1, this%ntdens
     tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
  end do
  call tdens2gfvar(ctrl%trans,ntdens,tdpot%tdens,tdpot%gfvar)
  
  !
  ! syncronized Pot
  !
  call this%syncronize_tdpot( &
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)

  ! update tdpot time related quantities
  if ( info == 0 ) then
     tdpot%deltat=ctrl%deltat
     tdpot%time_old = tdpot%time
     tdpot%time=tdpot%time+ctrl%deltat
     tdpot%iter_nonlinear = 1
     tdpot%time_iteration = tdpot%time_iteration + 1
     tdpot%time_update    = tdpot%time_update + 1
  end if

  

end subroutine explicit_euler_gfvar



!>----------------------------------------------------
!> Procedure for update the system via accelerated gradeint
!> descent procedure
!> Same Input/Ouput interface of update
!<---------------------------------------------------    
subroutine accelerated_gradient_decent_tdens(this,&
     tdpot,&
     ode_inputs,&
     ctrl,&
     info)
    use Globals
    use TimeInputs
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use LinearSolver
    implicit none
    class(admk),            intent(inout) :: this
    type(tdpotsys),         intent(inout) :: tdpot
    type(DmkInputs),        intent(in   ) :: ode_inputs
    type(DmkCtrl),          intent(in   ) :: ctrl
    integer,                intent(inout) :: info

    !local
    logical :: endfile,assembly
    integer :: lun_err
    integer :: icell
    integer :: ntdens,npot
    real(kind=double) :: decay, pflux,pmass,pode
    real(kind=double) :: dnrm2

    lun_err = ctrl%lun_err
    ntdens = this%ntdens
    npot   = this%npot

    if ( .not. this%all_syncr)  then
       write(lun_err,*) 'Not all varibles are syncronized'
       stop
    end if


    decay = ode_inputs%decay
    pflux = ode_inputs%pflux
    pmass = ode_inputs%pmass
    pode  = ode_inputs%pode
    ! pode = pflux for id_ode = 1 
    ! pode = 2.0   for id_ode = 2
    ! pode = 
    
    !
    ! Compute rhs ode 
    !
    this%scr_ntdens = tdpot%tdens + &
         (this%temporal_iteration-1) * one / (this%temporal_iteration + 2) * &
         ( tdpot%tdens-tdpot%tdens_old)

   
    this%rhs_ode = &
         (this%scr_ntdens  ** pflux ) * (this%nrm_grad ** pode) - &
         decay * ode_inputs%kappa * this%scr_ntdens ** pmass

    tdpot%tdens = this%scr_ntdens + ctrl%deltat* this%rhs_ode

    do icell = 1, this%ntdens
       tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
    end do
    
    !
    ! syncronized Pot
    !
    call this%syncronize_tdpot(&
         tdpot,&
         ode_inputs,&
         ctrl,&
         info)

    if ( info == 0 ) then
       tdpot%deltat=ctrl%deltat
       tdpot%time_old = tdpot%time
       tdpot%time=tdpot%time+ctrl%deltat
       tdpot%iter_nonlinear = 1
    end if

end subroutine accelerated_gradient_decent_tdens



subroutine new_implicit_euler_newton_gfvar(this,&
     tdpot,&
     ode_inputs,&
     ctrl,&
     info)
    use Globals
    use TimeInputs
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use LinearSolver
    use SaddlePointMatrix
    use dagmg_wrap

    implicit none
    class(admk),    target,        intent(inout) :: this
    type(tdpotsys),         intent(inout) :: tdpot
    type(DmkInputs),        intent(in   ) :: ode_inputs
    type(DmkCtrl),          intent(in   ) :: ctrl
    integer,                intent(inout) :: info

    ! local
    integer :: lun_err,lun_out,lun_stat,icell
    integer :: flag,info_newton
    real(kind=double),  allocatable :: xvar(:)
    real(kind=double),  allocatable :: increment(:)
    real(kind=double),  allocatable :: FNEWTON(:)
    real(kind=double),  allocatable :: D22(:),trans_prime(:), trans_second(:),W22pure(:)
    real(kind=double),  allocatable :: rhs_full(:),scr_nfull(:),tdens_reduced(:)
    real(kind=double),  allocatable :: tdens(:), pot(:),gfvar(:),grad(:),gfvar_old(:)
    real(kind=double),  allocatable :: tdens_stiff(:),gradinc(:),rhs_reduced(:)
    real(kind=double),  allocatable :: W22_pure(:),gfvarnew(:),gradnew(:),potnew(:)
    real(kind=double), target, allocatable :: rhs_selected(:)
    real(kind=double) :: tolerance_linear_solver,alpha,epsilon_W22
    real(kind=double) :: relax_limit,line_search_contraction_rate
    logical :: test_cycle,test
    integer :: imax_internal
    real(kind=double) :: tol_internal,lambda_prec,limit_relax

    type(input_solver) :: ctrl_outer_solver
    type(input_prec) :: ctrl_inner_solver
    type(scalmat),  target :: identity_npot_on,identity_ntdens_on,identity_nfull_on 
    
    real(kind=double) :: previuous_fnewton_norm  ,relax_tdens, sign_swap   
    integer :: max_nonlinear_step
    real(kind=double) ::  absolute_growth_factor,relative_growth_factor,prev_res
    integer :: i,ntdens,npot,nfull
    character(len=256) :: msg=' ',msg1=' ',msg2=' ',head=' '
    character(len=256) :: str=' ',sepa=' ',out_format
    real(kind=double) :: dnrm2,tol_linear_newton
    type(file) :: fmat
    type(spmat),target :: matrix2prec,matrix2solve,temp_spmat
    class(spmat), pointer :: matrix_to_solve
    real(kind=double), pointer :: rhs_linsys(:)
    integer :: trans
    real(kind=double) :: deltat
    type(input_prec) :: ctrl_prec
   
    !
    type(array_linop):: components(5)
    integer          :: W_block_structure(3,4), debug=0
    type(diagmat) ,target   :: W11_scaled,W11,W12,W21,W22,W22_scaled
    type(diagmat) ,target   :: invmatrixC
    ! saddle point type
    type(stiffmat),target :: matrixA_old
    type(pair_linop),target :: matrixAbis,matrixB1T,matrixB2
    type(block_linop),target :: matrixM
    type(saddleprec), target :: jacobian_prec
    type(inv) :: inverse_of_SchurAC

    ! components of jacobian matrix
    type(new_linop),target :: matrixA,matrixBT,matrixB
    type(diagmat),  target :: matrixC
    type(block_linop),target :: jacobian_matrix 
    
    ! inversion via reduction
    type(new_linop), target :: ABTinvCB
    type(diagmat),target :: invC,Wreduced
    type(spmat),target   :: formed_ABTinvCB
    type(inv),target :: inv_ABTinvCB
    type(newton_input)  :: newton_ctrl
    type(newton_output) :: newton_info
    
    
    trans=1

    !
    ! set controls of newton algorithm
    !
    newton_ctrl%max_nonlinear_iterations=ctrl%max_nonlinear_iterations
    newton_ctrl%tolerance_nonlinear=ctrl%tolerance_nonlinear
    newton_ctrl%alpha=1.0d0

    !
    ! other controls
    !
    epsilon_W22=ctrl%epsilon_W22
    sign_swap=one;



    !
    ! shorthands for frequently used vars
    !
    ntdens=this%ntdens
    npot=this%npot
    nfull=ntdens+npot

    lun_err =ctrl%lun_err
    lun_out =ctrl%lun_out
    lun_stat=ctrl%lun_statistics

    deltat=ctrl%deltat

    !
    ! initialization of matrices and vectors
    !
    call identity_npot_on%eye(tdpot%npot_on)
    identity_npot_on%name='I_n'
    call identity_ntdens_on%eye(tdpot%ntdens_on)
    identity_ntdens_on%name='I_m'
    call identity_nfull_on%eye(tdpot%npot_on+tdpot%ntdens_on)

    allocate( &
         W22pure(ntdens),&
         trans_prime(ntdens),&
         trans_second(ntdens),&
         tdens_reduced(ntdens),&
         xvar(nfull), &
         FNEWTON(nfull),&
         increment(nfull),&
         scr_nfull(nfull),&
         rhs_full(nfull),&
         D22(ntdens),&
         gfvar(ntdens),&
         gfvar_old(ntdens),&
         gfvarnew(ntdens),&
         grad(ntdens),&
         gradnew(ntdens),&
         gradinc(ntdens),&
         tdens(ntdens),&
         pot(npot),&
         rhs_reduced(tdpot%npot),&
         rhs_selected(tdpot%npot_on),&
         tdens_stiff(ntdens),&
         )

    !
    ! init W_scaled=(W11/L W12  )
    !               (W21   W22*L)
    call W11_scaled%init(lun_err,tdpot%ntdens_on)
    call W11%init(lun_err,tdpot%ntdens_on)
    call W12%init(lun_err,tdpot%ntdens_on)
    call W21%init(lun_err,tdpot%ntdens_on)
    call W22%init(lun_err,tdpot%ntdens_on)
    call W22_scaled%init(lun_err,tdpot%ntdens_on)
    call Wreduced%init(lun_err,tdpot%ntdens_on)
    call invC%init(lun_err,tdpot%ntdens_on)


    !
    ! matrixC=-W22_scaled
    !
    call matrixC%init(lun_err,tdpot%ntdens_on)
    call invmatrixC%init(lun_err,tdpot%ntdens_on)

   
    
    !
    ! set initial data 
    !
    call tdens2gfvar(trans,ntdens,tdpot%tdens,gfvar)
    gfvar_old          = gfvar
    xvar(1:npot)       = tdpot%pot
    xvar(npot+1:nfull) = gfvar
    
    

    flag=1
    info_newton=0
    newton_info%current_newton_step=0
    do while ( ( flag .gt. 0 ) .and. (info_newton .eq. 0) )
       write(head,'(I2,a)') newton_info%current_newton_step,' | '
       select case ( flag)
       case (1)
          if (newton_info%current_newton_step>0) call ctrl%print_string('update',2,ctrl%separator())
          !
          ! assembly Fnewton
          ! 
          pot   = xvar(1:npot)
          gfvar = xvar(npot+1:nfull)
          call gfvar2tdens(trans,ntdens,gfvar,tdens)
          call gfvar2f(trans,ntdens,gfvar,trans_prime,trans_second)
         
          !
          ! FNEWTON POT
          !
          tdens_stiff=tdens+ctrl%relax_tdens
          call this%build_grad(this%matrixAT,pot,grad, this%inv_weight)
          this%scr_ntdens=tdens_stiff*grad
          call this%matrixA%Mxv(this%scr_ntdens,FNEWTON(1:npot))
          FNEWTON(1:npot)       = FNEWTON(1:npot)-this%rhs;
          
          !
          ! FNEWTON TDENS
          !
          FNEWTON(npot+1:nfull) = -this%weight*(&
               (gfvar-gfvar_old)/deltat + onehalf*trans_prime* ( - grad**2 + one))

          !
          ! assign rhs 
          !
          rhs_full = -FNEWTON
          
       case (2)
          !
          ! compute norm of F 
          !
          newton_ctrl%fnewton_norm=dnrm2(nfull,FNEWTON,1)
          !
          ! print info
          !
          write(msg,'(a,a,(1pe12.3),a,a,(1pe12.3)a,a,(1pe12.3))') &
               etb(head),&
               'NEWTON FUNC=',newton_ctrl%fnewton_norm,&
               ' | ',&
               'POT =', dnrm2(npot,FNEWTON(1:npot),1),&
               ' | ',&
               'GFVAR =', dnrm2(ntdens,FNEWTON(npot+1:nfull),1)
          call ctrl%print_string('update',2,msg)
         
       case (3)
          !
          ! 1 - assembly jacobian J 
          ! 2 - solve J s = -F 
          !         
          pot   = xvar(1:npot)
          gfvar = xvar(npot+1:nfull)
          call this%build_grad(this%matrixAT,pot,grad, this%inv_weight)
          call gfvar2tdens(trans,ntdens,gfvar,tdens)
          call gfvar2f(trans,ntdens,gfvar,trans_prime,trans_second)
          tdens_stiff=tdens+ctrl%relax_tdens



          !
          ! assembly W component
          !
          W11%diagonal = tdens_stiff
          W11%name='W11'
          W12%diagonal = trans_prime * grad 
          W12%name='W12'
          W21%diagonal = trans_prime * grad
          W21%name='W21'
          call build_W22_pure(ntdens,trans_second,grad,W22pure)
          W11%name='W22'
          W22%diagonal = - one/deltat + W22pure
          W21%diagonal = sign_swap*W21%diagonal
          W22%diagonal = sign_swap*W22%diagonal
          W11_scaled%diagonal=W11%diagonal*this%inv_weight
          W11_scaled%name='_W11*invL_'
          W22_scaled%diagonal=W22%diagonal*this%weight
          W22_scaled%name='W2*L'

          ! info on block W22
          out_format=ctrl%formatting('arar')
          write(msg,out_format) &
               etb(head), minval(W22%diagonal),' <= W22 <= ',maxval(W22%diagonal)
          call ctrl%print_string('update',3,msg)
          
          
          
          !
          ! set matrix
          !
          matrixA  = this%matrixA*W11_scaled*this%matrixAT
          if (debug .ge. 2) then
             write(*,*)
             call matrixA%info(6)
             write(*,*)
          end if
          !
          ! BT matrix
          !
          matrixBT = this%matrixA * W12
          if (debug .ge. 2) then
             write(*,*)
             call matrixBT%info(6)
             write(*,*)
          end if
             
          !
          ! B matrix
          !
          matrixB  = W21*this%matrixAT
          if (debug .ge. 2) then
             write(*,*)
             call matrixB%info(6)
             write(*,*)
          end if
          !
          ! C matrix
          !
          matrixC%diagonal  = -one* W22_scaled%diagonal
          matriXC%name='-W2*L'
          
          call jacobian_matrix%saddle_point(&
               lun_err,&
               matrixA, matrixBT,matrixB,.True.,&
               matrixC)
          if (debug .ge. 2) then
             write(*,*)
             call jacobian_matrix%info(6)
             write(*,*)
          end if
          

          select case (ctrl%solve_jacobian_approach)
          case ('reduced')
             !
             ! invert C only if strictly positive
             !
             if (minval(matrixC%diagonal) < epsilon_W22) then
                info=210
                exit
             end if
             invC%diagonal = one/matrixC%diagonal
             Wreduced%diagonal = W11_scaled%diagonal &
                  + W12%diagonal*invC%diagonal*W12%diagonal
             Wreduced%name='Wr'

             !
             ! form Schur Complement SAC = A+BT(C)^{-1}B
             !
             ! form implicitely
             ABTinvCB = this%matrixA*Wreduced*this%matrixAT +ctrl%relax_direct*identity_npot_on
             !ABTinvCB = matrixA + matrixBT* invC * matrixB  +ctrl%relax_direct*identity_npot_on
             ! set properties
             ABTinvCB%is_symmetric = .TRUE.
             ! set this flag to speed assembly
             ABTinvCB%pair_list(1)%pattern = 'AWAT+rho*I'
             ! form explicitely 
             call formed_ABTinvCB%form_new_linop(info,lun_err,ABTinvCB)
             
             !
             ! SOLVE LINEAR SYSTEM
             !
             
             !
             ! define (approximate) inverse 
             !
             call inv_ABTinvCB%init(&
                  formed_ABTinvCB,ctrl%ctrl_outer_solver,ctrl%ctrl_outer_prec,&
                  lun=lun_err)
             if ( debug.ge. 2) call inv_ABTinvCB%info(6)


             !
             ! initialized precondtioner of saddle point matrix 
             !
             call jacobian_prec%init(&
                  lun_err,&
                  jacobian_matrix,&
                  prec_type='SchurACFull',&
                  invSchurAC=inv_ABTinvCB, invC=invC)

             !
             ! apply inverse
             !
             call jacobian_prec%matrix_times_vector(rhs_full, increment,info,lun_err)

             !
             ! copy info solver
             !
             this%info_solver=inv_ABTinvCB%info_solver
             


          case ('full')
             select case (ctrl%outer_solver_approach) 
             case ('reduced')
                !
                ! 
                !
             end select
          end select

          ! set info flags
          if (this%info_solver%ierr .ne. 0) then
             info_newton=110
          end if
          
          ! print linear solver info
          call this%info_solver%info2str(str) 
          write(msg,'(a,a,a)') &
               etb(head),'LINEAR SOLVER :  ',etb(str)
          call ctrl%print_string('update',2,msg)
          
          call jacobian_matrix%matrix_times_vector(increment, scr_nfull(1:nfull),info,lun_err)
          scr_nfull=scr_nfull-rhs_full
          write(msg,'(a,a,1pe9.2,a,1pe9.2)') &
               etb(head),'RESIDUA : \|J s + F\|=',dnrm2(nfull,scr_nfull,1),&
               '\|J s + F\|/|F|=',&
               dnrm2(nfull,scr_nfull,1)/dnrm2(nfull,rhs_full,1)
          call ctrl%print_string('update',2,msg)
          
          
          
          ! set newton incremental step
          call this%build_grad(this%matrixAT,increment(1:npot),gradinc, this%inv_weight)
          newton_ctrl%alpha=one
          test_cycle=.True.
          do while (test_cycle)
             potnew     = pot   + newton_ctrl%alpha * increment(1:npot)
             gfvarnew   = gfvar + newton_ctrl%alpha * increment(npot+1:nfull)
             gradnew    = grad  + newton_ctrl%alpha * gradinc 
             call gfvar2f(trans,ntdens,gfvarnew,trans_prime,trans_second)
             call build_W22_pure(ntdens,trans_second,gradnew,W22pure)
             test = maxval(-1/deltat+W22pure) < -epsilon_W22
             if ( test ) exit
             newton_ctrl%alpha=&
                  newton_ctrl%alpha/newton_ctrl%line_search_contraction_rate
             test_cycle = ( newton_ctrl%alpha > newton_ctrl%alpha_limit) 
             if ( .not. test_cycle ) then
                info_newton=230
             end if
          end do
          
          write(msg,'(a,a,1pe9.2)') &
               etb(head), 'Alpha Dumping =', newton_ctrl%alpha
          call ctrl%print_string('update',2,msg)
             
          
       end select
       !
       ! reverse communication newton
       !
       call newton_raphson_reverse_with_dt(&
            flag,info_newton, nfull, xvar, increment,&
            newton_ctrl,newton_info)

       
       
    end do

    call ctrl%print_string('update',2,ctrl%separator())
    
    if ( info_newton .eq. 0)  then
       
       tdpot%pot=pot
       tdpot%gfvar=gfvar
       call gfvar2tdens( trans ,ntdens,gfvar,tdpot%tdens)

       write(msg,'(a,1x,I2,1x,a,1pe8.2)') 'NEWTON CONVERGED IN', newton_info%current_newton_step,&
            'ITERATIONS WITH RESIDUA=',newton_ctrl%fnewton_norm 
       call ctrl%print_string('update', 1, msg)
    else
       if (info_newton == -1) info=220
       if (info_newton == -2) info=220
       info=info_newton
       write(msg,'(a,1x,I2,1x,a,1pe8.2,a,I3)') 'NEWTON FAILED AFTER ', newton_info%current_newton_step,&
            'ITERATIONS WITH RESIDUA=',newton_ctrl%fnewton_norm, ' info= ',info
       call ctrl%print_string('update', 1, msg)
    end if
    info=info_newton


    ! update tdpot time related quantities
    if ( info == 0 ) then
       tdpot%deltat=ctrl%deltat
       tdpot%time_old = tdpot%time
       tdpot%time=tdpot%time+ctrl%deltat
       tdpot%iter_nonlinear = newton_info%current_newton_step
       tdpot%time_iteration = tdpot%time_iteration + 1
       tdpot%time_update    = tdpot%time_update + 1
    end if

  end subroutine new_implicit_euler_newton_gfvar

  subroutine new_implicit_euler_newton_tdens(this,&
     tdpot,&
     ode_inputs,&
     ctrl,&
     info)
    use Globals
    use TimeInputs
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use LinearSolver
    use SaddlePointMatrix
    use dagmg_wrap

    implicit none
    class(admk),    target,        intent(inout) :: this
    type(tdpotsys),         intent(inout) :: tdpot
    type(DmkInputs),        intent(in   ) :: ode_inputs
    type(DmkCtrl),          intent(in   ) :: ctrl
    integer,                intent(inout) :: info

    ! local
    integer :: lun_err,lun_out,lun_stat,icell
    integer :: flag,info_newton
    real(kind=double),  allocatable :: xvar(:)
    real(kind=double),  allocatable :: increment(:)
    real(kind=double),  allocatable :: FNEWTON(:)
    real(kind=double),  allocatable :: D22(:),trans_prime(:), trans_second(:),W22pure(:)
    real(kind=double),  allocatable :: rhs_full(:),scr_nfull(:),tdens_reduced(:)
    real(kind=double),  allocatable :: tdens(:), pot(:),grad(:),tdens_old(:)
    real(kind=double),  allocatable :: tdens_stiff(:),gradinc(:),rhs_reduced(:)
    real(kind=double),  allocatable :: W22_pure(:),tdensnew(:),gradnew(:),potnew(:)
    real(kind=double), target, allocatable :: rhs_selected(:)
    real(kind=double) :: tolerance_linear_solver,alpha,epsilon_W22
    real(kind=double) :: relax_limit,line_search_contraction_rate
    logical :: test_cycle,test
    integer :: imax_internal
    real(kind=double) :: tol_internal,lambda_prec,limit_relax

    type(input_solver) :: ctrl_solver
    type(scalmat),  target :: identity_npot_on,identity_ntdens_on,identity_nfull_on 
    
    real(kind=double) :: previuous_fnewton_norm  ,relax_tdens, sign_swap   
    integer :: max_nonlinear_step
    real(kind=double) ::  absolute_growth_factor,relative_growth_factor,prev_res
    integer :: i,ntdens,npot,nfull
    character(len=256) :: msg=' ',msg1=' ',msg2=' ',head=' '
    character(len=256) :: str=' ',sepa=' ',out_format
    real(kind=double) :: dnrm2,tol_linear_newton
    type(file) :: fmat
    type(spmat),target :: matrix2prec,matrix2solve,temp_spmat
    class(spmat), pointer :: matrix_to_solve
    real(kind=double), pointer :: rhs_linsys(:)
    integer :: trans
    real(kind=double) :: deltat
    type(input_prec) :: ctrl_prec
   
    !
    type(array_linop):: components(5)
    integer          :: W_block_structure(3,4), debug=0
    type(diagmat) ,target   :: W11_scaled,W11,W12,W21,W22,W22_scaled
    type(diagmat) ,target   :: invmatrixC
    ! saddle point type
    type(stiffmat),target :: matrixA_old
    type(pair_linop),target :: matrixAbis,matrixB1T,matrixB2
    type(block_linop),target :: matrixM
    type(saddleprec), target :: jacobian_prec
    type(inv) :: inverse_of_SchurAC

    ! components of jacobian matrix
    type(new_linop),target :: matrixA,matrixBT,matrixB
    type(diagmat),  target :: matrixC
    type(block_linop),target :: jacobian_matrix 
    
    ! inversion via reduction
    type(new_linop), target :: ABTinvCB
    type(diagmat),target :: invC,Wreduced
    type(spmat),target   :: formed_ABTinvCB
    type(inv),target :: inv_ABTinvCB
    type(newton_input)  :: newton_ctrl
    type(newton_output) :: newton_info


   
    !
    ! set controls of newton algorithm
    !
    newton_ctrl%max_nonlinear_iterations=ctrl%max_nonlinear_iterations
    newton_ctrl%tolerance_nonlinear=ctrl%tolerance_nonlinear
    newton_ctrl%alpha=1.0d0

    !
    ! other controls
    !
    epsilon_W22=ctrl%epsilon_W22

    !
    ! shorthands for frequently used vars
    !
    ntdens=this%ntdens
    npot=this%npot
    nfull=ntdens+npot

    lun_err =ctrl%lun_err
    lun_out =ctrl%lun_out
    lun_stat=ctrl%lun_statistics

    deltat=ctrl%deltat

    !
    ! initialization of matrices and vectors
    !
    call identity_npot_on%eye(tdpot%npot_on)
    identity_npot_on%name='I_n'
    call identity_ntdens_on%eye(tdpot%ntdens_on)
    identity_ntdens_on%name='I_m'
    call identity_nfull_on%eye(tdpot%npot_on+tdpot%ntdens_on)

    allocate( &
         W22pure(ntdens),&
         trans_prime(ntdens),&
         trans_second(ntdens),&
         tdens_reduced(ntdens),&
         xvar(nfull), &
         FNEWTON(nfull),&
         increment(nfull),&
         scr_nfull(nfull),&
         rhs_full(nfull),&
         D22(ntdens),&
         tdens_old(ntdens),&
         tdensnew(ntdens),&
         grad(ntdens),&
         gradnew(ntdens),&
         gradinc(ntdens),&
         tdens(ntdens),&
         pot(npot),&
         rhs_reduced(tdpot%npot),&
         rhs_selected(tdpot%npot_on),&
         tdens_stiff(ntdens),&
         )

    !
    ! init W_scaled=(W11/L W12  )
    !               (W21   W22*L)
    call W11_scaled%init(lun_err,tdpot%ntdens_on)
    call W11%init(lun_err,tdpot%ntdens_on)
    call W12%init(lun_err,tdpot%ntdens_on)
    call W21%init(lun_err,tdpot%ntdens_on)
    call W22%init(lun_err,tdpot%ntdens_on)
    call W22_scaled%init(lun_err,tdpot%ntdens_on)
    call Wreduced%init(lun_err,tdpot%ntdens_on)
    call invC%init(lun_err,tdpot%ntdens_on)


    !
    ! matrixC=-W22_scaled
    !
    call matrixC%init(lun_err,tdpot%ntdens_on)
    call invmatrixC%init(lun_err,tdpot%ntdens_on)

   
    
    !
    ! set initial data 
    !
    tdens_old          = tdpot%tdens
    xvar(1:npot)       = tdpot%pot
    xvar(npot+1:nfull) = tdpot%tdens
    
    

    flag=1
    info_newton=0
    newton_info%current_newton_step=0

    !
    ! keep cycling when info_newton equal zero
    ! or exit command is passed (remember to assign)
    ! info consistent with error dictonary
    !
    do while ( ( flag .gt. 0 ) .and. (info_newton .eq. 0) )
       write(head,'(I2,a)') newton_info%current_newton_step,' | '
       select case ( flag)
          ! with this flag==1 the non-linear equation must be assembled
       case (1)          
          call ctrl%print_string('update',2,ctrl%separator())
          !
          ! assembly Fnewton
          ! 
          pot   = xvar(1:npot)
          tdens = xvar(npot+1:nfull)

          !
          ! FNEWTON POT
          !
          tdens_stiff=tdens+ctrl%relax_tdens
          call this%build_grad(this%matrixAT,pot,grad, this%inv_weight)
          this%scr_ntdens=tdens_stiff*grad
          call this%matrixA%Mxv(this%scr_ntdens,FNEWTON(1:npot))
          FNEWTON(1:npot) = FNEWTON(1:npot)-this%rhs;
          
          !
          ! FNEWTON TDENS
          !
          call this%assembly_mirrow_gradient_lyapunov_tdens(&
               ode_inputs,&
               tdens,pot,&
               FNEWTON(npot+1:nfull))
          FNEWTON(npot+1:nfull) = - ( this%weight * (tdens-tdens_old)/deltat + FNEWTON(npot+1:nfull) )

          !
          ! assign rhs 
          !
          rhs_full = -FNEWTON

          
       case (2)
          ! with this flag==2 the residdum of the non-linear equation must be computed
          newton_ctrl%fnewton_norm=dnrm2(nfull,FNEWTON,1)

          ! print info
          write(msg,'(a,a,(1pe12.3),a,a,(1pe12.3)a,a,(1pe12.3))') &
               etb(head),&
               'NEWTON FUNC=',newton_ctrl%fnewton_norm,&
               ' | ',&
               'POT =', dnrm2(npot,FNEWTON(1:npot),1),&
               ' | ',&
               'TDENS=', dnrm2(ntdens,FNEWTON(npot+1:nfull),1)
          call ctrl%print_string('update',2,msg)
       case (3)
          !
          ! 1 - assembly jacobian J 
          ! 2 - solve J s = -F 
          !         
          pot   = xvar(1:npot)
          tdens = xvar(npot+1:nfull)
          call this%build_grad(this%matrixAT,pot,grad, this%inv_weight)
          tdens_stiff=tdens+ctrl%relax_tdens

          out_format=ctrl%formatting('rar')
          write(msg,out_format) minval(tdens_stiff),'<=TDENS+R<=' ,maxval(tdens_stiff)
          call ctrl%print_string('update', 3 ,etb(msg))


          
          !
          ! assembly W component
          !
          W11%diagonal = tdens_stiff
          W11%name='W11'

          W11_scaled%diagonal=W11%diagonal*this%inv_weight
          W11_scaled%name='_W11*invL_'

          W12%diagonal = grad 
          W12%name='W12'

          W21%diagonal = -  ( - tdens**ode_inputs%pflux * &
               (ode_inputs%pode*abs(grad)**(ode_inputs%pode-two))*grad )
          !W21%diagonal = tdens**ode_inputs%pflux * (ode_inputs%pode*grad ) 
          W21%name='W21'

          out_format=ctrl%formatting('rar')
          write(msg,out_format) minval(W21%diagonal),'<=W21<=' ,maxval(W21%diagonal)
          call ctrl%print_string('update', 3 ,etb(msg))
          
          out_format=ctrl%formatting('rar')
          write(msg,out_format) minval(grad),'<=grad<=' ,maxval(grad)
          call ctrl%print_string('update', 3 ,etb(msg))
          
          call this%assembly_mirrow_hessian_lyapunov_tdens(&
               ode_inputs,tdens,grad,&
               W22_scaled%diagonal)

          out_format=ctrl%formatting('rar')
          write(msg,out_format) minval(W22_scaled%diagonal),'<=Hessian<=' ,maxval(W22_scaled%diagonal)
          call ctrl%print_string('update', 3 ,etb(msg))

          
          W22_scaled%diagonal = - ( this%weight/deltat + W22_scaled%diagonal ) 
          W22_scaled%name='W2*L'

          out_format=ctrl%formatting('rar')
          write(msg,out_format) minval(W22_scaled%diagonal),'<=W22<=' ,maxval(W22_scaled%diagonal)
          call ctrl%print_string('update', 3 ,etb(msg))
          
          W22%diagonal=W22_scaled%diagonal*this%inv_weight
          W22%name='W2'
          
          out_format=ctrl%formatting('arar')
          write(msg,out_format) &
               etb(head), minval(W22%diagonal),' <= W22 <= ',maxval(W22%diagonal)
          call ctrl%print_string('update', 3 ,etb(msg))
          
          !
          ! set matrix
          !
          matrixA  = this%matrixA*W11_scaled*this%matrixAT
          if (debug .ge. 2) then
             write(*,*)
             call matrixA%info(6)
             write(*,*)
          end if
          !
          ! BT matrix
          !
          matrixBT = this%matrixA * W12
          if (debug .ge. 2) then
             write(*,*)
             call matrixBT%info(6)
             write(*,*)
          end if
             
          !
          ! B matrix
          !
          matrixB  = W21*this%matrixAT
          if (debug .ge. 2) then
             write(*,*)
             call matrixB%info(6)
             write(*,*)
          end if
          !
          ! C matrix
          !
          matrixC%diagonal  = -W22_scaled%diagonal
          matriXC%name='-W2*L'

          out_format=ctrl%formatting('rar')
          write(msg,out_format) minval(matrixC%diagonal),'<=C<=' ,maxval(matrixC%diagonal)
          call ctrl%print_string('update', 3 ,etb(msg))

          
          call jacobian_matrix%saddle_point(&
               lun_err,&
               matrixA, matrixBT,matrixB,.False.,&
               matrixC)
          if (debug .ge. 2) then
             write(*,*)
             call jacobian_matrix%info(6)
             write(*,*)
          end if
          

          select case (ctrl%solve_jacobian_approach)
          case ('reduced')
             !
             ! invert C only if strictly positive
             !
             if (minval(matrixC%diagonal) < epsilon_W22) then
                info = 210
                out_format=ctrl%formatting('rar')
                write(msg,out_format) minval(matrixC%diagonal),'<=C<=' ,maxval(matrixC%diagonal)
                call ctrl%print_string('update',2,msg)
                exit
             end if
             invC%diagonal = one/matrixC%diagonal
             Wreduced%diagonal = W11_scaled%diagonal &
                  + W12%diagonal*invC%diagonal*W21%diagonal
             Wreduced%name='Wr'

             !
             ! form Schur Complement SAC = A+BT(C)^{-1}B
             !
             ! form implicitely
             ABTinvCB = this%matrixA*Wreduced*this%matrixAT +ctrl%relax_direct*identity_npot_on
             !ABTinvCB = matrixA + matrixBT* invC * matrixB  +ctrl%relax_direct*identity_npot_on
             ! set properties
             ABTinvCB%is_symmetric = .TRUE.
             ! set this flag to speed assembly
             ABTinvCB%pair_list(1)%pattern = 'AWAT+rho*I'
             ! form explicitely 
             call formed_ABTinvCB%form_new_linop(info,lun_err,ABTinvCB)
             formed_ABTinvCB%name='SchurAC'
             
             !
             ! SOLVE LINEAR SYSTEM
             !
             
             !
             ! set controls per inner solver (solver used in preconditioner)
             !
             call ctrl_solver%init(lun_err,&
                  approach=ctrl%outer_solver_approach,&
                  scheme=ctrl%outer_krylov_scheme,&
                  imax=ctrl%outer_imax,&
                  iexit=ctrl%outer_iexit,&
                  isol=ctrl%outer_iexit,&
                  nrestart=ctrl%krylov_nrestart,&
                  tol_sol=ctrl%outer_tolerance)
             if (ctrl_solver%approach=='ITERATIVE') then
                !
                ! set inversion controls from ctrl
                !
                call ctrl_prec%init(&
                     lun_err,&
                     ctrl%outer_prec_type,&
                     ctrl%outer_prec_n_fillin,&
                     ctrl%outer_prec_tol_fillin)

                if (ctrl%info_update .ge. 3) call ctrl_prec%info(lun_out)
             end if

             !
             ! define (approximate) inverse 
             !
             call inv_ABTinvCB%init(&
                  formed_ABTinvCB,ctrl_solver,ctrl_prec,&
                  lun=lun_err)
             if ( debug.ge. 2) call inv_ABTinvCB%info(6)
             inv_ABTinvCB%name='invSchurAC'

             !
             ! initialized precondtioner of saddle point matrix 
             !
             call jacobian_prec%init(&
                  lun_err,&
                  jacobian_matrix,&
                  prec_type='SchurACFull',&
                  invSchurAC=inv_ABTinvCB, invC=invC)
             jacobian_prec%name='SaddlePointSolver'

             !
             ! apply inverse
             !
             call jacobian_prec%Mxv(rhs_full, increment,info,lun_err)
             
             !
             ! copy info solver
             !
             this%info_solver=inv_ABTinvCB%info_solver
             


          case ('full')
             select case (ctrl%outer_solver_approach) 
             case ('reduced')
                !
                ! 
                !
             end select
          end select

          
          !
          ! print info of linear solver
          !
          call this%info_solver%info2str(str) 
          write(msg,'(a,a,a)') &
               etb(head),'LINEAR SOLVER :  ',etb(str)
          call ctrl%print_string('update',2,msg)
          
          call jacobian_matrix%Mxv(increment, scr_nfull(1:nfull))
          scr_nfull=scr_nfull-rhs_full
          write(msg2,'(a,a,1pe9.2,a,1pe9.2)') &
               etb(head),'RESIDUA : \|J s + F\|=',dnrm2(nfull,scr_nfull,1),&
               '\|J s + F\|/|F|=',&
               dnrm2(nfull,scr_nfull,1)/dnrm2(nfull,rhs_full,1)
          call ctrl%print_string('update',2,msg2)

          ! break in case of linear solver error
          if (this%info_solver%ierr .ne. 0) then
             info_newton=110
             info = 110
             exit
          end if
          
          !
          ! set newton incremental step
          !
          call this%build_grad(this%matrixAT,increment(1:npot),gradinc, this%inv_weight)
          newton_ctrl%alpha=one
          test_cycle=.True.
          do while (test_cycle)
             potnew     = pot   + newton_ctrl%alpha * increment(1:npot)
             tdensnew   = tdens + newton_ctrl%alpha * increment(npot+1:nfull)
             gradnew    = grad  + newton_ctrl%alpha * gradinc 

             call this%assembly_mirrow_hessian_lyapunov_tdens(&
                  ode_inputs,tdensnew,gradnew,&
                  W22pure)
             W22pure=W22pure*this%inv_weight
             test = maxval(-1/deltat-W22pure) < -epsilon_W22
             if ( test ) exit
             newton_ctrl%alpha=&
                  newton_ctrl%alpha/newton_ctrl%line_search_contraction_rate
             test_cycle = ( newton_ctrl%alpha > newton_ctrl%alpha_limit) 
             if ( .not. test_cycle ) then
                info_newton=230
                info=info_newton
             end if
          end do
          write(msg,'(a,a,1pe9.2)') &
               etb(head), 'Alpha Dumping =', newton_ctrl%alpha
          call ctrl%print_string('update',2,msg)

          ! break if dumping is too small
          if (info .ne. 0) exit
          
       end select
       
       call newton_raphson_reverse_with_dt(&
            flag,info_newton, nfull, xvar, increment,&
            newton_ctrl,newton_info)

       ! cheack if newton had some problem and set info flag
       if (info_newton .eq. -1 ) then
          info=210
          exit
       else if (info_newton .eq. -2 ) then
          info=220
          exit
       end if
       
    end do

    write(*,*) 'info', info,'info_newton',info_newton

    if ( info .eq. 0)  then
       tdpot%pot=pot
       tdpot%tdens=tdens
       
       write(sepa,'(a)') ctrl%separator()              
       write(msg,'(a,1x,I2,1x,a,1pe8.2)') 'NEWTON CONVERGED IN', newton_info%current_newton_step,&
            'ITERATIONS WITH RESIDUA=',newton_ctrl%fnewton_norm
       call ctrl%print_string('update',1,sepa)
       call ctrl%print_string('update',1,msg)
    else
       write(sepa,'(a)') ctrl%separator()              
       write(msg,'(a,1x,I2,1x,a,1pe8.2,a,I3)') 'NEWTON FAILED AFTER ', newton_info%current_newton_step,&
            'ITERATIONS WITH RESIDUA=',newton_ctrl%fnewton_norm,' info=',info
       call ctrl%print_string('update',1,sepa)
       call ctrl%print_string('update',1,msg)
    end if
    

    ! update tdpot time related quantities
    if ( info == 0 ) then
       tdpot%deltat=ctrl%deltat
       tdpot%time_old = tdpot%time
       tdpot%time=tdpot%time+ctrl%deltat
       tdpot%iter_nonlinear = newton_info%current_newton_step
       tdpot%time_iteration = tdpot%time_iteration + 1
       tdpot%time_update    = tdpot%time_update + 1
    end if

    
  end subroutine new_implicit_euler_newton_tdens



  
!!$  subroutine solve_with_least_square_precondiitoner(this,ctrl,jacobian_matrix,rhs, sol)
!!$    implicit none
!!$    type(admk),      intent(in   ) :: this
!!$    type(dmkctrl),   intent(in   ) :: ctrl
!!$    type(new_linop), intent(inout) :: jacobian_matrix
!!$

   ! least square precondioner    
!!$    type(stiffmat) :: laplacian ! A L^-1 AT
!!$    type(stiffmat) :: weighted_laplacian ! A Tdens L^-1 AT
!!$    type(spmat)    :: spmat_laplacian,spmat_weighted_laplacian
!!$    type(agmg_inv) :: mg_laplacian_inverse
!!$    type(agmg_inv) :: mg_weighted_laplacian_inverse
!!$  type(diagmat) ,target   :: invW11,invW12,invW21,invW22,invL
!!$    type(diagmat) ,target   :: V11,V12,V21,V22
!!$    type(diagmat) ,target   :: invV11,invV12,invV21,invV22
!!$    type(diagmat) ,target   :: LEN,invLEN,SQLEN,invSQLEN
!!$    type(block_linop),target:: diag_AT_eye,diag_A_eye,W_block,invV_block,invZZT
!!$    type(pair_linop),target :: jacobian,least_square_preconditioner
!!$    type(block_linop), target :: diag_invSQLEN_SQLEN
!!$    type(block_linop), target :: invDG
!!$    type(pair_linop),target :: div_invSQLEN,invSQLEN_grad
!!$    type(spmat),target :: div_op,grad_op,divgrad
!!$    type(block_linop), target :: relaxation_matrix
!!$    type(linear_combination_linop), target :: relaxed_jacobian

!!$   !
!!$    ! init saddle point matrix M
!!$    !
!!$    call matrixM%saddle_point(&
!!$         lun_err,&
!!$         !matrixA,&
!!$         matrixAbis,&
!!$         matrixB1T,matrixB2,.True.,&
!!$         matrixC)
!!$
!!$    !
!!$    if (ctrl%reduced_jacobian .eq. 0) then
!!$       call V11%init(lun_err,ntdens)
!!$       call V12%init(lun_err,ntdens)
!!$       call V21%init(lun_err,ntdens)
!!$       call V22%init(lun_err,ntdens)
!!$
!!$       ! helman prec with V^{-1}
!!$       !
!!$       call invV11%init(lun_err,ntdens)
!!$       call invV12%init(lun_err,ntdens)
!!$       call invV21%init(lun_err,ntdens)
!!$       call invV22%init(lun_err,ntdens)
!!$
!!$       !
!!$       ! tilde J=(L^{-1/2} 0       )
!!$       !         (0        L^{1/2} )
!!$       ! 
!!$       components(1)%linop => invSQLEN
!!$       components(2)%linop => SQLEN
!!$       call diag_invSQLEN_SQLEN%block_diagonal(lun_err, &
!!$            2,components(1:2))
!!$
!!$
!!$       !
!!$       ! init D = ( AT L^{-1/2} 0       )=(AT  )( L^{-1(2}         )
!!$       !          ( 0           L^{1/2} ) (   I)(          L^{1/2} )
!!$       components(1)%linop => diag_AT_eye
!!$       components(2)%linop => diag_invSQLEN_SQLEN
!!$       call div_invSQLEN%init(info, lun_err, &
!!$            2, components(1:2),'LP')!
!!$
!!$       !
!!$       ! init G = ( L^{-1/2}AT  0       )=(AT  )( L^{-1(2}         )
!!$       !          ( 0           L^{1/2} ) (   I)(          L^{1/2} )
!!$       components(1)%linop => diag_invSQLEN_SQLEN
!!$       components(2)%linop => diag_A_eye
!!$       call invSQLEN_grad%init(info, lun_err, &
!!$            2, components(1:2),'LP')!
!!$
!!$       !
!!$       ! init inv_W_scaled=(W11/L W12  )^{-1} = (invW11 invW12 )
!!$       !                   (W21   W22*L)        (invW21 invW22 )
!!$       if ( ctrl%debug .eq. 1 ) &
!!$            write( lun_out, *) &
!!$            ' Init diagonals block matrix '
!!$       components(1)%linop => invV11
!!$       W_block_structure(:,1) = (/1,1,1/) 
!!$       components(2)%linop => invV12
!!$       W_block_structure(:,2) = (/2,1,2/) 
!!$       components(3)%linop => invV21
!!$       W_block_structure(:,3) = (/3,2,1/) 
!!$       components(4)%linop => invV22
!!$       W_block_structure(:,4) = (/4,2,2/)
!!$       call invV_block%init(&
!!$            lun_err,&
!!$            4, components(1:4),&
!!$            2 , 2, 4, &
!!$            W_block_structure, .True.)
!!$       !
!!$       ! eps I  0
!!$       ! 0      0
!!$
!!$       components(1)%linop => identity_npot_on
!!$       
!!$       components(2)%linop => identity_ntdens_on
!!$       call relaxation_matrix%block_diagonal(lun_err, &
!!$            2,components(1:2),(/ctrl%relax_direct,zero/))
!!$       call relaxation_matrix%info(6)
!!$
!!$      
!!$       
!!$    end if
!!$
!!$
!!$
!!$  
!!$    write(*,*) tdpot%ntdens_on
!!$    if ( ctrl%debug .eq. 1 ) &
!!$         write( lun_out, *) &
!!$         ' Init diagonals block matrix '
!!$    components(1)%linop => W11_scaled 
!!$    W_block_structure(:,1) = (/1,1,1/) 
!!$    components(2)%linop => W12
!!$    W_block_structure(:,2) = (/2,1,2/) 
!!$    components(3)%linop => W21
!!$    W_block_structure(:,3) = (/3,2,1/) 
!!$    components(4)%linop => W22_scaled
!!$    W_block_structure(:,4) = (/4,2,2/) 
!!$
!!$    call W_block%init(&
!!$         lun_err,&
!!$         4, components(1:4),&
!!$         2 , 2, 4, &
!!$         W_block_structure, .True.)
!!$
!!$    !
!!$    ! init (A  )
!!$    !      (  I)
!!$    components(1)%linop => this%matrixAT
!!$    components(2)%linop => identity_ntdens_on
!!$    call diag_A_eye%block_diagonal(lun_err, &
!!$         2,components(1:2))
!!$    if ( ctrl%debug .eq. 1 ) then
!!$       write( lun_out, *) 'Init Diag_A_eye'
!!$       !call diag_A_eye%info(6)
!!$    end if

  
!!$      !
!!$    ! init (A  )
!!$    !      (  I)
!!$    components(1)%linop => this%matrixA
!!$    components(2)%linop => identity_ntdens_on
!!$    call diag_AT_eye%block_diagonal(lun_err, &
!!$         2,components(1:2) )
!!$    if ( ctrl%debug .eq. 1 ) then
!!$       write( lun_out, *) 'Init Diag_AT_eye'
!!$       !call diag_AT_eye%info(6)
!!$    end if
!!$
!!$    !
!!$    ! tilde J=(AT_eye mass W mass A_eye) 
!!$    ! 
!!$    components(1)%linop => diag_AT_eye
!!$    components(2)%linop => W_block
!!$    components(3)%linop => diag_A_eye
!!$    call jacobian%init( info, lun_err, &
!!$         3, components(1:3),'LP')
!!$    if ( ctrl%debug .eq. 1 ) then
!!$       write( lun_out, *)  'Init jacobian'
!!$       !call jacobian%info(6)
!!$    end if
!!$
!!$
!!$    !
!!$    !   matrixA = AT W11 scaled A
!!$    !
!!$    this%scr_ntdens=one
!!$    call matrixA_old%init(lun_err,&
!!$         this%matrixAT,this%scr_ntdens)
!!$
!!$        
!!$    !
!!$    !  matrixB1T=AT W12 
!!$    !
!!$    components(1)%linop => this%matrixA
!!$    components(2)%linop => W11_scaled
!!$    components(3)%linop => this%matrixAT
!!$    call matrixAbis%init( info, lun_err, 3, components(1:3),'LP')
!!$
!!$    
!!$    !
!!$    !  matrixB1T=AT W12 
!!$    !
!!$    components(1)%linop => this%matrixA
!!$    components(2)%linop => W12
!!$    call matrixB1T%init( info, lun_err, 2, components(1:2),'LP')
!!$
!!$    !
!!$    !  matrixB2=W12 A 
!!$    !
!!$    components(1)%linop => W21
!!$    components(2)%linop => this%matrixAT
!!$    call matrixB2%init( info, lun_err, 2, components(1:2),'LP')



!!$      call LEN%init(lun_err,tdpot%ntdens_on)
!!$    call INVLEN%init(lun_err,tdpot%ntdens_on)
!!$    call SQLEN%init(lun_err,tdpot%ntdens_on)
!!$    call INVSQLEN%init(lun_err,tdpot%ntdens_on)
!!$    LEN%diagonal=this%weight
!!$    INVLEN%diagonal=this%inv_weight
!!$    SQLEN%diagonal=sqrt(this%weight)
!!$    INVSQLEN%diagonal=one/SQLEN%diagonal


  
!!$    !
!!$    ! Full Approach inverting the full jacobian
!!$    !
!!$    V11%diagonal = W11%diagonal
!!$    V12%diagonal = W12%diagonal
!!$    V21%diagonal = W21%diagonal
!!$    V22%diagonal = W22%diagonal 
!!$
!!$    !
!!$    ! init W^{-1}
!!$    !
!!$    do icell=1,ntdens
!!$       call inverse22(&
!!$                                ! direct
!!$            V12%diagonal(icell),& 
!!$            V12%diagonal(icell),&
!!$            V21%diagonal(icell),&
!!$            V22%diagonal(icell),&
!!$                                ! inverse component
!!$            invV11%diagonal(icell),& 
!!$            invV12%diagonal(icell),&
!!$            invV21%diagonal(icell),&
!!$            invV22%diagonal(icell))
!!$    end do
!!$
!!$    !
!!$    ! init (D G)^{-1}=( AT L^{-1} A   0)
!!$    !                 ( 0             L)
!!$    select type (mat=>this%matrixAT)
!!$    type is (spmat)
!!$       !
!!$       ! grad_op=A^T div_op=A 
!!$       !
!!$       grad_op=mat
!!$       div_op=mat
!!$       call div_op%transpose(lun_err)
!!$       this%scr_ntdens=invLEN%diagonal
!!$
!!$       !
!!$       ! divgrad=A^T L^{-1} A
!!$       !
!!$       call divgrad%mult_MDN(lun_err,&
!!$            div_op, grad_op,&
!!$            20, 20*ntdens,&
!!$            this%scr_ntdens, &
!!$            div_op)
!!$       matrix2solve=divgrad
!!$       call matrix2solve%aMpN(ctrl%relax_direct, identity_npot_on)             
!!$
!!$       !
!!$       ! init (divgrad)^{-1}
!!$       !
!!$       call ctrl_multigrid%init(lun_err,&
!!$            scheme='PCG',&
!!$            iexit=1,&
!!$            imax=ctrl%imax,&
!!$            isol=0,&
!!$            tol_sol=ctrl%tolerance_linear_newton)
!!$
!!$
!!$       call multigrid_solver%init(lun_err,matrix_to_solve,ctrl_multigrid)
!!$
!!$       !
!!$       ! init DG^{-1} = (divgrad)^{-1}        )
!!$       !                (              L^{-1} )
!!$       !             
!!$       components(1)%linop => multigrid_solver
!!$       components(2)%linop => invLEN
!!$       call invDG%block_diagonal(lun_err, &
!!$            2,components(1:2))
!!$
!!$
!!$       !
!!$       ! tilde J=(AT_eye mass W mass A_eye) 
!!$       ! 
!!$       components(1)%linop => invDG
!!$       components(2)%linop => div_invSQLEN
!!$       components(3)%linop => invV_block
!!$       components(4)%linop => invSQLEN_grad
!!$       components(5)%linop => invDG
!!$       call least_square_preconditioner%init( lun_err, &
!!$            5, components(1:5),.True.)!
!!$    end select
!!$
!!$
!!$    
!!$
!!$    subroutine inverse22(a,b,c,d,inva,invb,invc,invd)
!!$      use Globals
!!$      implicit none
!!$      real(kind=double),intent(in) :: a,b,c,d 
!!$      real(kind=double),intent(inout) :: inva,invb,invc,invd
!!$      !local
!!$      real(kind=double) :: invdets
!!$
!!$      invdets=a*d-b*c
!!$
!!$      inva= d*invdets
!!$      invb=-b*invdets
!!$      invc=-c*invdets
!!$      invd=-a*invdets
!!$    end subroutine inverse22
!!$
!!$  end subroutine least_square_precondiitoner


  
  !>------------------------------------------------------
  !> procedure to compute the gradient of the potential
  !>     \Grad = [mweight]^{-1} \matrix_a \Pot
  !> with mweight optional (default = Id )
  !>------------------------------------------------------
  subroutine build_grad(matrixAT,pot,grad,inv_weight)
    use Globals
    use Matrix
    implicit none
    class(abs_linop),             intent(inout) :: matrixAT
    real(kind=double),            intent(in   ) :: pot(matrixAT%ncol)
    real(kind=double),            intent(inout) :: grad(matrixAT%nrow)
    real(kind=double),  optional, intent(in   ) :: inv_weight(matrixAT%nrow)
    !local
    integer :: icell

    call matrixAT%Mxv(pot,grad)
    if ( present(inv_weight) ) then
       do icell=1,matrixAT%nrow
          grad(icell) =  grad(icell) * inv_weight(icell)
       end do
    end if
  end subroutine build_grad


    !>--------------------------------------------------------------
  !> Subroutine to assign the pointer prec_zero to a
  !> stdprec. Two option are available
  !> prec_zero => prec_saved
  !> prec_zero => prec_local
  !>--------------------------------------------------------------
  subroutine assembly_prec(lun_err,&
       matrix2prec,&
       build_prec,&
       ctrl_prec,&
       info_prec,&
       prec_saved,&
       nnz,selection,&
       lasso)
      use Globals
      implicit none
      integer,                  intent(in   ) :: lun_err
      type(stiffmat),           intent(inout) :: matrix2prec
      integer,                  intent(in   ) :: build_prec
      type(input_prec),    intent(in   ) :: ctrl_prec
      integer,                  intent(inout) :: info_prec
      type(stiffprec), target,  intent(inout) :: prec_saved
      integer, intent(in) :: nnz
      integer, intent(in) :: selection(nnz)
      real(kind=double), optional, intent(in) :: lasso
      !type(stiffprec), pointer, intent(inout) :: prec_zero
      ! local
      logical :: rc,test
      integer :: i
      type(stiffprec) :: prec_local
      type(spmat) :: lower, test_factor

      if ( build_prec .eq. 1 ) then
         ! assembly explicit matrix is required
         !if ( ctrl_prec%should_I_assembly(matrix2prec) ) then
         !write(*,*) 'lasso lift',lasso
         call matrix2prec%assembly(nnz,selection,.True.,lasso)
         
         ! assembly prec on local work prec
         call prec_local%init(lun_err, &
               matrix2prec, ctrl_prec, info_prec)
         call prec_local%assembly(lun_err, &
               matrix2prec, ctrl_prec, info_prec)
                  
         
         if ( info_prec .eq. 0 ) then
            ! prec_saved is updated with new preconditioner
            prec_saved = prec_local

            ! free memory
            !
            call prec_local%kill(lun_err)
         else
            !
            ! write warning 
            !
            rc = IOerr(lun_err, wrn_inp, 'assembly_prec', &
                 ' prec. assembly failed info_prec=',info_prec)
            if ( .not. prec_saved%is_built) then
               ! check if prec_saved contains something
               rc = IOerr(lun_err, err_inp, 'assembly_prec', &
                    ' prec. saved not built')
            else
               ! assign saved prec
               info_prec=0
               !prec_zero => prec_saved 
            end if
         end if
      else
         ! use saved prec.
         if ( .not. prec_saved%is_built) then
            ! check if prec_saved contains something
            rc = IOerr(lun_err, err_inp, 'assembly_prec', &
                 ' prec. saved not built')
         else
            ! assign saved prec
            !prec_zero => prec_saved 
         end if
      end if

    end subroutine assembly_prec


    subroutine set_active_tdpot(this,tdpot,ctrl)
      use Globals
      implicit none
      class(admk),    intent(inout) :: this
      type(tdpotsys), intent(inout) :: tdpot
      type(DmkCtrl),  intent(in   ) :: ctrl
      ! local 
      integer :: icell,icell_on,inode,j
      logical :: on
      character(len=256):: out_format,msg

      select case (ctrl%selection )
      case (1)
         tdpot%ntdens_on    = 0
         tdpot%ntdens_off   = 0
         tdpot%active_tdens = 0
         tdpot%onoff_tdens  = .False.

         !
         ! set active those tdens above given threeshold
         !
         do icell=1,tdpot%ntdens
            on = ( tdpot%tdens(icell) .ge. ctrl%threshold_tdens)
            tdpot%onoff_tdens(icell) = on
            if ( on ) then
               tdpot%ntdens_on = tdpot%ntdens_on + 1
               tdpot%active_tdens(tdpot%ntdens_on) = icell
            else
               tdpot%ntdens_off = tdpot%ntdens_off + 1 
               tdpot%inactive_tdens(tdpot%ntdens_off) = icell
            end if
         end do

         !
         ! create vector selection_pot with non zeros
         ! for active nodes, rows
         !
         select type(mat=> this%matrixAT)
         type is (spmat)
            tdpot%scr_npot=zero
            do icell_on = 1,tdpot%ntdens_on
               icell=tdpot%active_tdens(icell_on)
               do j=mat%ia(icell),mat%ia(icell+1)-1
                  tdpot%scr_npot(mat%ja(j))= tdpot%scr_npot(mat%ja(j))+one
               end do
            end do
         end select

         !
         ! set active nodes
         !
         tdpot%npot_on    = 0
         tdpot%npot_off   = 0
         tdpot%active_pot = 0
         tdpot%onoff_pot  = .False.
         do inode=1,tdpot%npot
            if ( tdpot%scr_npot(inode)>1e-12 )then
               tdpot%npot_on = tdpot%npot_on + 1
               tdpot%active_pot(tdpot%npot_on) = inode
               tdpot%onoff_pot(inode)=.True.
            else
               tdpot%npot_off = tdpot%npot_off + 1
               tdpot%inactive_pot(tdpot%npot_off) = inode
            end if
         end do
      end select
    end subroutine set_active_tdpot

    !>----------------------------------------------------
    !> Procedure to set controls for next update within
    !> the dmk_cycle_reverse_communication subroutine.
    !> Derived type  this(dmkpair), ode_inputs and tdpot
    !> are used to extimate best controls for next update.
    !> ( public procedure for type dmkpair,
    !> used in dmk_cycle_reverse_communication)
    !> 
    !> usage: call var%set_controls_next_update(lun_err,itemp)
    !> 
    !> where:
    !> \param[in   ] tdpot -> type(tdpotsys). Tdens-pot system
    !>                          syncronized at time $t^{k}$
    !>                          to be updated at time $t^{k+1}$
    !> \param[in   ] odein -> type(DmkInputs). Input data
    !>                          at time t^k or t^{k+1}, according
    !>                          time discretization scheme
    !> \param[inout] ctrl  -> type(CtrlPrm). Controls of DMK
    !>                        evolution (time-step, linear system approach)
    !>
    !> COMMENTS:
    !> This is passed with intent inout for using scratch
    !> arrays and avoid allocation/deallocation
    !> of temporary arrays
    !<---------------------------------------------------    
    subroutine set_controls_next_update(this,&   
         ode_inputs,&
         tdpot,&
         ctrl,input_time)
      use DmkInputsData 
      use TdensPotentialSystem
      use DmkControls
      implicit none
      class(admk),    intent(inout) :: this
      type(DmkInputs),   intent(in   ) :: ode_inputs
      type(tdpotsys),    intent(in   ) :: tdpot
      type(DmkCtrl),     intent(inout) :: ctrl
      real(kind=double), intent(inout) :: input_time

      ! local
      integer :: ntdens
      real(kind=double) :: deltat,max_W22,max_grad_gfvar
      real(kind=double),allocatable :: gfvar(:),trans_prime(:),trans_second(:)

      ntdens=tdpot%ntdens
      !
      ! time step controls
      !
      select case ( ctrl%deltat_control ) 
      case( 1 )
         ! constant time step
         deltat     = ctrl%deltat
         input_time = tdpot%time
      case( 2 )
         ! increasing time step
         deltat=ctrl%deltat * ctrl%deltat_expansion_rate
         deltat=min(deltat,ctrl%deltat_upper_bound)
         deltat=max(deltat,ctrl%deltat_lower_bound)

         input_time = tdpot%time + ctrl%deltat
      case (3 )
         select case (ctrl%time_discretization_scheme)
         case (4)
            !
            ! deltat is settled to get block 2-2 in jacobian to be above threshold
            ! ctrl%epsilon_W22
            !
            allocate(gfvar(ntdens),trans_prime(ntdens),trans_second(ntdens))
            call this%build_grad(this%matrixAT,tdpot%pot,this%grad, this%inv_weight)
            call tdens2gfvar(ctrl%trans,ntdens,tdpot%tdens,gfvar)
            call gfvar2f(ctrl%trans,ntdens,gfvar,trans_prime,trans_second)
            call build_W22_pure(this%ntdens,trans_second,this%grad,this%scr_ntdens)
            max_grad_gfvar= maxval(abs(this%weight*(onehalf*trans_prime* ( this%grad**2-one))))
            max_W22=maxval(this%scr_ntdens)

            if ( max_W22 <0 ) then
               deltat=1./(16*max_grad_gfvar);
            else
               deltat=1/( max_W22 + ctrl%epsilon_W22);
            end if

            input_time = tdpot%time + deltat
            
            deallocate(gfvar,trans_prime,trans_second)
         end select
         



      end select
      ctrl%deltat=deltat


      select case (ctrl%time_discretization_scheme)
      case (1)
         call set_threshold(this,tdpot,ctrl)
      end select

      select case ( ctrl%time_discretization_scheme )
       case (1)
          input_time=tdpot%time
       case (2)
          input_time=tdpot%time+ctrl%deltat
       case (3)
          input_time=tdpot%time
       case (4)
          input_time=tdpot%time+ctrl%deltat
       end select


    contains
      subroutine set_threshold(this,tdpot,ctrl)
        implicit none
        type(admk),     intent(in   ) :: this
        type(tdpotsys), intent(in   ) :: tdpot
        type(DmkCtrl),  intent(inout) :: ctrl
        !local
        logical :: debug=.False.

        if (debug) write(*,*) 'BEGIN : set_threshold'
        if (debug) write(*,*) 'ctrl%id_buffer_prec',ctrl%id_buffer_prec

        ! threshold is the minimum between the 
        ! number of iterations 
        ! required just after preconditioning and
        ! reference thereshold
        select case (ctrl%id_buffer_prec)
        case (0)
           ctrl%build_prec=1
        case(1)
           ! cosntant threshold given in controls file
           ctrl%threshold_prec  = ctrl%ref_iter
        case (2)
           !
           ! threshold is average of the linear solver iterations
           ! 
           ctrl%threshold_prec  = &
                int( tdpot%total_iterations_linear_system  / &
                tdpot%total_number_linear_system ) 
        case (3)
           ! threshold is the number of iterations 
           ! required using the optimal preconditioner
           ctrl%threshold_prec =  tdpot%iter_last_prec
        case (4)
           ! threshold is the minimum between the 
           ! number of iterations 
           ! required just after preconditioning and
           ! reference thereshold
           ctrl%threshold_prec =  min(ctrl%ref_iter, tdpot%iter_last_prec)
        case (5)
           ! iteration for solution of first elliptic 
           ctrl%threshold_prec = tdpot%iter_first_system
        end select

        if ( ctrl%id_buffer_prec .ne. 0) then
           if (debug) write(*,*) 'this%info_solver%iter=', this%info_solver%iter,&
                'ctrl%prec_growth=',ctrl%prec_growth,&
                'ctrl%threshold_prec=',ctrl%threshold_prec
           if ( this%info_solver%iter > & 
                int(ctrl%prec_growth  * ctrl%threshold_prec ) ) then
              ctrl%build_prec=1
           else
              ctrl%build_prec   = 0

           end if
        else
           ctrl%build_prec=1
        end if

        if (debug) write(*,*) 'END : set_threshold'
      end subroutine set_threshold

      
      
    end subroutine set_controls_next_update


      !>----------------------------------------------------
  !> Procedure to reset controls after failure of update_tdpot
  !> procedure within the dmk_cycle_reverse_communication
  !> procedure.
  !> Derived type  this(dmkpair), ode_inputs and tdpot
  !> are used to extimate best controls for next update.
  !> ( public procedure for type dmkpair,
  !> used in dmk_cycle_reverse_communication)
  !> 
  !> usage: call var%update(lun_err,itemp)
  !> 
  !> where:
  !> \param[in   ] tdpot -> type(tdpotsys). Tdens-pot system
  !>                          syncronized at time $t^{k}$
  !>                          to be updated at time $t^{k+1}$
  !> \param[in   ] odein -> type(DmkInputs). Input data
  !>                          at time t^k or t^{k+1}, according
  !>                          time discretization scheme
  !> \param[inout] ctrl  -> type(CtrlPrm). Controls of DMK
  !>                        evolution (time-step, linear system approach)
  !>
  !> COMMENTS:
  !> This is passed with intent inout for using scratch
  !> arrays and avoid allocation/deallocation
  !> of temporary arrays
  !<---------------------------------------------------   
  subroutine reset_controls_after_update_failure(this,&
       info,&
       ode_inputs,&
       tdpot,&
       original_ctrl,&
       ctrl,&
       input_time,&
       ask_for_inputs)
    implicit none
    class(admk),       intent(inout) :: this
    integer,           intent(in   ) :: info
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(DmkCtrl),     intent(in   ) :: original_ctrl
    type(DmkCtrl),     intent(inout) :: ctrl
    real(kind=double), intent(inout) :: input_time
    logical,           intent(inout) :: ask_for_inputs
    ! local
    integer :: digit1,digit2,digit3

    digit1=info/100
    digit2=mod(info,100)/10
    digit3=mod(info,10)

    write(*,*) 'Digit 1',  digit1, ' 2 ',  digit2,' 3 ', digit3
    select case ( digit1 )
           
    case (1)
       ! failure of update due failure in linear system
       ! solution 
       ctrl%deltat=max(ctrl%deltat_lower_bound,&
            min(ctrl%deltat_upper_bound,ctrl%deltat/ctrl%deltat_expansion_rate))
    case (2)
       ! failure of non linear solver for update 
       ! (convergence not achivied, diverging residua, etc) 
       ! 
       ctrl%deltat=max(ctrl%deltat_lower_bound,&
            min(ctrl%deltat_upper_bound,ctrl%deltat/ctrl%deltat_expansion_rate))
    end select


    select case ( ctrl%time_discretization_scheme )
    case (1)
       input_time=tdpot%time
    case (2)
       input_time=tdpot%time+ctrl%deltat
    case (3)
       input_time=tdpot%time
    case (4)
       input_time=tdpot%time+ctrl%deltat
    end select
    
       


  end subroutine reset_controls_after_update_failure


  subroutine build_W22_pure(ntdens,trans_second,grad,W22pure)
    use Globals
    implicit none
    integer,                           intent(in   ) :: ntdens
    real(kind=double),                 intent(in   ) :: trans_second(ntdens)
    real(kind=double),                 intent(in   ) :: grad(ntdens)
    real(kind=double),                 intent(inout) :: W22pure(ntdens)

    W22pure = onehalf * trans_second * ( grad**2-one)
  end subroutine build_W22_pure

    subroutine gfvar2tdens( trans ,ntdens,gfvar,tdens)
      use Globals
      implicit none
      integer , intent(in) :: trans
      integer , intent(in) :: ntdens
      real(kind=double), intent(in) :: gfvar(ntdens)
      real(kind=double), intent(inout) :: tdens(ntdens)
      select case ( trans ) 
      case (0)
         tdens=gfvar;
      case (1)
         tdens=onefourth*gfvar**2
      case (2)
         tdens=exp(gfvar)
      end select
    end subroutine gfvar2tdens

    subroutine tdens2gfvar( trans ,ntdens,tdens,gfvar)
      use Globals
      implicit none
      integer , intent(in) :: trans
      integer , intent(in) :: ntdens
      real(kind=double), intent(in) :: tdens(ntdens)
      real(kind=double), intent(inout) :: gfvar(ntdens)
      select case ( trans ) 
      case (0)
         gfvar=tdens
      case (1)
         gfvar=sqrt(4.0d0*tdens)
      case (2)
         gfvar=log(tdens)
      end select
    end subroutine tdens2gfvar

    subroutine gfvar2f( trans ,ntdens,gfvar,trans_prime,trans_second)
      use Globals
      implicit none
      integer , intent(in) :: trans
      integer , intent(in) :: ntdens

      real(kind=double), intent(in) :: gfvar(ntdens)
      real(kind=double), intent(inout) :: trans_prime(ntdens)
      real(kind=double), intent(inout) :: trans_second(ntdens)
      select case ( trans ) 
      case (0)
         trans_prime = one
         trans_second = zero
      case (1)
         trans_prime  = onehalf*gfvar
         trans_second = onehalf
      case (2)
         trans_prime  = exp(gfvar)
         trans_second = exp(gfvar)
      end select
    end subroutine gfvar2f

    subroutine build_hessian_lyapunov(this,ode_inputs,tdpot,trans,ctrl,info,hessian)
      implicit none
      class(admk),       intent(in   ) :: this
      type(DmkInputs),   intent(in   ) :: ode_inputs
      type(tdpotsys),    intent(in   ) :: tdpot
      integer ,          intent(in   ) :: trans
      type(DmkCtrl),     intent(in   ) :: ctrl
      integer ,          intent(inout) :: info
      type(densemat),     intent(inout) :: hessian
      ! local
      integer :: i,j,ntdens,npot
      real(kind=double), allocatable :: pot(:),grad(:),gfvar(:),tdens(:),tdens_stiff(:)
      real(kind=double), allocatable :: trans_prime(:),trans_second(:),scr_npot(:),scr_npot_bis(:)
      type(diagmat) ,target   :: W11_scaled,W11,W12,W21,W22,W22_scaled
      type(inv),   target :: inv_stiff
      type(spmat), target :: formed_matrixB,formed_matrixA
      type(new_linop),target :: matrixA,matrixB,matrixBT
      type(input_solver) ::ctrl_outer_solver 
      
      type(input_prec) :: ctrl_inner_solver

      integer :: lun_err

      
      ntdens=tdpot%ntdens
      npot  =tdpot%npot
      info  = 0
      lun_err=ctrl%lun_err

      call hessian%init(lun_err, ntdens,ntdens,is_symmetric=.True.,&
           triangular='N',symmetric_storage='F')
      
      allocate(&
           scr_npot_bis(npot),&
           scr_npot(npot),&
           pot(npot),&
           grad(ntdens),&
           gfvar(ntdens),&
           tdens(ntdens),&
           tdens_stiff(ntdens))
      call this%build_grad(this%matrixAT,pot,grad, this%inv_weight)
      call gfvar2tdens(trans,ntdens,gfvar,tdens)
      call gfvar2f(trans,ntdens,gfvar,trans_prime,trans_second)
      tdens_stiff=tdens+ctrl%relax_tdens

      call W11_scaled%init(lun_err,tdpot%ntdens_on)
      call W11%init(lun_err,tdpot%ntdens_on)
      call W12%init(lun_err,tdpot%ntdens_on)
      call W21%init(lun_err,tdpot%ntdens_on)
      call W22%init(lun_err,tdpot%ntdens_on)


      !
      ! assembly W component
      !
      W11%diagonal = tdens_stiff
      W11%name='W11'
      W12%diagonal = trans_prime * grad 
      W12%name='W12'
      W21%diagonal = trans_prime * grad
      W21%name='W21'
      W11_scaled%diagonal= W11%diagonal *  this%inv_weight

      !
      ! set matrix
      !
      matrixA  = this%matrixA * W11_scaled * this%matrixAT
      matrixBT = this%matrixA * W12
      matrixB  = W21*this%matrixAT

      !
      ! form explicitely matrices
      !
      call formed_matrixA%form_new_linop(info,lun_err,matrixA)
      call formed_matrixB%form_new_linop(info,lun_err,matrixB)

      !
      ! create A inverse
      ! set controls per inner solver (solver used in preconditioner)
      ctrl_outer_solver=ctrl%ctrl_outer_solver   
      if (ctrl%inner_prec_type=='INNER_SOLVER') then
        ctrl_outer_solver=ctrl%ctrl_inner_solver
      else
         !
         ! set inversion controls from ctrl
         !
         call ctrl_inner_solver%init(&
              lun_err,&
              ctrl%inner_prec_type,&
              ctrl%inner_prec_n_fillin,&
              ctrl%inner_prec_tol_fillin)
         call ctrl_inner_solver%info(6)
      end if

      !
      ! define (approximate) inverse 
      !
      call inv_stiff%init(formed_matrixA,ctrl_outer_solver,ctrl_inner_solver,lun=lun_err)
      if ( ctrl%debug.ge. 2) call inv_stiff%info(6)


      select type (mat => this%matrixA)
      type is (spmat)
         
         do i=1,tdpot%npot
            !
            ! create vector with i^th column of matrix BT
            ! 
            scr_npot=zero
            do j=mat%ia(i),mat%ia(i+1)-1
               scr_npot(mat%ja(j))=mat%coeff(j)
            end do
            !
            ! compute A^{-1} b^i with  b^i=B^T(:,i)
            !
            call inv_stiff%Mxv(this%scr_npot, scr_npot_bis,info,lun_err)
            call inv_stiff%info_solver%info(6)

            !
            ! compute B A^{-1} b^i 
            !
            call matrixB%Mxv(scr_npot_bis, hessian%coeff(:,i),info,lun_err)
         end do

      end select

    end subroutine build_hessian_lyapunov

    subroutine assembly_gradient_lyapunov_gfvar(this,&
       ode_inputs,&
       trans,&
       gfvar,tdens,pot,&
       grad_lyap)
    use DmkInputsData

    implicit none
    class(admk),       intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    integer,           intent(in   ) :: trans
    real(kind=double), intent(in   ) :: gfvar(this%ntdens) 
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: pot(this%npot)     
    real(kind=double), intent(inout) :: grad_lyap(this%ntdens) 
    !local
    integer :: icell,j
    integer :: ntdens
    real(kind=double) :: min,max, pflux, pmass,decay,pode
    real(kind=double) :: ptrans, gf_pflux, gf_pmass,power
    real(kind=double) :: penalty_factor
    ! local
    real(kind=double), allocatable :: trans_prime(:), trans_second(:), grad(:)
    
    ntdens = this%ntdens

    allocate( trans_prime(ntdens), trans_second(ntdens), grad(ntdens))

    pflux = ode_inputs%pflux
    pode  = ode_inputs%pode
    decay = ode_inputs%decay
    pmass = ode_inputs%pmass

   
    if( abs(pode-two) > 1e-12) then
       write(*,*) 'only pode=2.0, in assembly_gradient_lyapunov_gfvar'
       stop

    end if

    if( abs(pflux-one) > 1e-12) then
       write(*,*) 'only pflux=1.0, in assembly_gradient_lyapunov_gfvar'
       stop
    end if
    
    if( abs(pmass-one) > 1e-12) then
       write(*,*) 'only pmass=1.0, in assembly_gradient_lyapunov_gfvar'
       stop
    end if
    call gfvar2f(trans,ntdens,gfvar,trans_prime,trans_second)
    call this%build_grad(this%matrixAT,pot,grad,this%inv_weight)
    grad_lyap=this%weight*(-onehalf*trans_prime*(grad**2-one))
    

    deallocate( trans_prime, trans_second, grad)
  
  end subroutine assembly_gradient_lyapunov_gfvar

  subroutine assembly_mirrow_gradient_lyapunov_tdens(this,&
       ode_inputs,&
       tdens,pot,&
       rhs_ode)
    use DmkInputsData

    implicit none
    class(admk),       intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: pot(this%npot)     
    real(kind=double), intent(inout) :: rhs_ode(this%ntdens) 
    !local
    integer :: icell,j
    integer :: ntdens
    real(kind=double) :: min,max, pflux, pmass,decay,pode
    real(kind=double) :: ptrans, gf_pflux, gf_pmass,power
    real(kind=double) :: penalty_factor
    ! local
    real(kind=double), allocatable ::  grad(:)

    ntdens = this%ntdens

    allocate(  grad(ntdens) )

    pflux = ode_inputs%pflux
    pode  = ode_inputs%pode
    decay = ode_inputs%decay
    pmass = ode_inputs%pmass

    ! write(*,*) 'pflux = ', ode_inputs%pflux,&
    !      'pode  = ',ode_inputs%pode,&
    !      'decay = ',ode_inputs%decay,&
    !      'pmass = ',ode_inputs%pmass
    
    !
    ! Compute rhs ode 
    !
    call this%build_grad(this%matrixAT,pot,grad, this%inv_weight)
    rhs_ode = -(tdens  ** pflux ) * (abs(grad) ** pode) &
         + decay * ode_inputs%kappa * tdens ** pmass

    rhs_ode = this%weight * rhs_ode 


    deallocate( grad)

  end subroutine assembly_mirrow_gradient_lyapunov_tdens

  subroutine assembly_mirrow_hessian_lyapunov_tdens(this,&
       ode_inputs,&
       tdens,gradpot,&
       mirrow_hessian_lyapunov_tdens)
    use DmkInputsData

    implicit none
    class(admk),       intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    real(kind=double), intent(in   ) :: tdens(this%ntdens) 
    real(kind=double), intent(in   ) :: gradpot(this%ntdens)     
    real(kind=double), intent(inout) :: mirrow_hessian_lyapunov_tdens(this%ntdens) 
    !local
    integer :: icell,j
    integer :: ntdens
    real(kind=double) :: min,max, pflux, pmass,decay,pode
    real(kind=double) :: ptrans, gf_pflux, gf_pmass,power
    real(kind=double) :: penalty_factor
   
    ntdens = this%ntdens

    pflux = ode_inputs%pflux
    pode  = ode_inputs%pode
    decay = ode_inputs%decay
    pmass = ode_inputs%pmass

    !
    ! Compute rhs ode 
    !
    mirrow_hessian_lyapunov_tdens = -(pflux* tdens  ** (pflux-one) ) * (abs(gradpot) ** pode) + &
         decay * ode_inputs%kappa * pmass * tdens ** (pmass-one)
    !mirrow_hessian_lyapunov_tdens = - (abs(gradpot) ** pode) + one

    mirrow_hessian_lyapunov_tdens = this%weight * mirrow_hessian_lyapunov_tdens


    
  end subroutine assembly_mirrow_hessian_lyapunov_tdens


  

  !>----------------------------------------------------
  !> It evaluates the energy $\int \tdens |\nabla u|^2$
  !> (procedure public for type tdpotsys)
  !> 
  !> usage:    call var%energy()
  !>    
  !> where:
  !> \param  [in ] var    -> type(tdpotsys) 
  !> \result [out] energy -> real. Lyap. functional
  !<----------------------------------------------------
  function energy(this,tdens,gradpot) result(sum)
    implicit none
    class(admk),       intent(inout) :: this
    real(kind=double), intent(in   ) :: tdens(this%ntdens)
    real(kind=double), intent(in   ) :: gradpot(this%ntdens)
    real(kind=double) :: sum
    !local 
    integer :: icell


    sum = zero
    do icell = 1, this%ntdens
       sum = sum + &
            tdens( icell ) * &
            gradpot(icell)**2  * &
            this%weight(icell)
    end do
    sum = onehalf * sum
  end function energy

  !>------------------------------------------------------------
  !> Procedure evaluating $pvel$ exponent wihich is the candidate
  !> branch exponent for give pflux and pmass
  !>-------------------------------------------------------------
  function exponent_wmass(ode_inputs) result(exponent)
    implicit none
    type(DmkInputs),  intent(in   ) :: ode_inputs
    real(kind=double) :: exponent
    !local 
    real(kind=double) :: pflux
    real(kind=double) :: pmass
    real(kind=double) :: pode

    pflux = ode_inputs%pflux
    pmass = ode_inputs%pmass
    pode  = ode_inputs%pode

    exponent = zero
    if ( abs(pode-two) < small  ) then
       !pode = 2 pflux
       if (  abs(1 - pflux + pmass ) > 1.0d-10 ) then 
          exponent = 1.0d0 + pmass - pflux 
       else
          exponent = zero                  
       end if
    else
       !pode = pflux
       if ( ( abs(2 * pmass - pflux ) > 1.0d-10 ) .and. &
            ( abs(pflux             ) > 1.0d-10 ) ) then
          exponent = (2 * pmass - pflux ) / pflux 
       else
          exponent = zero
       end if
    end if

  end function exponent_wmass



  !>---------------------------------------------------------
  !> It evaluates the lyap. fun. 
  !> $\int tdens \ \int \tdens |\nabla u|^2+ \int \tdens$
  !> (procedure public for type fun)
  !> 
  !> usage: var%s_lyap()
  !>    
  !> where:
  !> \param  [in ] var     -> type(tdpotsys) 
  !> \result [out] s_lyap -> real. s_lyap. functional
  !<------------------------------------------------------
  function weighted_mass(this,tdens,ode_inputs) result(w_mass)
    implicit none
    class(admk),       intent(inout) :: this 
    real(kind=double), intent(in   ) :: tdens(this%ntdens)
    type(DmkInputs),   intent(in   ) :: ode_inputs
    ! output
    real(kind=double) :: w_mass
    !local 
    integer           :: icell
    real(kind=double) :: power, pflux, pmass,pode

    pflux = ode_inputs%pflux
    pmass = ode_inputs%pmass
    pode  = ode_inputs%pode

    if (abs(pode-two)>small ) then
       power = exponent_wmass(ode_inputs)
       if ( abs(power) .gt. 1.0d-10 ) then
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) *&
                  tdens(icell) ** power * &
                  this%weight(icell)
          end do
          w_mass = onehalf*w_mass/(power) 
       else
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) * &
                  log(tdens(icell))/log(exp(1.0d0))* &
                  this%weight(icell)
          end do
          w_mass = onehalf * w_mass 
       end if
    else
       power = exponent_wmass(ode_inputs)
       if ( abs(power) .gt. 1.0d-9 ) then
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) * &
                  tdens(icell) ** power            * &
                  this%weight(icell)
          end do
          w_mass =  onehalf*w_mass/(power) 
       else
          w_mass = zero
          do icell=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(icell)**2) * &
                  log(tdens(icell))/log(exp(1.0d0))* &
                  this%weight(icell)
          end do
          w_mass = onehalf * w_mass 
       end if
    end if

    !write(*,*) pflux, pmass,pode,power
  end function weighted_mass

  !>---------------------------------------------------------
  !> It evaluates the lyap. fun. 
  !> (procedure public for type fun)
  !> 
  !> usage: var%lyap()
  !>    
  !> where:
  !> \param  [in ] var     -> type(tdpotsys) 
  !> \result [out] s_lyap -> real. s_lyap. functional
  !<------------------------------------------------------
  function lyapunov(this,tdens,gradpot,ode_inputs) result(res)
    implicit none
    class(admk),       intent(inout) :: this ! we need a work array
    real(kind=double), intent(in   ) :: tdens(this%ntdens)
    real(kind=double), intent(in   ) :: gradpot(this%ntdens)
    type(DmkInputs),   intent(in   ) :: ode_inputs
    real(kind=double) :: res

    res = this%energy(tdens,gradpot) + this%weighted_mass(tdens,ode_inputs)

  end function lyapunov

  !>----------------------------------------------------
  !> Compute all functionals 
  !<---------------------------------------------------    
  subroutine compute_functionals(this,&
       ode_inputs,&
       tdpot,&
       ctrl)
    implicit none
    class(admk),       intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(inout) :: tdpot
    type(DmkCtrl),     intent(in   ) :: ctrl


    call this%build_grad(this%matrixAT,tdpot%pot,this%grad, this%inv_weight)

    tdpot%system_variation    = this%compute_tdpot_variation(tdpot,ctrl)
    tdpot%energy              = this%energy(tdpot%tdens,this%grad)
    tdpot%weighted_mass_tdens = this%weighted_mass(tdpot%tdens,ode_inputs)
    tdpot%lyapunov            = tdpot%energy + tdpot%weighted_mass_tdens
    tdpot%min_tdens           = minval(tdpot%tdens)
    tdpot%max_tdens           = maxval(tdpot%tdens)

  end subroutine compute_functionals






  !>-------------------------------------------------
  !> Precedure to store in timefun quantities
  !> (errors, number of iterations,etc) along
  !> time evolution.
  !> 
  !> ( public procedure for type p1p0_space_discretization)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine store_evolution(this,timefun,itemp,tdpot,ode_inputs,ctrl)
    use TdensPotentialSystem
    use DmkControls
    use TdensPotentialSystem
    use DmkInputsData
    use TimeFunctionals

    implicit none
    class(admk), target, intent(in   ) :: this
    type(evolfun),  intent(inout) :: timefun
    integer,        intent(in   ) :: itemp
    type(tdpotsys), intent(in   ) :: tdpot
    type(DmkInputs),intent(in   ) :: ode_inputs
    type(DmkCtrl),  intent(in   ) :: ctrl

    !local
    real(kind=double) :: power,w1dist

    !>----------------------------------------------------------------
    !> Integer Valued Fucntional
    !>----------------------------------------------------------------
    timefun%last_time_iteration = itemp
    timefun%iter_solver(1:tdpot%iter_nonlinear,itemp) = &
         this%sequence_info_solver(1:tdpot%iter_nonlinear)%iter
    timefun%iter_nonlinear(itemp) = tdpot%iter_nonlinear
    timefun%iter_total(itemp)=sum(timefun%iter_solver(1:tdpot%iter_nonlinear,itemp))
    timefun%iter_media(itemp)=int(timefun%iter_total(itemp)*one/tdpot%iter_nonlinear)

    !----------------------------------------------------------------
    ! Real Valued Fucntional
    !---------------------------------------------------------------
    if(itemp>0) timefun%var_tdens(itemp) = tdpot%system_variation
    timefun%time(itemp) = tdpot%time
    if(itemp>0) timefun%deltat(itemp) = tdpot%deltat

    if (itemp==0) then
       timefun%cpu_time(itemp)=tdpot%cpu_time
    else
       timefun%cpu_time(itemp)=timefun%cpu_time(itemp-1)+tdpot%cpu_time
    end if
    timefun%CPU_iterations(itemp)=tdpot%cpu_time
    timefun%CPU_iterations_wasted(itemp)=tdpot%cpu_wasted

    timefun%mass_tdens(itemp)= tdpot%mass_tdens
    timefun%weighted_mass_tdens(itemp)= tdpot%weighted_mass_tdens
    timefun%energy(itemp) = tdpot%energy
    timefun%lyapunov(itemp)  =  tdpot%lyapunov
    timefun%min_tdens(itemp)= tdpot%min_tdens
    timefun%max_tdens(itemp)= tdpot%max_tdens
    timefun%max_nrm_grad(itemp)     = tdpot%max_nrm_grad
    timefun%max_nrm_grad_avg(itemp) = tdpot%max_nrm_grad_avg 


    timefun%res_elliptic(itemp) = tdpot%res_elliptic
    timefun%deltat(itemp) = ctrl%deltat 

  end subroutine store_evolution

end module AlgebraicDmk
