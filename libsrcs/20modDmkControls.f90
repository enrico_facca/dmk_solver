!> Defines simulation control parameters
module DmkControls
  use KindDeclaration
  use Globals
  use LinearSolver
  use StdSparsePrec
  implicit none
  private
  ! Here we list the procudre we want to wrap with python
  ! It is not clear why the definition as procedure of
  ! the derived type is not wrapped.
  public :: DmkCtrl, get_from_file, dmkctrl_constructor,dmkctrl_copy
  !>----------------------------------------------------------
  !> Derived type containg all controls for Dynamics
  !> Monge-Kantorovich Simulations (tolerances, iterations limits, etc.)
  !<----------------------------------------------------------
  type :: DmkCtrl
     !>--------------------------------------------------------
     !> CONVERGENCE CONTROLS
     !>--------------------------------------------------------
     !> Maximum number of time iterations
     !> max_time_it=0 => Only one elliptic equation is solved
     integer :: max_time_iterations=100
     !> Convergence tolerances
     !> Tolerance of var_tdens 
     real(kind=double) :: tolerance_system_variation=1.0d-4
     !>--------------------------------------------------------
     !> Time stepping approach
     !>--------------------------------------------------------
     !> Time discretization (Default=1). Time-stepping procedure used.
     !> Available options:
     !> * 1 : Explicit Euler in Tdens
     !> * 2 : Implicit Euler in Tdens + Newton
     !> * 3 : Explicit Euler in Gfvar
     !> * 4 : Implicit Euler in Gfvar + Newton
     !> * 5 : Accelerate Explicit Euler Gfvar
     integer :: time_discretization_scheme=1    
     !> Maximum number of nonlinear iterations
     !> (e.g. max Newton iterations for time_discretization_scheme=2)
     integer :: max_nonlinear_iterations=10
     !> Tolerance in fix_point scheme for implicit time stepping
     !> It is the accuracy at which the PDE 
     !> -div(\mu \tdens) = f
     !> or 
     !> A W^{-1} diag(Mu) AT u=f
     real(kind=double) :: tolerance_nonlinear=1e-8
     !> Tolerance in fix_point scheme for implicit time stepping
     real(kind=double) :: tolerance_linear_newton=1.0d-5
     !> Maximum number of time iterations
     integer :: max_restart_update=15
     !<--------------------------------------------------------
     !> TIME CONTROLS 
     !>--------------------------------------------------------
     !> Initial time
     real(kind=double) :: tzero=zero
     !> Control of the time step
     !> * 1 => constant time step
     !> * 2 => increasing time step, with factor 
     !> "exp_rate" and upper bound "bound_deltat"
     !> * 3 => adaptive deltat
     integer :: deltat_control=1
     !> Time step
     real(kind=double) :: deltat=0.1d0
     !> Expansion rate in deltat_control == 2
     real(kind=double) :: deltat_expansion_rate=1.05d0
     !> Upper bound for deltat in deltat_control == 2,3
     real(kind=double) :: deltat_upper_bound=1.0d0
     !> Lower bound for deltat in deltat_control == 2,3
     real(kind=double) :: deltat_lower_bound=1.0d-3
     !> Controls for Algebraic Multigrid
     !> Lower bound for W22= block 2-2 jacobian
     !> Used deltat_control=3
     real(kind=double) :: epsilon_W22=1.0d-4
     !>--------------------------------------------------------
     !> Ode Global Controls
     !>--------------------------------------------------------
     !> Flag for use of subgrid (0= no subgrid, 1= subgrid )
     !> Used only in DMK for continous densities.
     integer :: id_subgrid=1
     !> lower bound for tdens
     real(kind=double) :: min_tdens=1.0d-10
     !> Norm for extimate tdens variation
     !> * p>=1.0 : $L^p$ norm
     !> * p =0.0 : $L^{\infty}$-norm
     real(kind=double) :: norm_tdens=2.0d0
     !> Flag for selection of tdpot variable
     !> * 0 : selection off
     !> * 1 : secection on tdens varaible
     !>      pot selection is done :
     !>      ADMK: remove zero coloumns after elimination of   
     integer :: selection=0
     !> Selection parameter for tdens
     real(kind=double) ::  threshold_tdens = 0.0d0
     !> Flag to define transformation of Algebraic DMK
     !> Used only in new_implicit_euler_newton_gfvar (time_discretization_scheme=1)
     !> * 0 : tdens=gfvar
     !> * 1 : tdens=gfvar^2/4
     !> * 2 : tdens=exp(gfvar)
     integer :: trans=1
     !<--------------------------------------------------------
     !> INPUT/OUTPUT CONTROLS
     !>--------------------------------------------------------
     !<--------------------------------------------------------
     !> Information Controls
     !>--------------------------------------------------------
     !>
     !> Print debug output for debug > 0.
     !> * 0  : no debug
     !> * 1  : print main steps (initialization)
     !> * 2  : print inputs and controls
     !> * 12 : 1 + 2
     integer :: debug=0
     !> Flag to control print information on state of system
     !> energy lyapunov etc.
     integer :: info_state = 1
     !> Flag to print info in update procedure
     !> * 0 :: no print
     !> * 1 :: print basic output (linear solver check)
     !> * 2 :: inputs used
     integer :: info_update = 1
     !> Format for integer output 
     character(len=20) :: iformat='I12'
     !> Format for real output
     character(len=20) :: rformat='1e18.10'
     !> Format for integer output 
     character(len=256) :: iformat_info='I9'
     !> Format for real output
     character(len=256) :: rformat_info='1pe9.2'
     !>---------------------------------------------------------------
     !>  Saving Procedure
     !>---------------------------------------------------------------
     !> control saving into vtk files
     !> * 0 => no save
     !> * 1 => save all
     !> * 2 => save with fixed frequency
     !> * 3 => save when the first digit of var_tdens changes
     !> Id. saving data into .dat
     integer :: id_save_dat=0
     !> Saving frequency
     integer :: freq_dat=100
     !> Logical unit for Tdens
     integer :: lun_tdens=1000
     !> File name for Tdens
     character(1024) :: fn_tdens='tdens.dat'
     !> Logical unit for Potential
     integer :: lun_pot=1001
     !> File name for Tdens
     character(1024) :: fn_pot='pot.dat'
     !> ON/OFF flag for saving log of time evolution
     integer :: id_save_statistics=1
     !> Logical unit for log file.
     integer :: lun_statistics=999
     !> File name for log
     character(1024) :: fn_statistics='dmk.log'
     !> File variables 
     !> File where to write tdens data
     type(file) :: file_tdens
     !> File where to write potential
     type(file) :: file_pot
     !> File where to write statistics
     type(file) :: file_statistics
     !> ON/OFF flag for saving matrices
     integer :: id_save_matrix=0
     !>------------------------------------------------------
     ! Linear solver Controls
     !>------------------------------------------------------
     !> Diagonal scaling of linear system
     integer :: id_singular=0
     !> Diagonal scaling of linear system
     integer :: diagonal_scaling=0
     !> Flag to active to print steps for debugging
     !> debug=0 => no print 
     !> debug=1 => print steps
     integer :: debug_solver=0
     !> Restart bufffer for Flexible PCG/ GMRES
     integer :: krylov_nrestart=20
     !>-------------------------------------------------------
     !> OUTER SOLVER
     !>-------------------------------------------------------
     !> Solver scheme
     !> *AGMG   :: AGgregation based Multigrid by Notay, Napov
     !>           (when installed)
     !> *ITERATIVE :: Iterative Krylov methods (see krylov_scheme)  
     character(len=20) :: outer_solver_approach='ITERATIVE'
     !> Solver scheme:
     !> * PCG
     !> * BICGSTAB
     !> * GMRES
     !> * MINRES
     !> * FGMRES(when HSL library are installed)
     character(len=20) :: outer_krylov_scheme='BICGSTAB'
     !> I/O err msg. unit
     integer :: outer_lun_err=0
     !> I/O out msg. unit
     integer :: outer_lun_out=6
     !> Integer identifying the Preconditinoer
     !>     iexit=0 => exit on absolute residual               
     !>     iexit=1 => exit on |r_k|/|b|     
     integer :: outer_iexit=1
     !> Number of maximal iterations for iterative solver
     integer :: outer_imax=1000
     !> Flag for printing option for itereative solver
     integer :: outer_iprt=0
     !> Flag for starting solution
     !>  isol=0 => SOL      is the initial solution for pcg
     !>  isol=1 => PREC*RHS is the initial solution for pcg
     integer :: outer_isol=0
     !> Linear Solver Tolerance
     real(kind=double) :: outer_tolerance=1e-13
     !> Ortogonalized w.r.t. to given vectors 
     !> (usally the kernel of the matrix)
     integer :: outer_iort=0
     !>------------------------------------------------------
     !> Preconditioner controls 
     !>------------------------------------------------------
     !> Integer identifying the Preconditinoer
     !> * 'identity'     P=Id (nothing is done)
     !> * 'diag'         P=diag(A)^{-1}
     !> * 'IC'           P=(U^{T}U)^{-1} with A~(U^T)U
     !> * 'ILU'          P=(LU)^{-1} with A~LU
     !> * 'C'            P=(M^{T}M)^{-1} with A=(U^T)U
     !> * 'LU'           P=(LU)^{-1} with A=LU
     !> * 'INNER_SOLVER' P=(A^-1) with agmg 
     character(len=20) :: outer_prec_type='ILU'
     !> Number of maximal extra-non-zero tem for
     !> row in the construction of IC_{fillin}
     integer :: outer_prec_n_fillin=30
     !> Tolerance for the dual drop procedure
     !> in the construction of IC_{fillin} (iprec=4)
     real(kind=double) :: outer_prec_tol_fillin=1.0d-5
     !>-------------------------------------------------------
     !> INNER SOLVER
     !>-------------------------------------------------------
     !> Solver scheme
     !> * AGMG   :: AGgregation based Multigrid by Notay, Napov
     !>           (when installed)
     !> * ITERATIVE :: Iterative Krylov methods (see inner_krylov_scheme)  
     character(len=20) :: inner_solver_approach='ITERATIVE'
     !> Solver scheme
     !> * PCG
     !> * BICGSTAB
     !> * GMRES
     !> * MINRES
     !> * FGMRES(when HSL library are installed)
     character(len=20) :: inner_krylov_scheme='BICGSTAB'
     !> I/O err msg. unit
     integer :: inner_lun_err=0
     !> I/O out msg. unit
     integer :: inner_lun_out=6
     !> Integer identifying the Preconditinoer
     !>     iexit=0 => exit on absolute residual               
     !>     iexit=1 => exit on |r_k|/|b|     
     integer :: inner_iexit=1
     !> Number of maximal iterations for iterative solver
     integer :: inner_imax=1000
     !> Flag for printing option for itereative solver
     integer :: inner_iprt=0
     !> Flag for starting solution
     !>  isol=0 => SOL      is the initial solution for pcg
     !>  isol=1 => PREC*RHS is the initial solution for pcg
     integer :: inner_isol=0
     !> Linear Solver Tolerance
     real(kind=double) :: inner_tolerance=1e-13
     !> Ortogonalized w.r.t. to given vectors 
     !> (usally the kernel of the matrix)
     integer :: inner_iort=0
     !>------------------------------------------------------
     !> Preconditioner controls 
     !>------------------------------------------------------
     !> Integer identifying the Preconditinoer
     !> * 'identity'  P=Id (nothing is done)
     !> * 'diag'      P=diag(A)^{-1}
     !> * 'IC'        P=(U^{T}U)^{-1} with A~(U^T)U
     !> * 'ILU'       P=(LU)^{-1} with A~LU
     !> * 'C'         P=(M^{T}M)^{-1} with A=(U^T)U
     !> * 'LU'        P=(LU)^{-1} with A=LU
     !> * 'AGMG'      P=(A^-1) with agmg 
     character(len=20) :: inner_prec_type='ILU'
     !> Number of maximal extra-non-zero tem for
     !> row in the construction of IC_{fillin}
     integer :: inner_prec_n_fillin=30
     !> Tolerance for the dual drop procedure
     !> in the construction of IC_{fillin} (iprec=4)
     real(kind=double) :: inner_prec_tol_fillin=1.0d-5
     !>--------------------------------------------------------
     !> Newton controls
     !>--------------------------------------------------------
     !> Maximum groth of Newton residuum in one step
     real(kind=double) :: relative_growth_factor=1.0d4
     !> Maximum groth of Newton residuum with respect to
     !> initial residuum 
     real(kind=double) :: absolute_growth_factor=1.0d8
     !> Solution appraoch for saddle point system
     !> arising form newton scheme applied to implicit time-stepping
     !> * 'reduced':: invert the C and solve reduced system
     !> * 'full'   :: solve full system with iterative approach
     !>             set controls of outer and inner solvers
     character(len=70) :: solve_jacobian_approach='reduced'
     !> Limit where to inverse diagonal matrix C
     !> in jacobian in reduced approach 
     real(kind=double) :: limit_C=1.0d-8
     !> Limit for dump parameter for Newton increment
     real(kind=double) :: relax_limit=5.0d-2
     !>-------------------------------------
     !> Precotioner Buffering
     !>-------------------------------------
     !> Flag for buffering the calculation of the prec.
     !> * 0 : build at each iteration (no buffer)
     !> * 1 : build if iterations >= ref_iter
     !> * 2 : build if iterations >= prec_growth* average iteration number
     !> * 3 : build if iterations >= prec_growth* last iterations with new preconditoner
     !> * 4 : build if iterations >= prec_growth* min(ref_iter, last iterations with new preconditoner)
     !> * 5 : build if iterations >= prec_growth* iterations at first linear system
     integer :: id_buffer_prec=0
     !> Reference Number of iteration 
     !> Used for id_buffer_prec=1,...
     integer :: ref_iter=200
     !> Factor of growth of iterations
     real(kind=double) :: prec_growth=1.3d0
     !> Work value for memory purpose
     real(kind=double) :: threshold_prec = 0.0d0
     !>---------------------------------------------------
     !> Control for factorization breakdown
     !> 0 = stop factorization 
     !> 1 = try to recover factorization 
     !> 2 = try recovering in regime of "normal" number
     integer :: factorization_job=2
     !> Maximal number of BFGS update for triangular preconditioner
     integer :: max_bfgs=10
    
     
     !> relaxation parameter add to the diagonal of the 
     !> stiffness matrix before building the preconditioner
     real(kind=double) ::  relax4prec=1.0d-7
     !> relaxation parameter add to the diagonal of the 
     !> stiffness matrix before building the preconditioner
     real(kind=double) ::  relax_direct=0.0d0
     !> Relazation of tdens weight
     !> stiffness matrix before building the preconditioner
     real(kind=double) ::  relax_tdens=0.0d0
    
     !>--------------------------------------------------------------
     !> INFO CONTROLS
     !>-------------------------------------------------------------
     !> Flag to control print of time evolution 
     integer :: info_time_evolution=1
     !> Flag to control print of time evolution 
     integer :: info_functional=1
     !> I/O err msg. unit
     integer :: lun_err=0
     !> I/O out msg. unit
     integer :: lun_out=6
     
     !>---------------------------------------------------------------
     !> Controls dedicated to P1-P0 and P1-P1 approach for DMK
     !>---------------------------------------------------------------
     
     !>---------------------------------------------------------------
     !> Controls dedicated to SPECTRAL-P0 approach for DMK
     !>---------------------------------------------------------------
     integer :: preassembly_matrices=1
     !> Degree of 1d-polynomial
     integer :: ndeg=15
     integer :: gfvar_approach
     character(len=1) :: separator_char='*'
     !> Flag for construction of preconditoner
     integer :: build_prec=1
     !>
     type(input_solver) :: ctrl_outer_solver,ctrl_inner_solver
     type(input_prec) :: ctrl_outer_prec,ctrl_inner_prec

     !>---------------------------------------------
     !> Eigenvalues controls
     !>--------------------------------------------
     !> Eigenvalues approach:
     !> * DACG 
     !> * ARAPCK
     !> * LAPACK
     character(len=256) :: eigen_approach='DACG'
     !> Eigenvalue number
     integer :: neigen=1
     !> Eigenvalue tolerance
     real(kind=double) :: tolerance_eigen=1.0d-9
     !> Maximum iterations in iterative methods for eigenvalues
     integer :: max_iterations_eigen=200
     !>-----------------------------------------------
     !> Reverse communication controls
     !>-----------------------------------------------
     !> Flag for using user-defined evalutation of
     !> system variation.
     !> * 0 : default-evalution defined in type 
     !> * 1 : user-defined evaluation
     integer :: user_system_variation=0
     !> Flag for using user-defined setting of controls
     !> for next update.
     !> * 0 : default-evalution defined in type 
     !> * 1 : user-defined setting
     integer :: user_control_set=0
     !> Flag for using user-defined resetting of controls
     !> after update failure.
     !> * 0 : default-evalution defined in type 
     !> * 1 : user-defined setting
     integer :: user_control_reset=0
   contains
     !> Initilization procedure
     procedure, public, pass :: init => init_dmkctrl
     !> Initilization procedure from file
     procedure, public, pass :: readfromfile    
     !> Procedure handling data saving along time evolution.
     procedure, public, pass :: write_data2file
     !>------------------------------------------
     !> Formatting procedures
     !>-----------------------------------------
     !> Function to build a separator in output or log files
     !> (public for type CtrlPrm)
     procedure, public, pass :: separator
     !> Procedure to generate formats 
     !> based on  iformat, rformat
     !> (public for type CtrlPrm)
     procedure, public, pass :: formatting
     !> Standard info formatting after update
     !> (public for type CtrlPrm)
     !>------------------------------------------
     !> Info procedures
     !>-----------------------------------------
     procedure, public, pass :: print_info_update 
     !> Standard info formatting after update
     !> (public for type CtrlPrm)
     procedure, public, pass :: print_info_tdpot
     !> Standard info formatting after update
     !> (public for type CtrlPrm)
     procedure, public, pass :: print_string
     !>------------------------------------------
     !> Procuderes accessible from Python
     !>-----------------------------------------
     !> Procedure for reading controls from file
     !procedure, public, nopass :: get_from_file
     !> Constructor procedure
     !procedure, public, nopass :: dmkctrl_constructor
  end type DmkCtrl

contains

  !>-------------------------------------------------------------
  !> Procedure for initializing controls.
  !> (procedure public for type CtrlPrm, used in init)
  !>
  !> usage:
  !>     call 'var'%init()
  !<-------------------------------------------------------------
  subroutine init_dmkctrl(this)
    use Globals
    implicit none
    !vars
    class(DmkCtrl),        intent(inout) :: this

    !
    ! init higher level controls for linear solvers
    !
    call this%ctrl_outer_solver%init(0,&
         approach=this%outer_solver_approach,&
         scheme=this%outer_krylov_scheme,&
         imax=this%outer_imax,&
         iexit=this%outer_iexit,&
         isol=this%outer_iexit,&
         nrestart=this%krylov_nrestart,&
         tol_sol=this%outer_tolerance)

    call this%ctrl_inner_solver%init(0,&
         approach=this%inner_solver_approach,&
         scheme=this%inner_krylov_scheme,&
         imax=this%inner_imax,&
         iexit=this%inner_iexit,&
         isol=this%inner_iexit,&
         nrestart=this%krylov_nrestart,&
         tol_sol=this%inner_tolerance)

    !
    ! set inversion controls from ctrl
    !
    if ( this%outer_prec_type .ne. 'INNER_SOLVER') Then
       call this%ctrl_outer_prec%init(&
            0,&
            this%outer_prec_type,&
            this%outer_prec_n_fillin,&
            this%outer_prec_tol_fillin)

    end if
    call this%ctrl_inner_prec%init(&
         0,&
         this%inner_prec_type,&
         this%inner_prec_n_fillin,&
         this%inner_prec_tol_fillin)

  end subroutine init_dmkctrl

  !>-------------------------------------------------------------
  !> Procedure for initializing controls reading them from files
  !> (procedure public for type CtrlPrm, used in init)
  !>
  !> usage:
  !>     call 'var'%readformfile(lun)
  !>
  !> where:
  !> \param[in] lun -> integer. Logical unit for reading.
  !>                            It must be associated to an OPEN file.
  !<-------------------------------------------------------------
  subroutine readfromfile(this, lun)
    use Globals
    implicit none
    !vars
    class(DmkCtrl), intent(inout) :: this
    integer,        intent(in   ) :: lun
    ! local vars
    type(file) :: file2read
    integer :: stderr   
    integer :: n_fillin
    real(kind=double) :: tol_fillin
    character(len=256) fname,scheme_read
    character(len=256) input
    character(len=256) clean,string
    character(len=256) prec_type

    integer :: ieig=0
    integer :: nev=0
    integer :: ituning=0
    integer :: npres=0

    integer :: lun_err=6
    integer :: lun_out=6
    integer :: iexit=1
    integer :: imax=1000
    integer :: iprt=1
    integer :: isol=0
    integer :: i,res
    real(kind=double) :: tol_pcg=1.0d-12, tol_dacg
    integer :: iort
    integer :: info


    !****************************************************
    ! CONVERGENCE CONTROLS
    !****************************************************
    

    call find_nocomment(lun,stderr,input,fname,'max_time_iterations')
    read(input,*) this%max_time_iterations

    ! Convergence tolerance
    call find_nocomment(lun,stderr,input,fname,'tolerance_system_variation')
    read(input,*) this%tolerance_system_variation

    
    !> TIME STEPPING APPROACH
    call find_nocomment(lun,stderr,input,fname,'time_discretization_scheme')
    read(input,*) this%time_discretization_scheme

    call find_nocomment(lun,stderr,input,fname,'max_nonlinear_iterations')
    read(input,*) this%max_nonlinear_iterations

    call find_nocomment(lun,stderr,input,fname,'tol_nonlinear')
    read(input,*) this%tolerance_nonlinear
    
    call find_nocomment(lun,stderr,input,fname,'nrestart_newton',info)
    read(input,*) this%max_restart_update

    !
    ! time step controls
    !
    call find_nocomment(lun,stderr,input,fname,'tzero')
    read(input,*) this%tzero
    
    call find_nocomment(lun,stderr,input,fname,'deltat_control')
    read(input,*) this%deltat_control

    call find_nocomment(lun,stderr,input,fname,'deltat')
    read(input,*) this%deltat

    call find_nocomment(lun,stderr,input,fname,'deltat_expansion_rate')
    read(input,*) this%deltat_expansion_rate

    call find_nocomment(lun,stderr,input,fname,'upper_bound_deltat')
    read(input,*) this%deltat_upper_bound

    call find_nocomment(lun,stderr,input,fname,'lower_bound_deltat')
    read(input,*) this%deltat_lower_bound


    
    !
    ! Global controls
    !
    call find_nocomment(lun,stderr,input,fname,'id_subgrid')
    read(input,*) this%id_subgrid
    
    call find_nocomment(lun,stderr,input,fname,'min_tdens')
    read(input,*) this%min_tdens

    call find_nocomment(lun,stderr,input,fname,'norm_tdens',info)
    read(input,*) this%norm_tdens

    call find_nocomment(lun,stderr,input,fname,'selection_tdens',info)
    read(input,*) this%selection

    call find_nocomment(lun,stderr,input,fname,'selection_tdens',info)
    read(input,*) this%threshold_tdens

    !>-----------------------------------------------------------
    !> Simulation control
    call find_nocomment(lun,stderr,input,fname,'debug')
    read(input,*) this%debug

    call find_nocomment(lun,stderr,input,fname,'info state')
    read(input,*) this%info_state

    call find_nocomment(lun,stderr,input,fname,'info state')
    read(input,*) this%info_update

    call find_nocomment(lun,stderr,input,fname,'iformat')
    clean = erase_comment(input)
    read(clean,*) this%iformat

    call find_nocomment(lun,stderr,input,fname,'rformat')
    clean = erase_comment(input)
    read(clean,*) this%rformat

 
    !
    ! Saving Data
    !
    ! saving dat
    call find_nocomment(lun,stderr,input,fname,'id_save_dat')
    read(input,*) this%id_save_dat
    call find_nocomment(lun,stderr,input,fname,'freq_dat')
    read(input,*) this%freq_dat


    

    !
    ! linear solver controls
    !
    call find_nocomment(lun,stderr,input,fname,'id_singular')
    read(input,*) this%id_singular
    
    call find_nocomment(lun,stderr,input,fname,'diagonal_scaling')
    read(input,*)this%diagonal_scaling

    !
    ! inner solver controls
    !
    call find_nocomment(lun,stderr,input,fname,'linear_solver')
    read(input,*) clean
    this%outer_solver_approach=erase_comment(clean)
    
    call find_nocomment(lun,stderr,input,fname,'scheme')
    read(input,*) clean
    this%outer_krylov_scheme=erase_comment(clean)

    call find_nocomment(lun,stderr,input,fname,'file_err')
    read(input,*) this%outer_lun_err

    call find_nocomment(lun,stderr,input,fname,'file_out')
    read(input,*) this%outer_lun_out

    call find_nocomment(lun,stderr,input,fname,'iexit')
    read(input,*) this%outer_iexit

    call find_nocomment(lun,stderr,input,fname,'imax')
    read(input,*) this%outer_imax

    call find_nocomment(lun,stderr,input,fname,'iprt')
    read(input,*) this%outer_iprt

    call find_nocomment(lun,stderr,input,fname,'isol')
    read(input,*) this%outer_isol
    
    call find_nocomment(lun,stderr,input,fname,'tol_pcg')
    read(input,*) this%outer_tolerance

    call find_nocomment(lun,stderr,input,fname,'iort')
    read(input,*) this%outer_iort
   
    !
    ! Preconditioner for outer solver
    !
    call find_nocomment(lun,stderr,input,fname,'prec_type')
    read(input,*) this%outer_prec_type

    call find_nocomment(lun,stderr,input,fname,'n_fillin')
    read(input,*) this%outer_prec_n_fillin

    call find_nocomment(lun,stderr,input,fname,'tol_fillin')
    read(input,*) this%outer_prec_tol_fillin

    !
    ! inner solver ctrl
    !
    call find_nocomment(lun,stderr,input,fname,'linear_solver')
    read(input,*) clean
    this%inner_solver_approach=erase_comment(clean)
    
    call find_nocomment(lun,stderr,input,fname,'scheme')
    read(input,*) clean
    this%inner_krylov_scheme=erase_comment(clean)

    call find_nocomment(lun,stderr,input,fname,'file_err')
    read(input,*) this%inner_lun_err

    call find_nocomment(lun,stderr,input,fname,'file_out')
    read(input,*) this%inner_lun_out

    call find_nocomment(lun,stderr,input,fname,'iexit')
    read(input,*) this%inner_iexit

    call find_nocomment(lun,stderr,input,fname,'imax')
    read(input,*) this%inner_imax

    call find_nocomment(lun,stderr,input,fname,'iprt')
    read(input,*) this%inner_iprt

    call find_nocomment(lun,stderr,input,fname,'isol')
    read(input,*) this%inner_isol
    
    call find_nocomment(lun,stderr,input,fname,'tol_pcg')
    read(input,*) this%inner_tolerance

    call find_nocomment(lun,stderr,input,fname,'iort')
    read(input,*) this%inner_iort

    !
    ! Preconditioner for inner solver
    !
    call find_nocomment(lun,stderr,input,fname,'prec_type')
    read(input,*) this%inner_prec_type

    call find_nocomment(lun,stderr,input,fname,'n_fillin')
    read(input,*) this%inner_prec_n_fillin

    call find_nocomment(lun,stderr,input,fname,'tol_fillin')
    read(input,*) this%inner_prec_tol_fillin

        

    ! buffering construction prec
    ! working variables controlled by id_buffer_prec
    call find_nocomment(lun,stderr,input,fname,'id_buffer_prec')
    read(input,*) this%id_buffer_prec

    call find_nocomment(lun,stderr,input,fname,'ref_iter')
    read(input,*) this%ref_iter

    call find_nocomment(lun,stderr,input,fname,'prec_growth')
    read(input,*) this%prec_growth

    !
    ! NEWTON VARIABLE
    !
    call find_nocomment(lun,stderr,input,fname,'solve_jacobian_approach')
    read(input,*) clean
    this%solve_jacobian_approach=erase_comment(clean)
    
    call find_nocomment(lun,stderr,input,fname,'relax_limit',info)
    read(input,*) this%relax_limit

    !
    ! RELAXATIONS
    !
    call find_nocomment(lun,stderr,input,fname,'relax_tdens',info)
    read(input,*) this%relax_tdens

    call find_nocomment(lun,stderr,input,fname,'relax_direc',info)
    read(input,*) this%relax_direct
    
    call find_nocomment(lun,stderr,input,fname,'relaxiation_prec',info)
    read(input,*) this%relax4prec

    
  contains 
    subroutine find_nocomment(lun,stderr,input,fname,var_name,info)
      use Globals
      integer,            intent(in   ) :: lun
      integer,            intent(in   ) :: stderr
      character(len=256), intent(in   ) :: fname
      character(len=256), intent(inout) :: input
      character(len=*),   intent(in   ) :: var_name
      integer, optional,intent(inout) :: info
      !local
      logical :: rc
      integer :: res

      character(len=256) clean
      character(len=1) first
      logical :: found
      
      if ( present(info)) info=0

      found = .false.
      do while( .not. found ) 
         read(lun,'(a)',iostat = res) input
         if(res .ne. 0) then
            !rc = IOerr(stderr, wrn_inp , 'Ctrl_read', &
            !     fname//'input text for member'//etb(var_name),res)
            if ( present(info)) info=-1
            return
         end if
            
         clean = etb(input)
         read(clean,*) first
         if ( ( first .eq. '!') .or. &
              ( first .eq. '%') .or. &
              ( first .eq. '#') ) then
            found=.false.
         else
            found=.true.
         end if 
      end do
    end subroutine find_nocomment
        
  end subroutine readfromfile

  
   subroutine write_data2file(ctrl,flag,tdpot)
    use TdensPotentialSystem
    use TimeInputs
    use Globals
    implicit none
    class(DmkCtrl),     intent(inout) :: ctrl
    character(len=4),   intent(in   ) :: flag
    type(tdpotsys),     intent(inout) :: tdpot
    ! local
    integer :: i
    logical :: save_data
    real(kind=double) :: time
    real(kind=double) ,allocatable :: temp(:,:)


    select case( flag )
    case ('head') 
       ! open file statistic
       if (ctrl%id_save_statistics>0) then
          call ctrl%file_statistics%init(ctrl%lun_err, ctrl%fn_statistics, ctrl%lun_statistics, 'out')
       end if
       ! 
       ! write head of timedata file
       ! 
       if ( ctrl%id_save_dat >0 ) then
          if (ctrl%lun_tdens>0) then
             call ctrl%file_tdens%init(ctrl%lun_err, ctrl%fn_tdens, ctrl%lun_tdens, 'out')
             call writearray2file(ctrl%lun_err, 'head',&
                  tdpot%time,tdpot%ntdens, tdpot%tdens, &
                  ctrl%lun_tdens, ctrl%fn_tdens)
          end if
          if (ctrl%lun_pot>0) then
             call ctrl%file_pot%init(ctrl%lun_err, &
                  ctrl%fn_pot, ctrl%lun_pot, 'out')
            call write2file(ctrl%lun_err, 'head',&
                  tdpot%number_of_potentials,tdpot%npot/tdpot%number_of_potentials,&
                  tdpot%pot, & ! this argument is ignored
                  tdpot%time, ctrl%file_pot)
          end if
          
       end if
    case ('body')
       !
       ! save at itermediate time 
       !
       call should_I_save(ctrl,tdpot%steady_state,&
            tdpot%time_iteration,&
            tdpot%int_before,&
            tdpot%system_variation,&
            save_data )

       if ( save_data  ) then
          if (ctrl%lun_tdens>0) then
             call writearray2file(ctrl%lun_err, 'body',&
                  tdpot%time,&
                  tdpot%ntdens, tdpot%tdens,&
                  ctrl%lun_tdens, ctrl%fn_tdens)
          end if
          if (ctrl%lun_pot>0) then
             allocate(temp(tdpot%number_of_potentials,tdpot%npot/tdpot%number_of_potentials))
             !write(*,*) tdpot%npot/tdpot%number_of_potentials,tdpot%number_of_potentials
             do i=1,tdpot%number_of_potentials
                temp(i,:) = tdpot%pot(&
                     (i-1)*tdpot%npot/tdpot%number_of_potentials+1:&
                     i*tdpot%npot/tdpot%number_of_potentials)
             end do
             call write2file(ctrl%lun_err, 'body',&
                  tdpot%number_of_potentials,tdpot%npot/tdpot%number_of_potentials,&
                  temp, & ! this argument is ignored
                  tdpot%time, ctrl%file_pot)
              deallocate(temp)
          end if
       end if
       
    case ('tail')
       !
       ! optionally close logical unit for tdens and potential
       !
       if (ctrl%lun_statistics>0) then
          call ctrl%file_statistics%kill(ctrl%lun_err)
       end if
       if  ( ctrl%id_save_dat >0 ) then
          time=tdpot%time
          if (tdpot%time<1e-14) time=1e30
          if (ctrl%lun_tdens>0) then
             call writearray2file(ctrl%lun_err, 'tail',&
                  time, tdpot%ntdens, tdpot%tdens, &
                  ctrl%lun_tdens, ctrl%fn_tdens)
             call ctrl%file_tdens%kill(ctrl%lun_err)
          end if

          if (ctrl%lun_pot>0) then
             call write2file(ctrl%lun_err, 'tail',&
                  tdpot%number_of_potentials,tdpot%npot/tdpot%number_of_potentials,&
                  tdpot%pot, & ! this argument is ignored
                  tdpot%time, ctrl%file_pot)
             call ctrl%file_pot%kill(ctrl%lun_err)
          end if
       end if
    end select
  contains
    subroutine should_I_save(this,&
         steady_state,&
         itemp,&
         int_before,&
         current_var,&
         save_test)
      use KindDeclaration
      implicit none
      type(DmkCtrl),     intent(in   ) :: this    
      logical,           intent(in   ) :: steady_state
      integer,           intent(in   ) :: itemp
      integer,           intent(inout) :: int_before
      real(kind=double), intent(in   ) :: current_var
      logical,intent(inout) :: save_test

      !local 
      integer :: int_now

      save_test = .false.

      select case ( this%id_save_dat ) 
      case (1)
         !
         ! save each time step
         !
         save_test = .true.
      case (2)
         !
         ! save with give frequency and the last one
         !
         if ( (mod(itemp,this%freq_dat) == 0) .or. ( steady_state) ) then
            save_test = .true.
         end if
      case (3)
         !
         ! save when variation change digit the last one
         !
         if ( ( itemp .eq. 0) ) then 
            save_test = .true.
         else
            int_now=int(current_var*10.0d0**(-int(log10(current_var))+1))
            if ( (int_now /= int_before) .or. ( steady_state ) ) then
               int_before=int_now
               save_test = .true.
            end if
         end if
      case (4)
         !
         ! save only last
         !
         if ( steady_state ) then
            save_test = .true.
         end if
      end select
      
    end subroutine should_I_save
  end subroutine write_data2file

   !>-------------------------------------------------------------
   !> Function for generation a separation line with a special character,
   !> defined in separator_char
   !> (procedure public for type CtrlPrm)
   !>
   !> usage:
   !>     sep='var'%separator()
   !> 
   !> where:
   !> \return sep -> string. One line with the same character
   !<-------------------------------------------------------------
   function separator(this,title) result (outstring)
     use Globals
     implicit none
     class(DmkCtrl),             intent(in   ) :: this
     character(len=*), optional, intent(in   ) :: title
     integer, parameter                        :: width=71
     integer, parameter                        :: start=10
     character(len=width) :: outstring
     !local 
     integer :: i,lentitle
     character(len=start) :: strstart
     character(len=width-start-2) :: strend
     character(len=1) :: symbol

     symbol=this%separator_char


     if ( present(title)) then
        lentitle=len(etb(title))
        do i=1,start
           write(strstart(i:i),'(a)') symbol
        end do

        do i=1,width-start-2-lentitle
           write(strend(i:i),'(a)') symbol
        end do
        outstring=strstart//' '//etb(title)//' '//strend
     else
        do i=1,width
           write(outstring(i:i),'(a)') symbol
        end do
     end if

   end function separator


  !>-------------------------------------------------------------
  !> Function for quick generation of format string for integer, real and
  !> character combinations.
  !> (procedure public for type CtrlPrm)
  !>
  !> usage:
  !>     call 'var'%formattion(type, compressed, style)
  !> 
  !> where:
  !> \param[in] compressed -> string. Compressed string containg
  !>                          'a','i','r' according to the sequence of
  !>                          data to write
  !> \param[in,opt] style  -> integer.  
  !>                          * 1 : [i,r]format_info are used
  !>                          * 2 : [i,r]format are used
  !> \return out_format    -> string. Output format string usable as
  !>                          write(*,out_format) ....
  !<-------------------------------------------------------------
  function formatting(this,compressed,style,remove_brackets) result (out_format)
    use Globals
    implicit none
    class(DmkCtrl),    intent(in   ) :: this
    character(len=*),  intent(in   ) :: compressed
    integer, optional, intent(in   ) :: style
    logical, optional, intent(in   ) :: remove_brackets
    character(len=256) :: out_format
    !local
    integer :: i,length
    character(len=1) :: singleton
    logical :: remove=.False.

    if (present(remove_brackets)) then
       remove=remove_brackets
    end if

    ! opening bracket
    if (.not. remove ) out_format='('  



    length=len(compressed)


    if ( .not. present(style) .or. (style .eq. 1) ) then
       do i=1, length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat_info)
          case('r')
             out_format=etb(etb(out_format)//this%rformat_info)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    if ( present(style) .and. (style .eq. 2) ) then
       do i=1,length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat)
          case('r')
             out_format=etb(etb(out_format)//this%rformat)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    ! closing bracket
    if (.not. remove ) out_format=etb(etb(out_format)//')')       

  end function formatting


  !>-------------------------------------------------------------
  !> Procedure for handling print a string to stsandard output
  !> and log file according to flags info_state info_update.
  !> (procedure public for type CtrlPrm)
  !>
  !> usage:
  !>     call 'var'%init(type, depth,sting)
  !>
  !> where:
  !> \param[in] type    -> character(len=*)
  !>                      'state ': state info
  !>                      'update': update info
  !> \param[in] verbose -> integer: verbosity level
  !>                       * =0 : no print
  !>                       * >0 : incresively verbose
  !> \param[in] string  -> character(len=*)
  !>                     String to print
  !<-------------------------------------------------------------
  subroutine print_string(this,&
       type,verbose,string)
    use Globals
    implicit none
    class(DmkCtrl),    intent(in   ) :: this
    character(len=*),  intent(in   ) :: type
    integer,           intent(in   ) :: verbose
    character(len=*),  intent(in   ) :: string

    select case ( type )
    case ('state')
       if ( (this%debug .ge. 1) .or. (this%info_state  .ge. verbose) ) write(this%lun_out,'(a)') etb(string)
       write(this%lun_statistics,'(a)') etb(string)
    case ('update')
       if ( (this%debug .ge. 1) .or.  (this%info_update .ge. verbose)) write(this%lun_out,'(a)') etb(string)
       write(this%lun_statistics,'(a)') etb(string)
    end select

  end subroutine print_string


  !>-------------------------------------------------------------
  !> Procedure for printing update informations
  !> (procedure public for type CtrlPrm)
  !>
  !> usage:
  !>     call 'var'%print_info_update(type, depth,sting)
  !>
  !> where:
  !<------------------------------------------------------------- 
  subroutine print_info_update(this,&
       flag_info,&
       info,&
       time_iteration,&
       nrestart)
    use Globals    
    implicit none
    class(DmkCtrl),    intent(in) :: this
    character(len=*),  intent(in) :: flag_info
    integer,           intent(in) :: info
    integer,           intent(in) :: time_iteration
    integer,           intent(in) :: nrestart
    
    
    !local
    character(len=77) :: result_update
    character(len=30) :: flag
    character(len=77) :: msg,msg_out
    character(len=256) :: out_format,str
    character(len=77) :: method
    

    flag=etb(flag_info) 

    select case (flag)
    case ( 'before_inside') 
       if (  nrestart .eq. 0) then
          if ((this%info_update .ge. 1) .and. (this%lun_out .gt. 0 ) ) write(this%lun_out,*) ' '
          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) ' ' 
          out_format='(a,I5.5,a,1pe8.2,a,I2)'
          write(str,out_format) 'UPDATE ',time_iteration,' | TIME STEP = ', this%deltat
          write(msg,'(a)') this%separator(str) 
       else
          out_format='(a,I5.5,a,1pe8.2,a,I2)'
          write(str,out_format) 'UPDATE ',time_iteration,' | TIME STEP = ', this%deltat,&
               ' RESTART ', nrestart
          write(msg,'(a)') this%separator(str)              
       end if
       if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,'(a)') etb(msg)
       if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 )) write(this%lun_out,'(1X,a)') etb(msg)

    case ( 'after_inside') 
       if ( info .eq. 0) then
          if ( nrestart .eq. 0) then
             write(msg,*) this%separator('UPDATE SUCCEED ')
          else
             write(str,'(I3)') nrestart
             write(msg,*) this%separator('UPDATE SUCCEED with '//etb(str)//' restarts')
          end if
          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
          if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 )) write(this%lun_out,*) etb(msg)
       else
          if ( nrestart + 1 .eq. this%max_restart_update) then
             write(msg,*) this%separator('UPDATE FAILED ')
             if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
             if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 ) ) write(this%lun_out,*) etb(msg)
             write(this%lun_err,*) etb(msg)
          end if
       end if
    
    end select


  end subroutine print_info_update


  !>-------------------------------------------------------------
  !> Procedure for printing state informations
  !> (procedure public for type CtrlPrm)
  !>
  !> usage:
  !>     call 'var'%print_info_tdpot(type, depth,sting)
  !>
  !> where:
  !<------------------------------------------------------------- 
  subroutine print_info_tdpot(this,&
       tdpot)
    use Globals 
    use TdensPotentialSystem
    implicit none
     class(DmkCtrl),    intent(in) :: this
     type(tdpotsys),    intent(in) :: tdpot
     !local
     character(len=256) :: msg

     
     if (this%info_state .gt. 0) then
        if ( this%lun_out > 0 ) then 
           call tdpot%info_time_evolution(this%lun_out)
        end if
     end if
     if ( this%lun_statistics > 0 ) then 
        call tdpot%info_time_evolution(this%lun_statistics)
     end if
   end subroutine print_info_tdpot





  
  !>-------------------------------------------------------------
  !> Constructor procedure for python wrap.  
  !> (procedure public for type CtrlPrm, used in init)
  !>
  !> usage:
  !>     call dmkctrl_constructor(var)
  !<-------------------------------------------------------------
  subroutine dmkctrl_constructor(this)
    implicit none
    type(DmkCtrl),       intent(inout) :: this
    call this%init()
  End subroutine dmkctrl_constructor

  !>-------------------------------------------------------------
  !> Copy procedure for python wrap.  
  !> (procedure public for type CtrlPrm, used in init)
  !>
  !> usage:
  !>     call dmkctrl_constructor(var)
  !<-------------------------------------------------------------
  subroutine dmkctrl_copy(this,copy)
    implicit none
    type(DmkCtrl),       intent(in   ) :: this
    type(DmkCtrl),       intent(inout) :: copy
    copy = this
    call copy%init()
  End subroutine dmkctrl_copy


  !>-------------------------------------------------------------
  !> Procedure for python wrapping
  !> (procedure public for type CtrlPrm, used in init)
  !>
  !> usage:
  !>     call 'var'%get_from_file()
  !> where:
  !> \param[in] filepah -> character(len=1024).
  !>                       File path where controls are stored.
  !>                       See dmk.ctrl for example.
  !<-------------------------------------------------------------
  subroutine get_from_file(this, filepath)
    use Globals
    implicit none
    !vars
    type(DmkCtrl),       intent(inout) :: this
    character(len=1024), intent(in   ) :: filepath
    ! local
    type(file) :: fctrl

    call fctrl%init(0, etb(filepath), 10, 'in', .True., .False.)
    call this%readfromfile(fctrl%lun)
    call this%init()
    call fctrl%kill(0)
    
  End subroutine get_from_file


  




end module DmkControls
