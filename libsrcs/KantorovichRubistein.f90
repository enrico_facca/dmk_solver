module KantorovichRubistein
  use DmkDiscretization
  use Globals
  use LinearSolver
  use AbstractGeometry
  use DmkP1P0Discretization
  use DmkControls
  implicit none
  
  type, public, extends(problem) :: prob_kantrub
     ! grid tdens and nu variable
     class(abs_simplex_mesh), pointer :: grid_munu
     ! grid kantorovich potential
     class(abs_simplex_mesh), pointer :: grid_pot
     ! p1p0 discretization
     type(dmkp1p0) :: p1p0
     ! forcing term
     real(kind=double), allocatable :: rhs(:)
   contains
     !> Static constructor
     !> (procedure public for type kantrub)
     procedure, public , pass :: init => init_prob_kantrub
     !> Static destructor
     !> (procedure public for type kantrub)
     procedure, public , pass :: kill => kill_prob_kantrub
  end type prob_kantrub

  
  type, public, extends(solution) :: sol_kantrub
     ! kantorovich potential
     integer :: n_pot=0
     real(kind=double), allocatable :: pot(:)
     ! mu lagrange multiplier
     integer :: n_mu=0
     real(kind=double), allocatable :: mu(:)
     ! nu lagrange multiplier
     integer :: n_nu=0
     real(kind=double), allocatable :: nu(:)
   contains
     !> Static destructor
     !> (procedure public for type sol_kantrub)
     procedure, public , pass :: init => init_sol_kantrub
     !> Static destructor
     !> (procedure public for type sol_kantrub)
     procedure, public , pass :: kill => kill_sol_kantrub
  end type sol_kantrub

  type, public :: ctrl_kantrub
     integer :: max_nonlinear_iterations=20
     integer :: max_linear_iterations=20
     real(kind=double) :: tolerance_nonlinear=1.0d-7
     real(kind=double) :: tolerance_linear=1.0d-5
  end type ctrl_kantrub
  
  type, public, extends(iterative_solver) :: solver_kantrub
     type(ctrl_kantrub) :: ctrl
   contains
     procedure, public , pass :: init => init_solver_kantrub
     !> Static destructor solver_kantrub
     !> (procedure public for type )
     procedure, public , pass :: kill => kill_solver_kantrub
  end type solver_kantrub

  type, public, extends(iterative_algorithm) :: dmk_kantrub
     class(prob_kantrub), pointer :: prob => null()
     class(sol_kantrub), pointer :: sol => null()
   contains
     procedure, public , pass :: iterate => iterate_dmk_kantrub
  end type dmk_kantrub

contains

  subroutine init_prob_kantrub(this, lun_err,ctrl,id_subgrid, grid_munu, grid_pot)
    implicit none
    class(prob_kantrub),             intent(inout) :: this
    integer,                         intent(in   ) :: lun_err
    type(DmkCtrl),                   intent(in   ) :: ctrl
    integer,                         intent(in   ) :: id_subgrid
    class(abs_simplex_mesh), target, intent(in   ) :: grid_munu
    class(abs_simplex_mesh), target, intent(in   ) :: grid_pot

    call this%p1p0%init(ctrl,1, grid_munu, grid_pot)
    this%grid_munu => grid_munu
    this%grid_pot  => grid_pot

    allocate(this%rhs(grid_pot%nnode))
    
  end subroutine init_prob_kantrub

   subroutine kill_prob_kantrub(this, lun_err)
    implicit none
    class(prob_kantrub), intent(inout) :: this
    integer,        intent(in   ) :: lun_err

    call this%p1p0%kill(lun_err)
    this%grid_munu => null()
    this%grid_pot  => null()

    deallocate(this%rhs)
  end subroutine kill_prob_kantrub

  subroutine init_sol_kantrub(this, lun_err, inputs)
    implicit none
    class(sol_kantrub), intent(inout) :: this
    integer,            intent(in   ) :: lun_err
    class(prob_kantrub), intent(in   ) :: inputs

    this%n_pot = inputs%grid_pot%nnode
    this%n_mu = inputs%grid_munu%ncell
    this%n_nu = inputs%grid_munu%ncell

    allocate(this%pot(this%n_pot))
    allocate(this%mu(this%n_mu))
    allocate(this%nu(this%n_nu))
    
  end subroutine init_sol_kantrub

   subroutine kill_sol_kantrub(this, lun_err)
    implicit none
    class(sol_kantrub), intent(inout) :: this
    integer,            intent(in   ) :: lun_err


    this%n_pot = 0
    this%n_mu = 0
    this%n_nu = 0

    deallocate(this%pot)
    deallocate(this%mu)
    deallocate(this%nu)
  end subroutine kill_sol_kantrub

  subroutine init_solver_kantrub(this, lun_err, inputs)
    implicit none
    class(solver_kantrub), intent(inout) :: this
    integer,               intent(in   ) :: lun_err
    class(prob_kantrub),  intent(in   ) :: inputs

    
    
  end subroutine init_solver_kantrub

  subroutine kill_solver_kantrub(this, lun_err)
    implicit none
    class(solver_kantrub), intent(inout) :: this
    integer,               intent(in   ) :: lun_err
    
  end subroutine kill_solver_kantrub


  subroutine init_dmk_kantrub(this,prob,sol)
    implicit none
    class(dmk_kantrub), intent(inout) :: this
    class(prob_kantrub), target, intent(in) :: prob
    class(sol_kantrub), target, intent(in) :: sol

    this%sol => sol 
    this%prob => prob
    
  end subroutine init_dmk_kantrub

  subroutine iterate_dmk_kantrub(this,info)
    implicit none
    class(dmk_kantrub), intent(inout) :: this
    integer,            intent(inout) :: info

    write(*,*) this%sol%n_pot
    
  end subroutine iterate_dmk_kantrub

  
    
  

end module KantorovichRubistein
