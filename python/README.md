### What is this directory for ?

*** What this directory contains

This directory contains a series of python tools for using the DMK via
python.  Some programs/scripts are purelly pyhton written other are a python
wrap for the fortran programs/subroutines written in fortran. The
latter can be used only if the f90-wrap program worked fine.
The programs currently available are:

* dmk_p1p0           : (python-fortran) Solution of the OTP on 2d, 3d and surface domain.
* dmk_graph          : (python-fortran) Solution of the OTP on graphs.


The work_in_progress directory contains not fullied developted tools
For more informations check each sub-directoriy.

## How do I get set up? Dependencies? Compile
Check README in parent folder (../README.md)





