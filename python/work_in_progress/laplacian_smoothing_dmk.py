#  f90wrap: F90 to Python interface generator with derived type support
#
#  Copyright James Kermode 2011-2018
#
#  This file is part of f90wrap
#  For the latest version see github.com/jameskermode/f90wrap
#
#  f90wrap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  f90wrap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with f90wrap. If not, see <http://www.gnu.org/licenses/>.
#
#  If you would like to license the source code under different terms,
#  please contact James Kermode, james.kermode@gmail.com
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import network_impainting_tools as NetImp

from ExampleDerivedTypes import ( Dmkcontrols,
                                  Abstractgeometry,
                                  Dmkinputsdata,
                                  data2grids,
                                  dmkgraph_steady_data,
                                  build_subgrid_rhs,
                                  Tdenspotentialsystem)

import sys
sys.path.append('../../globals/python_timedata')
import timedata as td
sys.path.append('../preprocess/2d_assembly/')
import example_grid as ex_grid
import example_forcing as ex_forcing
import meshtools as mt
import rectangle_grid as rectgrid

# read file paths
fgrid=sys.argv[1]

# init grid
coord,topol,flags =mt.read_grid(fgrid)
ntdens=len(topol)
npot=len(coord)
topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1
nnode=len(coord)
ncell=len(topol)
grid=Abstractgeometry.abs_simplex_mesh()
subgrid=Abstractgeometry.abs_simplex_mesh()

#
# init. Fortan structure for grid and subgrid
#
data2grids(6, 3, nnode, ncell, coordT, topolT, grid, subgrid)


#
# init input data for dmk solver set rhs, penalty_weight, weight
#
dmkin=Dmkinputsdata.DmkInputs()
Dmkinputsdata.dmkinputs_constructor(dmkin,6,grid.ncell, subgrid.nnode,True)
mt.write_grid(coord,topol,'grid.dat','dat')

#
# set dirac rhs 
#
forcing=np.zeros(grid.ncell)
dirac_rhs=np.zeros(grid.nnode)
subgrid_rhs=np.zeros(subgrid.nnode)
coord_dirac=[[-0.3,0.7,0.0],[0.3,0.7,0.0],[0,-0.7,0.0]];
value_dirac=[-0.5,-0.5,1];
for count,node in enumerate(coord_dirac):
    inode=mt.Inode(coord,node);
    dirac_rhs[inode]=value_dirac[count];
    print(inode,coord[inode,:])
td.write_steady_timedata('dirac_rhs.dat',dirac_rhs.reshape([grid.nnode,1]))

dmkin.rhs=0.0
build_subgrid_rhs(subgrid, dmkin.rhs, forcing,dirac_rhs)
# set pflux
dmkin.pflux=1.3

td.write_steady_timedata('rhsdirac.dat',subgrid_rhs.reshape([subgrid.nnode,1]))




topolsub=subgrid.topol[0:3,:].transpose()-1
mt.write_grid(subgrid.coord.transpose(),topolsub,'subgrid.dat','dat')

print(topolsub.shape)
triang = mtri.Triangulation(subgrid.coord[0,:],subgrid.coord[1,:], topolsub)
fig1, ax1 = plt.subplots(figsize=(16, 20))
ax1.set_aspect('equal')
tpc = ax1.tripcolor(triang, dmkin.rhs,   cmap='cubehelix')
fig1.colorbar(tpc)
ax1.set_title('Forcing Term')
plt.show()

"""
#
# init tdens potential datadmkp1p0_steady_data
#
tdpot=Tdenspotentialsystem.tdpotsys()
Tdenspotentialsystem.tdpotsys_constructor(tdpot,0,grid.ncell, subgrid.nnode)
tdpot.tdens[:]=1.0
tdpot.pot[:]=0.0
"""
#
# set some controls parameters
#
ctrl = Dmkcontrols.DmkCtrl()
# globals controls
ctrl.selection=0
ctrl.threshold_tdens=1e-10
ctrl.debug=0  

# linear solver ctrl
ctrl.linear_solver='AGMG'
ctrl.krylov_scheme='PCG'
ctrl.prec_type='IC'
ctrl.relax4prec=1e-09
ctrl.imax_internal=20
ctrl.imax=50
ctrl.tol_internal=1e-12

# time steppin ctrl
ctrl.time_discretization_scheme=5
ctrl.newton_method=12
ctrl.max_time_iterations=100
ctrl.max_nonlinear_iterations=20
ctrl.tol_nonlinear=1e-08
ctrl.tolerance_system_variation=1e-04

# delta ctrl
ctrl.deltat = 1
ctrl.deltat_control=2
ctrl.deltat_expansion_rate=2.0
ctrl.deltat_lower_bound=0.01
ctrl.deltat_upper_bound=100
ctrl.max_restart_update=20

# info , saving ctrl
ctrl.info_update=3
ctrl.info_state=2
ctrl.id_save_dat=3
ctrl.lun_statistics=10
ctrl.lun_out=6
ctrl.lun_tdens = 10
ctrl.fn_tdens='tdens.dat'
ctrl.lun_pot=11
ctrl.fn_pot='pot.dat'

#
# solve with dmk
#
"""
info=0
dmkp1p0_steady_data(grid, subgrid, tdpot, dmkin, ctrl, info)
"""
