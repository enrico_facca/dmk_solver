#  f90wrap: F90 to Python interface generator with derived type support
#
#  Copyright James Kermode 2011-2018
#
#  This file is part of f90wrap
#  For the latest version see github.com/jameskermode/f90wrap
#
#  f90wrap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  f90wrap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with f90wrap. If not, see <http://www.gnu.org/licenses/>.
#
#  If you would like to license the source code under different terms,
#  please contact James Kermode, james.kermode@gmail.com
import numpy as np
import ioctrl
from ExampleDerivedTypes import ( Dmkcontrols,simple)

import sys
sys.path.append('../../globals/python_timedata')
import timedata as td
sys.path.append('../preprocess/2d_assembly/')
import example_grid as ex_grid
import example_forcing as ex_forcing
import timecell as timecell
import make_optimal_tdens as make_opttdens
import meshtools as mt

ctrlpath=sys.argv[1]
gridpath=sys.argv[2]
forcingpath=sys.argv[2]
pflux=flaot(sys.path[3])

ctrl=read(ctrlpath)

simple(ctrl,gridpath,forcingpath,pflux)
