#  f90wrap: F90 to Python interface generator with derived type support
#
#  Copyright James Kermode 2011-2018
#
#  This file is part of f90wrap
#  For the latest version see github.com/jameskermode/f90wrap
#
#  f90wrap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  f90wrap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with f90wrap. If not, see <http://www.gnu.org/licenses/>.
#
#  If you would like to license the source code under different terms,
#  please contact James Kermode, james.kermode@gmail.com
import numpy as np
from ExampleDerivedTypes import ( Dmkcontrols,
                                  example_monge_kantorovich_spectral,
                                  otpdmk,
                                  Tdenspotentialsystem)

import sys
sys.path.append('../../globals/python_timedata')
import timedata as td
sys.path.append('../preprocess/2d_assembly/')
import example_grid as ex_grid
import example_forcing as ex_forcing
import meshtools as mt

# set grid
length=1.0/16
flag_grid='rect_cnst'
extra_grid='rect_cnst'
# build grid
points, vertices, coordinates,elements,element_attributes = ex_grid.example_grid(flag_grid,length,extra_grid)
coord=np.array(coordinates)
if ( coord.shape[1] == 2 ):
    zcoord = np.zeros([coord.shape[0],1])
    coord=np.append(coord, zcoord, axis=1)
topol=np.array(elements)
try:
    print (len(element_attributes))
    flags=np.array(element_attributes)
except NameError:
    flags=np.int_(range(len(topol)))

mt.write_grid(coord,topol,'grid.dat','dat',flags)


nnode=len(coord)
ncell=len(topol)
size_cell=mt.make_size(coord,topol)
bar_cell=mt.make_bar(coord,topol)

# set forcing term
flag_source='rect_cnst'
flag_sink='rect_cnst'
extra_source=''
extra_sink=''
# build
sources=[];
sinks=[];
dirac_sources=[];
dirac_sinks=[];
source_tria=np.zeros([ncell,1])
sink_tria=np.zeros([ncell,1])
steady_source=True
steady_sink=True
ex_forcing.example_source(sources,dirac_sources,
                          str(flag_source),str(extra_source))
ex_forcing.example_sink(sinks,dirac_sinks,
                        str(flag_sink),str(extra_sink))
print(len(sources),len(sinks))
source_tria, steady_source=ex_forcing.make_source(
    source_tria,steady_source,
    sources,
    0,flags,bar_cell)
print(min(source_tria),max(source_tria))


sink_tria, steady_sink=ex_forcing.make_source(
    sink_tria,steady_sink,
    sinks,
    0,flags,bar_cell)
print(min(sink_tria),max(sink_tria))
forcing=source_tria-sink_tria

print(min(forcing),max(forcing))

td.write_steady_timedata('forcing.dat',forcing)


#a = define_a_type.atype() # calls initialise()

# solve with dmk
info=0
dmkp1p0_steady_data(grid, subgrid, tdpot, dmkin, ctrl, info)


#Create triangulation.
triang = mtri.Triangulation(coord[:,0], coord[:,1], topol)
fig1, ax1 = plt.subplots()
#a.rl = 3.0 # calls set()
#print(a.prova)
#a.prova='gmres'
#print(a.prova)
#assert(a.rl == 3.0)

ctrl = Dmkcontrols.DmkCtrl()
print (ctrl.krylov_scheme)
ctrl.krylov_scheme='PCG'
ctrl.prec_type='IC'

print (ctrl.krylov_scheme)

ctrl.max_time_iteration = 0

#ctrl%ndeg = ndeg
#ctrl%max_time_iterations = 10
ctrl.deltat = 0.4
ctrl.info_state = 2

ctrl.id_save_dat = 10
ctrl.lun_tdens = 10
ctrl.fn_tdens='tdens.dat'
ctrl.lun_pot=11
ctrl.fn_pot='pot.dat'

ctrl.lun_statistics=10
ctrl.lun_out=6


pflux=1.0

ctrl.time_discretization_scheme=5
ctrl.max_time_iterations=2
ctrl.info_update=3
ctrl.info_state=2

ctrl.id_save_dat=3

solve_pure_neumann(grid,ctrl,forcing)



#example_monge_kantorovich_spectral(16, ctrl )
tdens=np.zeros(ncell)
pot  =np.zeros(nnode)
tdens[:]=1.0
pflux=1.0

topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1

info=0
otpdmk(3, nnode, ncell, topolT, coordT, pflux, forcing, tdens, pot, \
    ctrl, info)

#tdpot = Tdenspotentialsystem.tdpotsys()
#Tdenspotentialsystem.tdpotsys_construct(tdpot,6,6, 100000, 200000)
