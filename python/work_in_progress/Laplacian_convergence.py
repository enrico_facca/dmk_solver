#  f90wrap: F90 to Python interface generator with derived type support
#
#  Copyright James Kermode 2011-2018
#
#  This file is part of f90wrap
#  For the latest version see github.com/jameskermode/f90wrap
#
#  f90wrap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  f90wrap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with f90wrap. If not, see <http://www.gnu.org/licenses/>.
#
#  If you would like to license the source code under different terms,
#  please contact James Kermode, james.kermode@gmail.com
import numpy as np
from ExampleDerivedTypes import ( Dmkcontrols,
                                  example_monge_kantorovich_spectral,
                                  Abstractgeometry,data2grids,build_refinement,data2grid,
                                  Dmkinputsdata,build_subgrid_rhs,solve_laplacian,
                                  otpdmk,
                                  Tdenspotentialsystem)

import sys
sys.path.append('../../globals/python_timedata')
import timedata as td
sys.path.append('../preprocess/2d_assembly/')
import example_grid as ex_grid
import example_forcing as ex_forcing
import meshtools as mt
"""
# set grid
length=1.0/64
flag_grid='rect_cnst'
extra_grid='rect_cnst'
# build grid
points, vertices, coordinates,elements,element_attributes = ex_grid.example_grid(flag_grid,length,extra_grid)
coord=np.array(coordinates)
if ( coord.shape[1] == 2 ):
    zcoord = np.zeros([coord.shape[0],1])
    coord=np.append(coord, zcoord, axis=1)
topol=np.array(elements)
try:
    print (len(element_attributes))
    flags=np.array(element_attributes)
except NameError:
    flags=np.int_(range(len(topol)))

mt.write_grid(coord,topol,'grid.dat','dat',flags)


nnode=len(coord)
ncell=len(topol)
size_cell=mt.make_size(coord,topol)
bar_cell=mt.make_bar(coord,topol)

#
# init. Fortan structure for grid and subgrid
#
topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1

grid=Abstractgeometry.abs_simplex_mesh()
data2grid(6, 3, len(coord), len(topol), coordT, topolT, grid)
print('grid0')


subgrid=Abstractgeometry.abs_simplex_mesh()
build_refinement(0,grid,subgrid)
print('subgrid')

sub2grid=Abstractgeometry.abs_simplex_mesh()
build_refinement(0,subgrid,sub2grid)
print('sub2grid')

sub3grid=Abstractgeometry.abs_simplex_mesh()
build_refinement(0,sub2grid,sub3grid)
print('sub3grid')

sub4grid=Abstractgeometry.abs_simplex_mesh()
build_refinement(0,sub3grid,sub4grid)
print('sub3grid')



#Abstractgeometry.mesh_destructor(subgrid,0)


topolsub=sub4grid.topol[0:3,:].transpose()-1
coordsub=sub4grid.coord.transpose()

mt.write_grid(coordsub,topolsub,'subgrid4.dat','dat')
"""

coord, topol,flags =mt.read_grid('subgrid4.dat')
#
# init. Fortan structure for grid and subgrid
#
topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1

grid=Abstractgeometry.abs_simplex_mesh()
data2grid(6, 3, len(coord), len(topol), coordT, topolT, grid)
print('grid0')


#
# init set controls
#
ctrl = Dmkcontrols.DmkCtrl()
# globals controls
ctrl.selection=0
ctrl.threshold_tdens=1e-10
ctrl.debug=0


# linear solver ctrl
ctrl.linear_solver='ITERATIVE'
#ctrl.linear_solver='MG'
ctrl.krylov_scheme='PCG'
ctrl.prec_type='IC'
ctrl.n_fillin=30
ctrl.tol_fillin=1e-3
ctrl.relax4prec=1e-09
ctrl.relax_direct=1e-09
ctrl.imax_internal=20
ctrl.imax=500
ctrl.tolerance_linear_solver=1e-12
ctrl.tol_internal=1e-12

solve_laplacian(grid,1,ctrl)
#solve_laplacian(subgrid,1,ctrl)
#solve_laplacian(sub2grid,1,ctrl)
#solve_laplacian(sub3grid,1,ctrl)
    
    

"""
# set forcing term
flag_source='rect_cnst'
flag_sink='rect_cnst'
extra_source=''
extra_sink=''
# build
sources=[];
sinks=[];
dirac_sources=[];
dirac_sinks=[];
source_tria=np.zeros([ncell,1])
sink_tria=np.zeros([ncell,1])
steady_source=True
steady_sink=True
ex_forcing.example_source(sources,dirac_sources,
                          str(flag_source),str(extra_source))
ex_forcing.example_sink(sinks,dirac_sinks,
                        str(flag_sink),str(extra_sink))
print(len(sources),len(sinks))
source_tria, steady_source=ex_forcing.make_source(
    source_tria,steady_source,
    sources,
    0,flags,bar_cell)
print(min(source_tria),max(source_tria))


sink_tria, steady_sink=ex_forcing.make_source(
    sink_tria,steady_sink,
    sinks,
    0,flags,bar_cell)
print(min(sink_tria),max(sink_tria))
forcing=source_tria-sink_tria

print(min(forcing),max(forcing))

td.write_steady_timedata('forcing.dat',forcing)


#
# init input data for dmk solver set rhs, penalty_weight, weight
#
dmkin=Dmkinputsdata.DmkInputs()
Dmkinputsdata.dmkinputs_constructor(dmkin,6,grid.ncell, subgrid.nnode,True)
# set rhs given by forcing = source-sink
#build_subgrid_rhs(subgrid, dmkin.rhs, forcing,np.zeros(grid.nnode))

#td.write_steady_timedata('rhs.dat',dmkin.rhs.reshape([subgrid.nnode,1]))
#td.write_steady_timedata('kappa.dat',dmkin.kappa.reshape([grid.ncell,1]))





#a = define_a_type.atype() # calls initialise()

#a.rl = 3.0 # calls set()
#print(a.prova)
#a.prova='gmres'
#print(a.prova)
#assert(a.rl == 3.0)

ctrl = Dmkcontrols.DmkCtrl()
#
# init set controls
#
ctrl = Dmkcontrols.DmkCtrl()
# globals controls
ctrl.selection=0
ctrl.threshold_tdens=1e-10
ctrl.debug=0


# linear solver ctrl
ctrl.linear_solver='AGMG'
ctrl.krylov_scheme='PCG'
ctrl.prec_type='IC'
ctrl.relax4prec=1e-09
ctrl.relax_direct=1e-09
ctrl.imax_internal=20
ctrl.imax=500
ctrl.tol_internal=1e-12

# time steppin ctrl
ctrl.time_discretization_scheme=5
ctrl.newton_method=12
ctrl.max_time_iterations=1
ctrl.max_nonlinear_iterations=20
ctrl.tol_nonlinear=1e-08
ctrl.tolerance_system_variation=1e-04

ctrl.deltat = 1
ctrl.deltat_control=2
ctrl.deltat_expansion_rate=2.0
ctrl.deltat_lower_bound=0.01
ctrl.deltat_upper_bound=100
ctrl.max_restart_update=20

# info , saving ctrl
ctrl.info_update=3
ctrl.info_state=2
ctrl.id_save_dat=3
ctrl.lun_statistics=10
ctrl.lun_out=6
ctrl.lun_tdens = 10
ctrl.fn_tdens='tdens.dat'
ctrl.lun_pot=11
ctrl.fn_pot='pot.dat'


ctrl.debug=1
ctrl.max_nonlinear_iterations=200
ctrl.tolerance_nonlinear=1e-09
ctrl.relax4prec=1e-10
ctrl.tolerance_system_variation=1e-04

# set dmk inputs
dmkin.penalty_factor=0.0
dmkin.pflux=1.0



#example_monge_kantorovich_spectral(16, ctrl )
tdens=np.zeros(ncell)
pot  =np.zeros(nnode)
tdens[:]=1.0
pflux=1.0

topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1

info=0
otpdmk(3, nnode, ncell, topolT, coordT, pflux, forcing, tdens, pot, \
    ctrl, info)

#tdpot = Tdenspotentialsystem.tdpotsys()
#Tdenspotentialsystem.tdpotsys_construct(tdpot,6,6, 100000, 200000)
"""
