try: range = xrange
except: pass

import sys, petsc4py
petsc4py.init(sys.argv)

from petsc4py import PETSc


import numpy as np
import scipy as sp
import networkx as nx
import matplotlib.pylab as plt
import os
import pyamg
import time


#globals/python_timedata/
import sys
sys.path.append('../../globals/python_timedata/')
import timedata as td

sys.path.append('../../muffe_p1p0/preprocess/2d_assembly/')
import meshtools as mt

def SetConductivity(Graph,cond):
    i=0
    for edge in (Graph.edges()):
        Graph.edges[edge]['weight']=cond[i]
        i+=1

def Errors(IncidenceMatrix,Length,rhs,tdens,pot,OptTdens,OptPot):
    GradPot=(IncidenceMatrix.transpose()*pot)/Length
    ResElliptic=np.linalg.norm(IncidenceMatrix*GradPot-rhs)
    ErrorSlack=np.linalg.norm(tdens*(GradPot**2-1))
    ErrorDualConstraint=abs(max(abs(GradPot))-1.0)
    MaxGrad=max(GradPot)
    ErrTdens=np.linalg.norm(tdens-OptTdens)/np.linalg.norm(OptTdens)
    ErrPot  =np.linalg.norm(pot-OptPot  )/np.linalg.norm(OptPot  )

    return ResElliptic,ErrorSlack,ErrorDualConstraint,MaxGrad,ErrTdens,ErrPot;

def PrintErrors(ResElliptic,ErrorSlack,ErrorDualConstraint,
                MaxGrad,ErrTdens,ErrPot):
    print("|Lapl(tdens)pot-rhs|= %9.2e max |grad|= %9.2e Slack= %9.2e " %
          (ResElliptic,MaxGrad,ErrorSlack))
    print(" ErrTdens= %9.2e ErrorPot= %9.2e"  % (ErrTdens,ErrPot))

    return

    
    


def graph2dat(filename,g):
    f_grid = open(filename, 'w')
    # writing data
    f_grid.write(str(len(g.node))+'\n')
    f_grid.write(str(len(g.edges))+" 2  \n")
    for inode  in range(len(g.node)):
        f_grid.write(str(g.node[inode]['pos'][0]) + " " + str(g.node[inode]['pos'][1])+"\n")

    i=0
    for edge in (g.edges()):
        i=i+1
        f_grid.write(str(edge[0]+1) + " " + str(edge[1]+1) + " " + str(i)+ " \n")
    f_grid.close()


import numpy as np
from collections import Counter

# This will yield weighted edges on the fly, no storage cost occurring 
def gen_edges(counter):
    for k, v in counter.items():  # change to counter.items() for Py3k+
        yield k[0], k[1], v



def dat2pygraph(filename):
    file_name='.dat'

    G=nx.Graph()
    G.add_node(0)
    G.add_nodes_from([2,3])
    G.add_edge(0,2)


    f_grid = open(filename, 'r')

    input_lines = f_grid.readlines()
    nnode= int(input_lines[0].split()[0])
    ntria= int(input_lines[1].split()[0])
    nnodeintria=int(input_lines[1].split()[1])
    coord=np.zeros([nnode,2])
    triang=np.zeros([ntria,2],dtype=int)
    flags=np.zeros(ntria,dtype=int)
    inode=0
    for line in input_lines[2:2+nnode]:
        coord[inode][:]=[float(w) for w in line.split()[0:2]]
        inode=inode+1
        itria=0
    for line in input_lines[2+nnode:]:
        triang[itria][:]=[int(w)-1 for w in line.split()[0:nnodeintria]]
        flags[itria]=line.split()[nnodeintria]
        itria=itria+1

    g=nx.Graph()
    g.add_edges_from(triang)
    
    for i in range(len(coord)):
        g.add_node(i, pos=coord[i])
        #g.node(i)['pos']=coord[i]

    return g

def eikonal(Graph,isource):
    distance_function=nx.shortest_path_length(Graph,inode,None,weight='weight')
    optpot=np.zeros([Graph.number_of_nodes(),1])
    for i in range(Graph.number_of_nodes()):
        optpot[i]=distance_function[i]

    return optpot;


def compute_opttdens(Graph,rhs):
    opttdens=np.zeros(len(Graph.edges()))
    factor=max(rhs);
    i=0
    for edge in (Graph.edges()):
        x1,y1=Graph.nodes[edge[0]]['pos']
        x2,y2=Graph.nodes[edge[1]]['pos']


        xbar=0.5*(x1+x2)
        ybar=0.5*(y1+y2)

        if ( x1 == x2 ):
            opttdens[i]=0.0
        elif( x1!=x2 and y1!=y2 ):
            opttdens[i]=0.0
        else:
            if (ybar<0.25 or ybar>0.75 ):
                opttdens[i]=0.0
            else:
                if( xbar >= 0.125 and xbar <=0.375 ):
                    opttdens[i] = factor*np.fix((xbar-0.125)/abs(x1-x2)+1);
                elif ( xbar > 0.375 and xbar <0.625 ):
                    opttdens[i] = factor*np.fix((0.375-0.125)/abs(x1-x2)+1);
                elif ( xbar >= 0.625 and xbar <=0.875 ):
                    opttdens[i] = factor*np.fix((0.875-xbar)/abs(x1-x2)+1);
                else:
                    opttdens[i]=0;
        i=i+1

    return opttdens

# transform tdens into gradient flow variable
def tdens2gfvar( transformation ,power, tdens ):
        if (  transformation=='identity'):
                gfvar=tdens
        elif (  transformation== 'square'):
                gfvar=np.sqrt(4.0*tdens)
        elif (  transformation== 'cube'):
                gfvar=(3*tdens)**(1.0/3.0)
        elif (  transformation== 'power'):
                gfvar=real((power*tdens)**(1/power))
        elif (  transformation== 'exp'):
                gfvar=log(tdens)
        else:
                print('Not supported')

        if ( not np.isreal(gfvar).all() ):
                print('Not  real gfvar ')

        return gfvar

# Compute tdens from gfvar
def gfvar2tdens( transformation ,power, gfvar ):
    if (  transformation=='identity'):
        tdens=gfvar
    elif (  transformation== 'square'):
        tdens=(gfvar**2)/4.0
    elif (  transformation== 'cube'):
        tdens=(gfvar**3.0)/3.0
    elif (  transformation== 'power'):
        tdens=(gfvar**power)/power
    elif (  transformation== 'exp'):
        tdens=exp(gfvar)
    else:
        print('Not supported')

    if ( not np.isreal(tdens).all()):
        print('Not  real tdens ')

    return tdens

# Compute trans'(gfvar), trans''(gfvar)
def gfvar2f(transformation ,power, gfvar):
    ntdens=len(gfvar);
    trans_prime=np.zeros(ntdens)
    trans_second=np.zeros(ntdens)
    tprime_sqrttdens=np.zeros(ntdens)
    if (  transformation=='identity'):
        trans_prime[:]  = 1.0
        trans_second[:] = 0.0
        tprime_sqrttdens = 1.0/gfvar

    elif (  transformation== 'square'): # mu=(sigma^2/)4
        trans_prime  = 0.5*gfvar
        trans_second[:] = 0.5
        tprime_sqrttdens[:] = 1.0

    elif (  transformation== 'cube'):
        trans_prime  = gfvar**2
        trans_second = 2*gfvar
        tprime_sqrttdens = np.sqrt(gfvar)/np.sqrt(1.0/3.0)

    elif (  transformation== 'power'):
        trans_prime  = real(gfvar**(power-1));
        trans_second = real((power-1)*gfvar**(power-2));
        tprime_sqrttdens = real(gfvar**(power/2-1))/sqrt(p)

    elif (  transformation== 'exp'):
        trans_prime  = exp(gfvar)
        trans_second = exp(gfvar)
        tprime_sqrttdens = gfvar

    else:
        print('Not supported')

    if ( not (np.isreal(trans_prime).all())):
        return

    if ( not (np.isreal(trans_second).all())):
        return
    return trans_prime,trans_second, tprime_sqrttdens

# Compute Wmatrix component
def Wbuild(gfvar,tdens,gradpot,trans_prime,trans_second,invlength, relax_tdens,deltat):
    ntdens=len(gfvar);
    W11=tdens+relax_tdens#+gamma*(gradpot*trans_prime)**2
    W22=-1/deltat+0.5*trans_second*(gradpot**2-1)
    W12=gradpot*trans_prime#*(1+gamma*invlength*W22)
    W21=gradpot*trans_prime

    return W11,W12,W21,W22

# Compute Vmatrix component
def Vbuild(gfvar,tdens,gradpot,trans_prime,trans_second,tprime_sqrttdens,relax_tdens,deltat):
    ntdens=len(gfvar);
    V11=np.ones(ntdens)
    V22=-1/deltat+0.5*trans_second*(gradpot**2-1)
    V12=gradpot*tprime_sqrttdens
    V21=gradpot*tprime_sqrttdens

    return V11,V12,V21,V22

# Compute Wmatrix component
def invert2by2(A11,A12,A21,A22):
    dets=A11*A22-A12*A21
    print(" %9.2e <= dets <=%9.2e" % (min(dets),max(dets)))
    invA11=A22/dets
    invA12=-A12/dets
    invA21=-A21/dets
    invA22=A11/dets

    return invA11,invA12,invA21,invA22




def Dmk_dynamic(incidence_matrix,length,forcing,tdens_0,pflux,tol_var_tdens,opttdens=None):
    # initialization of common quanties
    #print(sparse.csr_matrix(transpose(incidence_matrix)))
    inc_transpose=sp.sparse.csr_matrix(incidence_matrix.transpose())
    inv_len_mat=sp.sparse.diags(1/length,0)



    npot=len(forcing) # nuumer of nodes
    # number of commidities
    #ncomm=forcing.shape[1]

    ntdens=len(tdens_0) # number of edges


    #initialization of paramters and controls
    relax_linsys=1.0e-8
    max_time_iterations=10
    deltat=2e0

    scheme='ei'



    time_iteration=0
    time=0

    #initilaize at time zero
    tdens=tdens_0

    td_mat=sp.sparse.diags(tdens,0)
    stiff=incidence_matrix*td_mat*inv_len_mat*inc_transpose
    #print(stiff.todense(),'-',(sp.sparse.identity(npot)).todense())
    stiff_relax=stiff+relax_linsys*sp.sparse.identity(npot)
    pot=sp.sparse.linalg.spsolve(stiff_relax,forcing)


    res=np.linalg.norm(stiff*pot-forcing)/np.linalg.norm(forcing)

    print(res)

    convergence_achieved = False

    total_linear_sytem=0

    while (( not convergence_achieved ) and
           ( time_iteration < max_time_iterations)):
        time_iteration+=1


        # update whole tdens-pot system
        tdens_old=tdens
        pot_old=pot
        print("************UPDATE #%d BEGIN******************************"% (time_iteration))
        tdens, pot,info,nlinsys = update(scheme,tdens,pot,forcing,incidence_matrix,inc_transpose,inv_len_mat,deltat)
        gradpot=(1.0/length)*(inc_transpose*pot)
        total_linear_sytem=total_linear_sytem+nlinsys
        if (info==0):
            print("************UPDATE #%d END  ******************************"% (time_iteration))
            var_tdens=np.linalg.norm(length*(tdens-tdens_old))/(deltat*np.linalg.norm(length*tdens_old))
            print("Time step=%d Var Tdens=%9.2e"%(time_iteration,var_tdens))
            err_dual=max(gradpot**2-1)
            err_slack=max(tdens*(gradpot**2-1))
            print("Time step=%d max (gradpot^2-1)=%9.2e max tdens*(grapot^2-1)=%9.2e "% (time_iteration,err_dual,err_slack))

            if ( opttdens is not None):
                err_tdens=np.linalg.norm(np.sqrt(length)*(tdens-opttdens))/(deltat*np.linalg.norm(np.sqrt(length)*opttdens))
                print("Time step=%d Err Tdens=%9.2e"%(time_iteration,err_tdens))

        else:
            print("************UPDATE #%d FAILED  ******************************"% (time_iteration))
            tdens=tdens_old
            pot=tdens_old
            break
        print(' ')




        if ( var_tdens< tol_var_tdens) :
            convergence_achieved = True
            info=0
            break





    if ( info !=0):
        print('Convergence not achieved')

    return tdens, pot,info




def update(scheme,tdens,pot,forcing,incidence_matrix,inc_transpose,inv_len_mat,deltat):
    #
    max_nrestart=6

    nlinearsystem=0

    # info set to non zero
    info_update=-1

    # consant quantities
    ntdens=len(tdens)
    npot=len(forcing)
    nfull=npot+ntdens

    # linear solver quantities
    relax_linsys=1.0e-10
    max_linear_iteration = 500
    tol_linear_newton=1e-3
    tol_internal_prec=1e-3
    max_iteration_internal_prec=1

    #
    linesearch=0
    W22_bound=1e-4


    # non linear solver quanties
    max_nonlinear_iteration=15
    tol_nonlinear=1e-8


    # linear solver approach
    # 0 =use direct solver for the full jacobian
    # 1 =invert w.r.t W22 block if negative
    # 2 = augmented lagrangian
    solver_approach=4

    # solver_approach ==1
    solver_reduced=2

    # 1 : # W=length S=-(1/gamma)*W-C
    # 2 :
    auguemented_choice=1
    gamma=0.0 # augmented lagrangian relaxation


    # relaxation
    #transformation='square'
    transformation='cube'
    power=3

    relax_tdens=1e-12
    relax_newton=1.0



    # algebraic multigrid paremeters
    # 1= pyamg.smoothed_aggregation_solver
    # 2= pyamg.rootnode_solver
    amg_choice=2




    if (scheme=='ee'):
        # compute the gradient and rhs of dmk ode
        grad=inv_len_mat*inc_transpose*pot
        rhs_ode=tdens**pflux*abs(grad)**pflux-tdens
        # multi commodity rhs ode
        # mgrad=grad**2
        # temp=sum(mgrad, axis=0)
        # rhs_ode=tdens**pflux*temp-tdens

        # possible solution for adaptive time step
        """
        deltat=one/np.norm(rhs_ode)
        """

        # update tdens variable
        tdens=tdens+deltat * rhs_ode

        # update pot variable
        td_mat=sp.sparse.diags(tdens,0)
        stiff=incidence_matrix*td_mat*inv_len_mat*inc_transpose
        #print(stiff.todense(),'-',(sp.sparse.identity(npot)).todense())
        stiff_relax=stiff+relax_linsys*sp.sparse.identity(npot)



        try:
            pot=sp.sparse.linalg.spsolve(stiff_relax,forcing)
            info_update=0
            pass
        except:
            info_update=-1

        res=np.linalg.norm(stiff*pot-forcing)/np.linalg.norm(forcing)

        print(res)


    if (scheme=='ei'):
        # fixed matrices
        sqLEN=sp.sparse.diags(np.sqrt(length),0)
        invsqLEN=sp.sparse.diags(1/np.sqrt(length),0)
        invLEN=sp.sparse.diags(1/length,0)
        lapl=incidence_matrix*inv_len_mat*inc_transpose
        inc=np.zeros(nfull)
        AinvsqLEN=incidence_matrix*invsqLEN
        invlength=1/length

        #
        # Zblock ( AL^{-1/2} 0       )
        #        ( 0         L^{1/2} )
        Zblock=sp.sparse.block_diag((AinvsqLEN, sqLEN))
        ZTblock=Zblock.transpose()

        #
        # Yblock ( A 0  )
        #        ( 0 I  )
        Yblock=sp.sparse.block_diag((incidence_matrix, sp.sparse.eye(ntdens)))
        YTblock=Yblock.transpose()


        # store initial data
        gfvar=tdens2gfvar( transformation ,power, tdens )
        tdens_old=tdens
        gfvar_old=gfvar
        pot=pot-(np.sum(pot)/(1.0*npot))*np.ones(npot)
        pot_old=pot
        gradpot=invlength*(inc_transpose*pot)
        trans_prime,trans_second, tprime_sqrttdens = gfvar2f(transformation ,power, gfvar)
        W22_pure=max(0.5*trans_second*(gradpot**2-1))
        if (W22_pure<0):
            deltat=2.0*deltat
        else:
            deltat=1.0/(W22_pure+W22_bound)

        nrestart=0
        while True:
            print(nrestart ,max_nrestart)
            print("************deltat=%9.2e******************************"% (deltat))
            if (nrestart -1 == max_nrestart):
                info_update=1
                break
            if (nrestart !=0):
                print("************RESTART %d deltat=%9.2e******************************"% (nrestart,deltat))

            rhs_norm=1e30;
            inewton=0
            convergence_achieved=False
            while ( not convergence_achieved  ):
                print ("****************************************************************************")
                inewton=inewton+1
                if (inewton==max_nonlinear_iteration-1):
                    info_newton=2
                    break


                #
                # build relax tdens and gradpot
                #
                tdens=gfvar2tdens(transformation ,power, gfvar)
                tdens_relaxed=tdens+relax_tdens


                # build derived quantities ( trans_prime, grad etc)
                trans_prime,trans_second, tprime_sqrttdens = gfvar2f(transformation ,power, gfvar)
                gradpot=invlength*(inc_transpose*pot)
                W11,W12,W21,W22=Wbuild(gfvar,tdens,gradpot,trans_prime,trans_second,invlength,relax_tdens,deltat)
                print ("%2d | INPUT: max W22=%9.2e min_tdens=%9.2e errgrad=%9.2e"% ( inewton,max(W22),min(tdens),max(tdens*(gradpot-1))))


                #
                # assembly sparse matrices
                #
                # Wbock
                diagonals = [W21,np.hstack((W11,W22)), W12]
                Wblock=sp.sparse.diags(diagonals, [-ntdens, 0, ntdens])

                # jacobian
                #    = Z * (W11 W12 ) *Z^T
                #          (W21 W22 )
                def mvjac(x):
                    return Zblock*Wblock*ZTblock*x
                jacobian=sp.sparse.linalg.LinearOperator(matvec=mvjac, shape=(nfull, nfull), dtype=float)


                # build rhs_newton
                # rhs_gfvar
                rhs_gfvar=length*(-1/deltat*(gfvar-gfvar_old)+trans_prime*0.5*(gradpot**2-1))

                # rhs_pot=stiff-pot
                grad=inc_transpose*pot
                temp=tdens_relaxed*invlength*grad
                rhs_pot=incidence_matrix*temp-forcing
                rhs_newton=np.hstack((-rhs_pot,-rhs_gfvar))

                # check on rhs_norm
                if ( inewton > 1 ):
                    rhs_norm_old=rhs_norm

                rhs_norm=np.linalg.norm(rhs_newton)
                rhs_pot_norm=np.linalg.norm(rhs_newton[0:npot])
                rhs_gfvar_norm=np.linalg.norm(rhs_newton[npot:nfull])
                print ("%2d | NEWTON: rhs=%9.2e rhs_pot=%9.2e rhs_gfvar=%9.2e"% (inewton, rhs_norm,rhs_pot_norm,rhs_gfvar_norm))

                if ( rhs_norm < tol_nonlinear):
                    convergence_achieved=True
                    info_newton=0
                    break

                if ( ( inewton > 1 ) and ( rhs_norm > 1e1*rhs_norm_old) ):
                    info_newton=1
                    break

                #
                # solve J        inc = -f
                #       jacobian inc = rhs_newton
                # direct solver
                nlinearsystem=nlinearsystem+1
                if ( solver_approach == 0) :
                    # Reduced approach
                    # solve inverting with respect to block (2,2)
                    print('not done')

                elif ( solver_approach == 1) :
                    # check for positivity of W22
                    if ( max(W22)>0 ):
                        info_newton=2
                        break

                    t = time.time()
                    # build rhs_reducced sytem f-BT/W22
                    r=W12/(length*W22)*rhs_newton[npot:nfull];
                    v=incidence_matrix*r;
                    rhs_reduced=rhs_newton[0:npot]-v

                    # build jacobian reduced
                    tdens_reduced = W11/length - W12*W21/(W22*length)
                    td_mat=sp.sparse.diags(tdens_reduced,0)
                    jacobian_reduced=incidence_matrix*td_mat*inc_transpose+relax_linsys*sp.sparse.identity(npot)

                    # solve via multigrid solver
                    if (amg_choice == 1) :
                        method = pyamg.smoothed_aggregation_solver
                    elif (amg_choice == 2):
                        method = pyamg.rootnode_solver
                    else:
                        raise ValueError("Enter a choice of 1 or 2")

                    inc[:]=0
                    if (solver_reduced ==1 ):
                        mls = method(jacobian_reduced)
                        #global iterml=0
                        iterml=[]
                        def mlapply(x):
                            residuals=[]
                            y=mls.solve(x,
                                        maxiter=max_iteration_internal_prec,
                                        tol=tol_internal_prec,
                                        residuals=residuals)
                            #print(np.linalg.norm(x),len(residuals),residuals[-1])
                            iterml.append(len(residuals))
                            return y
                        preconditioner=sp.sparse.linalg.LinearOperator(matvec=mlapply, shape=(npot, npot), dtype=float)

                        relres = []
                        iters =0
                        class Counter(object):
                            def __init__(self):
                                self.iter = 0
                                self.realres=[]
                            def __str__(self):
                                return str(self.iter)
                            def addone(self, xk):
                                self.iter += 1
                                #self.realres.append(np.linalg.norm(jacobian_reduced*xk- rhs_reduced))

                        info_solver = Counter()
                        def res(xk):
                            print(np.linalg.norm(jacobian_reduced*xk- rhs_reduced)/np.linalg.norm(rhs_reduced))
                            # solve finally using conjugate gradient with MultiLevel preconditoner
                        inc[0:npot],info = sp.sparse.linalg.cg(jacobian_reduced, rhs_reduced,x0=np.zeros(npot),
                                                                   tol=tol_linear_newton,atol=0.0,maxiter=max_linear_iteration,
                                                                   M=preconditioner,
                                                                   callback=info_solver.addone)
                        #print('iternal',len(iterml))
                        iter=info_solver.iter


                    elif (solver_reduced == 2):
                        accelerated_residuals=[]
                        mls = method(jacobian_reduced)
                        #print(mls)
                        inc[0:npot] = mls.solve(rhs_reduced, tol=tol_linear_newton,
                                                maxiter=max_linear_iteration, accel=None,
                                                residuals=accelerated_residuals)
                        accelerated_residuals = np.array(
                            accelerated_residuals) / accelerated_residuals[0]
                        iter=len(accelerated_residuals)
                        if(iter==max_linear_iteration):
                            info=iter
                        else:
                            info=0

                    # get inc_tden variable
                    inc[0:npot]=inc[0:npot]-(np.sum(inc[0:npot])/(1.0*npot))*np.ones(npot)
                    v=inc_transpose*inc[0:npot]
                    inc[npot:nfull]=(rhs_newton[npot:nfull]-W21*v)/(W22*length);
                    elapsed = time.time() - t

                    info_newton=info

                    res=jacobian*inc-rhs_newton
                    normres=np.linalg.norm(res)



                    print ("%2d | LINSYS: %d %d  normrhs=%9.2e resreal=%9.2e CPU=%9.2e "%
                           ( inewton,info, iter,rhs_norm,normres,elapsed))

                # Augemented lagrangian approach
                elif ( solver_approach == 2 ) :
                    # pass to ( A BT  ) form
                    #         ( B -C  )
                    A11=W11/length
                    A12=W12
                    A21=W21
                    A22=-W22*length

                    # define different augmented lagrangian approach
                    #
                    if ( auguemented_choice==1):
                        weight=length
                        inv_weight=1/length
                        S=-(1/gamma)*weight-A22

                        A11=A11 + gamma*(gradpot*trans_prime)**2*inv_weight
                        A12=A12 - gamma*A22#inv_weight*A22

                    if ( auguemented_choice==2):
                        W=length*(gamma+W22)

                    # define new operators
                    # pass to ( A BT  ) form
                    #         ( B -C  )
                    diagonals = [A21,np.hstack((A11,-A22)), A12]
                    Ablock=sp.sparse.diags(diagonals, [-ntdens, 0, ntdens])

                    def mvjac(x):
                        return Yblock*Ablock* YTblock*x
                    aug_jacobian=sp.sparse.linalg.LinearOperator(matvec=mvjac, shape=(nfull, nfull), dtype=float)


                    #
                    # add augmented lagrangia term
                    #
                    r=W12/weight*rhs_gfvar;
                    v=incidence_matrix*r;
                    rhs_pot=rhs_pot+gamma*v;
                    rhs_newton_augemented=np.hstack((-rhs_pot,-rhs_gfvar))



                    #
                    # Triangual preconditioner for Augmented Lagrangian
                    #

                    # B matrixs
                    A21_mat=sp.sparse.diags(W21,0)
                    Bmat=A21_mat*inc_transpose

                    # invAgamma
                    td_mat=sp.sparse.diags( A11,0)
                    Agamma=incidence_matrix*td_mat*inc_transpose+relax_linsys*sp.sparse.identity(npot)
                    if (amg_choice == 1) :
                        method = pyamg.smoothed_aggregation_solver
                    elif (choice == 2):
                        method = pyamg.rootnode_solver
                    else:
                        raise ValueError("Enter a choice of 1 or 2")
                    mls = method(Agamma)
                    def mlapply(x):
                        return mls.solve(x, tol=tol_internal_prec)
                    invAgamma=sp.sparse.linalg.LinearOperator(matvec=mlapply, shape=(npot, npot), dtype=float)

                    # invS
                    invS=1/S


                    #
                    # ( A 0)^{-1} = ( A^{-1}
                    # ( B S)        (
                    #
                    def mv(v):
                        ntdens=Bmat.shape[0]
                        npot=Bmat.shape[1]
                        nfull=npot+ntdens
                        ypot=invAgamma*v[0:npot]
                        ygfvar=Bmat*ypot
                        ygfvar=v[npot:nfull]-ygfvar
                        ygfvar=-invS*ygfvar
                        return np.hstack((ypot,ygfvar))

                    TriangPrec= sp.sparse.linalg.LinearOperator(matvec=mv, shape=(nfull, nfull), dtype=float)
                    relres=[]
                    inc, info= pyamg.krylov.fgmres(jacobian, rhs_newton_augemented, x0=None,
                                            tol=tol_linear_newton, restrt=None,
                                            maxiter=max_linear_iteration, xtype=None,
                                            M=TriangPrec, callback=None, residuals=relres)

                    info_newton=info

                # Helman approach
                elif ( solver_approach == 4) :
                    if ( max(W22) > 0 ) :

                        info_newton=1
                        break
                    # define matrix component
                    V11,V12,V21,V22=Vbuild(gfvar,tdens,gradpot,trans_prime,trans_second,
                                           tprime_sqrttdens,relax_tdens,deltat)
                    invV11,invV12,invV21,invV22=invert2by2(V11,V12,V21,V22)

                    # define component of helman preconditioner
                    #
                    # Vblock
                    #
                    diagonals = [invV21,np.hstack((invV11,invV22)), invV12]
                    invV=sp.sparse.diags(diagonals, [-ntdens, 0, ntdens])


                    #
                    # ZTDblock ( A(\Tdens) L^{-1/2} 0       )
                    #          ( 0                  L^{1/2} )
                    sqtd_mat=sp.sparse.diags(np.sqrt(tdens*invlength),0)
                    ATdensinvsqLEN=incidence_matrix*sqtd_mat
                    ZTdensblock=sp.sparse.block_diag((ATdensinvsqLEN, sqLEN))
                    ZTdensblockT=ZTdensblock.transpose()


                    #
                    # build stiff
                    #
                    td_mat=sp.sparse.diags( tdens_relaxed*invlength,0)
                    stiff=incidence_matrix*td_mat*inc_transpose+relax_linsys*sp.sparse.identity(npot)

                    #
                    # inverse stiff
                    #
                    if (amg_choice == 1) :
                        method = pyamg.smoothed_aggregation_solver
                    elif (amg_choice == 2):
                        method = pyamg.rootnode_solver
                    else:
                        raise ValueError("Enter a choice of 1 or 2")
                    mls = method(stiff)
                    #global iterml
                    iterml=[]
                    def invstiff(x):
                        residuals=[]
                        y=mls.solve(x,
                                    tol=tol_internal_prec,
                                    maxiter=max_iteration_internal_prec,
                                    residuals=residuals)
                        #print(np.linalg.norm(x),len(residuals),residuals[-1])
                        iterml.append(len(residuals))
                        return y
                    precstiff=sp.sparse.linalg.LinearOperator(matvec=invstiff, shape=(npot, npot), dtype=stiff.dtype)

                    # combine
                    # invZZT=invSTIFF 0
                    #        0        L^-1
                    def applyinvZZT(x):
                        npot=precstiff.shape[0]
                        ntdens=len(invlength)
                        v=np.zeros(npot+ntdens)
                        v[0:npot]=precstiff*x[0:npot]
                        v[npot:npot+ntdens]=invlength*x[npot:npot+ntdens]
                        return v
                    #invZZT=sp.sparse.block_diag((precstiff, invLEN))
                    invZZT=sp.sparse.linalg.LinearOperator(matvec=applyinvZZT, shape=(nfull, nfull), dtype=stiff.dtype)

                    #
                    # Helman V preconditioner
                    #
                    def precfullapply(x):
                        y0=invZZT*x
                        y1=ZTdensblockT*y0
                        y2=invV*y1
                        y3=ZTdensblock*y2
                        y4=invZZT*y3
                        return y4   #invZZT*ZTdensblock*invV*(ZTdensblockT*(invZZT*x))
                    HelmanV = sp.sparse.linalg.LinearOperator(matvec=precfullapply, shape=(nfull, nfull), dtype=float)

                    #
                    # solve
                    #
                    relative_residua=[]
                    inc, info= pyamg.krylov.fgmres(jacobian, rhs_newton, x0=None,
                                            tol=tol_linear_newton, restrt=None,
                                            maxiter=max_linear_iteration, xtype=None,
                                            M=HelmanV, callback=None, residuals=relative_residua)

                    print('inc pot * ones',np.sum(inc[0:npot]))
                    inc[0:npot]=inc[0:npot]-(np.sum(inc[0:npot])/(1.0*npot))*np.ones(npot)

                    print (inewton,' | info=',info,
                           'iter=', len(relative_residua),
                           'resini=',relative_residua[0],
                           'resend=',relative_residua[-1])
                    info_newton=info

                if ( info_newton == 0) :
                    limit=100
                    alpha=1.0
                    if ( linesearch == 0) :
                        # update pot, gradpot,tdens, varaible
                        pot=pot+alpha*inc[0:npot]
                        gfvar=gfvar+alpha*inc[npot:nfull]
                        tdens=gfvar2tdens(transformation ,power, gfvar)
                    else:
                        potnow=pot
                        gfvarnow=gfvar
                        while( limit > -W22_bound ) :
                            # update pot, gradpot,tdens, varaible
                            pot=potnow+alpha*inc[0:npot]
                            gfvar=gfvarnow+alpha*inc[npot:nfull]
                            tdens=gfvar2tdens(transformation ,power, gfvar)

                            # build derived quantities ( trans_prime, grad etc)
                            trans_prime,trans_second, tprime_sqrttdens = gfvar2f(transformation ,power, gfvar)
                            gradpot=invlength*(inc_transpose*pot)
                            W11,W12,W21,W22=Wbuild(gfvar,tdens,gradpot,trans_prime,trans_second,invlength,relax_tdens,deltat)
                            limit=max(W22)
                            alpha=alpha/1.05
                            print ("%2d | alpha=%9.2e "% (inewton, alpha))
                            if( alpha < 1e-2 ):
                                break
                                info_newton=1


                    pot=pot-(np.sum(pot)/(1.0*npot))*np.ones(npot)

                if (info_newton!=0):
                    break


            print( convergence_achieved, nrestart)
            if ( convergence_achieved ):
                info_update=0
                break
            else:
                gfvar=gfvar_old
                tdens=tdens_old
                pot=pot_old
                deltat=deltat/2
                nrestart=nrestart+1;
                print ( 'deltat=',deltat)


    return tdens,pot,info_update,nlinearsystem
#
# read paht for graph and rhs
#
file_graph=sys.argv[1]
file_rhs=sys.argv[2]


# Read graph
print('Reading ',file_graph)
Graph=dat2pygraph(file_graph)
print('DONE')

# assembly signed incidence matrix
IncidenceMatrix=nx.incidence_matrix(Graph,oriented=True)
IncidenceMatrixTranspose=IncidenceMatrix.transpose()

# build length vectors
Length=np.zeros(len(Graph.edges()))
i=0
Graph.edges(data=True)
for edge in (Graph.edges()):
    Length[i]=sp.spatial.distance.euclidean(
        Graph.nodes[edge[0]]['pos'],Graph.nodes[edge[1]]['pos'])
    Graph.edges[edge]['weight']=Length[i]
    i+=1


# read rhs 
Rhs=td.read_steady_timedata(file_rhs).flatten()
inode=np.argmax(Rhs)

npot  =nx.number_of_nodes(Graph)
ntdens=nx.number_of_edges(Graph)



print (npot,ntdens)

#
t=time.time()
optpot=eikonal(Graph,inode)
elapsed = time.time() - t
print('time',elapsed)

td.write_steady_timedata('optpot.dat',optpot)

"""

try:
    filein=sys.argv[3]
    OptTdens=td.read_steady_timedata(filein).flatten()
except:
    OptTdens=np.ones(ntdens)

try:
    filein=sys.argv[4]
    OptPot=td.read_steady_timedata(filein).flatten()
except:    
    OptPot=np.ones(npot)

#
#td.write_steady_timedata('optpot.dat',optpot)

#path=nx.single_source_dijkstra_path(Graph, 0, cutoff=None, weight='length')
#elapsed = time.time() - t
#print('end')
#print(elapsed)
#exit
print('Assembly')

relax_tdens=0e-14
tdens0=np.ones(ntdens)
pflux=1
tol_var_tdens=1e-12

tdens=tdens0
pot=np.zeros(npot)
tdens_la=(tdens+relax_tdens)/Length
td_mat=sp.sparse.diags(tdens_la,0)
WeigthedLaplacian=IncidenceMatrix*(td_mat*IncidenceMatrixTranspose)

plt.spy(WeigthedLaplacian )
plt.show()
#WeigthedLaplacian=nx.laplacian_matrix(Graph, weight='weight')

Graph=SetConductivity(Graph,tdens_la)

lambda_direct=1e-13;
MatrixSolved=(WeigthedLaplacian+
               lambda_direct*sp.sparse.identity(npot))
print('Start linear System')

#
# Define preconditoner
# 
lambda_for_prec=0e-14
max_iteration_internal_prec=1
tol_internal_prec=1e-10


#
# init multigrid
# 
MatrixForPrec=(MatrixSolved+
               lambda_for_prec*sp.sparse.identity(npot))
Kernel=np.ones((npot,1))
mls = pyamg.smoothed_aggregation_solver(MatrixForPrec,
                                        Kernel,
                                        coarse_solver='pinv2')
#
# build linear Operator
# 
def mlapply(x):
    #residuals=[]
    #    cycle : {'V','W','F','AMLI'}
    y=mls.solve(x,
                maxiter=max_iteration_internal_prec,
                tol=tol_internal_prec,cycle='W')
    #print(np.linalg.norm(x),len(residuals),residuals[-1])
    #iterml.append(len(residuals))
    return y
preconditioner=sp.sparse.linalg.LinearOperator(
    matvec=mlapply, shape=(npot, npot), dtype=float)

#
# compute solution
#
num_iters=0
def callback(xk):
    global num_iters
    num_iters+=1

t=time.time()
pot,info = sp.sparse.linalg.cg(MatrixSolved,Rhs, x0=pot,tol=1e-12,
                               maxiter=60,
                               M=preconditioner,
                               callback=callback)

elapsed = time.time() - t
print(elapsed)
print(num_iters,info)


ResElliptic,ErrorSlack,ErrorDualConstraint,MaxGrad,ErrTdens,ErrPot=Errors(
    IncidenceMatrix,Length,Rhs,tdens,pot,OptTdens,OptPot)
PrintErrors(ResElliptic,ErrorSlack,
            ErrorDualConstraint,MaxGrad,ErrTdens,ErrPot)

print(ResElliptic/np.linalg.norm(Rhs))

OptDB = PETSc.Options()

petsc_mat = (
    PETSc.Mat().createAIJ(size=MatrixSolved.shape,
                          csr=(MatrixSolved.indptr, MatrixSolved.indices,
                               MatrixSolved.data)))

#a,KernelVec=petsc_mat.createVecs()
#KernelVec.setArray(np.ones(npot))
#nsp = PETSc.NullSpace().create(vectors=[KernelVec])
#petsc_mat.setNullSpace(nsp)


petsc_pot,petsc_rhs=petsc_mat.createVecs()

petsc_pot.set(0)
petsc_rhs.setArray(Rhs)

ksp = PETSc.KSP().create()
ksp.setOperators(petsc_mat)
ksp.setType('fcg')
pc = ksp.getPC()
#pc.setType('icc') 
pc.setType(pc.Type.GAMG) 
#OptDB['mg_coarse_pc_type'] = 'svd' 

ksp.setFromOptions()
ksp.setTolerances(rtol=1e-15, atol=None, divtol=None, max_it=200)
t=time.time()
ksp.solve(petsc_rhs,petsc_pot)
elapsed = time.time() - t
iter = ksp.getIterationNumber()
print(iter,elapsed)

pot = petsc_pot.getArray()

ResElliptic,ErrorSlack,ErrorDualConstraint,MaxGrad,ErrTdens,ErrPot=Errors(
    IncidenceMatrix,Length,Rhs,tdens,pot,OptTdens,OptPot)
PrintErrors(ResElliptic,ErrorSlack,
            ErrorDualConstraint,MaxGrad,ErrTdens,ErrPot)


"""

"""
accelerated_residuals=[]
Kernel=np.ones((MatrixSolved.shape[0],1))
mls = pyamg.smoothed_aggregation_solver(MatrixSolved,
                                        Kernel,
                                        max_levels=20,
                                        coarse_solver='pinv2')

accelerated_residuals=[]
pot = mls.solve(Rhs, tol=1e-12,
                maxiter=300,accel='cg',
                residuals=accelerated_residuals)
Iterations=len(accelerated_residuals)
print(Iterations)

ResElliptic,ErrorSlack,ErrorDualConstraint,MaxGrad,ErrTdens,ErrPot=Errors(
    IncidenceMatrix,Length,Rhs,tdens,pot,OptTdens,OptPot)
PrintErrors(ResElliptic,ErrorSlack,
            ErrorDualConstraint,MaxGrad,ErrTdens,ErrPot)



#opttdens, optpot,info= Dmk_dynamic(IncidenceMatrix,length,forcing,tdens_0,pflux,tol_var_tdens,exact_solution)
"""
