#  f90wrap: F90 to Python interface generator with derived type support
#
#  Copyright James Kermode 2011-2018
#
#  This file is part of f90wrap
#  For the latest version see github.com/jameskermode/f90wrap
#
#  f90wrap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  f90wrap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with f90wrap. If not, see <http://www.gnu.org/licenses/>.
#
#  If you would like to license the source code under different terms,
#  please contact James Kermode, james.kermode@gmail.com
import numpy as np
from ExampleDerivedTypes import ( Dmkcontrols,
                                  example_monge_kantorovich_spectral,
                                  Abstractgeometry,data2grids,build_refinement,data2grid,
                                  Dmkinputsdata,build_subgrid_rhs,solve_laplacian,
                                  otpdmk,
                                  Tdenspotentialsystem)

import sys
sys.path.append('../../globals/python_timedata')
import timedata as td
sys.path.append('../preprocess/2d_assembly/')
import example_grid as ex_grid
import example_forcing as ex_forcing
import meshtools as mt
import inspect

#
# init set controls
#
ctrl = Dmkcontrols.DmkCtrl()
# globals controls
ctrl.selection=0
ctrl.threshold_tdens=1e-10
ctrl.debug=0


# linear solver ctrl
ctrl.linear_solver='ITERATIVE'
#ctrl.linear_solver='MG'
ctrl.krylov_scheme='PCG'
ctrl.prec_type='IC'
ctrl.n_fillin=30
ctrl.tol_fillin=1e-3
ctrl.relax4prec=1e-09
ctrl.relax_direct=1e-09
ctrl.imax_internal=20
ctrl.imax=500
ctrl.tolerance_linear_solver=1e-12
ctrl.tol_internal=1e-12

def write(ctrl,filename):
    maxl=1
    for i in inspect.getmembers(ctrl): 
        # to remove private and protected 
        # functions 
        if not i[0].startswith('_'): 
            
            # To remove other methods that 
            # doesnot start with a underscore 
            if not inspect.ismethod(i[1]):
                name=i[0]
                value=i[1]
                try:
                    stringout=value.decode("utf-8").strip()
                except:
                    stringout=str(value)
                maxl=max(maxl,len(stringout))
    with open(filename, 'w') as f:
        for i in inspect.getmembers(ctrl): 
                         
            # to remove private and protected 
            # functions 
            if not i[0].startswith('_'): 
            
                # To remove other methods that 
                # doesnot start with a underscore 
                if not inspect.ismethod(i[1]):
                    name=i[0]
                    value=i[1]
                    try:
                        stringout=value.decode("utf-8").strip()
                    except:
                        stringout=str(value)
                        
                    print(stringout.ljust(maxl)+" ! "+ str(name),file=f)


def read(filepath):
    ctrl = Dmkcontrols.DmkCtrl()
    with open(filepath) as fp:
        cnt = 0
        for line in fp:
            value=line.split(' ')[0]
            name=line.split(' ')[2].rstrip()
            cnt += 1
            setattr(ctrl, name, value)
    return ctrl

#write(ctrl,'muffa.ctrl')
ctrlread=read('muffa.ctrl')
print(ctrlread.fn_tdens)
