from geometry import ( Abstractgeometry, build_refinement )
import sys
sys.path.append('/home/fh/src/muffe_p1p0/preprocess/2d_assembly/')
import example_grid as ex_grid
import example_forcing as ex_forcing
import timecell as timecell
import make_optimal_tdens as make_opttdens
import meshtools as mt

fn_grid=sys.argv[1]
fn_subgrid=sys.argv[2]

[coord,topol,flags]=mt.read_grid(fn_grid)

nnode=len(coord)
ncell=len(topol)
nnodeincell=topol.shape[1]
size_cell=mt.make_size(coord,topol)
bar_cell=mt.make_bar(coord,topol)

#
# init. Fortan structure for grid and subgrid
#
topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1

grid=Abstractgeometry.abs_simplex_mesh()
Abstractgeometry.constructor_from_data(grid, 6, nnode, ncell, nnodeincell, 'triangle', topolT, coordT)

subgrid=Abstractgeometry.abs_simplex_mesh()
build_refinement(6, grid, subgrid)

coordsub=subgrid.coord.transpose()
triangsub=subgrid.topol
triangsub=triangsub[0:3,:].transpose()-1
print(fn_subgrid)
mt.write_grid(coordsub,triangsub,fn_subgrid,'dat')
