

FV2DSWE             {#mainpage}
=======

Authors:
-------

Enrico Facca and Mario Putti

**Initial date:** December 2015


This document describes the source code for the FV2DSWE model.

The code solves the normally integrated shallow water equations
defined on an irregular bottom surface.

