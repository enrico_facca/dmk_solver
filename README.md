# README #

## What is this repository for? 

This repository contains the source code of the numerical methods
based on the Dynamical Monge-Kantorovich (DMK) model for solving:
1. Optimal Transport Problem with cost equal to the
    geodesic distance in 2d[^1], 3d and surface domain[^2].
2. Congested and Branched Transport Problems [^3]. 
3. Optimal Transport Problem on graphs taking
     the shortest path distance as transport cost [^4]
4. Basis-Pursuit (l^1-minimizaiton) and Lasso problem.
     ( see Facca-Cardin-Putti:2018 )
5. Shape optimization

The core project is written in object-oriented Fortran ( src/ directory).
Programs and examples can be found in tools/ and examples/.
There exists a fortran-pyhton interface to call some of the DMK solver
directly from python.

Version * 2.0

## How do I get set up?
### Dependencies
  * INSTALLED PROGRAMS/LIBRIERIES
    * Fortran compiler compatible with FORTRAN 2003 (gfortran v>=4.8 )
    * Cmake (version>=2.8)
    * doxygen (optional, rquired for generating user guide)
  * INSTALLED PROGRAMS/LIBRIERIES
    * Blas/Lapack libraries
  * EXTERNAL REPOSITORIES: Clone the following repositories in the directory
    at the same level of this repositories, using the command:
```
git clone https://gitlab.com/enrico_facca/globals.git        
git clone https://gitlab.com/enrico_facca/linear_algebra.git 
git clone https://gitlab.com/enrico_facca/geometry.git 
git clone https://gitlab.com/enrico_facca/p1galerkin.git 
```

    The results should be
    .
    ├── geometry
    ├── globals
    ├── linear_algebra
    ├── dmk_solver
    └── p1galerkin

  * PYTHON >=3.6 and PACKAGES 
    * numpy  (MANDATORY version=1.18.x)  "pip3 install --upgrade numpy==1.18.1" 
    * click  (RECOMMENDED "pip3 install click" )
    * meshpy (RECOMMENDED "pip3 install meshpy" ) 
    * f90wrap (MANDATORY  for fortran-python interface "pip3 install f90wrap")

    As alternative you can create an Anaconda enviroment from file
    dmk_enviroment_short.yml. First, in dmk_enviroment_short.yml
    replace the string within quotes with the path where conda is
    installed. Then run the command
    
```
conda env create dmk --file=dmk_enviroment_short.yml
```
  
## Compile pure Fortran sources ##
In Unix-base dsystem use the classical Cmake commands to compile
libraries and programs :
```
  mkdir build/;
  cd build;
  cmake ../; 
  make
```
The default Fortran compiler path used is "/usr/bin/gfortran".
You can pass another compiler path using, instead of "cmake ../" ,then
foloowing command

```
cmake -DMy_Fortran_Compiler="absolute complier path" ..
```

To create doxygen documentation in build/ use:
```
make docs_dmk
```

## Compile pure Fortran-Python interface ##
To create the fortran-to-python interface in build/ use:
```
make f2py_interface_dmk
```

## Troubleshooting ##
In case of troubles in the compilation of the fortran-2-python interface
try to pass the absolute path of the **DIRECTORIES** blas and lapack libraries using

```
cmake -DBLAS_DIR="absolute blas path" -DLAPACK_DIR="absolute lapack path" ..
```

Try also to pass the absolute path of the gfortran compiler also explained before.


### Configuration
### Database configuration
### How to run tests
Check in the tools/ and examples/ directories the pure-Fortran codes.

In the python/ directory check for the examples  
### Deployment instructions

We adpot the [GitFlow](https://nvie.com/posts/a-successful-git-branching-model/) approach 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
  * Enrico Facca (enrico.facca@sns.it)
  * Mario Putti  (putti@math.unipd.it) 
* Other community or team contact

### References

[^1]: E.Facca, F.Cardin, and M.Putti, [*Towards a stationary
  Monge-Kantorovich dynamics: the Physarum Polycephalum experience*](
  https://doi.org/10.1137/16M1098383),
  SIAM J. Appl. Math., 78 (2018), pp.651-676
  ([ArXiv version](https://arxiv.org/abs/1610.06325)).
  
[^2]: E.Facca, S.Daneri, F.Cardin, and M.Putti, [*Numerical solution of Monge-Mantorovich equations via a dynamic formulation*](https://doi.org/10.1007/s10915-020-01170-8), J. Scient. Comput., 82 (2020), pp.1-26.

[^3]: E.Facca, F.Cardin, and M.Putti, [*Branching structures emerging
  from a continuous optimal transport model*](https://arxiv.org/abs/1811.12691)
  ArXiv preprint 1811.12691, (2018)

[^4]: E.Facca, M.Benzi [*Fast Iterative Solution of the Optimal Transport Problem on Graphs*](https://arxiv.org/abs/2009.13478) ArXiv preprint 2009.13478, (2020)